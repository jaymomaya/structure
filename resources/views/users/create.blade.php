@extends('layouts.app')


@section('custom-styles')
  
    @include('layouts.style_loaders.token_loader')
    <link rel="stylesheet" type="text/css" href="//oss.maxcdn.com/jquery.trip.js/3.3.3/trip.min.css"/>
    <link href="{{ asset('/css/common/chosen.min.css') }}" rel="stylesheet" type="text/css" />
    
@endsection

@section('htmlheader_title')
    @include('layouts.custom_partials.htmlheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')])
@endsection


@section('contentheader_title')
    
    @include('layouts.custom_partials.contentheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')])

@endsection

@section('custom-breadcrumb')
    @include('layouts.custom_partials.breadcrumb')
@endsection

@section('contentheader_description')
@endsection

@section('main-content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Create New User</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('users.index') }}"> Back</a>
        </div>
    </div>
</div>


@if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
       @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
       @endforeach
    </ul>
  </div>
@endif



{!! Form::open(array('route' => 'users.store','method'=>'POST','class' => 'form-validate')) !!}
<section class="panel">
    <div class="panel-heading">
        <div class="row">
            
            <div class="col-xs-12 col-sm-12 col-md-3">
                <div class="form-group">
                    <strong>Name:</strong>
                    {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control required step1')) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3">
                <div class="form-group">
                    <strong>Email:</strong>
                    {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control step2')) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3">
                <div class="form-group">
                    <strong>Password:</strong>
                    {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control step3')) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3">
                <div class="form-group">
                    <strong>Confirm Password:</strong>
                    {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control step4')) !!}
                </div>
            </div>
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Role *</label>
                        {{
                            Form::select('roles',
                            htmlSelect('roles', $data),
                            [],
                            array('name'=>'roles','id' => 'roles','class' => 'form-control chosen-select roles','data-placeholder' => 'Select Roles', 'placeholder' => '', setDisable('roles' , $data)))
                        }}
                </div>
            </div>
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Branch *</label>
                        {{
                            Form::select('branch_list',
                            htmlSelect('branch_list', $data),
                            [],
                            array('name'=>'branch_master_id','id' => 'branch','class' => 'form-control chosen-select required branch','data-placeholder' => 'Select branch','placeholder' => '', setDisable('branch_list' , $data)))
                        }}
                </div>
            </div>
            
            <!-- <div id="log"></div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div> -->
        </div>
        
    </div>
</section>
@include('layouts.custom_partials.save_panel')
{!! Form::close() !!}
<!-- <button class="demo-change-theme">Run this demo</button> -->

@endsection
@section('custom-scripts')
    <script src="//oss.maxcdn.com/jquery.trip.js/3.3.3/trip.min.js"></script>
    <script src="{{ asset('/js/common/chosen.jquery.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            chosenInit();
            var size = 0;
            $('.chosen-results').scroll(function() {
                var search_url = window.location.href;
                if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                    $.ajax({
                        url: search_url,
                        type: 'get',
                        data : {
                            'size' : size
                        },
                        success: function(res) {
                            if(res.start == 0)
                            {
                                size = 0;
                            }
                            $.each(res.roles, function( key, value ) {
                                size = size + 1;
                                $('.chosen-select').append('<option value='+key+'>'+value+'</option>');
                            });
                            $('.chosen-select').trigger('chosen:updated');
                        },
                        error: function(err) {
                            console.log(err,"err");
                        }
                    })       
                }
            });
        });
        $(".demo-change-theme").on("click", function() {
            tripToChangeTheme.start();
        });

        var tripToChangeTheme = new Trip([
            { sel : $(".step1"), content : "Name",expose : true},
            { sel : $(".step2"), content : "Email", expose : true},
            { sel : $(".step3"), content : "Password",expose : true},
            { sel : $(".step4"), content : "Confirm Password", expose : true}
        ], {
            showNavigation : true,
            showCloseBox : true,
            delay : 3000
        });

    </script>
@endsection