<div class="modal fade modal-fullscreen force-fullscreen" id="leads_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog Modal_body" style="width: 100% !important;">
        {!! Form::open(['id' => 'lead_add','enctype' => 'multipart/form-data','autocomplete' => 'off','class' => 'form-validate']) !!}
        <div class="modal-content border_radius">
            <div class="modal-footer" style="z-index: 1">
               
            
                <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
                <!-- <h4 class="modal-title">Add New Company</h4> -->
            </div>
            <?php $prefix = config('constants.Leads.prefix'); ?>
            <div class="modal-body" style="padding: 0px !important; ">
                <div class="col-md-12" style="">  
                    <section class="panel">
                        <div class="panel-body">
                             <div class="row">
                                <div class="col-md-3"><i class="gf-customer"></i>
                                    <label style="font-size: 21px;font-weight: 800;color: #465260;">&nbsp;&nbsp;Add New Lead</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="btn-group pull-right col-md-1">
                                        <button type="button" id="#close_pd" class="btn btn-sharekhan" data-dismiss="modal">Cancel</button>
                                    </div>
                                    <div class="btn-group col-md-1 pull-right">
                                        <button class="btn btn-success" name="save" value="save_lead" type="submit" id="save_lead">Save</button>
                                    </div>
                                    
                               </div>
                            </div>
                        </div>
                    </section>
                    <section class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Lead Basic Details</label>
                                    </div>
                                </div>
                            </div>
                            <div class = "row">
                                <div class="col-md-2 form-group">
                                    <label>Contact Person Name *</label>
                                    <input type="text" class="form-control {{$prefix}}name" name="{{$prefix}}name" placeholder="Lead Name" timestamp = 0 data-name = "{{$prefix}}name" value="{{htmlValue($prefix.'name', $data)}}" {{setDisable($prefix.'name' , $data['disabled'])}}>
                                    <div class="errorname"></div>
                                </div>
                                <div class="col-md-2 form-group">
                                    <label>Company Name</label>
                                    <input type="text" class="form-control cm_name {{$prefix}}cmp_name" name="{{$prefix}}cmp_name" placeholder="Lead Company Name" timestamp = 0 data-name = "{{$prefix}}cmp_name" value="{{htmlValue($prefix.'cmp_name', $data)}}" {{setDisable($prefix.'cmp_name' , $data['disabled'])}}>
                                </div>
                                <div class="col-md-2 form-group">
                                    <label>Mobile No. *</label>
                                    <i data-toggle="tooltip" title = "Mobile Number has to be Unique" class="fa fa-question-circle pull-right"></i>
                                    <input type="text" class="form-control numeric {{$prefix}}mobile " name="{{$prefix}}mobile" placeholder="Lead Mobile" timestamp = 0 data-name = "{{$prefix}}mobile" value="{{htmlValue($prefix.'mobile', $data)}}" {{setDisable($prefix.'mobile' , $data['disabled'])}} maxlength="10" minlength="10">
                                    <div class="errormobile"></div>
                                </div>
                                <div class="col-md-2 form-group">
                                    <label>Email Id *</label>
                                    <i data-toggle="tooltip" title = "Email Id has to be Unique" class="fa fa-question-circle pull-right"></i>
                                    <input type="text" class="form-control {{$prefix}}email" name="{{$prefix}}email" placeholder="Lead email" timestamp = 0 data-name = "{{$prefix}}email" value="{{htmlValue($prefix.'email', $data)}}" {{setDisable($prefix.'email' , $data['disabled'])}} >
                                    <div class="erroremail"></div>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label>Product</label>
                                    {{
                                        Form::select('l_product',
                                        (isset($data) && isset($data['product_list'])) ? $data['product_list'] : [],
                                        htmlSelect($prefix.'product', $data),
                                        array('name'=>'l_product[]', 'class' => 'form-control inp_product chosen-select product_list', 'data-name' => 'l_product', 'multiple' => 'multiple', 'timestamp' => '0', 'data-placeholder' => 'Select Product', setDisable('l_product' , $data['disabled'])))
                                    }}
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label>Address Line 1</label>
                                    <input type="text" class="form-control {{$prefix}}address" name="l_address1" placeholder="Lead address" timestamp = 0 data-name = "{{$prefix}}address" value="{{htmlValue($prefix.'address1', $data)}}" {{setDisable($prefix.'address' , $data['disabled'])}} >
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>City</label>
                                        <input type="text" class="form-control {{$prefix}}city" name="{{$prefix}}city" placeholder="City" timestamp = 0 data-name = "{{$prefix}}city" value="{{ (isset($data) && isset($data['l_city']) ? htmlValue($prefix.'city', $data) : 'Mumbai') }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Pincode</label>
                                        <input type="text" class="form-control numeric {{$prefix}}pincode" name="{{$prefix}}pincode" placeholder="Pincode" timestamp = 0 data-name = "{{$prefix}}pincode" value="{{htmlValue($prefix.'pincode', $data)}}" {{setDisable($prefix.'pincode' , $data['disabled'])}} maxlength="6" minlength="6">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label>Address Line 2</label>
                                    <input type="text" class="form-control {{$prefix}}address" name="l_address2" placeholder="Lead address" timestamp = 0 data-name = "{{$prefix}}address" value="{{htmlValue($prefix.'address2', $data)}}" {{setDisable($prefix.'address' , $data['disabled'])}} >
                                </div>
                                <div class="col-md-3 form-group">
                                    <label>State</label>
                                    {{
                                        Form::select('l_state',
                                        (isset($data) && isset($data['state'])) ? $data['state'] : [],
                                        (isset($data) && isset($data['l_state']) ? htmlSelect('l_state', $data) : 19),
                                        array('name'=>'l_state', 'class' => 'form-control chosen-select state_list', 'placeholder' => '', 'data-placeholder' => 'Select State', 'autocomplete' => 'off', setDisable('l_state' , $data['disabled'])))
                                    }}
                                     <!-- <input type="hidden" name="l_state" id="l_state"> -->
                                </div>
                                <div class="col-md-3 form-group">
                                    <label>Country</label>
                                    {{
                                        Form::select('l_country',
                                        (isset($data) && isset($data['country'])) ? $data['country'] : [],
                                        (isset($data) && isset($data['l_country']) ? htmlSelect('l_country', $data) : 101),
                                        array('name'=>'l_country', 'class' => 'form-control chosen-select country_list', 'data-name' => 'l_country', 'timestamp' => '0', 'data-placeholder' => 'Select Country', 'placeholder' => '', setDisable('l_country' , $data['disabled'])))
                                    }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 form-group">
                                    <label>Landline Number</label>
                                    <input type="text" class="form-control numeric {{$prefix}}alt_mobile" name="{{$prefix}}alt_mobile" placeholder="Landline Number" timestamp = 0 data-name = "{{$prefix}}alt_mobile" value="{{htmlValue($prefix.'alt_mobile', $data)}}" {{setDisable($prefix.'alt_mobile' , $data['disabled'])}} maxlength="10" minlength="10">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label>Email CC(Add by comma separation)</label>
                                    <input type="text" class="form-control l_alt_email" name="l_alt_email" placeholder="Email CC(Add by comma separation)" timestamp = 0 data-name = "{{$prefix}}alt_email" value="{{htmlValue($prefix.'alt_email', $data)}}" {{setDisable($prefix.'alt_email' , $data['disabled'])}}>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label>Source</label>
                                    {{
                                        Form::select('l_source',
                                        (isset($data) && isset($data['lead_source'])) ? $data['lead_source'] : [],
                                        htmlSelect($prefix.'source', $data),
                                        array('name'=>'l_source', 'class' => 'form-control chosen-select lead_source', 'data-name' => 'l_source', 'timestamp' => '0', 'data-placeholder' => 'Select Source', 'placeholder' => '', setDisable('l_source' , $data['disabled'])))
                                    }}
                                </div>
                                <div class="col-md-3 form-group">
                                    <label>Remarks</label>
                                    <input type="text" class="form-control {{$prefix}}remarks" name="{{$prefix}}remarks" placeholder="Lead remarks" timestamp = 0 data-name = "{{$prefix}}remarks" value="{{htmlValue($prefix.'remarks', $data)}}" {{setDisable($prefix.'remarks' , $data['disabled'])}} >
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 form-group">
                                    <label>GSTIN</label>
                                    <input type="text" class="form-control tax_info {{$prefix}}gstin" name="{{$prefix}}gstin" placeholder="GSTIN" timestamp = 0 data-name = "{{$prefix}}gstin" value="{{htmlValue($prefix.'gstin', $data)}}" {{setDisable($prefix.'alt_mobile' , $data['disabled'])}} maxlength="15" minlength="15">
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <section class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Call Response</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Lead Response</label>
                                        {{
                                            Form::select('lc_response',
                                            (isset($data) && isset($data['lc_response'])) ? $data['lc_response'] : [],
                                            htmlSelect('lc_response', $data),
                                            array('name'=>'lc_response', 'class' => 'form-control chosen-select lc_response', 'data-name' => 'lc_response', 'timestamp' => '0', 'data-placeholder' => 'Select Response', 'placeholder' => '', setDisable('lc_response' , $data['disabled'])))
                                        }}
                                    </div>
                                </div>
                                <div class="col-md-3 form-group hide">
                                    <label>Assign To*</label>
                                    {{
                                        Form::select('l_sales_person',
                                        (isset($data) && isset($data['sales_person'])) ? $data['sales_person'] : [],
                                        htmlSelect('current_user', $data),
                                        array('name'=>'l_sales_person', 'class' => 'form-control chosen-select sales_person', 'data-name' => 'l_sales_person', 'timestamp' => '0', 'data-placeholder' => 'Select Sales Person', 'placeholder' => '', setDisable('l_product' , $data['disabled'])))
                                    }}
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label>Follow Up Date</label>
                                        <input type="text" class="datepicker form-control follow_up_date" id="follow_up_date" name="l_follow_up_date" placeholder="Follow Up Date">
                                        <i class="fa fa-calendar form-control-feedback"></i>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Note / Remarks</label>
                                        <input type="text" class="form-control" name="lc_remarks" placeholder="call note or remarks">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 hide">
                                    <div class="form-group has-feedback">
                                        <label>Appointment Date</label>
                                        <input type="text" class="datepicker form-control appnt_date" name="la_date" placeholder="Appointment Date">
                                        <i class="fa fa-calendar form-control-feedback"></i>
                                    </div>
                                </div>
                                <div class="col-md-2 hide">
                                    <div class="form-group has-feedback">
                                        <label>Appointment Time</label>
                                        <input type="time" step="2" class="form-control appnt_time" name="la_time" placeholder="Appointment Time">
                                        <i class="fa fa-clock-o form-control-feedback"></i>
                                    </div>
                                </div>   
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="checkbox" class="form-group" id="dead_mark" name="l_dead" value="yes">
                                        <label>Mark As Dead Lead</label>
                                    </div>
                                </div>              
                            </div>    
                        </div>
                    </section>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" id="#close_pd" class="btn btn-sharekhan" data-dismiss="modal">Close</button> -->
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
