
<div class="modal fade modal-fullscreen force-fullscreen" id="companyadd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog Modal_body" style="width: 100% !important;">
        {!! Form::open(['id' => 'company_add','enctype' => 'multipart/form-data','autocomplete' => 'off']) !!}
        <div class="modal-content border_radius">
            <div class="modal-footer" style="z-index: 1">
                <div class="row">
                    <div class="col-md-3  customer-header" style="margin-left: -118px;"><i class="gf-customer"></i>
                        <label style="font-size: 21px;font-weight: 800;color: #465260;">&nbsp;&nbsp;Add New Company</label>
                    </div>
                    <div class="col-md-9">
                        <div class="btn-group">
                            <button class="btn btn-success" name="save" value="save_company" type="submit" id="save_company">Save</button>
                        </div>
                        <div class="btn-group">
                            <button type="button" id="close_cm" class="btn btn-sharekhan" data-dismiss="modal">Cancel</button>
                        </div>
                        
                        
                    </div>
                </div>
            
                <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
                <!-- <h4 class="modal-title">Add New Company</h4> -->
            </div>
            
            <div class="modal-body" style="padding: 0px !important; ">
                <div class="col-md-12" style="">  
                    <section class="panel">
                        <div class="panel-body">
                            <label>Basic Detail</label>
                            <div class = "row">
                                <div class = "col-md-3">
                                    <div class="form-group">
                                        <label for="name">Company Name *</label>
                                        <input type="text" class="form-control cm_name " id="cm_name" name = "cm_name" placeholder="Enter Company Name" autocomplete="off" value = "{{(isset($data) && isset($data)) ? htmlValue('cm_name', $data) : ''}}" {{setDisable('cm_name' , $data['disabled'])}}>
                                        <input type="hidden" name="modal_call" value="1">
                                        <input type="hidden" name="filename" value="modal">
                                        <input type="hidden" name="company_master" value="company_master">
                                        <input type="hidden" name="manual" value="manual">
                                        <span class="comp_name_err" style="color: red"></span>
                                    </div>
                                </div>
                                <div class = "col-md-3">
                                    <div class="form-group">
                                        <label for="name">Contact Person Name *</label>
                                        <input type="text" class="form-control cm_contact_person " id="cm_contact_person" name = "cm_contact_person" placeholder="Enter Contact Person Name" autocomplete="off" value = "{{(isset($data) && isset($data)) ? htmlValue('cm_contact_person', $data) : ''}}" {{setDisable('cm_contact_person' , $data['disabled'])}}>
                                        <span class="cont_per_err" style="color: red"></span>
                                    </div>
                                </div>
                                <div class = "col-md-3">
                                    <div class="form-group">
                                        <label for="name">Phone Number *</label>
                                        <input type="text" class="form-control cm_phone numeric" id="cm_phone" name = "cm_phone" autocomplete="off" placeholder="Enter  Phone Number" value = "{{(isset($data) && isset($data)) ? htmlValue('cm_phone', $data) : ''}}"  maxlength="11" minlength="10"{{setDisable('cm_phone' , $data['disabled'])}}>
                                        <span class="cont_num_err" style="color: red"></span>
                                    </div>
                                </div>
                                <div class = "col-md-2">
                                    <div class="form-group">
                                        <label for="name">Landline Number</label>
                                        <input type="text" class="form-control cm_landline_num numeric" id="cm_landline_num" name = "cm_landline_num" autocomplete="off" placeholder="Enter  Landline Number" value = "{{(isset($data) && isset($data)) ? htmlValue('cm_landline_num', $data) : ''}}"  maxlength="12" minlength="10"{{setDisable('cm_landline_num' , $data['disabled'])}}>
                                    </div>
                                </div>
                                <div class = "col-md-3">
                                    <div class="form-group">
                                        <label for="name">Email *</label>
                                        <input type="email" autocomplete="off" class="form-control vendor_email" id="cm_email" name = "cm_email" placeholder="Enter  Email" value = "{{(isset($data) && isset($data)) ? htmlValue('cm_email', $data) : ''}}" {{setDisable('cm_email' , $data['disabled'])}}>
                                        <span class="comp_email_err" style="color: red"></span>

                                    </div>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class = "col-md-3">
                                    <div class="form-group">
                                        <label for="name">Email CC(Add by comma separation)</label>
                                        <input type="text" autocomplete="off" class="form-control cm_email_cc" id="cm_email_cc" name = "cm_email_cc" placeholder="Enter Multiple Email(comma separation)" value = "{{(isset($data) && isset($data)) ? htmlValue('cm_email_cc', $data) : ''}}" {{setDisable('cm_email_cc' , $data['disabled'])}}>

                                    </div>
                                </div>
                                <div class = "col-md-3">

                                    <div class="form-group has-feedback">
                                        <label for="name">Company Year Of Establishment</label>
                                        <i class="glyphicon glyphicon-calendar form-control-feedback"></i>
                                        <input type="text" autocomplete="off" class="form-control vendor_datepicker cm_created_at" id="cm_created_at" name = "cm_created_at" placeholder="Select Date" value = "{{(isset($data) && isset($data)) ? htmlValue('cm_created_at', $data) : ''}}" {{setDisable('cm_created_at' , $data['disabled'])}}>
                                        
                                    </div>
                                </div>
                                <div class = "col-md-3">
                                    <div class="form-group">
                                        <label for="name">Select Company Type *</label>
                                            {{
                                                Form::select('cm_type',
                                                (isset($data) && isset($data['company_type'])) ? $data['company_type'] : [],
                                                (isset($data) && isset($data["cm_type"])) ? $data["cm_type"] : htmlSelect('cm_type', $data),
                                                array('name'=>'cm_type','class' => 'form-control cm_type chosen-select', 'placeholder' => '','' => 'true', 'data-placeholder' => 'Select Type', 'placeholder' => '', setDisable('cm_type' , $data['disabled'])))
                                            }}
                                            <span class="comp_type_err" style="color: red"></span>
                                    </div>
                                </div>

                                <div class = "col-md-3">
                                    <div class="form-group">
                                        <label for="name">Select Currency *</label>
                                            {{
                                                Form::select('currency',
                                                (isset($data) && isset($data['currency'])) ? $data['currency'] : [],
                                                (isset($data) && isset($data["cm_currency"])) ? $data["cm_currency"] : htmlSelect('cm_currency', $data),
                                                array('name'=>'cm_currency','class' => 'form-control chosen-select cm_currency', 'placeholder' => '','' => 'true', 'data-placeholder' => 'Select Currency', 'placeholder' => '', setDisable('cm_currency' , $data['disabled'])))
                                            }}
                                            <span class="comp_currency_err" style="color: red"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class = "col-md-3">
                                    <div class="form-group">
                                        <label for="name">Company CIN Number</label>
                                        <input type="text" autocomplete="off" class="form-control cm_sin_number" id="cm_sin_number" name = "cm_sin_number" placeholder="Enter Company CIN Number" value = "{{(isset($data) && isset($data)) ? htmlValue('cm_sin_number', $data) : ''}}" {{setDisable('cm_sin_number' , $data['disabled'])}}>

                                    </div>
                                </div>
                            </div>
                             <div class="row">
                                <div class = "col-md-6">
                                    <div class="form-group">
                                        <input type="checkbox" id="same_as" name="cm_same_as">Same as billing address
                                    </div>
                                </div><br/>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class = "col-md-12">
                                            <div class="form-group">
                                                <label for="name">Billing Address</label>
                                                <input type="text" autocomplete="off" class="form-control cm_address" id="cm_created_at" name = "cm_address"  placeholder="Address Line 1" value = "{{(isset($data) && isset($data)) ? htmlValue('cm_address', $data) : ''}}" {{setDisable('cm_address' , $data['disabled'])}}>
                                                <input type="text" autocomplete="off" class="form-control cm_address2" id="cm_created_at" name = "cm_address2" placeholder="Address Line 2" value = "{{(isset($data) && isset($data)) ? htmlValue('cm_address2', $data) : ''}}" {{setDisable('cm_address2' , $data['disabled'])}}>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class = "col-md-6">
                                            <div class="form-group">
                                                <label for="name">Enter City</label>
                                                <input type="text" autocomplete="off" class="form-control cm_city" id="cm_created_at" name = "cm_city" placeholder="Enter City" value = "{{(isset($data) && isset($data['cm_city'])) ? htmlValue('cm_city', $data) : 'Mumbai'}}" {{setDisable('cm_city' , $data['disabled'])}}>
                                            </div>
                                        </div>
                                        <div class = "col-md-6">
                                            <div class="form-group">
                                                <label for="name">Enter Pincode</label>
                                                <input type="text" autocomplete="off" class="form-control cm_pincode numeric" id="cm_created_at" name = "cm_pincode" placeholder="Enter Pincode" maxlength="6" value = "{{(isset($data) && isset($data)) ? htmlValue('cm_pincode', $data) : ''}}" {{setDisable('cm_pincode' , $data['disabled'])}}>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class = "col-md-6">
                                            <div class="form-group">
                                                <label for="name">Select State *</label>
                                                    {{
                                                        Form::select('state',
                                                        (isset($data) && isset($data['state'])) ? $data['state'] : [],
                                                        (isset($data) && isset($data["cm_state"])) ? htmlSelect('cm_state', $data) : 19,
                                                        array('name'=>'cm_state1','class' => 'form-control chosen-select state', 'placeholder' => '', 'data-placeholder' => 'Select State', 'placeholder' => '', 'autocomplete' => 'off', setDisable('cm_state' , $data['disabled'])))
                                                    }}
                                                <input type="hidden" name="cm_state" id="cm_state"  value="{{(isset($data) && isset($data)) ? htmlValue('cm_state', $data) : '19'}}">
                                            </div>
                                        </div>
                                        <div class = "col-md-6">
                                            <div class="form-group">
                                                <label for="name">Select Country</label>
                                                    {{
                                                        Form::select('country',
                                                        (isset($data) && isset($data['country'])) ? $data['country'] : [],
                                                        (isset($data) && isset($data["cm_country"])) ? htmlSelect('cm_country', $data) : 101,
                                                        array('name'=>'cm_country','class' => 'form-control chosen-select country', 'placeholder' => '', 'data-placeholder' => 'Select Country', 'placeholder' => '', 'autocomplete' => 'off',setDisable('cm_country' , $data['disabled'])))
                                                    }}
                                            </div>
                                        </div>    
                                    </div>
                                    <div class="row">
                                        @if(isset($data) && isset($data['screen_name']) && $data['screen_name'] != 'create')
                                            @if(isset($data) && isset($data['cm_state']) && $data['cm_state'] == 0)
                                            <div class = "col-md-6 overseas_state">
                                                <div class="form-group">
                                                    <label for="name" style="color: purple;">Enter State</label>
                                                    <input type="text" autocomplete="off" style="border: 1px solid purple;" class="form-control cm_other_state" id="cm_created_at" name = "cm_other_state" placeholder="Enter other state" value = "{{(isset($data) && isset($data)) ? htmlValue('cm_other_state', $data) : ''}}" {{setDisable('cm_other_state' , $data['disabled'])}}>
                                                </div>
                                            </div>
                                            @endif
                                            @else
                                            <div class = "col-md-6 overseas_state hide">
                                                <div class="form-group">
                                                    <label for="name" style="color: purple;">Enter State</label>
                                                    <input type="text" autocomplete="off" style="border: 1px solid purple;" class="form-control cm_other_state" id="cm_created_at" name = "cm_other_state" placeholder="Enter other state" value = "{{(isset($data) && isset($data)) ? htmlValue('cm_other_state', $data) : ''}}" {{setDisable('cm_other_state' , $data['disabled'])}}>
                                                </div>
                                            </div>
                                            @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class = "col-md-12">
                                             <div class="form-group">
                                        <label for="name">Shipping Address</label>
                                        <input type="text" autocomplete="off" class="form-control cm_shipping_address" id="cm_created_at" name = "cm_shipping_address" placeholder="Address Line 1" value = "{{(isset($data) && isset($data)) ? htmlValue('cm_shipping_address', $data) : ''}}" {{setDisable('cm_shipping_address' , $data['disabled'])}}>
                                        <input type="text" autocomplete="off" class="form-control cm_shipping_address2" id="cm_created_at" name = "cm_shipping_address2" placeholder="Address Line 2" value = "{{(isset($data) && isset($data)) ? htmlValue('cm_shipping_address2', $data) : ''}}" {{setDisable('cm_shipping_address2' , $data['disabled'])}}>
                                             </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class = "col-md-6">
                                            <div class="form-group">
                                                <label for="name">Enter City</label>
                                                <input type="text" autocomplete="off" class="form-control cm_shipping_city" id="" name = "cm_shipping_city" placeholder="Enter City" value = "{{(isset($data) && isset($data)) ? htmlValue('cm_shipping_city', $data) : ''}}" {{setDisable('cm_shipping_city' , $data['disabled'])}}>
                                            </div>
                                        </div>
                                        <div class = "col-md-6">
                                            <div class="form-group">
                                                <label for="name">Enter Pincode</label>
                                                <input type="text" autocomplete="off" class="form-control cm_shipping_pincode numeric" id="cm_created_at" maxlength="6" name = "cm_shipping_pincode" placeholder="Enter Pincode" value = "{{(isset($data) && isset($data)) ? htmlValue('cm_shipping_pincode', $data) : ''}}" {{setDisable('cm_shipping_pincode' , $data['disabled'])}}>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class = "col-md-6">
                                            <div class="form-group">
                                            <label for="name">Select Shipping State *</label>
                                            {{
                                                Form::select('state',
                                                (isset($data) && isset($data['state'])) ? $data['state'] : [],
                                                (isset($data) && isset($data["cm_shipping_state"])) ? $data["cm_shipping_state"] : htmlSelect('cm_shipping_state', $data),
                                                array('name'=>'cm_shipping_state1','class' => 'form-control chosen-select cm_shipping_state', 'placeholder' => '', 'data-placeholder' => 'Select State', 'placeholder' => '', 'autocomplete' => 'off',setDisable('cm_shipping_state' , $data['disabled'])))
                                            }}
                                            <input type="hidden" name="cm_shipping_state" id="cm_shipping_state" value="{{(isset($data) && isset($data)) ? htmlValue('cm_shipping_state', $data) : ''}}">
                                            </div>
                                        </div>
                                        <div class = "col-md-6">
                                            <div class="form-group">
                                            <label for="name">Select Country</label>
                                            {{
                                                Form::select('country',
                                                (isset($data) && isset($data['country'])) ? $data['country'] : [],
                                                (isset($data) && isset($data["cm_shipping_country"])) ? $data["cm_shipping_country"] : htmlSelect('cm_shipping_country', $data),
                                                array('name'=>'cm_shipping_country1','class' => 'form-control chosen-select cm_shipping_country', 'placeholder' => '', 'data-placeholder' => 'Select Country', 'placeholder' => '','autocomplete' => 'off', setDisable('cm_shipping_country' , $data['disabled'])))
                                            }}
                                            <input type="hidden" name="cm_shipping_country" id="cm_shipping_country" value="{{(isset($data) && isset($data)) ? htmlValue('cm_shipping_country', $data) : ''}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                         @if(isset($data) && isset($data['screen_name']) && $data['screen_name'] != 'create')
                                            @if(isset($data) && isset($data['cm_shipping_state']) && $data['cm_shipping_state'] == 0)
                                            <div class = "col-md-6 overseas_state_shipping">
                                                <div class="form-group">
                                                    <label for="name" style="color: purple;">Enter State</label>
                                                    <input type="text" autocomplete="off" class="form-control cm_shipping_other_state" id="cm_shipping_other_state" style="border: 1px solid purple;" name = "cm_shipping_other_state" placeholder="Enter other state" value = "{{(isset($data) && isset($data)) ? htmlValue('cm_shipping_other_state', $data) : ''}}" {{setDisable('cm_shipping_other_state' , $data['disabled'])}}>
                                                </div>
                                            </div>
                                            @endif
                                        @else


                                        <div class = "col-md-6 overseas_state_shipping hide">
                                            <div class="form-group">
                                                <label for="name" style="color: purple;">Enter State</label>
                                                <input type="text" autocomplete="off" class="form-control cm_shipping_other_state" id="cm_shipping_other_state" style="border: 1px solid purple;" name = "cm_shipping_other_state" placeholder="Enter other state" value = "{{(isset($data) && isset($data)) ? htmlValue('cm_shipping_other_state', $data) : ''}}" {{setDisable('cm_shipping_other_state' , $data['disabled'])}}>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                </div>    
                            </div>
                            <div class="col-md-6">
                                
                            </div>
                            <hr style="border-top: 1px solid #8c8b8b;" />
                           
                            
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="name">Enter Number Of Employee</label>
                                        <input type="text" class="form-control cm_num_of_employee numeric" id="cm_num_of_employee" autocomplete="off" name = "cm_num_of_employee" placeholder="Enter Number Of Employee" value = "{{(isset($data) && isset($data)) ? htmlValue('cm_num_of_employee', $data) : ''}}" {{setDisable('cm_num_of_employee' , $data['disabled'])}}>
                                    </div>
                                    
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="name">Enter Shipping Email Address</label>
                                        <input type="email" class="form-control cm_shipping_email_address" id="cm_shipping_email_address" autocomplete="off" name = "cm_shipping_email_address" placeholder="Enter Shipping Email Address" value = "{{(isset($data) && isset($data)) ? htmlValue('cm_shipping_email_address', $data) : ''}}" {{setDisable('cm_shipping_email_address' , $data['disabled'])}}>
                                    </div>
                                    
                                </div>
                                
                            </div>
                            
                        </div>
                    </section>
                      <section class="panel">
                        <div class="panel-body">
                            <label>Compliance And Tax Information</label>
                            <div class="row">
                               <div class = "col-md-3">
                                    <div class="form-group">
                                        <label for="name">PAN Number</label>
                                        <input type="text" class="form-control cm_pan_num" id="cm_pan_num" autocomplete="off" name = "cm_pan_num" placeholder="Enter PAN Number" value = "{{(isset($data) && isset($data)) ? htmlValue('cm_pan_num', $data) : ''}}" maxlength="10"  minlength="10" {{setDisable('cm_pan_num' , $data['disabled'])}}>
                                    </div>
                                </div>
                                <div class = "col-md-3">
                                    <div class="form-group">
                                        <label for="name">GSTIN</label>
                                        <input type="text" autocomplete="off" class="form-control cm_gstin_num" id="cm_gstin_num" name = "cm_gstin_num" placeholder="Enter GSTIN Number" value = "{{(isset($data) && isset($data)) ? htmlValue('cm_gstin_num', $data) : ''}}" maxlength="14"  minlength="14" {{setDisable('cm_gstin_num' , $data['disabled'])}}>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="panel document_section">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-12">
                                    <label>Upload Logo(Best Resolution 50*50)</label>
                                    <input type="file" class="btn btn-sharekhan documents file" id = "documents" name = "{{$prefix}}file" id="file" accept='image/*'>
                                    </div>
                                    <div class="col-md-9 documents_names">
                                    </div>    
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <label>Upload Documents</label>
                                        <label class="btn btn-sharekhan" style="height: 34px; width: 169px;">
                                        <i class="fa fa-upload"></i>
                                        Upload Documents
                                        <input type="file" id="file" class="file comp_docmodal" name="{{$prefix}}docs[]" multiple="multiple" style="visibility: hidden;" title="" accept="image/jpeg,image/gif,image/png,application/pdf">
                                        </label>
                                         <div class="row">
                                        <!-- <div class="col-md-9 documents_names"> -->
                                        <div class="gallerymodalcom"></div>
                                    </div> 
                                </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="panel">
                        <div class="panel-body bank_row_parent">
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Bank Information</label>
                                </div>
                                <div class="col-md-9">
                                @if(isset($data) && isset($data['screen_name']) && $data['screen_name'] == 'show')
                                @else
                                    <i class="fa fa-plus add_bank_row pull-right" title="Add Row"></i>
                                @endif
                                </div>
                            </div>
                            @if(isset($data) && isset($data['company_bank']) && sizeof($data['company_bank']) > 0)
                                @foreach($data['company_bank'] as $key => $value)
                                    <div class="row bank_row">
                                        <div class="col-md-2">
                                            <div class="form-group">

                                                <label>Check for Default</label><br/>
                                                <input type="radio" name="cm_bk[{{$key}}][is_default]" class="has_radio" data-name = "is_default" value="1" id="cbi_is_default"{{setDisable('cbi_is_default' , $data['disabled'])}}
                                                @if(isset($value) && $value['cbi_is_default'] == '1')
                                                checked="checked"
                                                @else
                                                @endif 

                                                >Default
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Bank A/c Number</label>
                                                <input type="text" name="cm_bk[{{$key}}][bank_acc_num]" class="form-control bank_acc_num numeric" data-name = "bank_acc_num" placeholder="Bank A/c Number"  value="{{$value['cbi_bank_acc_num']}}" {{setDisable('cbi_bank_acc_num', $data['disabled'])}}>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>IFSC Code</label>
                                                <input type="text" name="cm_bk[{{$key}}][ifsc_code]" class="form-control ifsc_code" data-name = "ifsc_code" placeholder="IFSC Code" minlength="11" maxlength="11" value="{{$value['cbi_ifsc_code']}}" {{setDisable('cbi_ifsc_code', $data['disabled'])}}> 
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Branch Name</label>
                                                <input type="text" name="cm_bk[{{$key}}][branch_name]" class="form-control branch_name" data-name = "branch_name" placeholder="Branch Name" value="{{$value['cbi_branch_name']}}" {{setDisable('cbi_branch_name', $data['disabled'])}}>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Bank Name</label>
                                                <input type="text" name="cm_bk[{{$key}}][bank_name]" class="form-control bank_name" data-name = "bank_name" placeholder="Bank Name" value="{{$value['cbi_bank_name']}}" {{setDisable('cbi_bank_name', $data['disabled'])}}>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>A/c Holder Name</label>
                                                <input type="text" name="cm_bk[{{$key}}][acc_holder_name]" class="form-control acc_holder_name" data-name = "acc_holder_name" placeholder="A/c Holder Name" value="{{$value['cbi_acc_holder_name']}}" {{setDisable('cbi_acc_holder_name', $data['disabled'])}}>
                                            </div>
                                        </div>
                                        @if($key == 0)
                                        <div class="col-md-4">
                                            <i class="fa fa-trash delete_row hide pull-right" title="Delete Row"></i>
                                        </div>
                                        @else
                                            @if(isset($data) && isset($data['name']) && $data['name'] == 'show')
                                            @else
                                                <div class="col-md-4">
                                                    <i class="fa fa-trash delete_row pull-right" title="Delete Row"></i>
                                                </div>
                                            @endif
                                        @endif
                                    </div>
                                @endforeach
                            @else
                            <div class="row bank_row">
                                <div class="col-md-2">
                                    <div class="form-group">

                                        <label>Check for Default</label><br/>
                                        <input type="radio" value="1" class="has_radio" name="cm_bk[0][is_default]" data-name = "is_default" id="cbi_is_default">Default
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Bank A/c Number</label>
                                        <input type="text" name="cm_bk[0][bank_acc_num]" class="form-control bank_acc_num numeric"  data-name = "bank_acc_num" placeholder="Bank A/c Number" value="" >
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>IFSC Code</label>
                                        <input type="text" name="cm_bk[0][ifsc_code]" class="form-control ifsc_code" minlength="11" maxlength="11" data-name = "ifsc_code" placeholder="IFSC Code" value=""> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Branch Name</label>
                                        <input type="text" name="cm_bk[0][branch_name]" class="form-control branch_name" data-name = "branch_name" placeholder="Branch Name" value="" >
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Bank Name</label>
                                        <input type="text" name="cm_bk[0][bank_name]" class="form-control bank_name" data-name = "bank_name" placeholder="Bank Name" value="">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>A/c Holder Name</label>
                                        <input type="text" name="cm_bk[0][acc_holder_name]" class="form-control acc_holder_name" data-name = "acc_holder_name" placeholder="A/c Holder Name" value="">
                                    </div>
                                </div>
                                
                                <div class="col-md-4">
                                    <i class="fa fa-trash delete_row hide pull-right" title="Delete Row"></i>
                                </div>
                            </div>
                            @endif
                        </div>
                    </section>

                    <section class="panel">
                        <div class="panel-body">
                            <label>Additional Information</label>
                            <div class="row">
                               <div class = "col-md-3">
                                    <div class="form-group">
                                        <label for="name">Company Website</label>
                                        <input type="text" class="form-control cm_website" id="cm_website" autocomplete="off" name = "cm_website" placeholder="Enter Company Website" value = "{{(isset($data) && isset($data)) ? htmlValue('cm_website', $data) : ''}}"  {{setDisable('cm_website' , $data['disabled'])}}>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

            <div class="modal-footer">
                <!-- <button type="button" id="#close_pd" class="btn btn-sharekhan" data-dismiss="modal">Close</button> -->
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>