<?php $prefix = config('constants.PurchaseOrder.prefix'); ?>
<div class="modal fade modal-fullscreen force-fullscreen" id="companyadd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog Modal_body" style="width: 100% !important;">
        {!! Form::open(['id' => 'raise_po', 'class'=> 'form-validate','enctype' => 'multipart/form-data','autocomplete' => 'off']) !!}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="modal-content border_radius">
        	<div class="modal-footer" style="z-index: 1">
                <div class="row">
                    <div class="col-md-3  customer-header" style="margin-left: -118px;"><i class="gf-customer"></i>
                        <label style="font-size: 21px;font-weight: 800;color: #465260;">&nbsp;&nbsp;Raise Purchase Order</label>
                    </div>
                    <div class="col-md-9">
                        <div class="btn-group">
                            <label class="btn btn-success" name="save" value="save_company" type="submit" id="save_company">Save</label>
                        </div>
                        <div class="btn-group">
                            <label type="button" id="close_cm" class="btn btn-sharekhan" data-dismiss="modal">Cancel</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-body" style="padding: 0px !important; ">
                <div class="row">
                    <div class="col-md-9">
                        <section class="panel">
                            <div class="panel-body">
                                <h4> <i class="fa fa-book"></i>Order Basic Detail</h4>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Select Vendor *</label>
                                            <i class="fa fa-plus pull-right vendor_add"></i>
                                            <i class="fa fa-refresh pull-right vendor_refresh"></i>
                                            {{
                                                Form::select('vendor_id',
                                                (isset($data) && isset($data['vendor_list'])) ? $data['vendor_list'] : [],
                                                htmlSelect($prefix.'vendor_id', $data),
                                                array('name'=>$prefix.'vendor_id', 'id' => 'vendor_id', 'class' => 'form-control chosen-select required vendor_select', 'data-name' => $prefix.'type', 'placeholder' => '', 'data-placeholder' => 'Select Vendor'))
                                            }}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Purchase Order Number *</label>
                                            <input type="text" name="{{$prefix}}purchase_order_number"  class="form-control required" id="purchase_order_num" placeholder="Purchase Order Number" value="{{htmlValue($prefix.'purchase_order_number', $data)}}" readonly="readonly">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Date *</label>
                                            <input type="date" name="{{$prefix}}date" class="form-control required" value="{{htmlValue(($prefix.'date'), $data)}}">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Expected Delivery Date</label>
                                            <input type="date" name="{{$prefix}}exp_delivery_date" class="form-control" value="{{htmlValue(($prefix.'exp_delivery_date'), $data)}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Total Quantity</label>
                                            <input type="text" name="{{$prefix}}total_quantity"  class="form-control" id="total_quantity" placeholder="Total Quantity" readonly="readonly" value="{{htmlValue($prefix.'total_quantity', $data)}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Total Amount</label>
                                            <input type="text" name="{{$prefix}}total_amount"  class="form-control" id="total_amount" placeholder="Total Quantity" readonly="readonly" value="{{htmlValue($prefix.'total_amount', $data)}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Remakrs</label>
                                            <input type="text" name="{{$prefix}}remarks"  class="form-control" id="remarks" placeholder="Remarks" value="{{htmlValue($prefix.'remarks', $data)}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="panel">
                            <div class="panel-body item_parent_row">
                                <div class="row">
                                    <div class="col-md-4">
                                        <h4> <i class="fa fa-cubes"></i> Item Details </h4>
                                    </div>
                                    <div class="col-md-8">
                                        <i class="fa fa-plus-square fa-lg pull-right add-item-row" data-toggle="tooltip" title="Add Item Row"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Item</label>
                                            <i class="fa fa-plus pull-right item_add"></i>
                                            <i class="fa fa-refresh pull-right item_refresh"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Unit</label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Quantity</label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Price</label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Total Amount</label>
                                        </div>
                                    </div>
                                </div>
                                @if(isset($data) && isset($data['item_details']) && sizeof($data['item_details']))
                                    @foreach($data['item_details'] as $key => $value)
                                    <div class="row item_row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                {{
                                                    Form::select('raw_material',
                                                    (isset($data) && isset($data['raw_material_list'])) ? $data['raw_material_list'] : [],
                                                    htmlSelect('pod_item_id', $value),
                                                    array('name'=>'item_details[0][pod_item_id]', 'class' => 'form-control required chosen-select item_select', 'data-name' => 'pod_item_id', 'placeholder' => '', 'data-placeholder' => 'Select Raw Material'))
                                                }}
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                {{
                                                    Form::select('unit',
                                                    (isset($data) && isset($data['unit_master'])) ? $data['unit_master'] : [],
                                                    htmlSelect('pod_unit', $value),
                                                    array('name'=>'item_details[0][pod_unit]', 'data-name' => 'pod_unit', 'class' => 'form-control chosen-select', 'placeholder' => '', 'data-placeholder' => 'Select Unit'))
                                                }}
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input type="text" name='item_details[0][pod_quantity]' placeholder="Quantity" class="form-control pod_quantity required number" min="0" data-name ="pod_quantity" value="{{htmlValue('pod_quantity', $value)}}">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input type="text" name='item_details[0][pod_price]' placeholder="Price" class="form-control required pod_price number" min="0" data-name ="pod_price" value="{{htmlValue('pod_price', $value)}}">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input type="text" name='item_details[0][pod_total_amount]' placeholder="total_amount" class="form-control required total_amount" data-name ="pod_total_amount" readonly="readonly" value="{{htmlValue('pod_total_amount', $value)}}">
                                            </div>
                                        </div>
                                        @if($key == 0)
                                            <div class="col-md-1">
                                                <i class="fa fa-trash hide fa-lg pull-right delete_item_row"></i>
                                            </div>
                                        @else
                                            <div class="col-md-1">
                                                <i class="fa fa-trash fa-lg pull-right delete_item_row"></i>
                                            </div>
                                        @endif
                                    </div>
                                    @endforeach
                                @else
                                    <div class="row item_row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                {{
                                                    Form::select('raw_material',
                                                    (isset($data) && isset($data['raw_material_list'])) ? $data['raw_material_list'] : [],
                                                    htmlSelect('pod_item_id', $data),
                                                    array('name'=>'item_details[0][pod_item_id]', 'class' => 'form-control required chosen-select item_select', 'data-name' => 'pod_item_id', 'placeholder' => '', 'data-placeholder' => 'Select Raw Material'))
                                                }}
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                {{
                                                    Form::select('unit',
                                                    (isset($data) && isset($data['unit_master'])) ? $data['unit_master'] : [],
                                                    htmlSelect('pod_unit', $data),
                                                    array('name'=>'item_details[0][pod_unit]', 'data-name' => 'pod_unit', 'class' => 'form-control chosen-select', 'placeholder' => '', 'data-placeholder' => 'Select Unit'))
                                                }}
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input type="text" name='item_details[0][pod_quantity]' placeholder="Quantity" class="form-control pod_quantity required number" min="0" data-name ="pod_quantity">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input type="text" name='item_details[0][pod_price]' placeholder="Price" class="form-control required pod_price number" min="0" data-name ="pod_price">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input type="text" name='item_details[0][pod_total_amount]' placeholder="total_amount" class="form-control required total_amount" data-name ="pod_total_amount" readonly="readonly">
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <i class="fa fa-trash fa-lg pull-right delete_item_row"></i>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </section>
                    </div>
                    <div class="col-md-3">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12 table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <thead style="background-color:grey">
                                              <tr style="color:white">
                                                <th>Sr. No.</th>
                                                <th>Raw Material</th>
                                                <th>Available</th>
                                                <th>Required (BOM)</th>
                                                <th>Shortfall</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                                @if(isset($data) && isset($data['raw_material_need']))
                                                    @foreach($data['raw_material_need'] as $key => $value)
                                                        <tr>
                                                            <td>{{$key+1}}</td>
                                                            <td>{{$value['name']}}</td>
                                                            <td style="color:green; font-weight: bold">{{$value['available']}}</td>
                                                            <td style="color:orange; font-weight: bold">{{$value['quantity']}}</td>
                                                            @if($value['shortfall'] >= 0)
                                                                <td style="color:green; font-weight: bold">{{$value['shortfall']}}</td>
                                                            @else
                                                                <td style="color:red; font-weight: bold">{{$value['shortfall']}}</td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>