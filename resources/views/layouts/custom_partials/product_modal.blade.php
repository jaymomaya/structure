<div class="modal fade modal-fullscreen force-fullscreen" id="productadd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog Modal_body" style="width: 100% !important;">
        {!! Form::open(['id' => 'product_add','enctype' => 'multipart/form-data','autocomplete' => 'off','class' => 'form-validate']) !!}
        <div class="modal-content border_radius">
            <div class="modal-footer" style="z-index: 1">
                <div class="row">
                    <div class="col-md-3  customer-header" style="margin-left: -118px;"><i class="gf-customer"></i>
                        <label style="font-size: 21px;font-weight: 800;color: #465260;">&nbsp;&nbsp;Add New Product</label>
                    </div>
                    <div class="col-md-9">
                        <div class="btn-group">
                            <button class="btn btn-success" name="save" value="save_product" type="submit" id="save_product">Save</button>
                        </div>
                        <div class="btn-group">
                            <button type="button" id="close_pd" class="btn btn-sharekhan">Cancel</button>
                        </div>
                        
                        
                    </div>
                </div>
            
                <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
                <!-- <h4 class="modal-title">Add New Company</h4> -->
            </div>
            
            <div class="modal-body" style="padding: 0px !important; ">
                <div class="col-md-12" style="">  
                    <section class="panel">
                        <div class="panel-body">
                            <label>Product Detail</label>
                            <div class = "row">
                                <div class = "col-md-4">
                                    <div class="form-group">
                                        <label for="name">Item Name *</label>
                                        <input type="text" class="form-control pm_item_name" id="pm_item_name" name = "pm_item_name" autocomplete="off" placeholder="Enter Item Name" autocomplete="off" value = "{{(isset($data) && isset($data['pm_item_name'])) ? htmlValue('pm_item_name', $data) : ''}}" {{setReadonly('pm_item_name' , $data['readonly'])}}>
                                        <input type="hidden" name="product_modal_call" value="1">
                                        <input type="hidden" name="filename" value="manual">
                                        <span class="product_err" style="color: red"></span>
                                    </div>
                                </div>
                                <div class = "col-md-4">
                                    <div class="form-group">
                                        <label for="name">HSN Code/SAC Code </label>
                                        <input type="text" class="form-control pm_hsn_code " id="pm_hsn_code" name = "pm_hsn_code" placeholder="Enter HSN Code/SAC Code" value = "{{(isset($data) && isset($data['pm_hsn_code'])) ? htmlValue('pm_hsn_code', $data) : ''}}" {{setReadonly('pm_hsn_code' , $data['readonly'])}}>
                                    </div>
                                </div>
                                <div class = "col-md-4">
                                    <div class="form-group">
                                        <label for="name">Item Description </label>
                                        <input type="text" class="form-control pm_description" id="pm_description" name = "pm_description" placeholder="Enter Item Description" value = "{{(isset($data) && isset($data['pm_description'])) ? htmlValue('pm_description', $data) : ''}}" {{setReadonly('pm_description' , $data['readonly'])}}>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class = "col-md-4">
                                    <div class="form-group">
                                        <label for="name">Selling Price </label>
                                        <input type="text" class="form-control pm_selling_price numeric" id="pm_selling_price" name = "pm_selling_price" placeholder="Enter Selling Price Of Item" value = "{{(isset($data) && isset($data['pm_selling_price'])) ? htmlValue('pm_selling_price', $data) : ''}}" {{setReadonly('pm_selling_price' , $data['readonly'])}}>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="name">Apply Subscription </label>
                                    <div class="checkbox checkbox-sharekhan">
                                        <input type="checkbox" name="pm_subscription_flag" class="styled select_all pm_subscription_flag" value="1"
                                        @if(isset($data['pm_subscription_flag']) && $data['pm_subscription_flag'] == 1 && $data['screen_name'] != 'create')
                                        checked
                                        @endif 
                                        {{setDisable('pm_subscription' , $data['disabled'])}}
                                        >
                                        <label><b>Subscription Applicable</b></label>
                                    </div>
                                </div> 
                                <div class = "col-md-4 issubsscription hide">
                                    <div class="form-group">
                                        <label for="name">Type Of Subscription</label>
                                        {{
                                            Form::select('subscription',
                                            (isset($data) && isset($data['subscription'])) ? $data['subscription'] : [],
                                            (isset($data) && isset($data["pm_subscription"])) ? $data["pm_subscription"] : htmlSelect('pm_subscription', $data),
                                            array('name'=>'pm_subscription','class' => 'form-control chosen-select pm_subscription', 'placeholder' => '', 'data-placeholder' => 'Select Subscription', 'placeholder' => '', setDisable('pm_subscription' , $data['disabled'])))
                                        }}
                                    </div>
                                </div>
                                    <!-- <input type="checkbox" name="Subscription" id="subscription"> -->
                            </div>
                            <div class="row">
                                <div class = "col-md-4">
                                    <div class="form-group">
                                        <label for="name">Purchase Price </label>
                                        <input type="text" class="form-control pm_purchase_price numeric" id="pm_purchase_price" name = "pm_purchase_price" placeholder="Enter Purchase Price Of Item" value = "{{(isset($data) && isset($data['pm_purchase_price'])) ? htmlValue('pm_purchase_price', $data) : ''}}" {{setReadonly('pm_purchase_price' , $data['readonly'])}}>
                                    </div>
                                </div>
                                <div class = "col-md-4">
                                    <div class="form-group">
                                        <label for="name">Unit Of Measure</label>
                                        {{
                                            Form::select('unit_of_measure',
                                            (isset($data) && isset($data['unit_of_measure'])) ? $data['unit_of_measure'] : [],
                                            (isset($data) && isset($data["pm_unit_of_measure"])) ? $data["pm_unit_of_measure"] : htmlSelect('pm_unit_of_measure', $data),
                                            array('name'=>'pm_unit_of_measure','class' => 'form-control chosen-select', 'placeholder' => '', 'data-placeholder' => 'Select Unit Of Measure', 'placeholder' => '', setDisable('pm_unit_of_measure' , $data['disabled'])))
                                        }}
                                    </div>
                                </div> 
                                <div class = "col-md-4">
                                    <div class="form-group">
                                        <label for="name">GST Rate(in %)</label>
                                        {{
                                            Form::select('tax',
                                            (isset($data) && isset($data['tax'])) ? $data['tax'] : [],
                                            (isset($data) && isset($data["pm_tax"])) ? $data["pm_tax"] : htmlSelect('pm_tax', $data),
                                            array('name'=>'pm_tax','class' => 'form-control chosen-select', 'placeholder' => '', 'data-placeholder' => 'Select GST Rate', 'placeholder' => '', setDisable('pm_tax' , $data['disabled'])))
                                        }}
                                    </div>
                                </div>
                               
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="name">Vendor</label>
                                        {{
                                                Form::select('pm_vendor[]',
                                                (isset($data) && isset($data['vendor'])) ? $data['vendor'] : [],
                                                (isset($data) && isset($data['pm_vendor'])) ? $data['pm_vendor'] : null,
                                                array('class' => 'form-control chosen-select vendor','multiple' => 'multiple', 'placeholder' => '', 'data-placeholder' => 'Select Vendor', setDisable('pm_vendor' , $data['disabled'])))
                                        }}
                                        
                                    </div>
                                </div>
                                
                                <div class="col-md-3" style="width: 300px !important;">
                                    <div class="form-group">
                                        <label for="name">Product Group</label>
                                        {{
                                                Form::select('product_grouped',
                                                (isset($data) && isset($data['product_group'])) ? $data['product_group'] : [],
                                                (isset($data)   &&isset($data['pm_product_grouped'])) ? $data['pm_product_grouped'] : null,
                                                array('class' => 'form-control chosen-select', 'placeholder' => '', 'data-placeholder' => 'Select Product Group', setDisable('pm_product_grouped' , $data['disabled'])))
                                        }}
                                        
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="name">Product Code </label>
                                        <input type="text" class="form-control" id="pm_product_code" name = "pm_product_code" placeholder="Enter Product Code" value = "{{(isset($data) && isset($data['pm_product_code'])) ? htmlValue('pm_product_code', $data) : ''}}" {{setReadonly('pm_product_code' , $data['readonly'])}}>
                                    </div>
                                </div>
                                <div class="col-md-3" >
                                    <label for="name">Goods Or Services</label>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="radio" name="pm_goods_services" class="styled operation" value="1" 
                                                @if(isset($data) && isset($data['pm_goods_services']) && $data['pm_goods_services'] == '1')
                                                    checked="checked"
                                                @elseif(isset($data) && isset($data['pm_goods_services']) && $data['pm_goods_services'] == '2')
                                                @else
                                                    checked="checked"
                                                @endif
                                                        {{setDisable('pm_goods_services' , $data['disabled'])}}>Goods
                                                <br>
                                            </div>
                                        </div>    
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="radio" name="pm_goods_services" class="styled operation" value="2" {{setDisable('pm_goods_services' , $data['disabled'])}}
                                                @if(isset($data) && isset($data['pm_goods_services']) && $data['pm_goods_services'] == '1')
                                                @elseif(isset($data) && isset($data['pm_goods_services']) && $data['pm_goods_services'] == '2')
                                                    checked="checked"
                                                @else
                                                    
                                                @endif
                                                >Services<br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name">Map Company Against Product</label>
                                        {{
                                                Form::select('pm_comp_id[]',
                                                (isset($data) && isset($data['company_list'])) ? $data['company_list'] : [],
                                                (isset($data) && isset($data['pm_comp_id'])) ? $data['pm_comp_id'] : null,
                                                array('class' => 'form-control chosen-select company_list','multiple' => 'multiple', 'data-placeholder' => 'Map Company Against Product', setDisable('pm_comp_id' , $data['disabled'])))
                                        }}
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </section>

                    <section class="panel document_section">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Upload Documents</label>
                                   <!--  <input type="file" class="btn btn-sharekhan documents file prod_docmodal" name = "pm_file[]" multiple = "multiple" accept="image/jpeg,image/gif,image/png,application/pdf"> -->
                                    <label class="btn btn-sharekhan" style="height: 34px; width: 169px;">
                                    <i class="fa fa-upload"></i>
                                    Upload Documents
                                    <input type="file" class="btn btn-sharekhan documents file prod_docmodal" name = "pm_file[]" multiple = "multiple" style="visibility: hidden;" accept="image/jpeg,image/gif,image/png,application/pdf">
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="gallerymodal"></div>
                            </div>
                            <!--     <div class="col-md-9 documents_names">
                                </div>
                            </div> -->
                        </div>
                    </section>

                    <section class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Product Features</label>
                                    <textarea name="pm_feature1" id='pm_feature' class='pm_feature required' cols="40">{{(isset($data)) ? htmlValue('pm_feature', $data) : ''}}</textarea>
                                    <input type="hidden" name="u_file" id="u_file" value="0">
                                    <input type="hidden" name="full_html" id="full_html" value="">
                                </div>
                            </div>
                        </div>
                    </section>
                    

                </div>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" id="#close_pd" class="btn btn-sharekhan" data-dismiss="modal">Close</button> -->
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>

