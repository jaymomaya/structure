@extends('layouts.app')

@section('custom-styles')
  
    @include('layouts.style_loaders.token_loader')
    <link href="{{ asset('/css/common/chosen.min.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('htmlheader_title')
    @include('layouts.custom_partials.htmlheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')])
@endsection


@section('contentheader_title')
    
    @include('layouts.custom_partials.contentheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')])

@endsection

@section('custom-breadcrumb')
    @include('layouts.custom_partials.breadcrumb')
@endsection

@section('contentheader_description')
@endsection

@section('main-content')

@if (Request::segment(4))
    {!! Form::model($data, ['url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey()], 'method' => 'put']) !!}
@else
    {!! Form::open(['url' => "/".Request::segment(1)."/".Request::segment(2)]) !!}
@endif
<input type="hidden" name="screen_name" value = "{{(isset($data) && isset($data['screen_name'])) ? $data['screen_name'] : ''}}">

<section class="panel">
    <div class="panel-body">
        <div class = "row">
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Role Name *</label>
                    <input type="text" class="form-control title" id="name" name = "name" placeholder="Enter Role Name" value = "{{htmlValue('name', $data)}}">
                </div>
            </div>
            <div class = "col-md-6">
                <div class="form-group">
                    <label for="description">Role Description</label>
                    <input type="text" class="form-control" id="description" name = "description" placeholder="Enter Role Description" value = "{{htmlValue('description', $data)}}">
                </div>
            </div>
        </div>
    </div>
    
</section>
<section class="panel">
    <div class="panel-heading ">
        <label>Select Features</label>
    </div>
    <div class="panel-body">
        @foreach($data['tab'] as $key => $tab)     
            <div class="checkbox checkbox-sharekhan">
                <input type="checkbox" name="parent_tab" class = "styled parent_tab checkCBox" id = "{{$tab['id']}}" >
                <label>{{$key}}</label>
            </div>
            <div class = "row child_tab">

                @foreach($data['tab'][$key]['inner_tab'] as $innerkey => $innertab)
                    <div class = "col-md-3">
                        <ul>
                            <li>
                                <div class="checkbox checkbox-sharekhan">
                                    <input type="checkbox" name="inner_tab" class = "styled inner_tab checkCBox" id = "{{$innertab['id']}}">
                                    <label>{{$innerkey}}</label>
                                </div>
                                <ul>
                                     @foreach($data['tab'][$key]['inner_tab'][$innerkey]['permission_name'] as $permKey => $perm)
                                        <li>
                                            <?php 
                                                $tab = strtolower(str_replace(' ', '-', $key));
                                                $inner_tab = strtolower(str_replace(' ', '-', $innerkey));
                                                $permKey = strtolower(str_replace(' ', '-', $permKey));
                                                $val = 'permission['.$tab.']'.'['.$inner_tab.']'.'['.$permKey.']';

                                                if (isset($data) && isset($data['permission']) && isset($data['permission'][$tab]) && isset($data['permission'][$tab][$inner_tab]) && isset($data['permission'][$tab][$inner_tab][$permKey])) {
                                                    $checked = "checked";
                                                } else {
                                                    $checked = "";
                                                }
                                            ?>

                                            <div class="checkbox checkbox-sharekhan">
                                                <input type="checkbox" name='{{$val}}'' class = "styled permission checkCBox" id = "{{$perm['id']}}" dependent_id = "{{$perm['dependent_id']}}" value = {{$permKey}} {{$checked}}>
                                                <label>{{$permKey}}</label>
                                            </div>
                                        </li> 
                                    @endforeach
                                    
                                </ul>
                            </li>
                        </ul>
                    </div>
                @endforeach
            </div>       
            <hr>
        @endforeach
    </div>
</section>
@include('layouts.custom_partials.save_panel')
{!! Form::close() !!}
@endsection

@section('php-to-js')
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
    ?>
@endsection

@section('custom-scripts')
    
    <script src="{{ asset('/js/common/chosen.jquery.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        chosenInit();
        disableAll("{{Request::segment(2)}}", "{{Route::currentRouteName()}}");
        
        $('.btn-success').prop('disabled',true);

        $(document).on('change',':checkbox', function() {
            if ($("input:checkbox:checked").length > 2)
           {
                console.log($('#name').val(),'jhvj');
                if($('#name').val() != ''){
                    // $('.btn-success').prop('disabled',true);
                }
           }   
           else
           {

                $('.btn-success').prop('disabled',false);
           }
        });

        $('.sidebar > ul > li > a').each(function() {
            // console.log($(this).text())
            $(this).parent().find('ul > li').each(function() {
            // console.log($(this).find('a').text())
            })
        });

        $(document).on('change', '.parent_tab', function() {
            var that = this;
            if ($(that).is(':checked')) {
                $(that).closest('div').next('.child_tab').find('input').prop('checked', true);
            } else {
                
                $(that).closest('div').next('.child_tab').find('input').prop('checked', false);
            }

            if ($("input:checkbox:checked").length > 2)
           {
                if($('#name').val() != ''){
                    $('.btn-success').prop('disabled',false);
                }
                
           }   
           else
           {
                $('.btn-success').prop('disabled',true);
           }
        });

        $(document).on('change', '.inner_tab', function() {
            if ($(this).is(':checked')) {
                $(this).closest('div').siblings('ul').find('input').prop('checked', true);
            } else {
                $(this).closest('div').siblings('ul').find('input').prop('checked', false);
            }
            
            if ($("input:checkbox:checked").length > 2)
           {
                if($('#name').val() != ''){
                    $('.btn-success').prop('disabled',false);
                }
           }   
           else
           {
                $('.btn-success').prop('disabled',true);
           }
            checkParentTab();
        });

        $(document).on('change', '.permission', function() {
            
            var ul = $(this).closest('ul');
            var i_checked = $(ul).find('input:checked').length;
            var i_total = $(ul).find('input').length;

            var d_i = $(ul).find('input:checked').attr('dependent_id');
           
            checkDependent(ul ,d_i);            
            
            checkInnerTab(ul, i_checked, i_total);
            checkParentTab();

            if ($("input:checkbox:checked").length > 2)
           {
                if($('#name').val() != ''){
                    $('.btn-success').prop('disabled',false);
                }
           }   
           else
           {
                $('.btn-success').prop('disabled',true);
           }
        });

        $(document).on('keyup','#name',function(){
            if ($("input:checkbox:checked").length > 2)
           {
                if($('#name').val() != ''){
                    $('.btn-success').prop('disabled',false);
                }
           }   
           else
           {
                $('.btn-success').prop('disabled',true);
           }
        })

        /*$(document).on('keyup', '.title', function() {
                var id = $(this).val();
                console.log(id);
                $.ajax({
                    type: 'get',
                    url:'/access-control/role/checkTitle',
                    data : {
                        entity : 'check-name',
                        entity_val : id
                    },
                    success: function(res){
                        console.log(1);
                        if(res != 0) {
                            console.log($('.title').parent());
                            $('.title').parent().addClass('has-error');
                            var p = "<p class = 'help-block error'>Name already exists</p>";
                            $('.title').siblings('p').remove();
                            $('.title').parent().append(p);
                            $('#save_continue').prop('disabled', true);
                            $('#save_return').prop('disabled', true);
                            } else {
                                $('.title').parent().removeClass('has-error');
                                $('.title').siblings('p').remove();
                                $('#save_continue').prop('disabled', false);
                                $('#save_return').prop('disabled', false);
                            }

                        }       
                });
            });*/



        function checkDependent(ul, d) {
            
            var t = $(ul).find('[dependent_id="'+d+'"]:checked').length;

            if(t) {
                if (isNaN(d)) {
                    d = d.split(",").map(Number);
                    if (Array.isArray(d)) {
                        $.each(d, function(k, v) {
                            $(ul).find('#'+v).prop('checked', true);
                        })
                    }
                } else {
                    $(ul).find('#'+d).prop('checked', true);
                }
            } else {
                $(ul).find('#'+d).prop('checked', false);
            }

        }

        function checkInnerTab(ul, checked, total) {
            if (checked == total) {
                $(ul).siblings('div').find('input').prop('checked', true);
            } else {
                $(ul).siblings('div').find('input').prop('checked', false);
            }
        }

        function checkInnerWhileLoad() {
            $('.child_tab div ul ul').each(function(k, v) {
                var i_checked = $(v).find('input:checked').length;
                
                var i_total = $(v).find('input').length;
                checkInnerTab(v, i_checked, i_total)
            })
        }

        checkInnerWhileLoad();

        function checkParentTab() {
            var checked = $('.inner_tab:checked').length;
            var total = $('.inner_tab').length;
            if (checked == total) {
                $('.parent_tab').prop('checked', true);
            } else {
                $('.parent_tab').prop('checked', false);
            }
        }

        checkParentTab();
    });

    </script>

@endsection