@extends('layouts.app')

@section('custom-styles')
    
    @include('layouts.style_loaders.token_loader')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
    <link href="{{ asset('/css/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('htmlheader_title')
    {{ucwords(str_replace('-', ' ', Request::segment(1)))}} | {{ucwords(str_replace('-', ' ', Request::segment(2)))}}
@endsection


@section('contentheader_title')
    {{ucwords(str_replace('-', ' ', Request::segment(2)))}}
@endsection


@section('custom-breadcrumb')
    @include('layouts.custom_partials.breadcrumb')
@endsection

@section('contentheader_description')
    
@endsection

@section('main-content')
    
    <div class="panel">
        <div class="panel-body ">   
            <div class = "pull-right">
                <div class="btn-group">
                    <a href="/{{Request::path()}}/create">
                        <label class="btn btn-sharekhan" data-toggle="tooltip" data-target="" title="Add {{ucwords(str_replace('-', ' ', Request::segment(2)))}}" data-original-title="Add {{ucwords(str_replace('-', ' ', Request::segment(2)))}}"><i class="fa fa-plus"></i> Add {{ucwords(str_replace('-', ' ',Request::segment(2)))}}</label>
                    </a>
                </div>
            </div>
        <div class="panel-body padding_top_3">
            <table id="role" class="datatable table display compact table-striped table-bordered  hover nowrap" cellspacing="0" role="grid" width="100%">
                <thead>
                    <tr>
                        @forEach($data['columns'] as $column)
                            <th class = "thead_{{$column}}">{{ucwords(str_replace('_', ' ', $column))}}</th>
                        @endforeach
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        @forEach($data['columns'] as $key => $column)
                            @if(in_array($key ,$data['disable_footer_search']))
                                <th class = "tfoot_{{$column}} no-search"> {{ucwords(str_replace('_', ' ', $column))}} </th>
                            @else
                                <th class = "tfoot_{{$column}}"> {{ucwords(str_replace('_', ' ', $column))}} </th>
                            @endif
                        @endforeach
                    </tr>
                </tfoot>
            </table>
        </div>
        
    </div>

@endsection

@section('php-to-js')
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['columns'] = $data['columns'];
        $js_data['pk'] = $data['pk'];
        // $js_data['prefix'] = $data['prefix'];
        $js_data['status'] = config('constants.STATUS');
        $js_data['color'] = config('constants.COLOR_GENERAL');
    ?>
@endsection

@section('custom-scripts')

    @include('layouts.script_loaders.datatable_global')
    @include('layouts.script_loaders.excel_loader')
    <script src="{{ asset('/js/common/chosen.jquery.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('/js/notify.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            
            // initialize datatables
            var view = {
                display_name : "view",
                class : "view",
                title : "View",
                url : "/",
                href : true,
                i : {
                    class : "fa fa-eye width_icon clr-sk"
                }
            };
            var edit = {
                display_name : "edit",
                class : "edit",
                title : "Edit",
                url : "/edit",
                href : true,
                i : {
                    class : "fa fa-pencil width_icon clr-sk"
                }
            };
            var remove = {
                display_name : "delete",
                class : "delete",
                title : "Delete",
                url : "/",
                href : false,
                i : {
                    class : "fa fa-trash width_icon clr-sk"
                }
            };

            var actionArr = [view, edit,remove];
            // var actionResult = getActionIconAcl(actionArr);
            var action_obj = actionArr;

            var datatable_object = {
                table_name : $('table').attr('id'),
                order : {
                    state: false,
                    column : 1,
                    mode : "desc"
                },
                buttons : {
                    state : true,
                    colvis : true,
                    excel : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    },
                    pdf : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    },
                    csv : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    },
                    copy : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    }
                },
                info : true,
                paging : true,
                searching : true,
                ordering : true,
                iDisplayLength: 10,
                sort_disabled_targets : [],
                ajax_url : window.location.href,
                column_data : datatableColumn(lang.columns, action_obj, lang.pk),
            }

            table = datatableInitGeneral(datatable_object);         


           
    });
    </script>

@endsection