@extends('layouts.app')

@section('custom-styles')
  
    @include('layouts.style_loaders.token_loader')
    <link href="{{ asset('/css/common/bootstrap-switch.min.css') }}" media="screen" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/common/bootstrap-datepicker.css') }}" />
    <link href="{{ asset('/css/common/chosen.min.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('htmlheader_title')
    @include('layouts.custom_partials.htmlheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')])
@endsection


@section('contentheader_title')
    
    @include('layouts.custom_partials.contentheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')])

@endsection

@section('custom-breadcrumb')
    @include('layouts.custom_partials.breadcrumb')
@endsection

@section('contentheader_description')
@endsection

@section('main-content')

@if (Request::segment(4))
    {!! Form::model($data, ['class' => 'form-validate', 'url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey()], 'method' => 'put','enctype' => 'multipart/form-data','autocomplete' => 'off']) !!}
@else
    {!! Form::open(['class' => 'form-validate', 'url' => "/".Request::segment(1)."/".Request::segment(2),'enctype' => 'multipart/form-data','autocomplete' => 'off']) !!}
@endif
<?php $prefix = config('constants.UnitMaster.prefix'); ?>
<input type="hidden" name="screen_name" value = "{{(isset($data) && isset($data['screen_name'])) ? $data['screen_name'] : ''}}">
<section class="panel">
    <div class="panel-body">
        <h4>Basic Detail</h4>
        <div class="row">
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Unit Name *</label>
                    <input type="text" class="form-control unit_name required" id="{{$prefix}}name" name = "{{$prefix}}name" placeholder="Unit Name" value = "{{htmlValue($prefix.'name', $data)}}" {{setReadonly($prefix."name" , $data['readonly'])}}>
                </div>
            </div>
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Unit Symbol</label>
                    <input type="text" class="form-control unit_symbol required" id="{{$prefix}}symbol" name = "{{$prefix}}symbol" placeholder="Unit Symbol" value = "{{htmlValue($prefix.'name', $data)}}" {{setReadonly($prefix."symbol" , $data['readonly'])}}>
                </div>
            </div>
            <div class = "col-md-6">
                <div class="form-group">
                    <label for="name">Remarks</label>
                    <input type="text" class="form-control remarks" id="{{$prefix}}remarks" name = "{{$prefix}}remarks" placeholder="Remarks" value = "{{htmlValue($prefix.'name', $data)}}" {{setReadonly($prefix."remarks" , $data['readonly'])}}>
                </div>
            </div> 
        </div>
    </div>
</section>


@include('layouts.custom_partials.save_panel')
{!! Form::close() !!}
@endsection

@section('php-to-js')
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['screen_name'] = $data['screen_name'];
    ?>
@endsection

@section('custom-scripts')
    @include('layouts.script_loaders.datatable_loader')
    @include('layouts.script_loaders.excel_loader')
    <script src="{{ asset('/js/common/chosen.jquery.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        chosenInit();
        // $('.btn-success').prop('disabled',true);
        
    });

    </script>

@endsection