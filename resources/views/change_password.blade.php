@extends('layouts.app')

@section('custom-styles')
  
    @include('layouts.style_loaders.token_loader')
    <link href="{{ asset('/css/common/chosen.min.css') }}" rel="stylesheet" type="text/css" />
    

@endsection

@section('htmlheader_title')
    @include('layouts.custom_partials.htmlheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')])
@endsection


@section('contentheader_title')
    
   {{ucwords(str_replace('-', ' ', Request::segment(2)))}}  {{ucwords(str_replace('', ' ', isset($title) ? $title : 'Change Password')) }}

@endsection

@section('custom-breadcrumb')
    @include('layouts.custom_partials.breadcrumb')
@endsection

@section('contentheader_description')
@endsection

@section('main-content')

@if (Request::segment(4))
    {!! Form::model($data, ['url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey()], 'method' => 'put','class' => 'form-validate']) !!}
@else
    {!! Form::open(['url' => "/".Request::segment(1)."/".Request::segment(2), 'class' => 'form-validate']) !!}
@endif
<input type="hidden" name="screen_name" value = "{{(isset($data) && isset($data['screen_name'])) ? $data['screen_name'] : ''}}">

<section class="panel">
    <div class="panel-body">
        <div class = "row">
            <div class="col-md-9">             
                <label for="current-password" class="col-sm-4 control-label">Current Password</label>
                <div class="col-sm-8">
                  <div class="form-group">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                    <input type="password" class="form-control required" id="current-password" name="current_password" placeholder="Password">
                  </div>
                </div>
                <label for="password" class="col-sm-4 control-label">New Password</label>
                <div class="col-sm-8">
                  <div class="form-group">
                    <input type="password" class="form-control required" id="password" name="new_password" placeholder="Password">
                  </div>
                </div>
                <label for="password_confirmation" class="col-sm-4 control-label">Re-enter Password</label>
                <div class="col-sm-8">
                  <div class="form-group">
                    <input type="password" class="form-control required" id="password_confirmation" name="password_confirmation" placeholder="Re-enter Password">
                    <div id="error_password"></div>
                  </div>
                </div>
              </div>
              
        </div>
    </div>
    
</section>
<section class="panel">
    <div class="panel-body">

        <div class="btn-group">
            <button class="btn btn-success cbtn" name="save" value="save_return" type="submit" id="save_return">Save and Return</button>
        </div>
    </div>
</section>
{!! Form::close() !!}
@endsection

@section('php-to-js')
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
    ?>
@endsection

@section('custom-scripts')
    
    <script src="{{ asset('/js/common/chosen.jquery.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $(document).on('keyup','#password_confirmation',function(){
            var password = $('#password').val();
            var reenter_pass = $('#password_confirmation').val();
            if(reenter_pass != password)
            {
                console.log("not match");
                $('#error_password').html('Password Not match');
                $('#error_password').css('color','red');
                $(this).css('border-color','red');
                $('#password').css('border-color','red');
                $('#save_return').attr("disabled", true);
            }
            else{
                $('#error_password').html('');
                $('#error_password').css('color','');
                $(this).css('border-color','');
                $('#password').css('border-color','');
                $('#save_return').attr("disabled", false);

            }
        })
    });

    </script>

@endsection