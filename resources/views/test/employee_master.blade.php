@extends('layouts.app')

@section('custom-styles')
    
    @include('layouts.style_loaders.token_loader')
    @include('layouts.style_loaders.datatable_loader')
    <link href="{{ asset('/css/common/chosen.min.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('htmlheader_title')
    {{ucwords(str_replace('-', ' ', Request::segment(1)))}} | {{ucwords(str_replace('-', ' ', Request::segment(2)))}}
@endsection

@section('contentheader_title')
    {{ucwords(str_replace('-', ' ', Request::segment(2)))}}
@endsection

@section('custom-breadcrumb')
    <ol class="breadcrumb">
        <li><a href="javascript:;"><i class="fa fa-lock"></i> {{ucwords(str_replace('-', ' ', Request::segment(1)))}} </a></li>
        <li><a href="javascript:;"><i class="fa fa-user"></i> {{ucwords(str_replace('-', ' ', Request::segment(2)))}} </a></li>
    </ol>
@endsection

@section('contentheader_description')
    
@endsection

@section('main-content')

    <div class="panel">
        <div class="panel-heading">
            <div class = "pull-right">
            @if(isset($data['permissionList']) && 
            isset($data['permissionList'][Request::segment(1)]) && isset($data['permissionList'][Request::segment(1)][Request::segment(2)]) && isset($data['permissionList'][Request::segment(1)][Request::segment(2)]['create']) && $data['permissionList'][Request::segment(1)][Request::segment(2)]['create'] == true)
            <div class = "btn-group">
                    <a href="/{{Request::path()}}/create">
                        <button type="button" class="btn btn-sharekhan" data-original-title="Add Users" data-original-title="Add Users" data-toggle="tooltip" data-target="" title="Add {{ucwords(str_replace('-', ' ', Request::segment(2)))}}" data-original-title="Add {{ucwords(str_replace('-', ' ', Request::segment(2)))}}">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add {{ucwords(str_replace('-', ' ', Request::segment(2)))}}</button>
                    </a>
                </div>
            @endif
            </div>
           <div class="btn-group">
                <label class="btn btn-sharekhan btn-upload" data-toggle="tooltip" data-target="" title="Upload Fixed Ratio" data-original-title="Upload Fixed Ratio">
                        <i class="fa fa-upload" aria-hidden="true"></i>Upload Fixed Ratio
                </label>
            </div>
        <div class="panel-body padding_top_3">
            <table id="table" class="table display compact table-striped table-bordered  hover nowrap" cellspacing="0" role="grid" width="100%">
                <thead>
                    <tr>
                        @forEach($data['columns'] as $column)
                            
                            <th class = "thead_{{$column}}">{{ucwords(str_replace('_', ' ', $column))}}</th>
                                
                            
                        @endforeach
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        @forEach($data['columns'] as $key => $column)
                            @if(in_array($key ,$data['disable_footer_search']))
                                
                                    
                                <th class = "tfoot_{{$column}} no-search"> {{ucwords(str_replace('_', ' ', $column))}} </th>
                                    
                            @else

                                <th class = "tfoot_{{$column}}"> {{ucwords(str_replace('_', ' ', $column))}} </th>
                                    
                            @endif
                        @endforeach
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection
{!! Form::open(array('files'=>'true','id' => 'master_upload')) !!}
    <div id = "employee_modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content border_radius">
                <div class="modal-header bg-sharekhan border_radius_top">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Upload</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class = "col-md-12">
                            <div class="form-group has-feedback">
                                <label for="name">file upload</label>
                                {!! Form::file('sample_file', array('class' => 'sample_file form-control', 'id' => 'sample_file')) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input name="upload-file" id="upload-form" type="button" value="SUBMIT" class="btn btn-sharekhan btn-file"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                <!--<div class="modal-footer">
                    <label class="btn btn-sharekhan btn-file hide"  data-toggle="tooltip" data-target="" title="Upload Fixed Ratio Master" data-original-title="Add {{ucwords(str_replace('-', ' ', Request::segment(2)))}}">
                        <i class="fa fa-upload" aria-hidden="true"></i> Upload Fixed Ratio Master
                        <input  type="file" class = "upload" id="upload">
                    </label>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div> -->
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@section('php-to-js')
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['columns'] = $data['columns'];
        $js_data['pk'] = $data['pk'];
        $js_data['prefix'] = $data['prefix'];
        $js_data['status'] = config('constants.STATUS');
        $js_data['update'] = $data['update'];
        $js_data['user_type'] = $data['user_type'];
        // $js_data['permissionList'] = $data['permissionList'];
        $js_data['color'] = config('constants.COLOR_GENERAL');
    ?>
@endsection

@section('custom-scripts')

    @include('layouts.script_loaders.datatable_loader')
    @include('layouts.script_loaders.excel_loader')
    <script src="{{ asset('/js/common/chosen.jquery.min.js') }}" type="text/javascript"></script>
    

    <script type="text/javascript">
        $(document).ready(function() {
            
            // initialize datatables
            chosenInit("Select Designation", '11em');
            
            var view = {
                display_name : "view",
                class : "view",
                title : "View",
                url : "/",
                href : true,
                i : {
                    class : "fa fa-eye width_icon clr-sk"
                }
            };
            var edit = {
                display_name : "edit",
                class : "edit",
                title : "Edit",
                url : "/edit",
                href : true,
                i : {
                    class : "fa fa-pencil width_icon clr-sk"
                }
            };
            var remove = {
                display_name : "delete",
                class : "delete",
                title : "Delete",
                url : "/",
                href : false,
                i : {
                    class : "fa fa-trash width_icon clr-sk"
                }
            };

            var actionArr = [view, edit, remove];
            // console.log(actionArr);
            var actionResult = getActionIconAcl(actionArr);
            // console.log(actionResult);
            var action_obj = actionResult;

            var datatable_object = {
                table_name : $('table').attr('id'),
                order : {
                    state: false,
                    column : 1,
                    mode : "desc"
                },
                buttons : {
                    state : true ,
                    colvis : true,
                    excel : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    },
                    pdf : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    },
                },
                info : true,
                paging : true,
                searching : true,
                ordering : true,
                iDisplayLength: 10,
                sort_disabled_targets : [],
                ajax_url : window.location.href,
                column_data : datatableColumn(lang.columns, action_obj, lang.pk),
            }

            table = datatableInitWithButtonsAndDynamicRev(datatable_object);
            
            changeRowColorGeneral("thead_type_status" ,{"open":lang.color.OPEN, "active": lang.color.ACTIVE, "inactive":lang.color.INACTIVE, "closed":lang.color.CLOSED, "Deleted":lang.color.CLOSED, "system_defined":lang.color.system_defined}, table);
            statusChange();
            
            $(document).on('change', '.designation', function(e) {
                table.draw();
                e.preventDefault();
            })
            $(document).on('click','.btn-upload',function(){
                $('#employee_modal').modal('show');
            })
            $(document).on('click', '.delete', function() {
               var curr_url = $(this).attr('data-url');
               var row = $(this);
                bootbox.prompt("Are you sure you want to Delete this Record? Please add remark", function(result) {
                if (result) {
                   $.ajax({
                        url: curr_url,
                        type: 'DELETE',
                        data: {
                            entity : result
                        },
                        success: function(result) {
                            table.draw();
                        },
                        statusCode: ajaxStatusCode
                       });
                    }
                }); 
            });

            
            
        });
        $('#upload-form').on('click', function(e){
                console.log('hello world', $('#master_upload').serialize());
                e.preventDefault();
                handleBackendUpload(e);
            });
    </script>

@endsection