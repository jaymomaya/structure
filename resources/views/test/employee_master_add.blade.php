@extends('layouts.app')

@section('custom-styles')
  
    @include('layouts.style_loaders.token_loader')
    <link href="{{ asset('/css/common/bootstrap-switch.min.css') }}" media="screen" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/common/bootstrap-datepicker.css') }}" />
    <link href="{{ asset('/css/common/chosen.min.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('htmlheader_title')
    @include('layouts.custom_partials.htmlheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')])
@endsection


@section('contentheader_title')
    
    @include('layouts.custom_partials.contentheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')])

@endsection

@section('custom-breadcrumb')
    @include('layouts.custom_partials.breadcrumb')
@endsection

@section('contentheader_description')
@endsection

@section('main-content')

@if (Request::segment(4))
    {!! Form::model($data, ['url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey()], 'method' => 'put','enctype' => 'multipart/form-data','autocomplete' => 'off']) !!}
@else
    {!! Form::open(['url' => "/".Request::segment(1)."/".Request::segment(2),'enctype' => 'multipart/form-data','autocomplete' => 'off']) !!}
@endif
<?php $prefix = config('constants.EmployeeMaster.prefix'); ?>
<input type="hidden" name="screen_name" value = "{{(isset($data) && isset($data['screen_name'])) ? $data['screen_name'] : ''}}">

<section class="panel">
    <div class="panel-body">
        <label>Basic Detail</label>
        <div class="row">
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">First Name *</label>
                    <input type="text" class="form-control em_firstname required" id="em_firstname" name = "em_firstname" placeholder="Enter First Name" value = "{{htmlValue('em_firstname', $data)}}" {{setReadonly('em_firstname' , $data['readonly'])}}>
                </div>
            </div>
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Middle Name</label>
                    <input type="text" class="form-control em_middlename" id="em_middlename" name = "em_middlename" placeholder="Enter Middle Name" value = "{{htmlValue('em_middlename', $data)}}" {{setReadonly('em_middlename' , $data['readonly'])}}>
                </div>
            </div> 
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Last Name</label>
                    <input type="text" class="form-control em_lastname" id="em_lastname" name = "em_lastname" placeholder="Enter Last Name" value = "{{htmlValue('em_lastname', $data)}}" {{setReadonly('em_lastname' , $data['readonly'])}}>
                </div>
            </div>
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Reporting To</label>
                    {{
                        Form::select('employee_type',
                        (isset($data) && isset($data['employee_master'])) ? $data['employee_master'] : [],
                        (isset($data) && isset($data["em_reportingto"])) ? $data["em_reportingto"] : htmlSelect('em_reportingto', $data),
                        array('name'=>'em_reportingto','class' => 'form-control chosen-select', 'placeholder' => '', 'data-placeholder' => 'Select Reporting Person', 'placeholder' => '', setDisable('em_reportingto' , $data['disabled'])))
                    }}
                    
                </div>
            </div>
        </div>
        <div class = "row">
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Designation</label>
                    <input type="text" class="form-control em_designation" id="em_designation" name = "em_designation" placeholder="Enter Designation" value = "{{htmlValue('em_designation', $data)}}" {{setReadonly('em_designation' , $data['readonly'])}}>
                </div>
            </div>
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Mobile Number *</label>
                    <input type="text" class="form-control em_phone_number numeric" id="em_phone_number" name = "em_phone_number" placeholder="Enter Phone Number" value = "{{htmlValue('em_phone_number', $data)}}" maxlength="11" minlength="10" {{setReadonly('em_phone_number' , $data['readonly'])}}>
                </div>
            </div>
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Email *</label>
                    <input type="email" class="form-control em_email" id="em_email" name = "em_email" placeholder="Enter Email" value = "{{htmlValue('em_email', $data)}}" {{setReadonly('em_email' , $data['readonly'])}}>
                </div>
            </div>
        </div>
        
        <div class="row">
            
            <div class = "col-md-3 hide">
                <div class="form-group">
                    <label for="name">Designation Code</label>
                    <input type="text" class="form-control em_designationcode" id="em_designationcode" name = "em_designationcode" placeholder="Enter Designation Code" value = "{{htmlValue('em_designationcode', $data)}}" {{setReadonly('em_designationcode' , $data['readonly'])}}>
                </div>
            </div>
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Department</label>
                    <input type="text" class="form-control em_department" id="em_department" name = "em_department" placeholder="Enter Department" value = "{{htmlValue('em_department', $data)}}" {{setReadonly('em_department' , $data['readonly'])}}>
                </div>
            </div>
            
        </div>
    </div>
</section>





@if($data['screen_name']!='view' && $data['screen_name']!='edit')
<section class="panel">
    <div class="panel-body">
        <div class="btn-group">
                <button class="btn btn-success cbtn" name="save" value="save_continue" type="submit" id="save_continue">Save and Continue</button>
        </div>

        <div class="btn-group">
            <button class="btn btn-success cbtn" name="save" value="save_return" type="submit" id="save_return">Save and Return</button>
        </div>
   
        <div class="btn-group pull-right">
            <a href="/{{Request::segment(1)}}/{{Request::segment(2)}}/"><button class="btn btn-danger" name="cancel" value="cancel" type="button" id="cancel">Cancel</button></a>
        </div>
    </div>
</section>
@endif
@if($data['screen_name']=='edit')
<section class="panel">
    <div class="panel-body">
       
        <div class="btn-group">
            <button class="btn btn-success cbtn" name="save" value="save_return" type="submit" id="save_return">Save and Return</button>
        </div>
   
        <div class="btn-group pull-right">
            <a href="/{{Request::segment(1)}}/{{Request::segment(2)}}/"><button class="btn btn-danger" name="cancel" value="cancel" type="button" id="cancel">Cancel</button></a>
        </div>
    </div>
</section>

@endif
<div id ="document_modal" class="modal fade">
        <div class="modal-dialog" >
            <div class="modal-content border_radius">
                <div class="modal-header bg-sharekhan border_radius_top">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Company Documents</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="gallerymodal">
                            <img class="hide" width="90%" height="300px" style="margin :5px;margin-top: 5%;margin-left: 5%">
                            <iframe class="hide" width="90%" height="300px" style="margin :5px;margin-top: 5%;margin-left: 5%"></iframe>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection

@section('php-to-js')
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['screen_name'] = $data['screen_name'];
    ?>
@endsection

@section('custom-scripts')
    @include('layouts.script_loaders.datatable_loader')
    @include('layouts.script_loaders.excel_loader')
    <script src="{{ asset('/js/common/chosen.jquery.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        chosenInit();
        // $('.btn-success').prop('disabled',true);
        var chosen = $('.chosen-select').val();
        //disableAll("{{Request::segment(2)}}", "{{Route::currentRouteName()}}");
        var arr = [];
        $(document).on('keyup','.tax_info',function(){
            this.value = this.value.toUpperCase();
        })
        $(document).on('change', '.role', function(evt, params) {
            
            if(params.deselected) {
                arr.push(params.deselected);
            }
            $('.deselected_roles').val(arr);
            
        });
        $(document).on('change','.file',function(e){
            console.log(this.files[0].size);
            if(this.files[0].size > 5000000){
               alert("File Size Allowed Upto 5 Mb Only!");
               this.value = "";
            };
        });
        $(document).on('change', '.documents', function(e) {
            var files = e.target.files;
            $('.documents_names').empty();
            var names = [];
            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                names.push($(this).get(0).files[i].name);
            }
            if(names.length > 0)
            {
                var file_list = document.createElement('UL');
            
                $(names).each(function(k,v){
                    var remove_label = document.createElement('LABEL');
                    remove_label.append("REMOVE");
                    remove_label.setAttribute('style', 'color:blue; margin-left:15px');
                    
                    var file_name_label = document.createElement("LI");
                    file_name_label.setAttribute('class', 'file_name_label');
                    file_name_label.append(v);
                    // file_name_label.append(remove_label);
                    file_list.append(file_name_label);
                });
                $('.documents_names').append(file_list);
            }
        });

        $(document).on('click', '.add_incentive_row', function() {
            var row = $('.incentive_row:eq(0)').clone();
            var time = new Date().getTime();
            var checkflag = 0;
            $('.row:last', function() {
                if($('.incentive_row:last').find(".from").val() != ""){
                    if($('.incentive_row:last').find(".to").val() != ""){
                        if($('.incentive_row:last').find(".amount").val() != ""){

                        }else{
                            checkflag = 1;
                        }
                    }else{
                        checkflag = 1;
                    }
                }else{
                    checkflag = 1;
                }
            })
            if(checkflag == 0){ 
                row.find('input').each(function(){
                    var from_val = 0;
                    if($(this).hasClass('from'))
                    {
                        $(this).val(parseInt($('.to:last').val()) + 1);
                    }
                    else
                    {
                        $(this).val('');
                    }
                    var data_name = $(this).attr('data-name');
                    $(this).attr('name', 'e_inc['+time+']['+data_name+']');
                })
                row.find('.delete_row').removeClass('hide');
                $('.incentive_row_parent').append(row);
            }else{
                $('.error').remove();
                $('.incentive_row:last').parent().removeClass('has-error');
                $('.incentive_row:last').addClass('has-error');
                var p = "<p class = 'help-block error'>Required</p>";
                $('.incentive_row:last').siblings('p').remove();
                $('.incentive_row:last').append(p);
            }
        });

        $('.numeric').each(function (){
           $(this).keydown(function(e) {
               if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                   (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                   (e.keyCode >= 35 && e.keyCode <= 40)) {
                        return;
               }
               if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                   e.preventDefault();
               }
           });    
        });

        $(document).on("click", ".delete_row", function() {
            $(this).closest('div').parent('div').remove();
            $(this).closest('div').remove();
        });


        // $(document).on('keyup', '#em_email', function() {
        //     var id = $(this).val();
        //     var screen = lang.screen_name;
        //     var check_id = parseInt(location.pathname.split('/')[3]);
        //     console.log(screen);
        //     if(screen == 'create'){
        //             $.ajax({
        //                 type: 'get',
        //                 url:'/access-control/employee-master/checkEmail',
        //                 data : {
        //                     entity : 'check-name',
        //                     entity_val : id,
        //                     entity_data : screen
        //                 },
        //                 success: function(res){
        //                 console.log(1);
        //                 if(res != 0) {
        //                     console.log($('#em_email').parent());
        //                     $('#em_email').parent().addClass('has-error');
        //                     var p = "<p class = 'help-block error'>Email already exists</p>";
        //                     $('#em_email').siblings('p').remove();
        //                     $('#em_email').parent().append(p);
        //                     $('#save_continue').prop('disabled', true);
        //                     $('#save_return').prop('disabled', true);
        //                 } else {
        //                     $('#em_email').parent().removeClass('has-error');
        //                     $('#em_email').siblings('p').remove();
        //                     $('#save_continue').prop('disabled', false);
        //                     $('#save_return').prop('disabled', false);
        //                 }
        //             }       
        //         })
        //     }else{
        //         $.ajax({
        //             type: 'get',
        //             url:'/access-control/employee-master/checkEmail',
        //             data : {
        //                 entity : 'check-edit',
        //                 entity_val : id,
        //                 entity_data : check_id
        //             },
        //             success: function(res){
        //                console.log(1);
        //                if(res != 0) {
        //                 console.log($('#em_email').parent());
        //                     $('#em_email').parent().addClass('has-error');
        //                     var p = "<p class = 'help-block error'>Email already exists</p>";
        //                     $('#em_email').siblings('p').remove();
        //                     $('#em_email').parent().append(p);
        //                     $('#save_continue').prop('disabled', true);
        //                     $('#save_return').prop('disabled', true);
        //                 } else {
        //                     $('#em_email').parent().removeClass('has-error');
        //                     $('#em_email').siblings('p').remove();
        //                     $('#save_continue').prop('disabled', false);
        //                     $('#save_return').prop('disabled', false);
        //                 }
        //             }       
        //         })
        //     }
        // });

        $(document).on('click', '.delete_document', function() {
            var id = $(this).attr('data-id');
            var del = $(this);
            bootbox.confirm("Are you sure you want to Delete This Document", function(result) {
                if (result) {

                    $.ajax({
                        url : '/access-control/employee-master/delete-document',
                        type : 'get',
                        data : {
                            entity_type : "delete_document",
                            entity_id : id
                        },
                        success : function(res) {
                            console.log($(this))
                            $(del).parent().parent().remove();   
                        }
                    })
                }
            })
        });


        $(document).on('keyup', '#em_phone_number', function() {
            var id = $(this).val();
            var screen = lang.screen_name;
            var check_id = parseInt(location.pathname.split('/')[3]);
            if(screen== 'create'){
                    $.ajax({
                        type: 'get',
                        url:'/access-control/employee-master/checkPhone',
                        data : {
                            entity : 'check-name',
                            entity_val : id,
                            entity_data : screen
                        },
                        success: function(res){
                        console.log(1);
                        if(res != 0) {
                            console.log($('#em_phone_number').parent());
                            $('#em_phone_number').parent().addClass('has-error');
                            var p = "<p class = 'help-block error'>Phone already exists</p>";
                            $('#em_phone_number').siblings('p').remove();
                            $('#em_phone_number').parent().append(p);
                            $('#save_continue').prop('disabled', true);
                            $('#save_return').prop('disabled', true);
                        } else {
                            $('#em_phone_number').parent().removeClass('has-error');
                            $('#em_phone_number').siblings('p').remove();
                            $('#save_continue').prop('disabled', false);
                            $('#save_return').prop('disabled', false);
                        }
                    }       
                })
            }else{
                $.ajax({
                    type: 'get',
                    url:'/access-control/employee-master/checkPhone',
                    data : {
                        entity : 'check-edit',
                        entity_val : id,
                        entity_data : check_id
                    },
                    success: function(res){
                       console.log(1);
                       if(res != 0) {
                        console.log($('#em_phone_number').parent());
                            $('#em_phone_number').parent().addClass('has-error');
                            var p = "<p class = 'help-block error'>Phone already exists</p>";
                            $('#em_phone_number').siblings('p').remove();
                            $('#em_phone_number').parent().append(p);
                            $('#save_continue').prop('disabled', true);
                            $('#save_return').prop('disabled', true);
                        } else {
                            $('#em_phone_number').parent().removeClass('has-error');
                            $('#em_phone_number').siblings('p').remove();
                            $('#save_continue').prop('disabled', false);
                            $('#save_return').prop('disabled', false);
                        }
                    }       
                })
            }
        });

         $(document).on('change','.employee_doc',function(e){
            $('.gallery').empty();

            imagesPreview(this, 'div.gallery');
        })

      var imagesPreview = function(input, placeToInsertImagePreview) {
            //console.log(input);
            if (input.files) {
                var filesAmount = input.files.length;
                var count = 0;
                for (i = 0; i < filesAmount; i++) {
                    var name = $(input).get(0).files[i].name;
                    var ext = name.split('.');
                    console.log("exten",ext[1]);
                    $('.gallery').append('<input type="hidden" id=files_name'+i+' name=files_name[] value="'+name+'">');
                    if(ext[1] == "jpg" || ext[1] == "jpeg" || ext[1] == "png" || ext[1] == "gif")
                    {
                        console.log("here in if");
                        var reader = new FileReader();
                        reader.onload = function(event) {
                            //console.log(ext);
                            $($.parseHTML('<img class="imageview" data-file="image" width="75px" height="75px" style="margin :10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                            /*$($.parseHTML('<i class="fa fa-trash data_count='+count+' removeimage"><input type=hidden id=count name=count value='+count+'>')).appendTo(placeToInsertImagePreview);*/
                            $($.parseHTML('<input type=hidden id=file name=file'+count+' value='+event.target.result+'>')).appendTo(placeToInsertImagePreview);
                            $($.parseHTML('<i class="fa fa-trash removeimage" counter='+count+'>')).appendTo(placeToInsertImagePreview);
                            count = count + 1;
                        }
                    }
                    else
                    {
                        var reader = new FileReader();
                        reader.onload = function(event) {
                            //console.log(ext);
                            $($.parseHTML('<img class="hide" width="75px" height="75px" style="margin :10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                            var filename = event.target.result;
                             $($.parseHTML('<img class="imageview" data-file="notimage" width="75px" height="75px" style="margin :10px">')).attr('src', '/products/pdf.jpeg').appendTo(placeToInsertImagePreview);
                           /* $($.parseHTML('<i class="fa fa-trash data_count='+count+' removeimage"><input type=hidden id=count name=count value='+count+'>')).appendTo(placeToInsertImagePreview);*/
                            $($.parseHTML('<input type=hidden id=file name=file'+count+' value='+filename+'>')).appendTo(placeToInsertImagePreview);
                            $($.parseHTML('<i class="fa fa-trash removeimage" counter='+count+'>')).appendTo(placeToInsertImagePreview);
                            console.log("check_deatils",filename);
                            count = count + 1;
                        }
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }

        };

    $(document).on('click','.removeimage',function(){
        var curr = $(this);
        $(curr).prev().prev().remove();
        $(curr).remove();
        var c = $(curr).children().val();
        console.log(c);
         var counter = $(this).attr('counter');
        $('#files_name'+counter).remove();
    });
    $(document).on('click', '.imageview', function(){
        var image = $(this).attr('src');
        var file = $(this).next().val();
        var datafile = $(this).attr('data-file');
        console.log("file imageview",datafile);
        if(datafile == "image")
        {
            $('.gallerymodal').find('img').removeClass('hide');
            $('.gallerymodal').find('img').addClass('show');
            $('.gallerymodal').find('img').attr('src',file);
            $('.gallerymodal').find('iframe').removeClass('show');
            $('.gallerymodal').find('iframe').addClass('hide');
        }
        else
        {
            $('.gallerymodal').find('iframe').removeClass('hide');
            $('.gallerymodal').find('iframe').addClass('show');
            $('.gallerymodal').find('iframe').attr('src',file);
            $('.gallerymodal').find('img').removeClass('show');
            $('.gallerymodal').find('img').addClass('hide');
        }
        $('#document_modal').modal('show');
    });

        //JAY 26-07-2018 Check Duplicate Email
        $(document).on('keyup, blur', '#em_email', function(){
            var email = $(this).val();
            var check_id = parseInt(location.pathname.split('/')[3]);
            $.ajax({
                url: '/api/user/check-duplicate-email',
                data: {
                    entity_type: 'checkUserEmail',
                    entity_screen_name: lang.screen_name,
                    entity_id: check_id,
                    entity: email,
                },
                success: function(res){
                    hideWarning('#em_email');
                    if(res > 0){
                        showWarning('#em_email', 'This Email Id is already included in the System.')
                    }
                }
            })
        })

    });

    </script>

@endsection