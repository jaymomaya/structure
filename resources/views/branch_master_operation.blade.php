@extends('layouts.app')

@section('custom-styles')
  
    @include('layouts.style_loaders.token_loader')
    <link href="{{ asset('/css/common/bootstrap-switch.min.css') }}" media="screen" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/common/bootstrap-datepicker.css') }}" />
    <link href="{{ asset('/css/common/chosen.min.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('htmlheader_title')
    @include('layouts.custom_partials.htmlheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')])
@endsection


@section('contentheader_title')
    
    @include('layouts.custom_partials.contentheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')])

@endsection

@section('custom-breadcrumb')
    @include('layouts.custom_partials.breadcrumb')
@endsection

@section('contentheader_description')
@endsection

@section('main-content')

@if (Request::segment(4))
    {!! Form::model($data, ['class' => 'form-validate', 'url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey()], 'method' => 'put','enctype' => 'multipart/form-data','autocomplete' => 'off']) !!}
@else
    {!! Form::open(['class' => 'form-validate', 'url' => "/".Request::segment(1)."/".Request::segment(2),'enctype' => 'multipart/form-data','autocomplete' => 'off']) !!}
@endif
<?php $prefix = config('constants.BranchMaster.prefix'); ?>
<input type="hidden" name="screen_name" value = "{{(isset($data) && isset($data['screen_name'])) ? $data['screen_name'] : ''}}">
<section class="panel">
    <div class="panel-body">
        <h4>Basic Detail</h4>
        <div class="row">
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Branch Name *</label>
                    <input type="text" class="form-control required" id="{{$prefix}}name" name = "{{$prefix}}name" placeholder="Branch Name" value = "{{htmlValue($prefix.'name', $data)}}" {{setDisable($prefix."name" , $data['disabled'])}}>
                </div>
            </div>
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Mobile No.</label>
                    <input type="text" class="form-control numeric" id="{{$prefix}}mobile" name = "{{$prefix}}mobile" placeholder="Mobile" value = "{{htmlValue($prefix.'mobile', $data)}}" {{setDisable($prefix."mobile" , $data['disabled'])}} maxlength="10">
                </div>
            </div>
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Alternate Mobile</label>
                    <input type="text" class="form-control numeric" id="{{$prefix}}alt_mobile" name = "{{$prefix}}alt_mobile" placeholder="Alternate Mobile" value = "{{htmlValue($prefix.'alt_mobile', $data)}}" {{setDisable($prefix."alt_mobile" , $data['disabled'])}} maxlength="10">
                </div>
            </div>
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Email</label>
                    <input type="text" class="form-control email" id="{{$prefix}}email" name = "{{$prefix}}email" placeholder="Email" value = "{{htmlValue($prefix.'email', $data)}}" {{setDisable($prefix."email" , $data['disabled'])}}>
                </div>
            </div> 
        </div>
        <div class="row">
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Alternate Email</label>
                    <input type="text" class="form-control" id="{{$prefix}}alt_email" name = "{{$prefix}}alt_email" placeholder="Alternate Email" value = "{{htmlValue($prefix.'alt_email', $data)}}" {{setDisable($prefix."alt_email" , $data['disabled'])}}>
                </div>
            </div>
            <div class = "col-md-6">
                <div class="form-group">
                    <label for="name">Address</label>
                    <input type="text" class="form-control" id="{{$prefix}}address" name = "{{$prefix}}address" placeholder="Address" value = "{{htmlValue($prefix.'address', $data)}}" {{setDisable($prefix."address" , $data['disabled'])}}>
                </div>
            </div>
            
        </div>
    </div>
</section>


@include('layouts.custom_partials.save_panel')
{!! Form::close() !!}
@endsection

@section('php-to-js')
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['screen_name'] = $data['screen_name'];
    ?>
@endsection

@section('custom-scripts')
    @include('layouts.script_loaders.datatable_loader')
    @include('layouts.script_loaders.excel_loader')
    <script src="{{ asset('/js/common/chosen.jquery.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        chosenInit();
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        $(".numeric").bind("keypress", function (e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
            $(".error").css("display", ret ? "none" : "inline");
            return ret;
        });
        $(".numeric").bind("paste", function (e) {
            var element = this;
            setTimeout(function () {
                var tip = $(element).val();
                    // REMOVE NON-NUMERIC CHARACTERS
                var cleanTip = tip.replace(/\D/g,'');
                $(element).val(cleanTip);
            }, 100);
        });
    });

    </script>

@endsection