    <?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => []], function () {
    Route::auth();
    Route::get('/logout', function() {
        Auth::logout();
        //Added for session destroy on logout. Fix for Session Regeneration --AG 310717
        Session::flush();
        return redirect('/login');
    });

    // Route::get('/home', 'HomeController@index')->name('home');
});
Route::group(['middleware' => ['inputcheck','prevent-back-history']], function () {
    Route::get('/', function () {
        return view('home');
        // auth()->user()->assignRole('Super Admin');
    });
});

Route::middleware(['auth', 'aclmiddleware', 'inputcheck',  'prevent-back-history'])->group(function () {
    Route::auth();
    Route::get('/', function () {
        return redirect('/login');
    });

    
    Route::group(['prefix' => 'access-control'], function() {
        Route::resource('users', 'UserController');
        Route::resource('role/role-create', 'UserController@roleCreate');
        // Route::get('role/checkRoleName','RoleController@checkRoleName');
        Route::resource('role', 'RoleController');
        Route::resource('branch-master', 'BranchMasterController');
        Route::resource('data', 'DataController');
        Route::resource('permission', 'PermissionController');
    });

    
    Route::group(['prefix' => '/'], function() {
        // Route::get('/', 'MainPageController@index');
        Route::get('/change_password', 'MainPageController@ChangePassword');
        Route::post('/change_password', 'MainPageController@CheckPassword');

    });
    
    Route::post('/store-file', 'StoreFileController@store_file');
});

Route::post('/store-file', 'StoreFileController@store_file');
Route::get('notify/index', 'NotificationController@index');

Auth::routes();