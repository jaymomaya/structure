    $(document).ready(function() {
    // $(".uct_group_select").select2({
    //     tags: true
    // });
    if(lang.screen_name != 'view'){
        CallCkeditor();
    }
    var today = new Date();
    $('.datepicker').datepicker({ 
        format: 'dd-mm-yyyy' ,
        autoclose:true,
        minDate: today
    });
    var chosen = $('.chosen-select').val();

    $(document).on('change','.file',function(e){
        // console.log(this.files[0].size);
        if(this.files[0].size > 5000000){
           alert("File Size Allowed Upto 5 Mb Only!");
           this.value = "";
        };
    });

        
        var today = new Date();
        $('.vendor_datepicker').datepicker({ 
            format: 'dd-mm-yyyy' ,
            autoclose:true,

            endDate: "today",
            orientation: "bottom",
            maxDate: today 
        });
    $(document).on('change','.comp_docmodal',function(e){
        $('.gallerymodalcom').empty();
        imagesPreview(this, 'div.gallerymodalcom');
        
        console.log("modal");
        /*var files = e.target.files;
        $('.company_documents').empty();
        var names = [];
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            names.push($(this).get(0).files[i].name);
        }
        if(names.length > 0)
        {
            var file_list = document.createElement('UL');
        
            $(names).each(function(k,v){
                var remove_label = document.createElement('LABEL');
                remove_label.append("REMOVE");
                remove_label.setAttribute('style', 'color:blue; margin-left:15px');
                
                var file_name_label = document.createElement("LI");
                file_name_label.setAttribute('class', 'file_name_label');
                file_name_label.append(v);
                // file_name_label.append(remove_label);
                file_list.append(file_name_label);
            });
            $('.company_documents').append(file_list);
        }*/
    })

    /*$(document).on('change', '.documents', function(e) {
        var files = e.target.files;
        $('.documents_names').empty();
        var names = [];
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            names.push($(this).get(0).files[i].name);
        }
        if(names.length > 0)
        {
            var file_list = document.createElement('UL');
        
            $(names).each(function(k,v){
                var remove_label = document.createElement('LABEL');
                remove_label.append("REMOVE");
                remove_label.setAttribute('style', 'color:blue; margin-left:15px');
                
                var file_name_label = document.createElement("LI");
                file_name_label.setAttribute('class', 'file_name_label');
                file_name_label.append(v);
                // file_name_label.append(remove_label);
                file_list.append(file_name_label);
            });
            $('.documents_names').append(file_list);
        }
    });*/


    $(document).on('click', '.delete_com_document', function() {
        var id = $(this).attr('data-id');
        var del = $(this);
        console.log(111);
        bootbox.confirm("Are you sure you want to Delete This Document", function(result) {
            if (result) {
                $.ajax({
                    url : '/settings/company-master/delete-document',
                    type : 'get',
                    data : {
                        entity_type : "delete_document",
                        entity_id : id
                    },
                    success : function(res) {
                        console.log($(del),"res");
                        $(del).prev().prev().remove();   
                        $(del).remove();
                    }
                })
            }
        });
    });
    $(document).on('change','.state',function(){ 
        var selected_val = parseInt($(".state option:selected").val());
        $('#cm_state').val(selected_val);
        if(selected_val != 0){
            $('.country').val('101').prop('disabled', false).trigger('chosen:updated');
        }
    })
    $(document).on('change','.cm_shipping_state',function(){ 
        var selected_val = parseInt($(".cm_shipping_state option:selected").val());
        $('#cm_shipping_state').val(selected_val);
        if(selected_val != 0){
            $('.cm_shipping_country').val('101').prop('disabled', false).trigger('chosen:updated');
        }
    })

    $(document).on('change','.cm_shipping_country',function(){
        var selected_val = parseInt($(".cm_shipping_country option:selected").val());
        $('#cm_shipping_country').val(selected_val); 
        if(isNaN(selected_val))
        {
            $('.cm_shipping_state').val('').prop('disabled', false).trigger('chosen:updated');
            $('#cm_shipping_state').val(''); 
            
            $('.overseas_state_shipping').addClass('hide');   
        }
        else if(lang.india.code_val != selected_val){
            $('.cm_shipping_state').val(0).prop('disabled', true).trigger('chosen:updated'); 
            $('#cm_shipping_state').val(0);
            $('.overseas_state_shipping').removeClass('hide');   
        }else{
            $('.cm_shipping_state').val('').prop('disabled', false).trigger('chosen:updated'); 
            $('#cm_shipping_state').val('');
            $('.overseas_state_shipping').addClass('hide');   
        }
    });
        
    $(document).on('change','.country',function(){
        var selected_val = parseInt($(".country option:selected").val());
        if(isNaN(selected_val))
        {
            $('.state').val('').prop('disabled', false).trigger('chosen:updated'); 
            $('#cm_state').val('');
            $('.overseas_state').addClass('hide');
            if($("#same_as").prop('checked') == false){
                $('.overseas_state_shipping').addClass('hide'); 
            }else{
                $('.overseas_state_shipping').removeClass('hide');
            }   
        }
        else if('101' != selected_val){
            $('.state').val(0).prop('disabled', true).trigger('chosen:updated'); 
            $('#cm_state').val(0);
            $('.overseas_state').removeClass('hide');
            if($("#same_as").prop('checked') == true){
                $('.overseas_state_shipping').removeClass('hide'); 
            }else{
                $('.overseas_state_shipping').addClass('hide');
            }     
        }else{
            $('.state').val('').prop('disabled', false).trigger('chosen:updated'); 
            $('#cm_state').val('');
            $('.overseas_state').addClass('hide');
            if($("#same_as").prop('checked') == false){
                $('.overseas_state_shipping').addClass('hide'); 
            }else{
                $('.overseas_state_shipping').removeClass('hide');
            }     
            $.ajax({
                type: 'get',
                url:'/settings/company-master/getState',
                data : {
                    entity : 'get-state',
                    entity_val : selected_val
                },
                success: function(res){}
            });

        }

    })
    $(document).on('change','#same_as',function(){
        if($("#same_as").prop('checked') == true){
            var cm_address = $('.cm_address').val();
            var cm_address2 = $('.cm_address2').val();
            var cm_city = $('.cm_city').val();
            var cm_pincode = $('.cm_pincode').val();
            var country = parseInt($(".country option:selected").val());
            var state = parseInt($(".state option:selected").val());
            var cm_other_state = $('.cm_other_state').val();
            $('.cm_shipping_address').val(cm_address);
            $('.cm_shipping_address2').val(cm_address2);
            $('.cm_shipping_city').val(cm_city);
            $('.cm_shipping_pincode').val(cm_pincode);
            $('.cm_shipping_other_state').val(cm_other_state);
            $('.cm_shipping_country').val(country).prop('disabled', true).trigger('chosen:updated');
            $('.cm_shipping_state').val(state).prop('disabled', true).trigger('chosen:updated');
            $('#cm_shipping_state').val(state);
            $('#cm_shipping_country').val(country);
            // console.log(state);
            if(state = 0){
                $('.overseas_state_shipping').removeClass('hide');
            }else{
                $('.overseas_state_shipping').addClass('hide');
            } 
            $(document).on('keyup','.cm_address,.cm_address2,.cm_city,.cm_pincode,.cm_other_state',function(){

                $('.cm_shipping_address').val($('.cm_address').val());
                $('.cm_shipping_address2').val($('.cm_address2').val());
                $('.cm_shipping_city').val($('.cm_city').val());
                $('.cm_shipping_pincode').val($('.cm_pincode').val());
                $('.cm_shipping_other_state').val($('.cm_other_state').val());
            })
            $(document).on('change','.country,.state',function(){
                $('.cm_shipping_country').val(parseInt($(".country option:selected").val())).prop('disabled', true).trigger('chosen:updated');
                $('.cm_shipping_state').val(parseInt($(".state option:selected").val())).prop('disabled', true).trigger('chosen:updated');
                $('#cm_state').val(parseInt($(".state option:selected").val()));
                $('#cm_shipping_state').val(parseInt($(".state option:selected").val()));
                $('#cm_shipping_country').val(parseInt($(".country option:selected").val())); 

            })
            $(document).on('change','.state',function(){ 
                $('.country').val('101').prop('disabled', false).trigger('chosen:updated');
                $('#cm_state').val(parseInt($(".state option:selected").val()));
                $('#cm_shipping_state').val(parseInt($(".state option:selected").val()));
                $('.cm_shipping_country').val('101').prop('disabled', true).trigger('chosen:updated');
            })
        }else{
            $('.cm_shipping_address').val('');
            $('.cm_shipping_address2').val('');
            $('.cm_shipping_city').val('');
            $('.cm_shipping_pincode').val('');
            $('.cm_shipping_other_state').val('');
            $('.cm_shipping_country').val('').prop('disabled', false).trigger('chosen:updated');
            $('.cm_shipping_state').val('').prop('disabled', false).trigger('chosen:updated');
        }
    })

        $(document).on('blur', '.cm_email', function() {
            console.log(1111);
            var id = $(this).val();
            var company_type = $('.cm_type').val();
            var check_id = parseInt(location.pathname.split('/')[3]);
            if(lang.screen_name == 'create'){
                $.ajax({
                    type: 'get',
                    url:'/settings/company-master/checkEmail',
                    data : {
                        entity : 'check-name',
                        entity_val : id,
                        type : company_type
                    },
                    success: function(res){
                        console.log(res);
                        if(res != 0) {
                            console.log($(this).parent());
                            $('.cm_email').parent().addClass('has-error');
                            var p = "<p class = 'help-block error'>Email already exists</p>";
                            $('.cm_email').siblings('p').remove();
                            $('.cm_email').parent().append(p);
                            $('#save_continue').prop('disabled', true);
                            $('#save_return').prop('disabled', true);
                            } else {
                                $('.cm_email').parent().removeClass('has-error');
                                $('.cm_email').siblings('p').remove();
                                $('#save_continue').prop('disabled', false);
                                $('#save_return').prop('disabled', false);
                            }

                        }       
                });
            }
        });
  /*  $(document).on('keyup', '.cm_email', function() {
        // console.log(1111);
        var id = $(this).val();
        $.ajax({
            type: 'get',
            url:'/settings/company-master/checkEmail',
            data : {
                entity : 'check-name',
                entity_val : id
            },
            success: function(res){
                // console.log(1);
                if(res != 0) {
                    // console.log($('.cm_email').parent());
                    $('.cm_email').parent().addClass('has-error');
                    var p = "<p class = 'help-block error'>Email already exists</p>";
                    $('.cm_email').siblings('p').remove();
                    $('.cm_email').parent().append(p);
                    $('#save_continue').prop('disabled', true);
                    $('#save_return').prop('disabled', true);
                    } else {
                        $('.cm_email').parent().removeClass('has-error');
                        $('.cm_email').siblings('p').remove();
                        $('#save_continue').prop('disabled', false);
                        $('#save_return').prop('disabled', false);
                    }

                }       
        });
    });*/

    $(document).on('blur', '.cm_name', function() {
            console.log(1111);
            var id = $(this).val();
            var company_type = $('.cm_type').val();
            var check_id = parseInt(location.pathname.split('/')[3]);
            if(lang.screen_name == 'create'){
                $.ajax({
                type: 'get',
                url:'/settings/company-master/checkName',
                data : {
                    entity : 'check-name',
                    entity_val : id,
                    type : company_type
                },
                success: function(res){
                    console.log(1);
                    if(res != 0) {
                        console.log($('.cm_name').parent());
                        $('.cm_name').parent().addClass('has-error');
                        var p = "<p class = 'help-block error'>Name already exists</p>";
                        $('.cm_name').siblings('p').remove();
                        $('.cm_name').parent().append(p);
                        $('#save_continue').prop('disabled', true);
                        $('#save_return').prop('disabled', true);
                        } else {
                            $('.cm_name').parent().removeClass('has-error');
                            $('.cm_name').siblings('p').remove();
                            $('#save_continue').prop('disabled', false);
                            $('#save_return').prop('disabled', false);
                        }

                    }       
            });
            // }else{
            //     $.ajax({
            //     type: 'get',
            //     url:'/settings/company-master/checkName',
            //     data : {
            //         entity : 'check-name-edit',
            //         entity_val : id,
            //         entity_data : check_id
            //     },
            //     success: function(res){
            //         console.log(1);
            //         if(res != 0) {
            //             console.log($('.cm_name').parent());
            //             $('.cm_name').parent().addClass('has-error');
            //             var p = "<p class = 'help-block error'>Name already exists</p>";
            //             $('.cm_name').siblings('p').remove();
            //             $('.cm_name').parent().append(p);
            //             $('#save_continue').prop('disabled', true);
            //             $('#save_return').prop('disabled', true);
            //             } else {
            //                 $('.cm_name').parent().removeClass('has-error');
            //                 $('.cm_name').siblings('p').remove();
            //                 $('#save_continue').prop('disabled', false);
            //                 $('#save_return').prop('disabled', false);
            //             }

            //         }       
            // });
            }
            
        });

    $(document).on('click', '.add_bank_row', function() {
        if($('.bank_row:eq(0)').hide())
            $('.bank_row:eq(0)').show();
        var edit = $('.onedit').val();
        var row = $('.bank_row:eq(0)').clone();
        var time = new Date().getTime();
        var checkflag = 0;
        $('.row:last', function() {
            if($('.bank_row:last').find(".cm_bank_acc_num").val() != ""){
                if($('.bank_row:last').find(".ifsc_code").val() != ""){
                    if($('.bank_row:last').find(".acc_holder_name").val() != ""){

                    }else{
                        checkflag = 1;
                    }
                }else{
                    checkflag = 1;
                }
            }else{
                checkflag = 1;
            }
        })
        if(checkflag == 0){ 
            row.find('input').each(function(){
                var data_name = $(this).attr('data-name');
                if($(this).hasClass('has_radio'))
                {
                    $(this).removeAttr('checked');
                }
                else
                {
                    $(this).val('');
                }

                $(this).attr('name', 'cm_bk['+time+']['+data_name+']');
                $(this).attr('value','');
            })
            row.find('.delete_row').removeClass('hide');
            $('.bank_row_parent').append(row);
        }else{
            $('.error').remove();
            $('.bank_row:last').parent().removeClass('has-error');
            $('.bank_row:last').addClass('has-error');
            var p = "<p class = 'help-block error'>Required</p>";
            $('.bank_row:last').siblings('p').remove();
            $('.bank_row:last').append(p);
        }
        if(edit == "edit") 
            $('.bank_row:eq(0)').hide();
    });
    $(document).on('change','.has_radio',function(){
        $('.has_radio').not(this).prop('checked', false);
    })
    $(document).on("click", ".delete_row", function() {
        $(this).closest('div').parent('div').remove();
        $(this).closest('div').remove();
    });

    $('#company_add').submit(function(evt) {
        console.log('calling');
        evt.preventDefault();
        var setting = $('.invoice_setting').val();
        var formData = new FormData(this);
        if($('.cm_name').val() == ""){
            $('.comp_name_err').html('Required');
            $('.cm_name').focus();
            return false;
        }else if($('.cm_name').val() == ""){
            $('.comp_name_err').html('');
            $('.comp_name_err').html('Required');
            return false;
        }
        else if($('.cm_contact_person').val() == ""){
            $('.comp_name_err').html('');
            $('.cont_per_err').html('Required');
            return false;
        }
        else if($('.cm_phone').val() == ""){
            $('.cont_per_err').html('');
            $('.cont_num_err').html('Required');
            return false;
        }
        else if($('.cm_email').val() == ""){
            $('.cont_num_err').html('');
            $('.comp_email_err').html('Required');
            return false;
        }
        else if($('.vendor_email').val() == ""){
            $('.cont_num_err').html('');
            $('.comp_email_err').html('Required');
            return false;
        }
        else if($('.cm_type  option:selected').val() == ""){
            $('.comp_email_err').html('');
            $('.comp_type_err').html('Required');
            return false;
        }
        else if($('.cm_currency  option:selected').val() == ""){
            $('.comp_type_err').html('');
            $('.comp_currency_err').html('Required');
            return false;
        }
        else{
            $('.comp_currency_err').html('');
            var formData = new FormData($(this)[0]);
            var file = document.getElementById("file").files;
            var documents = document.getElementById("documents").files;
            for(i=0;i<file.length;i++)
            {
                formData.append("file["+i+"]",file[i]);
            }
            formData.append("_token", $('[name="_token"]').val());
            for(i=0;i<documents.length;i++)
            {
                formData.append("documents["+i+"]",documents[i]);
            }
            var xhr = new XMLHttpRequest();
            xhr.open('POST', '/settings/company-master/storecompany');
            xhr.setRequestHeader("Accept", "multipart/form-data");
            xhr.onload = function (e) {
                  if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        data = JSON.parse(xhr.response);
                        console.log(xhr.response,data,'xhr');
                        var temp;
                        $('.company_list').append(data);
                        appendSelectData('.company_list', data);
                        $('#companyadd').data('modal', null);
                        $('#companyadd').modal('hide');
                        if(setting != undefined){
                            location.reload();
                        }
                    } else {
                      console.error(xhr.statusText);
                    }
                }
            };
            xhr.send(formData);
            // $.ajax({
            //     type: 'post',
            //     url:'/settings/company-master/storecompany',
            //     // data :$('#company_add').serialize(),
            //     data :formData,
            //     success: function(res){
            //         var temp;
            //         $.each(res, function(key, value) {
            //             temp = key;
            //             $('.company_list').append($("<option></option>").attr("value",key).text(value)).trigger('chosen:updated'); 
            //         });
            //         // $('.company_list option[value='+temp+']').prop('selected',true).trigger('chosen:updated');
            //         // $('#companyadd').data('modal', null);
            //         // $('#companyadd').modal('hide');
            //         console.log(res);
            //     },
            //     cache: false,
            //     contentType : false,
            //     processData: false       
            // });
        }
    })
    $(document).on('blur', '.vendor_email', function() {
        var id = $(this).val();
        if(id != "")
        {
            var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            if(!expr.test(id))
            {
            //console.log(id);
                $('.vendor_email').parent().addClass('has-error');
                var p = "<p class = 'help-block error'>Invalid Email</p>";
                $('.vendor_email').siblings('p').remove();
                $('.vendor_email').parent().append(p);
                $('#save_company').prop('disabled', true);
            }
            else {
                $('.vendor_email').parent().removeClass('has-error');
                $('.vendor_email').siblings('p').remove();
                $('#save_company').prop('disabled', false);
            }
        }
        else 
        {
            $('#save_company').prop('disabled', false);
        }
    });
    $(document).on('change','.cm_type',function(e){
        var val = $('.cm_type option:selected').val();
        console.log("close");
        if(val == 1){
            $('.bank_row_parent').parent().addClass('hide');
        }else{
            $('.bank_row_parent').parent().removeClass('hide');
        }
        var company_type = $(this).val();
            var id = $('.cm_email').val();
            // console.log(,"seff");
            // var check_id = parseInt(location.pathname.split('/')[3]);
            if(lang.screen_name == 'create'){
                $.ajax({
                    type: 'get',
                    url:'/settings/company-master/checkEmail',
                    data : {
                        entity : 'check-name',
                        entity_val : id,
                        type : company_type
                    },
                    success: function(res){
                        console.log(1);
                        if(res != 0) {
                            console.log($('.cm_email').parent());
                            $('.cm_email').parent().addClass('has-error');
                            var p = "<p class = 'help-block error'>Email already exists</p>";
                            $('.cm_email').siblings('p').remove();
                            $('.cm_email').parent().append(p);
                            $('#save_continue').prop('disabled', true);
                            $('#save_return').prop('disabled', true);
                            } else {
                                $('.cm_email').parent().removeClass('has-error');
                                $('.cm_email').siblings('p').remove();
                                $('#save_continue').prop('disabled', false);
                                $('#save_return').prop('disabled', false);
                            }

                        }       
                });
            }
            var id = $('.cm_name').val();
            var company_type = $('.cm_type').val();
            var check_id = parseInt(location.pathname.split('/')[3]);
            if(lang.screen_name == 'create'){
                $.ajax({
                type: 'get',
                url:'/settings/company-master/checkName',
                data : {
                    entity : 'check-name',
                    entity_val : id,
                    type : company_type
                },
                success: function(res){
                    console.log(1);
                    if(res != 0) {
                        console.log($('.cm_name').parent());
                        $('.cm_name').parent().addClass('has-error');
                        var p = "<p class = 'help-block error'>Name already exists</p>";
                        $('.cm_name').siblings('p').remove();
                        $('.cm_name').parent().append(p);
                        $('#save_continue').prop('disabled', true);
                        $('#save_return').prop('disabled', true);
                        } else {
                            $('.cm_name').parent().removeClass('has-error');
                            $('.cm_name').siblings('p').remove();
                            $('#save_continue').prop('disabled', false);
                            $('#save_return').prop('disabled', false);
                        }

                    }       
                });
            } 

    });
    /*End Of Dynamic Company End*/

    /*Start OF Dynamic Product Add*/
     $(document).on('change','.prod_docmodal',function(e){
        $('.gallerymodal').empty();
        imagesPreview(this, 'div.gallerymodal');
        console.log("modal");
       /* var ext = $(this).get(0).files[0].name;
        console.log(ext);*/
    });

      var imagesPreview = function(input, placeToInsertImagePreview) {
            //console.log(input);
            if (input.files) {
                var filesAmount = input.files.length;
                var count = 0;
                console.log(filesAmount,"fileamount");
                for (i = 0; i < filesAmount; i++) {
                    var name = $(input).get(0).files[i].name;
                    var ext = name.split('.');
                    console.log("exten",ext[1]);
                    console.log(placeToInsertImagePreview,"place");
                    if(placeToInsertImagePreview == "div.gallerymodal"){
                        $('.gallerymodal').append('<input type="hidden" id=files_namemodal'+i+' name=files_name[] value="'+name+'">');
                    }
                    else{
                        $('.gallerymodalcom').append('<input type="hidden" id=files_namemodal'+i+' name=files_name[] value="'+name+'">');   
                    }
                    console.log(i, "iiii")
                    if(ext[1] == "jpg" || ext[1] == "jpeg" || ext[1] == "png" || ext[1] == "gif")
                    {
                        console.log("here in if");
                        var reader = new FileReader();
                        reader.onload = function(event) {
                            //console.log(ext);
                            $($.parseHTML('<img class="imageview" data-file="image" width="75px" height="75px" style="margin :10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                            /*$($.parseHTML('<i class="fa fa-trash data_count='+count+' removeimage"><input type=hidden id=count name=count value='+count+'>')).appendTo(placeToInsertImagePreview);*/
                            $($.parseHTML('<input type=hidden id=file name=file'+count+' value='+event.target.result+'>')).appendTo(placeToInsertImagePreview);
                            $($.parseHTML('<i class="fa fa-trash removeimage" counter='+count+'>')).appendTo(placeToInsertImagePreview);
                            count = count + 1;
                        }
                    reader.readAsDataURL(input.files[i]);
                    }
                    else if(ext[1] == "pdf")
                    {
                        var reader = new FileReader();
                        reader.onload = function(event) {
                            //console.log(ext);
                            $($.parseHTML('<img class="hide" width="75px" height="75px" style="margin :10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                            var filename = event.target.result;
                             $($.parseHTML('<img class="imageview" data-file="notimage" width="75px" height="75px" style="margin :10px">')).attr('src', '/products/pdf.jpeg').appendTo(placeToInsertImagePreview);
                           /* $($.parseHTML('<i class="fa fa-trash data_count='+count+' removeimage"><input type=hidden id=count name=count value='+count+'>')).appendTo(placeToInsertImagePreview);*/
                            $($.parseHTML('<input type=hidden id=file name=file'+count+' value='+filename+'>')).appendTo(placeToInsertImagePreview);
                            $($.parseHTML('<i class="fa fa-trash removeimage" counter='+count+'>')).appendTo(placeToInsertImagePreview);
                            console.log("check_deatils",filename);
                            count = count + 1;
                        }
                    reader.readAsDataURL(input.files[i]);
                    }
                    
                }
            }

        };
    $(document).on('click','.removeimage',function(){
        var curr = $(this);
        $(curr).prev().prev().remove();
        $(curr).remove();
        var c = $(curr).children().val();
        console.log(c);
        var counter = $(this).attr('counter');
        $('#files_namemodal'+counter).remove();
    });
     /*$(document).on('click', '.imageview', function(){
            var image = $(this).attr('src');
            var file = $(this).next().val();
            var datafile = $(this).attr('data-file');
            console.log("file imageview",datafile);
            if(datafile == "image")
            {
                $('.gallerymodal').find('img').removeClass('hide');
                $('.gallerymodal').find('img').addClass('show');
                $('.gallerymodal').find('img').attr('src',image);
                $('.gallerymodal').find('iframe').removeClass('show');
                $('.gallerymodal').find('iframe').addClass('hide');
            }
            else
            {
                $('.gallerymodal').find('iframe').removeClass('hide');
                $('.gallerymodal').find('iframe').addClass('show');
                $('.gallerymodal').find('iframe').attr('src',file);
                $('.gallerymodal').find('img').removeClass('show');
                $('.gallerymodal').find('img').addClass('hide');
            }
            $('#document_modal').modal('show');
            console.log(image);
    });*/
     /*$(document).on('click','#closeinner',function(e){
        $('#document_modal').modal('hide');
     })
*/
    $(document).on('change','.file',function(e){
        // console.log(this.files[0].size);
        if(this.files[0].size > 5000000){
           alert("File Size Allowed Upto 5 Mb Only!");
           this.value = "";
        };
    });
    $(document).on('change', '.documents', function(e) {
        var files = e.target.files;
        $('.documents_names').empty();
        var name = $('.documents').get(0).files[0].name;
        var ext = name.split('.');
        console.log(ext[1]);
        if(ext[1] == "jpg" || ext[1] == "jpeg" || ext[1] == "png" || ext[1] == "gif")
        {
            $('.logo_error').html("");
            $('.logo_error').css('color','');
            readURL(this);
        }
        else
        {
            $('.logo_error').html("Only image files allowed");
            $('.logo_error').css('color','red');
            $('#verify_img').empty();
        }
        // if(names.length > 0)
        // {
        //     var file_list = document.createElement('UL');
        
        //     $(names).each(function(k,v){
        //         var remove_label = document.createElement('LABEL');
        //         remove_label.append("REMOVE");
        //         remove_label.setAttribute('style', 'color:blue; margin-left:15px');
                
        //         var file_name_label = document.createElement("LI");
        //         file_name_label.setAttribute('class', 'file_name_label');
        //         file_name_label.append(v);
        //         // file_name_label.append(remove_label);
        //         file_list.append(file_name_label);
        //     });
        //     $('.documents_names').append(file_list);
        // }
    });
    function readURL(input) {
        $('#verify_img').empty();
        if (input.files) {
            var filesAmount = input.files.length;
            for(i=0;i<filesAmount;i++){    
                var reader = new FileReader();
                reader.onload = function (e) {
                var y = document.createElement("div");
                y.setAttribute("id","image_div_doc");
                y.setAttribute("class","col-md-4 image_div");
                var x = document.createElement("IMG");
                x.setAttribute("src", e.target.result );
                x.setAttribute("width", "75px");
                x.setAttribute("height", "75px");
                x.setAttribute("style", " margin: 10px");
                x.setAttribute("alt", "Upload Your File Here");
                y.append(x);
                $('#verify_img').append(y);
                $('#image_div_doc').append('<i class="fa fa-trash removelogo"></i>');
                }   
                reader.readAsDataURL(input.files[i]);   
            }    
        }
    }
     $(document).on('click','.removelogo',function(){
        $(this).parent().empty();
        $('.logo').val('');
    });
    $(document).on('click', '.add_incentive_row', function() {
        var row = $('.incentive_row:eq(0)').clone();
        var time = new Date().getTime();
        row.find('input').each(function(){
            var from_val = 0;
            if($(this).hasClass('from'))
            {
                $(this).val(parseInt($('.to:last').val()) + 1);
            }
            else
            {
                $(this).val('');
            }
            var data_name = $(this).attr('data-name');
            $(this).attr('name', 'e_inc['+time+']['+data_name+']');
        })
        row.find('.delete_row').removeClass('hide');
        $('.incentive_row_parent').append(row);
    });

    $(document).on('change','.pm_subscription_flag',function(){
        if($(".pm_subscription_flag").prop('checked') == true){
            $('.issubsscription').removeClass('hide');
            $('.cbtn').prop('disabled',true);
        }else{
            $('.issubsscription').addClass('hide');
            $('.cbtn').prop('disabled',false);
        }
    });
    
    var specialKeys = new Array();
    specialKeys.push(8); //Backspace
    $(".numeric").bind("keypress", function (e) {

        var keyCode = e.which ? e.which : e.keyCode
        var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
        $(".error").css("display", ret ? "none" : "inline");
        return ret;
    });
    $("numeric").bind("paste", function (e) {        
        var element = this;
        setTimeout(function () {
            var tip = $(element).val();
                // REMOVE NON-NUMERIC CHARACTERS
            var cleanTip = tip.replace(/\D/g,'');
            $(element).val(cleanTip);
        }, 100);
    });
    var specialKeys = new Array();
    specialKeys.push(8); //Backspace
    $(".numeric_field").bind("keypress", function (e) {
        var keyCode = e.which ? e.which : e.keyCode
        var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
        $(".error").css("display", ret ? "none" : "inline");
        return ret;
    });
    $(".numeric_field").bind("paste", function (e) {
        var element = this;
        setTimeout(function () {
            var tip = $(element).val();
                // REMOVE NON-NUMERIC CHARACTERS
            var cleanTip = tip.replace(/\D/g,'');
            $(element).val(cleanTip);
        }, 100);
    });
    $(document).on('keyup', '.pm_item_name', function() {
        // console.log(1111);
        var id = $(this).val();
        $.ajax({
            type: 'get',
            url:'/settings/product-master/checkTitle',
            data : {
                entity : 'check-name',
                entity_val : id
            },
            success: function(res){
                // console.log(1);
                if(res != 0) {
                    // console.log($('.pm_item_name').parent());
                    $('.pm_item_name').parent().addClass('has-error');
                    var p = "<p class = 'help-block error'>Name already exists</p>";
                    $('.pm_item_name').siblings('p').remove();
                    $('.pm_item_name').parent().append(p);
                    $('#save_continue').prop('disabled', true);
                    $('#save_return').prop('disabled', true);
                    } else {
                        $('.pm_item_name').parent().removeClass('has-error');
                        $('.pm_item_name').siblings('p').remove();
                        $('#save_continue').prop('disabled', false);
                        $('#save_return').prop('disabled', false);
                    }

                }       
        });
    });
    

    $(document).on('change', '.pm_subscription', function() {
        var pm_subscription = parseInt($(".pm_subscription option:selected").val());
        // console.log(pm_subscription,'sada');
        if(pm_subscription = ''){
            $('.cbtn').prop('disabled',true);
        }else{
            $('.cbtn').prop('disabled',false);
        }
           
    }); 

    $(document).on('click', '.delete_document', function() {
        var id = $(this).attr('data-id');
        var del = $(this);
        bootbox.confirm("Are you sure you want to Delete This Document", function(result) {
            if (result) {

                $.ajax({
                    url : '/settings/product-master/delete-document',
                    type : 'get',
                    data : {
                        entity_type : "delete_document",
                        entity_id : id
                    },
                    success : function(res) {
                        // console.log($(this))
                        // $(del).parent().parent().remove();   
                    }
                })
            }
        })
    });  
    $(document).on('click', '.add_feature_row', function() {
        var row = $('.feature_row:eq(0)').clone();
        var time = new Date().getTime();
        var checkflag = 0;
        $('.row:last', function() {
            if($('.feature_row:last').find(".feature").val() != ""){
                
            }else{
                checkflag = 1;
            }
        })
        if(checkflag == 0){
            row.find('input').each(function(){
                var data_name = $(this).attr('data-name');
                $(this).val('');
                if($(this).hasClass('has_checkbox'))
                {
                   $(this).val('1');
                }
               
                $(this).attr('name', 'pf_bk['+time+']['+data_name+']');
                $(this).attr('value','');
            })
            row.find('.delete_row').removeClass('hide');
            $('.feature_row_parent').append(row);
              
        }else{
            $('.error').remove();
            $('.feature_row:last').parent().removeClass('has-error');
            $('.feature_row:last').addClass('has-error');
            var p = "<p class = 'help-block error'>Required</p>";
            $('.feature_row:last').siblings('p').remove();
            $('.feature_row:last').append(p);
        }
        
    });
    $(document).on("click", ".delete_row", function() {
        $(this).closest('div').parent('div').remove();
        $(this).closest('div').remove();
    });

    $(document).on("click","#close_pd",function(){
        $('.gallerymodal').empty();
        $('#productadd').modal('hide');
    })
    $(document).on("click","#close_cm",function(){
        $('.gallerymodalcom').empty();
        $('#companyadd').modal('hide');
    })
    
    $('#product_add').submit(function(evt) {
        evt.preventDefault();
        var pm_item_name = $('#pm_item_name').val();
        if(pm_item_name == ""){
            $('.product_err').html('Required');
            return false;
        }else{
            var product_data = document.getElementById("product_add");
            var formdata = new FormData(product_data);
                 /*$.ajax({
                  type: 'post',
                  url : '/settings/company-master/'+company_id+'/update-company-data',
                  data : $('#company_add').serialize(),
                  success: function(res){
                    console.log(res);
                    //location.reload();
                      
                  }       
              });*/
            var xhr = new XMLHttpRequest();
            xhr.open('POST', '/settings/product-master/storeproduct');
            xhr.setRequestHeader("Accept", "multipart/form-data");
            xhr.onload = function (e) {
                  if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        location.reload();
                    } else {
                      console.error(xhr.statusText);
                    }
                }
            };
            xhr.send(formdata);
            // $.ajax({
            //     type: 'post',
            //     url:'/settings/product-master/storeproduct',
            //     data :$('#product_add').serialize(),
            //     success: function(res){
            //         console.log(res);
            //         var temp;
            //         // appendSelectData('.inp_product',res,res);
            //         $.each(res, function(key, value) {
            //             temp = key;
            //             $('.inp_product').append($("<option></option>").attr("value",key).text(value)).trigger('chosen:updated'); 
            //         });
            //         // $('.inp_product option[value='+temp+']').prop('selected',true).trigger('chosen:updated');
            //         $('#productadd').data('modal', null);
            //         $('#productadd').modal('hide');
                    
            //     }       
            // });
        }
    })
});