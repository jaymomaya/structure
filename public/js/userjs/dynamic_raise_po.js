$(document).ready(function() {
        console.log(lang.po_no);
        $('#purchase_order_num').val(lang.po_no);
        chosenInit();
        $(document).on('click', '.add-item-row', function(){
            var row = $('.item_row:eq(0)').clone();
            var time = new Date().getTime();
            $(row).find('input').each(function(){
                var name = $(this).data('name');
                $(this).attr('name', 'item_details['+time+']['+name+']');
                $(this).val('');
            });
            $(row).find('select').each(function(k, v) {
                var n = $(v).attr('data-name');    
                $(v).attr('name', 'item_details['+time+']['+n+']');
                $(v).siblings('div').remove();
            });
            $(row).find('.delete_item_row').removeClass('hide');
            $('.item_parent_row').append(row);
            chosenInit();
        });

        $(document).on('change', '#inventory_type', function(){
            inventory_type = $(this).val();
            $.ajax({
                url: window.location.href,
                type: 'get',
                data: {
                    inventory_type: inventory_type
                },
                success: function(res){
                    $('#source').empty();
                    appendSelectData('#source', res);
                }
            })
        })

        $(document).on('click', '.delete_item_row', function(){
            $(this).parent().parent().remove();
            calculateTotalAmount();
            calculateTotlaQuantity();
        })

        $(document).on('change', '#finish_goods', function(){
            var name = $('#finish_goods').find('option:selected').text();
            names = name.split(' ');
            var new_name = '';
            $(names).each(function(k,v){
                new_name += v[0];
            })
            new_name = new_name.toUpperCase();
            $('.bom_name').val(new_name);
        })

        $(document).on('keyup blur', '.pod_quantity, .pod_price', function(){
            var row = $(this).parent().parent().parent();
            calculateRowAmount(row);
        })

        function calculateRowAmount(ele){
            var price = $(ele).find('.pod_price').val();
            var qty = $(ele).find('.pod_quantity').val();
            if(isNaN(price)){
                price = 0;
            }
            if(isNaN(qty)){
                qty = 0;
            }
            var total = qty * price;
            $(ele).find('.total_amount').val(total);
            calculateTotalAmount();
            calculateTotlaQuantity();
        }

        function calculateTotalAmount(){
            total_amount = 0;
            $('.total_amount').each(function(){
                total_amount += parseFloat($(this).val());
            });
           $('#total_amount').val(total_amount) 
        }
        function calculateTotlaQuantity(){
            total_quantity = 0;
            $('.pod_quantity').each(function(){
                total_quantity += parseFloat($(this).val());
            })
            $('#total_quantity').val(total_quantity) 
        }

        $(document).on('click', '#save_company', function(){
            var form_data = $("#raise_po");
            var form_data = form_data.serialize();
            $.ajax({
                type: 'post',
                url: window.location.href,
                data: form_data,
                success: function(res){
                    location.reload();
                }                
            })
        })
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        $(".numeric").bind("keypress", function (e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
            $(".error").css("display", ret ? "none" : "inline");
            return ret;
        });
        $(".numeric").bind("paste", function (e) {
            var element = this;
            setTimeout(function () {
                var tip = $(element).val();
                    // REMOVE NON-NUMERIC CHARACTERS
                var cleanTip = tip.replace(/\D/g,'');
                $(element).val(cleanTip);
            }, 100);
        });

        $(document).on('change', '.item_select', function(){
            var id = $(this).val();
            var ele = $(this);
            showLoading();
            $.ajax({
                type: 'get',
                url:'/api/get-cost-sheet',
                data: {
                    entity_id: id,
                },
                success: function(res){
                    if(res && res[0] != undefined && res[0]['igm_cost_sheet'] != undefined && res[0]['igm_cost_sheet'] != null && res[0]['igm_cost_sheet'] != 0){
                        $(ele).parent().parent().parent().find('.pod_price').attr('max', res[0]['igm_cost_sheet']);
                    } else {
                        $(ele).parent().parent().parent().find('.pod_price').removeAttr('max');
                    }
                    stopLoading();
                }       
            });
        })
    });