$(document).ready(function() {
        $(document).on('keyup','.tax_info',function(){
            this.value = this.value.toUpperCase();
        })
        // dateInit('datepicker');
        //$('#timepicker').mdtimepicker();
        /*$( ".datepicker" ).datepicker();
        $('.datepicker').datepicker({ 
            format: 'dd-mm-yyyy' ,
            autoclose:true,
            minDate: "today" 
        });*/
        chosenInit('chosen-select');          

        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        $(".numeric_field").bind("keypress", function (e) {
            console.log("here");     
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
            $(".error").css("display", ret ? "none" : "inline");
            return ret;
        });
        /*$(".numeric_field").bind({paste : function(){  
            console.log("paste");
            }
        });*/
        $(".numeric_field").bind("paste", function (e) {   
            var element = this;
            setTimeout(function () {
                var tip = $(element).val();
                    // REMOVE NON-NUMERIC CHARACTERS
                var cleanTip = tip.replace(/\D/g,'');
                $(element).val(cleanTip);
            }, 100);
        });
        

        $(document).on('change', '.lc_response', function(){
            var response = $(this).val()
            if(response != '' && response == 1)
            {
                // $('.appnt_time').val('').addClass('required');
                // $('.appnt_date').val('').addClass('required');
                $('.sales_person').parent().removeClass('hide');
                $('.sales_person').val('').addClass('required').trigger('chosen:updated');
                $('.appnt_time').parent().parent().removeClass('hide');
                $('.appnt_date').parent().parent().removeClass('hide');
                $('.follow_up_date').val('');
                $('.follow_up_date').removeClass('required');
                $('.follow_up_date').parent().parent().addClass('hide');
            }
            else
            {
                // $('.appnt_time').val('').removeClass('required');
                // $('.appnt_date').val('').removeClass('required');
                $('.follow_up_date').val('');
                $('.follow_up_date').parent().parent().removeClass('hide');
                $('.appnt_time').parent().parent().addClass('hide');
                $('.follow_up_date').addClass('required');
                $('.appnt_date').parent().parent().addClass('hide');
                $('.sales_person').parent().addClass('hide');
                $('.sales_person').val('').removeClass('required').trigger('chosen:updated');
            }
        });

        $(document).on('keyup', '.l_name', function() {
            var id = $(this).val();
            var check_id = parseInt(location.pathname.split('/')[3]);
            if(screen== 'create'){ 
                if(id != ''){
                $.ajax({
                        type: 'get',
                        url:'/transaction/leads/checkName',
                        data : {
                            entity : 'check-name',
                            entity_val : id
                        },
                        success: function(res){
                            
                            if(res != 0) {
                                
                                $('.l_name').parent().addClass('has-error');
                                var p = "<p class = 'help-block error'>Name already exists</p>";
                                $('.l_name').siblings('p').remove();
                                $('.l_name').parent().append(p);
                                $('#save_lead').prop('disabled', true);
                                $('#save_return').prop('disabled', true);
                                } else {
                                    $('.l_name').parent().removeClass('has-error');
                                    $('.l_name').siblings('p').remove();
                                    $('#save_lead').prop('disabled', false);
                                    $('#save_return').prop('disabled', false);
                                }

                            }       
                    });
                }
            }else{
                if(id != ''){
                $.ajax({
                        type: 'get',
                        url:'/transaction/leads/checkName',
                        data : {
                            entity : 'check-name-edit',
                            entity_val : id,
                            entity_data : check_id
                        },
                        success: function(res){
                            
                            if(res != 0) {
                                
                                $('.l_name').parent().addClass('has-error');
                                var p = "<p class = 'help-block error'>Name already exists</p>";
                                $('.l_name').siblings('p').remove();
                                $('.l_name').parent().append(p);
                                $('#save_lead').prop('disabled', true);
                                $('#save_return').prop('disabled', true);
                                } else {
                                    $('.l_name').parent().removeClass('has-error');
                                    $('.l_name').siblings('p').remove();
                                    $('#save_lead').prop('disabled', false);
                                    $('#save_return').prop('disabled', false);
                                }

                            }       
                    });
                }
            } 
            
        });

        $(document).on('keyup', '.l_mobile', function() {
            var id = $(this).val();
            var check_id = parseInt(location.pathname.split('/')[3]);
            if(screen== 'create'){
                if(id != ''){

                    $.ajax({
                        type: 'get',
                        url:'/transaction/leads/checkMobile',
                        data : {
                            entity : 'check-mobile',
                            entity_val : id
                        },
                        success: function(res){
                            
                            if(res != 0) {
                                
                                $('.l_mobile').parent().addClass('has-error');
                                var p = "<p class = 'help-block error'>Mobile already exists</p>";
                                $('.l_mobile').siblings('p').remove();
                                $('.l_mobile').parent().append(p);
                                $('#save_lead').prop('disabled', true);
                                $('#save_return').prop('disabled', true);
                                } else {
                                    $('.l_mobile').parent().removeClass('has-error');
                                    $('.l_mobile').siblings('p').remove();
                                    $('#save_lead').prop('disabled', false);
                                    $('#save_return').prop('disabled', false);
                                }

                            }       
                    });
                }
            }else{
                if(id != ''){

                    $.ajax({
                        type: 'get',
                        url:'/transaction/leads/checkMobile',
                        data : {
                            entity : 'check-mobile-edit',
                            entity_val : id,
                            entity_data : check_id
                        },
                        success: function(res){
                            
                            if(res != 0) {
                                
                                $('.l_mobile').parent().addClass('has-error');
                                var p = "<p class = 'help-block error'>Mobile already exists</p>";
                                $('.l_mobile').siblings('p').remove();
                                $('.l_mobile').parent().append(p);
                                $('#save_lead').prop('disabled', true);
                                $('#save_return').prop('disabled', true);
                                } else {
                                    $('.l_mobile').parent().removeClass('has-error');
                                    $('.l_mobile').siblings('p').remove();
                                    $('#save_lead').prop('disabled', false);
                                    $('#save_return').prop('disabled', false);
                                }

                            }       
                    });
                }
            }
            
        });
        $(document).on('keyup', '.l_email', function() {
            var id = $(this).val();
            var check_id = parseInt(location.pathname.split('/')[3]);
            if(screen== 'create'){
                if(id != '' ){
                    $.ajax({
                        type: 'get',
                        url:'/transaction/leads/checkEmail',
                        data : {
                            entity : 'check-email',
                            entity_val : id
                        },
                        success: function(res){
                            
                            if(res != 0) {
                                
                                $('.l_email').parent().addClass('has-error');
                                var p = "<p class = 'help-block error'>Email already exists</p>";
                                $('.l_email').siblings('p').remove();
                                $('.l_email').parent().append(p);
                                $('#save_lead').prop('disabled', true);
                                $('#save_return').prop('disabled', true);
                                } else {
                                    $('.l_email').parent().removeClass('has-error');
                                    $('.l_email').siblings('p').remove();
                                    $('#save_lead').prop('disabled', false);
                                    $('#save_return').prop('disabled', false);
                                }

                            }       
                    });
                } 
            }else{
                if(id != '' ){
                    $.ajax({
                        type: 'get',
                        url:'/transaction/leads/checkEmail',
                        data : {
                            entity : 'check-email-edit',
                            entity_val : id,
                            entity_data : check_id
                        },
                        success: function(res){
                            
                            if(res != 0) {
                                
                                $('.l_email').parent().addClass('has-error');
                                var p = "<p class = 'help-block error'>Email already exists</p>";
                                $('.l_email').siblings('p').remove();
                                $('.l_email').parent().append(p);
                                $('#save_lead').prop('disabled', true);
                                $('#save_return').prop('disabled', true);
                                } else {
                                    $('.l_email').parent().removeClass('has-error');
                                    $('.l_email').siblings('p').remove();
                                    $('#save_lead').prop('disabled', false);
                                    $('#save_return').prop('disabled', false);
                                }

                            }       
                    });
                } 
            } 
            
        });

        $(document).on('blur', '.l_email', function() {
                var id = $(this).val();
                if(id != "")
                {
                    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                    if(!expr.test(id))
                    {
                    //console.log(id);
                    $('.l_email').parent().addClass('has-error');
                    var p = "<p class = 'help-block error'>Invalid Email</p>";
                    $('.l_email').siblings('p').remove();
                    $('.l_email').parent().append(p);
                    $('#save_lead').prop('disabled', true);
                    $('#save_return').prop('disabled', true);
                    }
                     else {
                        $('.l_email').parent().removeClass('has-error');
                        $('.l_email').siblings('p').remove();
                        $('#save_lead').prop('disabled', false);
                        $('#save_return').prop('disabled', false);
                    }
                }
        });

        $(document).on('blur','.l_alt_email',function(){    //yash jethva
            var emails=$(this).val();
            if(emails != "")
            {
                var temp = new Array();
                temp = emails.split(",");
                for (a in temp ) {
                    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                    console.log($.trim(temp[a]));
                      if(!expr.test($.trim(temp[a])))
                      {
                        //console.log(id);
                        $('.l_alt_email').parent().addClass('has-error');
                        var p = "<p class = 'help-block error'>Invalid Emails</p>";
                        $('.l_alt_email').siblings('p').remove();
                        $('.l_alt_email').parent().append(p);
                        $('#save_lead').prop('disabled', true);
                        $('#save_return').prop('disabled', true);
                        }
                         else {
                            $('.l_alt_email').parent().removeClass('has-error');
                            $('.l_alt_email').siblings('p').remove();
                            $('#save_lead').prop('disabled', false);
                            $('#save_return').prop('disabled', false);
                        }
                }
            }
                //console.log(emails);
        });
        
       $(document).on('change', '.country_list', function(){
        var country = $(this).val();
        console.log(country);
        if(country != '101')
        {
            console.log('here');
            $('.state_list').val('0');
            $('#l_state').val('0');
            $('.state_list').prop('disabled', true).trigger('chosen:updated');
        }
       });
        
        $('#lead_add').submit(function(evt) {
        evt.preventDefault();
        var lname = $('.l_name').val();
        var lmobile = $('.l_mobile').val();
        var lemail = $('.l_email').val();
        if(lname == ""){
            $('.l_name').css('border-color','red');
            $('.errorname').html("required");
            $('.errorname').css('color','red');
            return false;
        }
        else if(lmobile == ""){
            $('.l_mobile').css('border-color','red');
            $('.errormobile').html("required");
            $('.errormobile').css('color','red');
            return false;   
        }
        else if(lemail == ""){
            $('.l_email').css('border-color','red');
            $('.erroremail').html("required");
            $('.erroremail').css('color','red');
            return false;   
        }
        else{
            $('.l_name').css('border-color','');
            $('.errorname').html("");
            $('.errorname').css('color','');
            $('.l_mobile').css('border-color','');
            $('.errormobile').html("");
            $('.errormobile').css('color','');
            $('.l_email').css('border-color','');
            $('.erroremail').html("");
            $('.erroremail').css('color','');
            $.ajax({
                type: 'post',
                url:'/transaction/leads/store_leadmodal',
                data :$('#lead_add').serialize(),
                success: function(res){
                    console.log(res);
                    //$('#lead_allocation_modal').modal('show');
                    $('#leads_modal').data('modal',null);
                    $('#leads_modal').modal('hide');
                    //$('.select_leads').append('<option value=1>yash</option>');
                    /*var drop = $('.select_leads').val()
                    console.log(drop);*/
                    var temp;
                    // appendSelectData('.inp_product',res,res);
                    $.each(res, function(key, value) {
                        temp = key;
                        $('.lead_add').append($("<option></option>").attr("value",key).text(value)).trigger('chosen:updated'); 
                    });
                    $('.lead_add option[value='+temp+']').prop('selected',true).trigger('chosen:updated');
                    // $('#productadd').data('modal', null);
                    // $('#productadd').modal('hide');
                    
                }       
            });
        }
    })
   });