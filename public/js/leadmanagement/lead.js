$(document).ready(function () {
    
            
    selectToggle();

    chosenInit('Select Event', '11em');

    var multi_select_obj = {
        elem_name : '#lead_attendance_select',
        selectableHeader : {
            class : 'attending_search'
        },
        selectionHeader : {
            class : 'presenter_search'
        },
        selectableFooter : {
            class : '',
            name : 'Attending'
        },
        selectionFooter : {
            class : '',
            name : 'Present'
        }
    }

    multiSelect(multi_select_obj);

    var datatable_object = {
        table_name : 'lead',
        order : {
            column : 2,
            mode : "asc"
        },
        sort_disabled_targets : [0,1],
    }

    var datatable = datatableInitWithButtons(datatable_object);

    $(document).on('click', '.delete', function() {
        var curr_url = $(this).data('url');
        var row = $(this);
 
        bootbox.confirm("Are you sure, you want to delete the record? record once deleted cannot be recovered.", function(result) {
    
            if (result) {
                datatable.row( $(row).parents('tr') ).remove().draw();
                // bootBoxAlert("Venue Successfully Deleted.");
                // $.ajax({
                //     url: curr_url,
                //     data: { _token: $('meta[name="csrf-token"]').attr('content') },
                //     type: 'DELETE',
                //     success: function(result) {
                //     } 
                // });
            }
        }); 
    });

    $(document).on('click', '.delete_family', function() {
        bootbox.confirm("Are you sure, you want to delete the venue? venue once deleted cannot be recovered.", function(result) {
    
            if (result) {
                console.log("deleted");
            }
        }); 
    })
})