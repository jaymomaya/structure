$(document).ready(function () {

    selectToggle();
    chosenInit('Select Fees Type', '11em', false);

    var datatable_object = {
        table_name : 'without_amc_panel',
        order : {
            column : 3,
            mode : "asc"
        },
        sort_disabled_targets : [0,1]
    }

    var table = datatableInitWithButtons(datatable_object);

    $(document).on('change', '.chosen-select', function() {
        var datatable_name = $(this).val();
        
        table.destroy();
        
        $('table').addClass('hide');
        datatable_object.table_name = datatable_name+'_panel'
        table = datatableInitWithButtons(datatable_object);
        $('#'+datatable_name+'_panel').removeClass('hide');
    });

    $(document).on('click', '.delete', function() {
        var curr_delete_id = $(this).data('id');
        var curr_url = 'tapasvi/'+curr_delete_id;
        var row = $(this);
 
        bootbox.confirm("Are you sure, you want to delete the venue? venue once deleted cannot be recovered.", function(result) {
    
            if (result) {
                datatable.row( $(row).parents('tr') ).remove().draw();
                // bootBoxAlert("Venue Successfully Deleted.");
                // $.ajax({
                //     url: curr_url,
                //     data: { _token: $('meta[name="csrf-token"]').attr('content') },
                //     type: 'DELETE',
                //     success: function(result) {
                //     } 
                // });
            }
        }); 
    });

    $(document).on('click', '.delete_family', function() {
        bootbox.confirm("Are you sure, you want to delete the venue? venue once deleted cannot be recovered.", function(result) {
    
            if (result) {
                console.log("deleted");
            }
        }); 
    })

    $(document).on('change', '.select_all', function() {
        if ($('thead tr').find('input').is(':checked')) {
            $('tbody tr').find('input').prop('checked', true);
        } else {
            $('tbody tr').find('input').prop('checked', false);
        }
    })
})