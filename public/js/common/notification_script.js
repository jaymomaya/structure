

var timer = 2000;


function notification(msg, layout, effect, type, ttl) {
    // create the notification
    $('.ns-close').trigger('click'); //disable for multiple notification
    var notification = new NotificationFx({

      // element to which the notification will be appended
      // defaults to the document.body
      // wrapper : document.body,
      // wrapper : document.getElementById('subject_preview'),

      // the message
      message : msg,

      // layout type: growl|attached|bar|other
      layout : layout || 'other',

      // effects for the specified layout:
      // for growl layout: scale|slide|genie|jelly
      // for attached layout: flip|bouncyflip
      // for other layout: boxspinner|cornerexpand|loadingcircle|thumbslider
      // ...
      effect : effect || 'cornerexpand',

      // notice, warning, error, success
      // will add class ns-type-warning, ns-type-error or ns-type-success
      type : type || 'notice',

      // if the user doesn´t close the notification then we remove it 
      // after the following time
      ttl : ttl || 2000,

      // callbacks
      onClose : function() { return false; },
      onOpen : function() { return false; }

    });

    // show the notification
    notification.show();
}