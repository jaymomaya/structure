var global_counter = 0;
var global_page_counter = 0;

function pluploadFrontInit() {
    f_uploader = new plupload.Uploader({
        runtimes : 'html5,flash,silverlight,html4',
        browse_button : 'fileupload_front', // you can pass an id...
        // container: $('.subject_image_handler'), // ... or DOM Element itself
        url : 'subject/upload/front',
        flash_swf_url : '../js/plupload/Moxie.swf',
        silverlight_xap_url : '../js/plupload/Moxie.xap',
        
        
        
        
        filters : {
            max_file_size : '50mb',
            mime_types: [
                {title : "Image files", extensions : "jpg,gif,png"},
            ]
        },

        chunk_size: '2mb',

        multipart_params   : {
            _token: $('meta[name="csrf_token"]').attr('content')
        },

        buttons : {
            browse : true,
            start : false,
            stop : false
        },

        views: {
            list: true,
            thumbs: true, // Show thumbs
            active: 'thumbs'
        },

        // Rename files by clicking on their titles
        rename: true,
        multiple_queues : true,
        
        // Sort files
        sortable: true,

        preinit : {
            Init: function(up, info) {
                // console.log('[Init]', 'Info:', info, 'Features:', up.features);
                $('.subject_image_handler').sortable();
                
            },
 
            UploadFile: function(up, file) {
               // console.log('[UploadFile]', file);
 
                // You can override settings before the file is uploaded
                // up.setOption('url', 'upload.php?id=' + file.id);
                // up.setOption('multipart_params', {param1 : 'value1', param2 : 'value2'});
            }
        },

        init: {
            PostInit: function() {
                
                // document.getElementById('filelist').innerHTML = '';

                // document.getElementById('uploadfiles').onclick = function() {
                //     f_uploader.start();
                //     return false;
                // };
            },

            FilesAdded: function(up, files) {
                counter = 0;
                startLoader('sk-circle','UpLoading...');
                var obj = {};
                
                plupload.each(files, function(file, i) {
                   
                    var dvGen = $('.subject_image_demo').html();

                    var img = new mOxie.Image();
                    var img_o = document.createElement("IMG");

                    img.onload = function() {

                        // create a thumb placeholder
                        
                        img_o.id = file.id;
                        img_o.src = this.getAsDataURL();
                        $(img_o).attr('class', "add_subject_image margin head_subject_image width_height_100 lazyload img-responsive");
                        $(img_o).attr('data-level-val', "front");
                        $(img_o).attr('data-visibility-val', 1);
                        $(img_o).attr('data-media-name', file.name);
                        $(img_o).attr('data-media-width', img.width);
                        $(img_o).attr('data-media-height', img.height);
                        $(img_o).attr('data-media-type', file.type);
                        $(img_o).attr('data-media-size', Math.round(file.size/1024) +'KB');
                        $(img_o).attr('data-file_id', file.id);
                        // if ($('.subject_image_handler img').length > up.files.length) {
                        //     $(img_o).attr('data-sort-id', $('.subject_image_handler img').length + up.files.indexOf(file) + 1);
                        // } else {
                        if ($('#subject_select').val() == "add_new") {
                            
                            global_counter ++;
                            $(img_o).attr('data-sort-id', global_counter);
                        } else {
                            global_counter ++;
                            $(img_o).attr('data-sort-id', global_counter);

                        }
                        // }
                        console.log($(img_o).html());
                        dvPreview = $(dvGen).find('.thumbnail');
                        $(img_o).prependTo($(dvPreview));

                        
                        $(dvPreview).find('.book').find('i')
                        .addClass("fa fa-book")
                        .addClass('public_icon_color')
                        .attr('title','Album front for public users.');

                        

                        CounterInc("front")

                        var dvPre = $(dvPreview).parent()
                        $('.subject_image_handler').append($(dvPre));
                        
                        $('.dropdown_image_menu').smartmenus();
                       

                        $('.subject_image_handler').imagesLoaded( function() {

                            // $('.add_subject_image').parent().prepend($('.add_subject_image'))
                            stopLoader();
                            sortImage();


                        });
                        // if (up.files.length == counter) {
                        //     stopLoader();
                        //     sortImage();

                        // }

                    };

                    img.load(file.getSource())
                    

                });

                function sortImage() {
                    
                    tinysort(".subject_image_handler img","",{attr:"data-sort-id"})
                    counter = 0;
                }

                // global_counter += up.files.length;

            },
            
            
            UploadProgress: function(up, file) {
               

                // document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
            },
            UploadComplete: function(up, files) {
                // Called when all files are either uploaded or failed
                
                stopLoader();
                global_counter = 0;
                // notification(lang.chapter_operation_steps, 'growl', 'genie', 'notice', 15000);

            },


            Error: function(error, err) {
                if (err.code == "-601") {
                
                    notification( lang.front_back_upload_error, 'bar', 'slidetop', 'notice', 6000);
                } else if (err.code == "-602") {
                    notification( err.message, 'bar', 'slidetop', 'notice', timer);

                } else if (err.code == "-600") {
                    notification( "Only "+lang.front_max_file_size+" of uploadation is permitted.", 'bar', 'slidetop', 'notice', timer);

                } else if (err.code == "-200") {
                    notification( err.message, 'bar', 'slidetop', 'notice', timer);

                } else {
                    notification( lang.went_wrong_title, 'bar', 'slidetop', 'notice', timer);
                }
                stopLoader();
            },
        }
    });

    f_uploader.init();
}

function pluploadBackInit() {
    b_uploader = new plupload.Uploader({
        runtimes : 'html5,flash,silverlight,html4',
        browse_button : 'fileupload_back', // you can pass an id...
        // container: $('.subject_image_handler'), // ... or DOM Element itself
        url : 'subject/upload/back',
        flash_swf_url : '../js/plupload/Moxie.swf',
        silverlight_xap_url : '../js/plupload/Moxie.xap',
        drop_element : 'subject_image_handler',
        
        filters : {
            max_file_size : '10mb',
            mime_types: [
                {title : "Image files", extensions : "jpg,gif,png"},
            ]
        },

        chunk_size: '2mb',

        multipart_params   : {
            _token: $('meta[name="csrf_token"]').attr('content')
        },

        buttons : {
            browse : true,
            start : false,
            stop : false
        },

        views: {
            list: true,
            thumbs: true, // Show thumbs
            active: 'thumbs'
        },

        // Rename files by clicking on their titles
        rename: true,
        
        // Sort files
        sortable: true,

        // Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
        dragdrop: true,

        preinit : {
            Init: function(up, info) {
                $('.subject_image_handler').sortable();
                // console.log('[Init]', 'Info:', info, 'Features:', up.features);
            },
 
            UploadFile: function(up, file) {
                // console.log('[UploadFile]', file);
 
                // You can override settings before the file is uploaded
                // up.setOption('url', 'upload.php?id=' + file.id);
                // up.setOption('multipart_params', {param1 : 'value1', param2 : 'value2'});
            }
        },

        init: {
            PostInit: function() {
                // console.log("PostInit");
                // document.getElementById('filelist').innerHTML = '';

                // document.getElementById('uploadfiles').onclick = function() {
                //     uploader.start();
                //     return false;
                // };
            },

            FilesAdded: function(up, files) {
                startLoader('sk-circle','UpLoading...');
                counter = 0;
                plupload.each(files, function(file, i) {

                    var dvGen = $('.subject_image_demo').html();
                    var img = new o.Image();

                    img.onload = function() {
                        // create a thumb placeholder
                        var img_o = document.createElement("IMG");
                        
                        img_o.id = this.uid;
                        img_o.src = this.getAsDataURL();
                        $(img_o).attr('class', "add_subject_image margin head_subject_image width_height_100");
                        $(img_o).attr('data-level-val', "back");
                        $(img_o).attr('data-visibility-val', 1);
                        $(img_o).attr('data-media-name', file.name);
                        $(img_o).attr('data-media-width', img.width);
                        $(img_o).attr('data-media-height', img.height);
                        $(img_o).attr('data-media-type', file.type);
                        $(img_o).attr('data-media-size', Math.round(file.size/1024) +'KB');
                        $(img_o).attr('data-file_id', file.id);

                        if ($('#subject_select').val() == "add_new") {
                            console.log(global_counter);
                            global_counter ++;
                            $(img_o).attr('data-sort-id', global_counter);
                        } else {
                            global_counter ++;
                            $(img_o).attr('data-sort-id', global_counter);

                        }
                        
                        
                        
                        dvPreview = $(dvGen).find('.thumbnail');
                        $(dvPreview).prepend(img_o);
                        
                        $(dvPreview).find('.book').find('i')
                        .addClass("fa fa-book fa-flip-horizontal")
                        .addClass('public_icon_color')
                        .attr('title','Album back for public users.');

                        CounterInc("back")

                        $('.subject_image_handler').append($(dvPreview).parent());
                        $('.dropdown_image_menu').smartmenus();
                        counter++;
                        $('.subject_image_handler').imagesLoaded( function() {

                            stopLoader();
                            sortImage();
                        });
                    };
                    
                    img.load(file.getSource());    

                });

                function sortImage(){
                    tinysort(".subject_image_handler img","",{attr:"data-sort-id"})
                    counter = 0;
                }

                
            },

            UploadProgress: function(up, file) {
                // document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
            },
            UploadComplete: function(up, files) {
                // Called when all files are either uploaded or failed
                
                stopLoader();
                global_counter = 0;


            },
            Error: function(error, err) {
                if (err.code == "-601") {
                    notification( lang.front_back_upload_error, 'bar', 'slidetop', 'notice', 6000);
                } else if (err.code == "-602") {
                    notification( err.message, 'bar', 'slidetop', 'notice', timer);

                } else if (err.code == "-600") {
                    notification( "Only "+lang.back_max_file_size+" of uploadation is permitted.", 'bar', 'slidetop', 'notice', timer);

                } else if (err.code == "-200") {
                    notification( err.message, 'bar', 'slidetop', 'notice', timer);

                } else {
                    notification( lang.went_wrong_title, 'bar', 'slidetop', 'notice', timer);
                }
                stopLoader();
            },
        }
    });

    b_uploader.init();
}

function pluploadPageInit() {
    p_uploader = new plupload.Uploader({
        runtimes : 'html5,flash,silverlight,html4',
        browse_button : 'fileupload_page', // you can pass an id...
        // container: $('.subject_image_handler'), // ... or DOM Element itself
        url : 'page/upload',
        flash_swf_url : '../js/plupload/Moxie.swf',
        silverlight_xap_url : '../js/plupload/Moxie.xap',
        drop_element : 'subject_image_handler',
        
        filters : {
            max_file_size : '10mb',
            mime_types: [
                {title : "Image files", extensions : "jpg,gif,png"},
                { title: "Video files", extensions:  "mp4, mov" }
            ]
        },

        chunk_size: '2mb',

        multipart_params   : {
            _token: $('meta[name="csrf_token"]').attr('content')
        },

        buttons : {
            browse : true,
            start : false,
            stop : false
        },

        views: {
            list: true,
            thumbs: true, // Show thumbs
            active: 'thumbs'
        },

        // Rename files by clicking on their titles
        rename: true,
        
        // Sort files
        sortable: true,

        // Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
        dragdrop: true,

        preinit : {
            Init: function(up, info) {
                $('.page_image_handler').sortable();
                // console.log('[Init]', 'Info:', info, 'Features:', up.features);
            },
 
            UploadFile: function(up, file) {
                // console.log('[UploadFile]', file);
 
                // You can override settings before the file is uploaded
                // up.setOption('url', 'upload.php?id=' + file.id);
                // up.setOption('multipart_params', {param1 : 'value1', param2 : 'value2'});
            }
        },

        init: {
            PostInit: function() {
                // console.log("PostInit");
                // document.getElementById('filelist').innerHTML = '';

                // document.getElementById('uploadfiles').onclick = function() {
                //     uploader.start();
                //     return false;
                // };
            },

            FilesAdded: function(up, files) {
                startLoader('sk-circle','UpLoading...');
                counter = 0;
                plupload.each(files, function(file, i) {

                    var dvGen = $('.page_image_demo').html();
                    var img = new o.Image();
                    var fname = file.name;
                    var ext = fname.slice((fname.lastIndexOf(".") - 1 >>> 0) + 2);

                    if ((lang.valid_video_ext).indexOf(ext) > -1) {
                        
                            // create a thumb placeholder
                            var img_o = document.createElement("IMG");
                            
                            img_o.id = this.uid;
                            img_o.src = lang.url+"/img/default_video.png";
                            $(img_o).attr('class', "add_subject_image margin head_subject_image width_height_100");
                            $(img_o).attr('data-visibility-val', 1);
                            $(img_o).attr('data-media-name', file.name);
                            $(img_o).attr('data-media-width', img.width);
                            $(img_o).attr('data-media-height', img.height);
                            $(img_o).attr('data-media-type', file.type);
                            $(img_o).attr('data-media-size', Math.round(file.size/1024) +'KB');
                            $(img_o).attr('data-file_id', file.id);
                            
                            global_page_counter++;
                            $(img_o).attr('data-sort-id', global_page_counter);
                            
                            
                            dvPreview = $(dvGen).find('.thumbnail');
                            $(dvPreview).prepend(img_o);
                            
                            $(dvPreview).find('.visibility').find('i')
                            .addClass("fa fa-book fa-flip-horizontal")
                            .addClass('public_icon_color')
                            .attr('title','Album page images for public users.');
                            $('.page_image_handler').append($(dvPreview).parent());
                            $('.dropdown_image_menu').smartmenus();

                            counter++;
                            $('.page_image_handler').imagesLoaded( function() {

                                stopLoader();
                                sortImage();
                            });
                        

                    } else {
                        img.onload = function() {
                            // create a thumb placeholder
                            var img_o = document.createElement("IMG");
                            
                            img_o.id = this.uid;
                            img_o.src = this.getAsDataURL();
                            $(img_o).attr('class', "add_subject_image margin head_subject_image width_height_100");
                            $(img_o).attr('data-visibility-val', 1);
                            $(img_o).attr('data-media-name', file.name);
                            $(img_o).attr('data-media-width', img.width);
                            $(img_o).attr('data-media-height', img.height);
                            $(img_o).attr('data-media-type', file.type);
                            $(img_o).attr('data-media-size', Math.round(file.size/1024) +'KB');
                            $(img_o).attr('data-file_id', file.id);
                            global_page_counter++;

                            $(img_o).attr('data-sort-id', global_page_counter);

                            
                            
                            dvPreview = $(dvGen).find('.thumbnail');
                            $(dvPreview).prepend(img_o);
                            
                            $(dvPreview).find('.visibility').find('i')
                            .addClass("fa fa-book fa-flip-horizontal")
                            .addClass('public_icon_color')
                            .attr('title','Album page images for public users.');
                            $('.page_image_handler').append($(dvPreview).parent());
                            $('.dropdown_image_menu').smartmenus();

                            counter++;
                            $('.page_image_handler').imagesLoaded( function() {

                                stopLoader();
                                sortImage();
                            });
                        };
                        
                    }
                    
                    img.load(file.getSource());

                    function sortImage(){
                        tinysort(".page_image_handler img","",{attr:"data-sort-id"})
                        counter = 0;
                    }

                });
            },

            UploadProgress: function(up, file) {
                // document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
            },
            UploadComplete: function(up, files) {
                // Called when all files are either uploaded or failed
               
                stopLoader();
                global_page_counter = 0;
            },
            Error: function(error, err) {
                if (err.code == "-601") {
                console.log("back uploader");
                    notification( lang.front_back_upload_error, 'bar', 'slidetop', 'notice', 6000);
                } else if (err.code == "-602") {
                    notification( err.message, 'bar', 'slidetop', 'notice', timer);

                } else if (err.code == "-600") {
                    notification( "Only "+lang.page_max_file_size+" of uploadation is permitted.", 'bar', 'slidetop', 'notice', timer);

                } else if (err.code == "-200") {
                    notification( err.message, 'bar', 'slidetop', 'notice', timer);

                } else {
                    notification( lang.went_wrong_title, 'bar', 'slidetop', 'notice', timer);
                }
                stopLoader();
            },
        }
    });

    p_uploader.init();
}