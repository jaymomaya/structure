function datatableInitWithButtonsAndDynamicRev(obj) {
    
    // console.log("datatable")
    var datatableObj = {
        columnDefs: [{
            orderable: false, width: '1em', targets: obj.sort_disabled_targets,
        }],
            // autoWidth: false,
        colReorder: true,
        order: [],
        // fixedHeader: true,
        stateSave: true,
        stateDuration:-1,
        // lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        lengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],
        processing: true,
        serverSide: true,
        "scrollX": true,
        ajax: {
            ajax_url : obj.ajax_url,
            data : function(d) {
                d.designation = $('.designation').val(),
                d.event = $('.event').val()
                d.month = $('.select_month_filter').val()
                console.log(obj.ajax_data)
                if (obj.ajax_data && obj.ajax_data != "") {
                    $.each(obj.ajax_data, function(k, v) {
                        d[k] = v
                    })
                }
            }
        },
        "columns": obj.column_data,
        "drawCallback": function( settings ) {
            // var thead = $('#'+obj.table_name).find('tr:eq(0) th:eq(0)');
            var thead = $('.'+obj.table_name+':eq(0)').find('tr:eq(0) th:eq(0)');
            // console.log(thead, $('#'+obj.table_name).find('tr:eq(0) th:eq(0)').text(),'thead.html()')
            if (thead.text() == "All" || thead.text() == "all") {
                // $(thead).remove();
                $(thead).html(generateHeaderCheckbox());
            }
        }
    };

    
    if (obj.order.state) {
        datatableObj.order = [[ obj.order.column, obj.order.mode ]]
    }

    var button_dom = "";
    var buttons = [];
    
    
    if (obj.buttons.state) {
        button_dom = "<'row'<'col-sm-12 bottom-buffer'B>>";
        if (obj.buttons.colvis) {
            buttons[0] = 'colvis'
        }
        
        if (obj.buttons.excel.state && lang.user_type == 1) {
            buttons[1] = {

                extend: 'excelHtml5',
                // { extend: 'excelHtml5', 
                // customize: function(xlsx) { 
                //     var sheet = xlsx.xl.worksheets['sheet1.xml'];
                //     console.log(sheet);
                //     // Loop over the cells in column `F` 
                //     $('row c[r^="A"]', sheet).each( function () { 
                //         console.log(this);
                //     // Get the value and strip the non numeric characters .replace(/[^\d]/g, '')
                //         var text = $('is t', this).text();
                //         console.log('text front', $('is t', this).text(), 'text data', $('is t', this).text().replace(text, "'"+text)); 
                //         // if ( $('is t', this).text().replace(/[^\d]/g, '') * 1 >= 50000 ) { 
                //         //     $(this).attr( 's', '20' ); 
                //         // }
                //         $('is t', this).text().replace(text, "'"+text)
                //     }); 
                // },
                exportOptions: {
                    //to remove first column from export
                    // columns: function ( idx, data, node ) {
                    //     var action_col = $('thead th').index($('.thead_action'));
                    //     var isVisible = table.column( idx ).visible();
                    //    console.log(isVisible)
                    //     var isNotForExport = $.inArray( idx, action_col ) !== -1;
                       
                    //     return isVisible && !isNotForExport ? true : false;
                    // },
                    columns: obj.buttons.excel.columns,
                    // exclude: [ 0 ],
                    format: {
                        body: function ( data, column, row, node ) {
                            if ($(node).length) {
                                var status_col = $('thead th').index($('.thead_status'));
                                var action_col = $('thead th').index($('.thead_action'));
                                // console.log(status_col, action_col,$(node)[0].cellIndex)
                                if ($(node)[0].cellIndex == status_col) {
                                    return $($(node)).find('.label').text();
                                } else if ($(node)[0].cellIndex == action_col) {
                                    return ;
                                } else {
                                    if (typeof (data) == "number") {
                                        return data;
                                    } else {
                                        // console.log(data,'data');
                                        //added another replace to avoid execution of csv injection --AG 030817
                                        return data.replace(/<\/?[^>]+(>|$)/g, "").replace(data, "'" + data).replace(/&amp;/g, '&');
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if (obj.buttons.pdf.state && lang.user_type == 1) {
            buttons[2] = {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: obj.buttons.pdf.columns,
                    
                    format: {
                        body: function ( data, column, row, node ) {
                            if ($(node).length) {
                                var status_col = $('thead th').index($('.thead_status'));
                                var action_col = $('thead th').index($('.thead_action'));
                                // console.log(status_col, action_col,$(node)[0].cellIndex)
                                if ($(node)[0].cellIndex == status_col) {
                                    return $($(node)).find('.label').text();
                                } else if ($(node)[0].cellIndex == action_col) {
                                    return ;
                                } else {
                                    if (typeof (data) == "number") {
                                        return data
                                    } else {
                                        return data.replace(/<\/?[^>]+(>|$)/g, "");
                                    }
                                }
                            }
                            // return data;
                        }
                    }
                }
            }
        }
    }
    datatableObj.buttons = buttons;
    datatableObj.info = obj.info;
    datatableObj.paging = obj.paging;
    datatableObj.searching = obj.searching;
    datatableObj.ordering = obj.ordering;
    datatableObj.iDisplayLength = obj.iDisplayLength;

    datatableObj.dom = button_dom+
        "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
        "<'row'<'col-sm-12 bottom-buffer'tr>>" +
        "<'row'<'col-sm-5'i><'col-sm-7'p>>";


    var table = $('#'+obj.table_name).DataTable(datatableObj);
    
    datatableScroll();
    datatableFooter(obj, table);
    datatableLength();
    datatableAcl(table);

    return table;
}

function datatableAcl(table) {
    $.each(table.rows(':eq(0)').data(), function() {
       // console.log(this)
   });
}

function datatableScroll() {
    // $('.dataTable').wrap('<div class="dataTables_scroll" />');

    // $('.dataTables_scroll').slimscroll({
    //     alwaysVisible: true,
    //     railVisible: true,
    //     axis: 'x',
    // });
}

function datatableFooter(obj, table) {
    

    // $('#'+obj.table_name).closest('.dataTables_scroll').find('.dataTables_scrollHead thead th').each(function () {
    //     var title = $(this).text();
    //     $(this).not('.no-search').html('<input type="text" class = "form-control input-sm" placeholder="Search '+title+'" />');
    // });

    $('#'+obj.table_name).closest('.dataTables_scroll').find('.dataTables_scrollFoot tfoot th').each(function () {
        var title = $(this).text();
        $(this).not('.no-search').html('<input type="text" class = "form-control input-sm" placeholder="Search '+title+'" />');
    });

    var state = table.state.loaded();
    
    if (state) {
        table.columns().eq(0).each(function (colIdx) {
            var colSearch = state.columns[colIdx].search;
            if (colSearch.search) {
                $('input', table.column(colIdx).footer()).val(colSearch.search);
            }
        });
        table.draw();
    }

    table.columns().every(function() {
        var that = this;
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that.column( $(this).parent().index()).search( this.value ).draw();

            }
        });
    });
}

function datatableLength() {
    $('.dataTables_length select').addClass('chosen-lenght-select');
    chosenInit('', '5em', true, 'chosen-lenght-select');
}

function generateHeaderCheckbox() {

    var io = {};
    
    var l = $('<label />').html('&nbsp;All');

    io['type'] = "checkbox";
    io['name'] = "select_all";
    io['class'] = "styled select_all";

    var i = $('<input />', io);
    
    var d = $('<div />', {
        class : "checkbox checkbox-sharekhan"
    }).append(i).append(l);
    

    return $(d).get(0).outerHTML;
}

function datatableColumn(arr, action_obj = [], pk = "id") {
    var obj = [];
    arr.forEach(function(k) {
        if (k == "action" || k == "all" || k == "expand") {
            obj.push(action(action_obj, pk, k))
        } else if (k == "status") {
            // if(lang.update)
            obj.push(status(k))
        }else {
            obj.push(data(k))
        }
    });

    return obj;

    function action(action_obj, pk, view_type) {
        return {
            "data": "action",
            "searchable": false,

            "sortable": false,
            "render": function (data, type, full, meta) {  
                if (view_type == "action") {
                    return generateActionIcons(action_obj, full, pk);
                } 
                if (view_type == "event_operation") {
                    return generateEventRequestActionIcons(action_obj, full, pk);
                }
                if (view_type == "expand") {
                    return generateImageIcon(pk, full);
                }
                if (view_type == "all") {
                    return generateCheckbox(pk, full);
                }
                if (view_type == "leads_icons") {
                    return generateLeadsRequestActionIcons(action_obj, full, pk);
                }
                if(view_type == "invoice_icons"){
                    return generateInvoiceRequestActionIcons(action_obj,full,pk)
                }
            }
        }
    }

    function status(k) {
        return {
            "data": k,
            "searchable": true,

            "render": function (data, type, full, meta) {

                var class_name = "";
                var s = "";
                if (full.status) {
                    if (lang.status[full.status].SHOW) {
                        s += generateDropdown(lang.status[full.status].OPTION, '', '', 'status_dropdown hide');
                        class_name = "label_dropdown";
                    } else {
                        class_name = "";
                    }
                    s += '<span data-type = "'+k+'" data-url = "'+window.location.href+'/'+full[pk]+'" data-id = "'+full[pk]+'" class="label '+class_name+'" style="color:'+lang.status[full.status].COLOR+';cursor:pointer">'+lang.status[full.status].VALUE+'</span>';
                
                    convertLabelToDropdown()
                }
                return s;
            }
        }
    }

    function generateDropdown(data, selected_val, name_attr, class_attr = "chosen-select") {
        var s = $('<select />', {'data-placeholder' : "Select"});
        $('<option />', { value: "", text: "" }).appendTo(s);
        for (var val in data) {
            if (val == selected_val) {
                $('<option />', { value: val, text: data[val], selected:"selected" }).appendTo(s);
            } else {
                $('<option />', { value: val, text: data[val] }).appendTo(s);
            }
        }
        $(s).val(selected_val)
        $(s).attr('class', "chosen-select " + class_attr)
        $(s).attr('name', name_attr)
        $(s).appendTo('<div />')
        return $(s).parent().html();
        //s.appendTo('#ctl00_ContentPlaceHolder1_Table1');
    }

    function generateStatusLabelDepr(pk, full) {
        var class_name = "";
        var s = "";

        if (lang.status[full.status].SHOW) {
            s += generateDropdown(lang.status[full.status].OPTION, '', '', 'status_dropdown hide');
            class_name = "label_dropdown";
        } else {
            class_name = "";
        }
        s += '<span class="label '+class_name+'" style="color:'+lang.status[full.status].COLOR+';cursor:pointer">'+lang.status[full.status].VALUE+'</span>';
        
        
        $('.status_dropdown').siblings('.chosen-container').addClass('hide');
        convertLabelToDropdown();
        return s;
    }

    function generateCheckbox(pk, full) {
        var payment = parseFloat(full.total_paid);
        var total = parseFloat(full.net_total);
        
        if(payment == null){
            payment = 0;
        }
        if(lang.screen_name != 'DailyReceivable' && ((full.stm_status != undefined && full.stm_status != 4) && payment != undefined && total != undefined && payment < total))
        {
            var io = {};
            
            var l = $('<label />');

            io['type'] = "checkbox";
            io['name'] = pk+"_chk[]";
            io['class'] = pk+"_chk styled select";
            io['data-id'] = full[pk];
            var i = $('<input />', io);
            
            var d = $('<div />', {
                class : "checkbox checkbox-sharekhan"
            }).append(i).append(l);

            return $(d).get(0).outerHTML;
        }
        else
        {
            var io = {};
            
            var l = $('<label />');
            io['type'] = "checkbox";
            io['name'] = pk+"_chk[]";
            io['class'] = pk+"_chk styled select";
            io['data-id'] = full[pk];
            io['statement-type'] = full['statement_type'];
            var i = $('<input />', io);
            console.log(full['stm_status']);
            if(full['stm_status'] != 4 && full['phone'] != ''){
                var d = $('<div />', {
                    class : "checkbox checkbox-sharekhan"
                }).append(i).append(l);
                return $(d).get(0).outerHTML;
            } else {
                return '';
            }
        }
    }

    function generateEventRequestActionIcons(ob, full, pk) {
        var status = full.request_status_id;
        var str = "";
        $.each(ob, function(k, v) {
            var url = window.location.href;
            var params = refineUrl(url);
            if(params != "") {
                url = url.replace(params, "").replace("?", "");
            }
            if (status == 1) {
                if (v.class != "asset_raise" && v.class != "asset_approve") {
                    generate(k, v, full, pk);
                }
            }
            if (status == 2) {
                if (v.class == "asset_raise") {
                    generate(k, v, full, pk);
                }
            }

            if (status == 3) {
                
            }

            if (status == 4) {
                if (v.class == "asset_approve") {
                    generate(k, v, full, pk);
                }
            }

            if (status == 5) {
                
            }

            function generate(k, v, full, pk) {
                var a = "";
                var  o = {};
                
                var i = $('<i />', {
                    class : v.i.class
                });
                o['style'] = v.style;
                o['class'] = v.class;
                o['title'] = v.title;
                o['data-original-title'] = v.title;
                o['data-id'] = full[pk];
                o['data-url'] = window.location.href+'/'+full[pk]+v.url;
                if (v.href) {
                    o['href'] = window.location.href+'/'+full[pk]+v.url;
                }
                o['html'] = i;
                a = $('<a />', o);
                
                str += $(a).get(0).outerHTML;
            }
        })

        return str;
    }

    function generateInvoiceRequestActionIcons(ob,full,pk){
        var str = "";
        $.each(ob, function(k, v) {
            var url = window.location.href;
            var params = refineUrl(url);
            if(params != "") {
                url = url.replace(params, "").replace("?", "");
            }
            generate(k, v, full, pk);
            function generate(k, v, full, pk) {
                var a = "";
                var  o = {};
                
                var i = $('<i />', {
                    class : v.i.class
                });
                o['style'] = v.style;
                o['class'] = v.class;
                o['title'] = v.title;
                o['data-original-title'] = v.title;
                o['data-id'] = full[pk];
                o['data-url'] = url+'/'+full[pk]+v.url;
                if (v.href) {
                    o['href'] = url+'/'+full[pk]+v.url;
                }
                o['html'] = i;
                a = $('<a />', o);
                
                str += $(a).get(0).outerHTML;
            }
        })

        function refineUrl() {
            //get full url
            var url = window.location.href;
            //get url after/
            var value = url.substring(url.lastIndexOf('/') + 1);
            //get the part after before ?
            value  = value.split("?")[1];   
            // console.log(value);
            return value;     
        }

        return str;
    }

    function generateLeadsRequestActionIcons(ob, full, pk) {
        var payment = full.total_paid;
        var total = full.net_total;
        var payment_type = full.payment_type;
        var url = window.location.href;
        var type_status = full.statement_status;
        // console.log(type_status);
        var params = refineUrl(url);
        if(params != "") {
            url = url.replace(params, "").replace("?", "");
        }
        var str = "";
        $.each(ob, function(k, v) {
            // console.log(parseFloat(payment),parseFloat(total),'s');
            // if(parseFloat(payment) == parseFloat(total)){
            //     // console.log('in',v.class);
            //     if(v.class == 'print' || v.class == 'edit') {
            //         generate(k, v, full, pk);
            //     }
            // }else if(payment_type == 'Partial Payment'){
            //     if(v.class == 'print') {
            //         generate(k, v, full, pk);
            //     }
            // }else{
            //     generate(k, v, full, pk);
            // }
            if(parseFloat(payment) > 0 ){
                if(v.class == 'payment' || v.class == 'edit' || v.class == 'print' || v.class == 'mail'){
                    generate(k, v, full, pk);
                }
            }else{
                generate(k, v, full, pk);
            }

            // if(v.class == 'payment' && parseFloat(payment) == parseFloat(total)){
            //     console.log(parseFloat(payment),parseFloat(total));
            //     if((v.class == 'delete' && parseFloat(payment) == parseFloat(total) )|| v.class == 'edit' && parseFloat(payment) == parseFloat(total) ){
            //     }else{
            //         // generate(k, v, full, pk);
            //     }   
            // }
            // else if(type_status == 'Cancelled' || status == 'Cancelled') {
            //     if(v.class == 'print') {
            //         generate(k, v, full, pk);
            //     }
            // } 
            // else {
            //     generate(k, v, full, pk);
            // }
            function generate(k, v, full, pk) {
                var a = "";
                var  o = {};
                
                var i = $('<i />', {
                    class : v.i.class
                });
                o['style'] = v.style;
                o['class'] = v.class;
                o['title'] = v.title;
                o['data-original-title'] = v.title;
                o['data-id'] = full[pk];
                o['data-url'] = url+'/'+full[pk]+v.url;
                if (v.href) {
                    o['href'] = url+'/'+full[pk]+v.url;
                }
                o['html'] = i;
                a = $('<a />', o);
                
                str += $(a).get(0).outerHTML;
            }
        })

        function refineUrl() {
            //get full url
            var url = window.location.href;
            //get url after/
            var value = url.substring(url.lastIndexOf('/') + 1);
            //get the part after before ?
            value  = value.split("?")[1];   
            // console.log(value);
            return value;     
        }

        return str;
    }

    function generateActionIcons(ob, full, pk) {
        var str = "";
        var status = full.status;
        var params = refineUrl(url);
        if(params != "") {
            url = url.replace(params, "").replace("?", "");
        }
        
        $.each(ob, function(k, v) {
            generate(k, v, full, pk);
            function generate(k, v, full, pk) {
                var a = "";
                var  o = {};
                
                var i = $('<i />', {
                    class : v.i.class
                });
                o['class'] = v.class+ ' fa-lg ' + 'btn btn-icons';
                o['style'] = 'margin:5px!important; padding:5px!important';
                o['title'] = v.title;
                o['data-original-title'] = v.title;
                o['data-id'] = full[pk];
                o['data-url'] = url+'/'+full[pk]+v.url;
                if (v.href) {
                    o['href'] = url+'/'+full[pk]+v.url;
                }
                o['html'] = i;
                a = $('<a />', o);
                
                str += $(a).get(0).outerHTML;
            }
        })

        function refineUrl() {
            //get full url
            var url = window.location.href;
            //get url after/
            var value = url.substring(url.lastIndexOf('/') + 1);
            //get the part after before ?
            value  = value.split("?")[1];   
            return value;     
        }

        return str;
    }

    function generateImageIcon(pk, full) {
        var o = {};
        o["data-id"] = full[pk];
        o['src'] = "/img/plus.png";
        var img = $('<img />', o);
        return $(img).get(0).outerHTML;
    }

    function data(k) {
        return {
            "data": k,
            "searchable": true,

            "render": function (data, type, full, meta) {
                return data;
            }
        }
    }
}

function convertLabelToDropdown() {
    $(document).on('click', '.label_dropdown', function() {
        chosenInit('','50%',false,'chosen-select');
        $('.status_dropdown').siblings('.chosen-container').addClass('hide');
        $('.label_dropdown').removeClass('hide');
        $('.status_dropdown').addClass('hide');
        $('.status_dropdown').siblings('.chosen-container').addClass('hide');
        $(this).addClass('hide');
        $(this).siblings('select').removeClass('hide');
        $(this).siblings('.chosen-container').removeClass('hide');
    });
}

function statusChange() {
    var table = this.table;
    $(document).on('change', '.status_dropdown', function() {
        var that = $(this);
        var dropdown = $(this).siblings('span');
        bootbox.confirm("Are you sure you want to change the status?", function(result) {
            if (result) {
                $.ajax({
                    type : 'put',
                    url : $(dropdown).data('url'),
                    data : {
                        type : $(dropdown).data('type'),
                        status : $(that).val()
                    },
                    success : function(data) {
                        $(that).addClass('hide');
                        $(that).siblings('.chosen-container').addClass('hide');
                        $(that).siblings('span').removeClass('hide');
                        $(that).siblings('span').html($(that).val());
                        $(that).siblings('span').css('color',lang.status[$(that).val().toLowerCase()].COLOR);
                        table.draw();

                    }
                });
            } else {
                $('.status_dropdown').siblings('.chosen-container').addClass('hide');
                $('.label_dropdown').removeClass('hide');
                $('.status_dropdown').addClass('hide');
                $('.status_dropdown').siblings('.chosen-container').addClass('hide');
            }
        });
    })
}

// inner datatable functions -- JAY 05-09
/*To change the row color of lead batch allocation blade if after 24 hour calling is not completed*/
function changeBatchRowColor(column1,column2,column3, value, table_obj,table = "#table") {
    table_obj.on('draw', function(e) {
        var val = $(table).find('.'+column);
        var index = $(table).find('th').index(val);
        $(table).find('tbody tr').each(function(k,v) {

            var text = $(v).find('td').eq(index).text();
            $.each(value, function(k1, v1) {
                if (text == k1) {
                    $(v).css('color', v1)
                }
            });
        })
    })
}

function changeRowColorGeneral(column, value, table_obj,table = "#table") {
    table_obj.on('draw', function(e) {
        var val = $(table).find('.'+column);
        var index = $(table).find('th').index(val);
        $(table).find('tbody tr').each(function(k,v) {
            var text = $(v).find('td').eq(index).text();
            $.each(value, function(k1, v1) {
                if (text == k1) {
                    $(v).css('color', v1)
                }
            });
        })
    })
}

function removeRowIcon(column, value, table_obj, table = "#table") {

    table_obj.on('draw', function(e) {
        var val = $(table).find('.'+column);
        var index = $(table).find('th').index(val);
        $(table).find('tbody tr').each(function(k,v) {

            var text = $(v).find('td').eq(index).text();
            $.each(value, function(k1, v1) {
                if (text == k1) {
                    $(v).find('.'+ v1).remove();
                }
            });
        })
    })
}

function generateThRow(arr) {
    var data = "";
    for (var i = 0; i < arr.length; i++) {
        var th = $('<th />', {
            class : "thead_" + arr[i],
            html : arr[i].replace(/_/g, ' ')
        });
        data += th.prop('outerHTML')
    }
    return data;
}

function nestingDatatable(obj, that, th) {
    // console.log(obj, that, th);
    function fnFormatDetails(table_id, html) {
        var sOut = "<table id=\"table_" + table_id + "\" class = 'table display compact table-striped table-bordered hover nowrap' cellspacing='0' role='grid' width='100%'>";
        // var sOut = "<table id=\"inner_table\">";
        sOut += "<thead>";
            sOut += "<tr>";
                sOut += html;
            sOut += "</tr>";
        sOut += "</thead>";
        sOut += "<tfoot>";
            sOut += "<tr>";
                sOut += html;
            sOut += "</tr>";
        sOut += "</tfoot>";
        sOut += "</table>";
        
        return sOut;
    }

    var iTableCounter = Math.floor((Math.random() * 10000));
    var oInnerTable;
    var TableHtml;
    // Run On HTML Build


    // $(document).on('click', '#'+obj.table_name+' tbody td img' ,function () {
        var tr = $(that).closest('tr');
        var new_table = tr.closest('table');
        new_table = $('#' + $(new_table).attr('id')).dataTable().api();
        var row = new_table.row( tr );
        
        if ( row.child.isShown()) {
            // This row is already open - close it

            row.child.hide();
            that.src = "/img/plus.png";

        }
        else {
            // Open this row
            that.src = "/img/minus.png";
            
            row.child( fnFormatDetails(iTableCounter, th) ).show();
            
            tr.addClass('shown');
            
            obj.table_name = "table_"+iTableCounter;
            
            
            datatableInitWithButtonsAndDynamicRev(obj);
            
            $("#table_" + iTableCounter+"_wrapper").css("padding", "1em");
            
            iTableCounter = iTableCounter + 1;
        } 
    // })

}

function datatableInitWithButtonsAndDynamicNesting(obj) {
    var oTable = datatableInitWithButtonsAndDynamicRev(obj);
    nestingDatatable(obj, oTable);
    // console.log(oTable,'obj');
}




// removed code from datatable global for buttons
// if (obj.buttons.excel.state) {
        //     buttons[1] = {

        //         extend: 'excel',
        //         customize: function(xlsx) { 
        //             var sheet = xlsx.xl.worksheets['sheet1.xml'];
        //             // Loop over the cells in column `F` 
        //             $('row c[r^="A"]', sheet).each( function () { 
        //                 // Get the value and strip the non numeric characters .replace(/[^\d]/g, '')
        //                 var text = $('is t', this).text();
        //                 $('is t', this).text().replace(text, "'"+text)
        //             }); 
        //         },
        //         exportOptions: {
        //             //to remove first column from export
        //             // columns: function ( idx, data, node ) {
        //             //     var action_col = $('thead th').index($('.thead_action'));
        //             //     var isVisible = table.column( idx ).visible();
        //             //     var isNotForExport = $.inArray( idx, action_col ) !== -1;
                       
        //             //     return isVisible && !isNotForExport ? true : false;
        //             // },
        //             columns: obj.buttons.excel.columns,
        //             // exclude: [ 0 ],
        //             format: {
        //                 body: function ( data, column, row, node ) {
        //                     if ($(node).length) {
        //                         var status_col = $('thead th').index($('.thead_status'));
        //                         var action_col = $('thead th').index($('.thead_action'));
        //                         if ($(node)[0].cellIndex == status_col) {
        //                             return $($(node)).find('.label').text();
        //                         } else if ($(node)[0].cellIndex == action_col) {
        //                             return ;
        //                         } else {
        //                             if (typeof (data) == "number") {
        //                                 return data;
        //                             } else {
        //                                 //added another replace to avoid execution of csv injection --AG 030817
        //                                 return data.replace(/<\/?[^>]+(>|$)/g, "").replace(data, "'" + data).replace(/&amp;/g, '&');
        //                             }
        //                         }
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }
        // if (obj.buttons.pdf.state) {
        //     buttons[2] = {
        //         extend: 'pdfHtml5',
        //         orientation: 'landscape',
        //         pageSize: 'LEGAL',
        //         exportOptions: {
        //             columns: obj.buttons.pdf.columns,
                    
        //             format: {
        //                 body: function ( data, column, row, node ) {
        //                     if ($(node).length) {
        //                         var status_col = $('thead th').index($('.thead_status'));
        //                         var action_col = $('thead th').index($('.thead_action'));
        //                         if ($(node)[0].cellIndex == status_col) {
        //                             return $($(node)).find('.label').text();
        //                         } else if ($(node)[0].cellIndex == action_col) {
        //                             return ;
        //                         } else {
        //                             if (typeof (data) == "number") {
        //                                 return data
        //                             } else {
        //                                 return data.replace(/<\/?[^>]+(>|$)/g, "");
        //                             }
        //                         }
        //                     }
        //                     // return data;
        //                 }
        //             }
        //         }
        //     }
        // }
        // if (obj.buttons.csv.state) {
        //     buttons[3] = {
        //         extend: 'csvHtml5',
        //         orientation: 'landscape',
        //         pageSize: 'LEGAL',
        //         exportOptions: {
        //             columns: obj.buttons.pdf.columns,
                    
        //             format: {
        //                 body: function ( data, column, row, node ) {
        //                     if ($(node).length) {
        //                         var status_col = $('thead th').index($('.thead_status'));
        //                         var action_col = $('thead th').index($('.thead_action'));
        //                         if ($(node)[0].cellIndex == status_col) {
        //                             return $($(node)).find('.label').text();
        //                         } else if ($(node)[0].cellIndex == action_col) {
        //                             return ;
        //                         } else {
        //                             if (typeof (data) == "number") {
        //                                 return data
        //                             } else {
        //                                 return data.replace(/<\/?[^>]+(>|$)/g, "");
        //                             }
        //                         }
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }
        // if (obj.buttons.copy.state) {
        //     buttons[4] = {
        //         extend: 'copyHtml5'
        //     }
        // }