
$(document).ready(function() {

	var activeMaker = function() {
        curr_page = window.location.href;
        
        $url = $('a[href="'+curr_page+'"]').closest('li');
        // console.log($url);

        
        $url.addClass('active');
        $($url).children('ul').find('li').removeClass('active');
       
        if ($url.hasClass('active')) {

          $url.parent('ul').parent('li').addClass('active');
        }

        $('.control-sidebar-tabs').find('li:eq(1)').removeClass('active');
        $('#control-sidebar-home-tab').removeClass('active');
        
        
    }
    activeMaker();

    var csrf_token = $('meta[name="csrf_token"]').attr('content');

    $.ajaxPrefilter(function(options, originalOptions, jqXHR){
        // if (options.type.toLowerCase() === "post") {
            // initialize `data` to empty string if it does not exist
            options.data = options.data || "";

            // add leading ampersand if `data` is non-empty
            options.data += options.data?"&":"";

            // add _token entry
            options.data += "_token=" + csrf_token;

            options.async = true;
        // }
    });

    function memoriesQuickPick() {
        $.ajax(ApiPath+'/api/template/data?domain='+domain)
        .done(function(result) {
           
            siderbarGenerator(result);
        })
        .fail(function(err) {
          
        });
    }

    if (curr_page == AppUrl+'/form') {
        memoriesQuickPick()
    }
    


    function siderbarGenerator(result) {
        html = '';
        
        html += '<li class="treeview memories_quickpick">'
            html += '<a href=javascript:void(0);>'
                html += '<i class="fa fa-gift"></i> <span> Memories (Quick Pick)</span>'
                html += '<i class="fa fa-angle-left pull-right"></i>'
            html += '</a>'
            html += '<ul class="treeview-menu">'
            url = window.location.protocol+'//'+window.location.hostname;
                $.each(result, function(key, val) {
                    subject = val.head_content.replace(/(<([^>]+)>)/ig,"").replace(/&nbsp;/g, '');
                    (subject.length > 22) ? subject_val = subject.substring(0,22) + ' ...' : subject_val = subject;
                    (val.visible_id == 1) ? color = "#1DAF43 !important"  : (val.visible_id == 2) ? color = "#F9C124 !important" : color = "#E82C15 !important";
                    html += '<li><a href = "javascript:;"><i data-id = '+val.subject_id+' class="fa fa-edit sub_nav"></i><span style = "color:'+color+'">'+subject_val+'</span>'
                    // console.log(val);
                    if ("chapter" in val) {
                        html += '<i class="fa fa-angle-left pull-right"></i>'
                        html += '<ul class="treeview-menu">'
                            $.each(val.chapter, function(key1, val1) {
                                chapter = val1.chapter_name.replace(/(<([^>]+)>)/ig,"").replace(/&nbsp;/g, '');
                                
                                (chapter.length > 15) ? chapter_val = chapter.substring(0,15) + ' ...' : chapter_val = chapter;
                                (val1.visible_id == 1) ? color = "#1DAF43 !important"  : (val1.visible_id == 2) ? color = "#F9C124 !important" : color = "#E82C15 !important";
                                html += '<li><a href = "javascript:;"><i data-id = '+val1.subject_id+' data-chapter_id = '+val1.chapter_id+' class="fa fa-edit chp_nav"></i><span style = "color:'+color+'">'+chapter_val+'</span>'
                                if ("page" in val1) {
                                    $.each(val1.page, function(key2, val2) {
                                        (val2.visible_id == 1) ? color = "#1DAF43 !important"  : (val2.visible_id == 2) ? color = "#F9C124 !important" : color = "#E82C15 !important";
                                        html += '<i class="fa fa-angle-left pull-right"></i>'
                                        html += '<ul class="treeview-menu">'
                                        page = val2.page_content.replace(/(<([^>]+)>)/ig,"").replace(/&nbsp;/g, '');
                                        (page.length > 15) ? page_val = page.substring(0,15) + ' ...' : page_val = page;
                                        html += '<li><a href = "javascript:;"><i data-id = '+val2.subject_id+' data-chapter_id = '+val2.chapter_id+' data-page_id = '+val2.page_id+' class="fa fa-edit page_nav"></i><span style = "color:'+color+'">'+page_val+'</span></a></li></ul>'
                                    })
                                }
                                html += '</a>' 
                                html += '</li>' 
                            })
                        html += '</ul>'
                    }

                        html += '</a>' 
                    html += '</li>' 
                    
                })
            html += '</ul>'
        html += '</li>'
        
        if (!$('.sidebar-menu li').hasClass('memories_quickpick'))
        $('.sidebar-menu > li:nth-child(2)').after(html);
        activeMaker();
        
    }

    $(document).on('keyup', '#sidebar_search', function(e) {
        var filter = jQuery(this).val();
        jQuery(".sidebar ul li").each(function () {
            if (jQuery(this).text().search(new RegExp(filter, "i")) < 0) {
                jQuery(this).hide();
            } else {
                jQuery(this).show()
            }
        });
    });


})