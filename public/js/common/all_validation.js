

/* Form which needs to be validated, should have class :- form-validate */

// All the attributes defined here under should be inserted as class of all types of input

// eg: required,lettersOnly etc....

// Add below jquery code under document.ready event

$.validator.setDefaults({ ignore: ":hidden:not(select)" });

        $.validator.addClassRules({
            required: {
                required: true
            },
            email: {
                email: true
            },
            password: {
                required: true
            },
            confirmpassword: {
                required: true,
                equalTo: ".password"
            },
            decimalUpToTwo: {
                decimalUptoTwo: true
            },
            lettersOnly: {
                charachterOnly: true
            },
            imageOnly: {
                accept: "image/jpg,image/jpeg,image/png,image/gif"
            },
            uniqueValueonly: {
                uniqueValue: true
            },
            uniqueValueWhileEdit: {
                uniqueValueWhileEdit: true
            },
            uniqueValueWithParent: {
                uniqueValueWithParent: true
            },
            uniqueValueWithParentWhileEdit: {
                uniqueValueWithParentWhileEdit: true
            },
            telLen: {
                minlength : 10,
                maxlength : 10
            },
            pan: {
                pan: true
            }
        });

        //Add Diiferent Methods

        //This will decimal up to two place only
        $.validator.addMethod("decimalUptoTwo", function (value, element) {
            return this.optional(element) || /^\d{0,4}(\.\d{0,2})?$/i.test(value);
        }, "Decimal upto two place only");

        //This will allow only charachters(a-z) || (A-Z) not even space
        $.validator.addMethod("charachterOnly", function (value, element) {
            return this.optional(element) || /^[a-z]+$/i.test(value);
        }, "Only charachters (a-z) or (A-Z) allowed");

        //Check uniqueness of value
        var uniqueErrorMessage = "Not Unique value";
        if ($('#hdnUniqueMessage').length > 0) {
            uniqueErrorMessage = $("#hdnUniqueMessage").val();
        }

        $.validator.addMethod("uniqueValue", function (value, element) {
            var duplicate = false;
            errorMessage = $(element).data('msg');
            console.log(($(element).data('url')));
            $.ajax({
                type: 'POST',
                url: $(element).data('url'),
                dataType: 'json',
                data: { ExName: value }, //Here change country name to some common value through which you are going to search object
                async: false,
                success: function (oresponse) {
                    console.log(oresponse);
                    if (oresponse == true) {
                        duplicate = true;
                    }
                },
                error: function (ex) {
                    alert('Failed to perform unique validation' + ex);
                }
            });//end ajax
            return duplicate;
        }, uniqueErrorMessage);

        //Check uniqueness of value while edit
        $.validator.addMethod("uniqueValueWhileEdit", function (value, element) {
            var duplicate = false;
            console.log(($(element).data('url')));
            var IDVal = $(element).data('exid');
            $.ajax({
                type: 'POST',
                url: $(element).data('url'),
                dataType: 'json',
                data: { ExName: value, ExID: IDVal }, //Here change country name to some common value through which you are going to search object
                async: false,
                success: function (oresponse) {
                    console.log(oresponse);
                    if (oresponse == true) {
                        duplicate = true;
                    }
                },
                error: function (ex) {
                    alert('Failed to perform unique validation' + ex);
                }
            });//end ajax
            return duplicate;
        }, uniqueErrorMessage);

        $.validator.addMethod("uniqueValueWithParent", function (value, element) {
            var duplicate = false;
            var valParentID = $('.ParentID').val();
            console.log(($(element).data('url')));
            console.log(valParentID);
            $.ajax({
                type: 'POST',
                url: $(element).data('url'),
                dataType: 'json',
                data: { ExName: value, ExParentID: valParentID }, //ExName will be child name and ParentId will be parent ID (eg: StateName & CountryID)
                async: false,
                success: function (oresponse) {
                    console.log(oresponse);
                    if (oresponse == true) {
                        duplicate = true;
                    }
                },
                error: function (ex) {
                    alert('Failed to perform unique validation' + ex);
                }
            });//end ajax
            return duplicate;
        }, uniqueErrorMessage);

        $.validator.addMethod("uniqueValueWithParentWhileEdit", function (value, element) {
            var duplicate = false;
            var IDVal = $(element).data('exid');
            var valParentID = $('.ParentID').val();
            console.log(($(element).data('url')));
            console.log(valParentID);
            $.ajax({
                type: 'POST',
                url: $(element).data('url'),
                dataType: 'json',
                data: { ExName: value, ExParentID: valParentID, ExID: IDVal }, //ExName will be child name and ParentId will be parent ID and ExID will be PK (eg: StateName & CountryID & StateID)
                async: false,
                success: function (oresponse) {
                    console.log(oresponse);
                    if (oresponse == true) {
                        duplicate = true;
                    }
                },
                error: function (ex) {
                    alert('Failed to perform unique validation' + ex);
                }
            });//end ajax
            return duplicate;
        }, uniqueErrorMessage);

        $.validator.addMethod("pan", function(value, element) {
            return this.optional(element) || /^[A-Z]{5}\d{4}[A-Z]{1}$/.test(value);
        }, "Invalid Pan Number");

        $(".form-validate").validate({

            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'div',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                // error.insertAfter(element);
                //check whether chosen plugin is initialized for the element
                // if($(element).prop("class") === "chosen-select") {
                //     $(".chosen-select").append(error);
                // }
                // else {
                //     error.insertAfter(element); // default error placement.
                // }
                // Below code is added by vinay. Error placement for chosen down the input field
                if (element.next().hasClass('chosen-container')) { //or if (element.data().chosen) {
                    element.next().after(error);
                } else {
                    element.after(error);
                }
            }
        });
        
        $.extend($.validator.messages, {
            required: "Required",
            remote: "Please fix this field",
            email: "Invalid email",
            url: "Invalid URL",
            date: "Invalid date",
            dateISO: "Invalid date (ISO)",
            number: "Invalid number",
            digits: "Please enter only digits",
            creditcard: "Invalid credit card number",
            equalTo: "Please enter the same value again",
            accept: "Please select file with a valid extension",
            extension: "Please select file with a valid extension",
            maxlength: $.validator.format("Please enter no more than {0} numbers"),
            minlength: $.validator.format("Please enter at least {0} numbers"),
            rangelength: $.validator.format("Please enter a value between {0} and {1} characters long"),
            range: $.validator.format("Please enter a value between {0} and {1}"),
            max: $.validator.format("Max {0} allow"),
            min: $.validator.format("Min {0} allow")
        });