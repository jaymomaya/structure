function datatableInitGeneral(obj) {
    var datatableObj = {
        columnDefs: [{
            orderable: false, 
            targets: obj.sort_disabled_targets,
        }],
        colReorder: true,
        order: [],
        processing: true,
        serverSide: true,
        "scrollX": true,
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        ajax: {
            ajax_url : obj.ajax_url,
            data : function(d) {
            },
        },
        columns: obj.column_data,
        "drawCallback": function( settings ) {
            var thead = $('.'+obj.table_name+':eq(0)').find('tr:eq(0) th:eq(0)');
            if (thead.text() == "All" || thead.text() == "all") {
                $(thead).html(generateHeaderCheckbox());
            }
        },
        render: $.fn.dataTable.render.text(),
    }

    if (obj.order.state) {
        datatableObj.order = [[ obj.order.column, obj.order.mode ]]
    }

    var button_dom = "";

    var buttons = [];

    if (obj.buttons.state) {
        button_dom = "<'row'<'col-sm-12 bottom-buffer'B>>";
        if (obj.buttons.colvis) {
            buttons[0] = 'colvis'
        }
        buttons[1] = [
            {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            }
        ];
    }

    datatableObj.buttons = buttons;
    datatableObj.info = obj.info;
    datatableObj.paging = obj.paging;
    datatableObj.searching = obj.searching;
    datatableObj.ordering = obj.ordering;
    datatableObj.iDisplayLength = obj.iDisplayLength;
    datatableObj.dom = button_dom+
        "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
        "<'row'<'col-sm-12 bottom-buffer'tr>>" +
        "<'row'<'col-sm-5'i><'col-sm-7'p>>";

    var table = $('#'+obj.table_name).DataTable(datatableObj);

    datatableFooter(obj, table);
    datatableScroll();

    return table;
}



function datatableColumn(arr, action_obj = [], pk = "id") {
    var obj = [];
    arr.forEach(function(k) {
        if (k == "action" || k == "all" || k == "expand" || k == "event_operation" || k == 'leads_icons' || k == 'invoice_icons') {
            obj.push(action(action_obj, pk, k))
        } else if (k == "status") {
            obj.push(status(k))
        }else {
            obj.push(data(k))
        }
    });

    return obj;

    function action(action_obj, pk, view_type) {
        return {
            "data": "action",
            "searchable": false,

            "sortable": false,
            "render": function (data, type, full, meta) {  
                if (view_type == "action") {
                    if(lang.screen_name == 'purchase-order-view')
                    {
                        return generateActionIconsPurchaseOrder(action_obj, full, pk);
                    }
                    else
                    {
                        return generateActionIcons(action_obj, full, pk);
                    } 
                } 
                if (view_type == "expand") {
                    return generateImageIcon(pk, full);
                }
                if (view_type == "all") {
                    return generateCheckbox(pk, full);
                }
            }
        }
    }

    function status(k) {
        return {
            "data": k,
            "searchable": true,

            "render": function (data, type, full, meta) {

                var class_name = "";
                var s = "";
                if (full.status) {
                    if (lang.status[full.status].SHOW) {
                        s += generateDropdown(lang.status[full.status].OPTION, '', '', 'status_dropdown hide');
                        class_name = "label_dropdown";
                    } else {
                        class_name = "";
                    }
                    s += '<span data-type = "'+k+'" data-url = "'+window.location.href+'/'+full[pk]+'" data-id = "'+full[pk]+'" class="label '+class_name+'" style="color:'+lang.status[full.status].COLOR+';cursor:pointer">'+lang.status[full.status].VALUE+'</span>';
                
                    convertLabelToDropdown()
                }
                return s;
            }
        }
    }

    function generateDropdown(data, selected_val, name_attr, class_attr = "chosen-select") {
        var s = $('<select />', {'data-placeholder' : "Select"});
        $('<option />', { value: "", text: "" }).appendTo(s);
        for (var val in data) {
            if (val == selected_val) {
                $('<option />', { value: val, text: data[val], selected:"selected" }).appendTo(s);
            } else {
                $('<option />', { value: val, text: data[val] }).appendTo(s);
            }
        }
        $(s).val(selected_val)
        $(s).attr('class', "chosen-select " + class_attr)
        $(s).attr('name', name_attr)
        $(s).appendTo('<div />')
        return $(s).parent().html();
        //s.appendTo('#ctl00_ContentPlaceHolder1_Table1');
    }

    function generateStatusLabelDepr(pk, full) {
        var class_name = "";
        var s = "";

        if (lang.status[full.status].SHOW) {
            s += generateDropdown(lang.status[full.status].OPTION, '', '', 'status_dropdown hide');
            class_name = "label_dropdown";
        } else {
            class_name = "";
        }
        s += '<span class="label '+class_name+'" style="color:'+lang.status[full.status].COLOR+';cursor:pointer">'+lang.status[full.status].VALUE+'</span>';
        
        
        $('.status_dropdown').siblings('.chosen-container').addClass('hide');
        convertLabelToDropdown();
        return s;
    }

    function generateCheckbox(pk, full) {
        var payment = parseFloat(full.total_paid);
        var total = parseFloat(full.net_total);
        
        if(payment == null){
            payment = 0;
        }
        if(lang.screen_name != 'DailyReceivable' && ((full.stm_status != undefined && full.stm_status != 4) && payment != undefined && total != undefined && payment < total))
        {
            var io = {};
            
            var l = $('<label />');

            io['type'] = "checkbox";
            io['name'] = pk+"_chk[]";
            io['class'] = pk+"_chk styled select";
            io['data-id'] = full[pk];
            var i = $('<input />', io);
            
            var d = $('<div />', {
                class : "checkbox checkbox-sharekhan"
            }).append(i).append(l);

            return $(d).get(0).outerHTML;
        }
        else
        {
            var io = {};
            
            var l = $('<label />');
            io['type'] = "checkbox";
            io['name'] = pk+"_chk[]";
            io['class'] = pk+"_chk styled select";
            io['data-id'] = full[pk];
            io['statement-type'] = full['statement_type'];
            var i = $('<input />', io);
            if(full['stm_status'] != 4 && full['phone'] != ''){
                var d = $('<div />', {
                    class : "checkbox checkbox-sharekhan"
                }).append(i).append(l);
                return $(d).get(0).outerHTML;
            } else {
                return '';
            }
        }
    }

    function generateActionIcons(ob, full, pk) {
        var str = "";
        var status = full.status;
        var url = window.location.href;
        var params = refineUrl(url);
        var screen_name = lang.screen_name;
        if(params != "") {
            url = url.replace(params, "").replace("?", "");
        }

        $.each(ob, function(k, v) {
            if(screen_name == 'product-voucher-view'){
                if(full.approve_status == 'Pending' && (v.class == 'product_voucher_approve' || v.class == 'edit' || v.class == 'delete') || (v.class == 'supply') && parseFloat(full.supplied_qty) < parseFloat(full.total_qty) && full.approve_status == 'Approved' || v.class == 'view' && (full.supplied_qty != full.total_qty) || (v.class == 'edit' || v.class == 'delete') && full.approve_status == 'Approved' && parseFloat(full.supplied_qty) <= 0){
                    generate(k, v, full, pk);
                }
            }
            else if(screen_name == 'wo-invoices-view'){
                if((v.class == 'create' && full.to_be_raise > 0)|| (v.class == 'pre_closure' && full.to_be_raise > 0 )){
                    generate(k, v, full, pk);
                }
            }
            else if(screen_name == 'bill-of-material-view'){
                if(full.bom_status == 'closed')
                {
                    if(v.class != 'edit' && v.class != 'delete'){
                        generate(k, v, full, pk);    
                    }   
                }
                else if(full.new > 0)
                {
                    if(v.class != 'edit'){
                        generate(k, v, full, pk);    
                    }   
                }
                else{
                    generate(k, v, full, pk);    
                }

            }
            else if(screen_name == 'invoice-view'){
                generate(k, v, full, pk);
            }
            else if(screen_name == 'inventory-master-view'){
                var inventory_type = full.iim_type;
                var in_source = full.in_source;
                var out_scource = full.out_scource;
                if((inventory_type == 1 && in_source != 3) || (inventory_type == 2 && out_scource != 4)){
                    generate(k, v, full, pk);    
                } else {
                    if(v.class == 'view'){
                        generate(k, v, full, pk);    
                    }
                }
            }
            else if(screen_name == 'goods-received-view'){
                var total_quantity = full.total_quantity;
                var item_received = full.item_received;
                var gr_status = full.gr_status;
                if(total_quantity != item_received && gr_status != 'Done'){
                    if(v.class == 'store'){
                        generate(k, v, full, pk);    
                    }
                }
                if(gr_status == 'Partial')
                {
                    if(v.class == 'pre_closure'){
                        generate(k, v, full, pk);    
                    }   
                }
            }
            else if(screen_name == 'work-order-view'){
                var total_po_raised = parseInt(full.total_po_raised);
                var status = full.approval_status;
                var request_qty = full.request_qty;

                if(request_qty > 0){
                    if(v.class == 'print' || v.class == 'mail'){
                        generate(k, v, full, pk);    
                    }
                }
                else if(total_po_raised == 0 && status == 'Pending'){
                    if(v.class == 'edit' || v.class == 'delete' || v.class == 'accounts_approval' || v.class == 'print' || v.class == 'mail'){
                        generate(k, v, full, pk);    
                    }
                }
                else if(total_po_raised == 0 && status == 'Approved' || v.class == 'print' || v.class == 'mail'){
                    if(v.class == 'delete' || v.class == 'print' || v.class == 'mail'){
                        generate(k, v, full, pk);    
                    }   
                } else {
                    if(v.class == 'edit' || v.class == 'delete' || v.class == 'print' || v.class == 'mail'){
                        generate(k, v, full, pk);    
                    }
                }   
            } else if(screen_name == 'quality-check-view'){
                var qc_status = full.qc_status;
                var failed = full.fail_quantity;
                if(qc_status != 'Done' ){
                    if(v.class == 'quality_check')
                    {
                        generate(k, v, full, pk);
                    }
                }
                if(qc_status == 'Done' && failed > 0){
                    if(v.class == 'repair')
                    {
                        generate(k, v, full, pk);
                    }
                }

            } else if(screen_name == 'supply-raw-material-view'){
                var supply_status = full.real_supply_status;
                if(supply_status != 'Done'){
                    generate(k, v, full, pk);    
                }
                else if(v.class == 'print' || v.class == 'mail'){
                    generate(k, v, full, pk);    
                }
            } else if(screen_name == 'production-delivery-view'){
                var request_raw_material_status = full.real_request_raw_material_status;
                var work_order_end_date = full.work_order_end_date;
                var work_order_start_date = full.work_order_start_date;
                var send_for_packaging_status = full.send_for_packaging_status;
                var dates_approval = full.dates_approval;
                var supply_qty = full.supply_qty;
                if(v.class == 'raise_voucher' && (work_order_end_date != '' && work_order_start_date != '') || v.class == 'request_raw_materials' && request_raw_material_status != 'Done' && ((work_order_end_date != '' && work_order_end_date != null) && (work_order_start_date != '' && work_order_start_date != null)) || v.class == 'add_delivery_date' && ((work_order_end_date == '' || work_order_end_date == null) && (work_order_start_date == '' || work_order_start_date == null)) || v.class == 'send_for_packaging' && supply_qty > 0 && send_for_packaging_status != 'Done' && ((work_order_end_date != '' && work_order_end_date != null) && (work_order_start_date != '' && work_order_start_date != null) && (request_raw_material_status == 'Done' || request_raw_material_status == 'Partial')) || v.class == 'approve_dates' && dates_approval == 'Pending' && ((work_order_end_date != '' && work_order_end_date != null) && (work_order_start_date != '' && work_order_start_date != null)) || v.class == 'edit_delivery_date' && request_raw_material_status != 'Done' && (dates_approval == 'Approved' || dates_approval == 'Rejected')&& ((work_order_end_date != '' && work_order_end_date != null) && (work_order_start_date != '' && work_order_start_date != null))){
                    generate(k, v, full, pk);
                }
            } else if(screen_name == 'packaging-accept'){
                var accept_status = full.real_accept_status;
                if(accept_status != 'Accepted'){
                    generate(k, v, full, pk);    
                }
            } else if(screen_name == 'packaging-send-to-warehouse'){
                var total_quantity = full.total_quantity;
                var quantity_send_to_warehouse = full.real_quantity_send_to_warehouse;
                
                if(total_quantity != quantity_send_to_warehouse){
                    generate(k, v, full, pk);    
                }
            } else if(screen_name == 'warehouse-view'){
                var accept_status = full.real_accept_status;
                if(accept_status != 'Accepted'){
                    generate(k, v, full, pk);    
                }
            }
             else {
                generate(k, v, full, pk);
            }
            
            function generate(k, v, full, pk) {
                var a = "";
                var  o = {};
                
                var i = $('<i />', {
                    class : v.i.class
                });
                o['class'] = v.class+ ' fa-lg ' + 'btn btn-icons';
                o['style'] = 'margin:5px!important; padding:5px!important';
                o['title'] = v.title;
                o['data-original-title'] = v.title;
                o['data-id'] = full[pk];
                o['data-url'] = url+'/'+full[pk]+v.url;
                if (v.href) {
                    o['href'] = url+'/'+full[pk]+v.url;
                }
                o['html'] = i;
                a = $('<a />', o);
                
                str += $(a).get(0).outerHTML;
            }
        })


        function refineUrl() {
            //get full url
            var url = window.location.href;
            //get url after/
            var value = url.substring(url.lastIndexOf('/') + 1);
            //get the part after before ?
            value  = value.split("?")[1];   
            return value;     
        }

        return str;
    }
    
    function generateActionIconsPurchaseOrder(ob, full, pk) {
        var str = "";
        var status = full.status;
        var url = window.location.href;
        var params = refineUrl(url);
        if(params != "") {
            url = url.replace(params, "").replace("?", "");
        }
        $.each(ob, function(k, v) {
            if (full['admin_status'] == 'Pending' && full['accounts_status'] != 'Reject') {
                if (v.class == "admin_approve") {
                    generate(k, v, full, pk);
                }
            }
            else if (full['accounts_status'] == 'Pending' && full['admin_status'] == 'Approved'){
                if (v.class == "accounts_approve") {
                    generate(k, v, full, pk);
                }
            }
            if (full['admin_status'] == 'Reject' || full['accounts_status'] == 'Reject' || (full['accounts_status'] == 'Pending' && full['admin_status'] == 'Pending')) {
                if (v.class == "edit" || v.class == 'cancel') {
                    generate(k, v, full, pk);
                }
            }
            if (v.class == "print" || v.class == "mail") {
                generate(k, v, full, pk);
            }

            // generate(k, v, full, pk);
            function generate(k, v, full, pk) {
                var a = "";
                var  o = {};
                
                var i = $('<i />', {
                    class : v.i.class
                });
                o['class'] = v.class+ ' fa-lg ' + 'btn btn-icons';
                o['style'] = 'margin:5px!important; padding:5px!important';
                o['title'] = v.title;
                o['data-original-title'] = v.title;
                o['data-id'] = full[pk];
                o['data-url'] = url+'/'+full[pk]+v.url;
                if (v.href) {
                    o['href'] = url+'/'+full[pk]+v.url;
                }
                o['html'] = i;
                a = $('<a />', o);
                
                str += $(a).get(0).outerHTML;
            }
        })


        function refineUrl() {
            //get full url
            var url = window.location.href;
            //get url after/
            var value = url.substring(url.lastIndexOf('/') + 1);
            //get the part after before ?
            value  = value.split("?")[1];   
            return value;     
        }

        return str;
    }

    function generateImageIcon(pk, full) {
        var o = {};
        o["data-id"] = full[pk];
        o['src'] = "/img/plus.png";
        var img = $('<img />', o);
        return $(img).get(0).outerHTML;
    }

    function data(k) {
        return {
            "data": k,
            "searchable": true,

            "render": function (data, type, full, meta) {
                return data;
            }
        }
    }
}

function generateHeaderCheckbox() {
    var io = {};
    var l = $('<label />').html('&nbsp;All');
    io['type'] = "checkbox";
    io['name'] = "select_all";
    io['class'] = "styled select_all";
    var i = $('<input />', io);
    var d = $('<div />', {
        class : "checkbox checkbox-sharekhan"
    }).append(i).append(l);

    return $(d).get(0).outerHTML;
}

function datatableScroll() {
    // $('.dataTable').wrap('<div class="dataTables_scroll" />');

    $('.dataTables_scroll').slimscroll({
        alwaysVisible: true,
        railVisible: true,
        axis: 'x',
    });
}

function datatableFooter(obj, table) {

    $('#'+obj.table_name).closest('.dataTables_scroll').find('.dataTables_scrollFoot tfoot th').each(function () {
        var title = $(this).text();
        $(this).not('.no-search').html('<input type="text" class = "form-control input-sm" placeholder="Search '+title+'" />');
    });

    var state = table.state.loaded();
    
    if (state) {
        table.columns().eq(0).each(function (colIdx) {
            var colSearch = state.columns[colIdx].search;
            if (colSearch.search) {
                $('input', table.column(colIdx).footer()).val(colSearch.search);
            }
        });
        table.draw();
    }

    table.columns().every(function() {
        var that = this;
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that.column( $(this).parent().index()).search( this.value ).draw();

            }
        });
    });
}

function changeRowColorGeneral(column, value, table_obj,table = "#table") {
    table_obj.on('draw', function(e) {
        var val = $(table).find('.'+column);
        console.log($(table));
        var index = $(table).find('th').index(val);
        $(table).find('tbody tr').each(function(k,v) {
            var text = $(v).find('td').eq(index).text();
            $.each(value, function(k1, v1) {
                if (text == k1) {
                    $(v).css('color', v1)
                }
            });
        })
    })
}