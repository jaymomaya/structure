// $(document).ready(function() {
    $.fn.extend({
        animateCss: function (animationName) {
            var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            $(this).addClass('animated ' + animationName).one(animationEnd, function() {
                $(this).removeClass('animated ' + animationName);
            });
        }
    });



    $('#main-menu').smartmenus({
        subIndicatorsText:  '',

    });

    $('.tag_height_slim_scroll').slimScroll({
        height: '4em',
    });


    var myCustomTemplates = {
      html : function(locale) {
        return "<li>" +
               "<div class='btn-group'>" +
               "<a class='btn' data-wysihtml5-action='change_view' title='" + locale.html.edit + "'>HTML</a>" +
               "</div>" +
               "</li>";
      }
    }

  

    


    window.lazySizesConfig = window.lazySizesConfig || {};
    // window.lazySizesConfig.loadMode = 1;

    //Subject
    
    $(document).on('click', '#add_subject', function() {

        $.ajax({
            type : 'get',
            url : '/form/add',
            success : function(res) {
                

                $('body').html(res);
                notification( lang.album_operation_steps, 'attached', 'bouncyflip', 'notice', 20000);

                $('.progress .progress-bar').progressbar({display_text: 'fill'});
                $('#head_content').focus();
                // notification(lang.chapter_operation_steps, 'growl', 'genie', 'notice', 15000);
                

                
            },
            complete: function() {
                chosenInit('chosen-select');
                $('#subject_select').val("add_new");
                chosenUpdate();
                pluploadFrontInit();
                pluploadBackInit();
                initForm("post", "subject", "subject_data_form");
                slimScrollInit('.height_adjust', '20.9em');
                $('.head_content').focus();

                $('.dropdown_image_menu').smartmenus();
                

                
            }
        })
    });

    // edit subject
    $(document).on('click', '#edit_subject', function() {
        var id = $(this).attr('data-id');
        
        $.ajax({
            type : 'get',
            url : '/form/'+id,
            success : function(res) {
                $('body').html(res);
                // $('.head_content').focus();
                getSubjectData(id, 'edit_subject');
                getSubjectImage(id);

            },
            complete: function() {
                var album_title = $('.album_form').find('.head_content').val();
                
                $('.subject_title').html('Memories Album -' + album_title );
                chosenInit('chosen-select');
                initForm("put", "subject/"+id, "subject_data_form");
                slimScrollInit('.height_adjust', '20.9em');

                $('.dropdown_image_menu').smartmenus();
                

            }
        })
    });


    // Album lsit delete
    $(document).on('click', '#album_list_delete', function() {

        bootbox.confirm(lang.are_you_sure_title, function(res) {
            if (res) {
                deleteSubject($("#subject_select").val());
            }
        });
    })


    // Subject select
    $(document).on('change', '#subject_select', function() {

        getSubjectData($(this).val());
        getSubjectImage($(this).val());
           
    });

    // $(document).on('click', '#subject_save', function() {
        // 
    //     $('.subject_box_body').css('display','none');
    //     $('.chapter_content').focus();
    // })

    // Subject function
    function getSubjectData(val, operation_name = '') {
        $.ajax({
            type : 'get',
            url : '/subject/'+val,
            success : function(res) {
                // res = $.parseHTML(res);

                $('.chapter_box').remove();
                $('.subject_section').html(res.subject);
                $('.subject_box').after(res.chapter);
                
            },
            complete: function() {

                var album_title = $('.album_form').find('.head_content').val();
                
                $('.subject_title').html('Memories Album - &nbsp;' + album_title );
                $('.subject_title').addClass('album_title');
                
                $('#chapter_select').val("add_new");
                $('.add_chp_data').removeClass('hide');
                if (val == "add_new") {
                    $('#subject_select').val("add_new");
                    initForm("post", "subject", "subject_data_form");
                } else {
                    initForm("put", "subject/"+val, "subject_data_form");
                    
                }

                chosenInit('chosen-select');
                chosenUpdate();
                $('.custom_dropdown_menu').smartmenus();
                initFormForChapter('post', 'subject/'+val+'/chapter','chapter_data_form');

                if(operation_name == 'edit_subject'){
                    $('.head_content').focus();
                } else if (operation_name == 'add_album_chapter') {
                    $("#chapter_section").fadeIn("slow");

                    $('.chapter_content').focus();
                } else {
                    $('.head_content').focus();
                }


            }
        })
    }

    function getSubjectImage(val) {
        startLoader('sk-circle','Loading...');
        $.ajax({
            type : 'get',
            url : '/subject/media/'+val,
            success : function(res) {
                
                $('.image_section').html(res);
                $('.dropdown_image_menu').smartmenus();
            },
            complete: function() {
                pluploadFrontInit();
                pluploadBackInit();
                if (val == "add_new")
                slimScrollInit('.height_adjust', '24em');
                else
                slimScrollInit('.height_adjust', '24em');

                stopLoader();
                global_counter = $('.subject_image_handler .uploaded_img:last').attr('data-sort-id');
                tinysort(".subject_image_handler img","",{attr:"data-sort-id"})
                
            }
        })
    }

    function deleteSubject(data_id) {
        $.ajax({
            url : 'subject/' + data_id,
            type : 'delete',
            success : function(html) {
                getSubjectData("add_new");
                getSubjectImage("add_new");
                notification( lang.subject_delete_success, 'bar', 'slidetop', 'notice', timer);

            },
            error : function(err) {
                
            }
        });
    }


    // Subject Filters
    
    $(document).on('click', '.sub_visibility', function() {
        var vis_name = $(this).attr('data-vis-name');
        $('.sub_visibility').removeClass('border_color_radius').removeClass('active');
        $(this).addClass('border_color_radius').addClass('active');
    });

    $(document).on('click', '.visibility', function() {
        var vis_name = $(this).attr('data-vis-name');
        $('.visibility').removeClass('border_color_radius');
        $(this).addClass('border_color_radius');
        $('.subject_image_handler').find('.subject_image').not('.'+vis_name).addClass('hide');
        $('.subject_image_handler').find('.'+vis_name).removeClass('hide');
    });

    $(document).on('click', '.level', function() {
        var level_name = $(this).attr('data-level-name');
        $('.level').removeClass('active');
        $(this).addClass('active');
        if (level_name == "all") {
            $('.subject_image_handler').find('.subject_image').removeClass('hide'); 
        } else {
            $('.subject_image_handler').find('.subject_image').not('.'+level_name).addClass('hide');
            $('.subject_image_handler').find('.'+level_name).removeClass('hide');
        }
    });

    $(document).on('keyup', '#subject_image_search', function() {

        var val = $(this).val();
        var filter = $("img[data-image-name*='"+val+"']");

        if (filter.length == 0) {
            if (val == "") {
                $('.subject_image').removeClass('hide');
            } else {
                $('.subject_image').addClass('hide');
            }
        } else {
            $('.subject_image').addClass('hide');
            $.each(filter, function(k ,v) {
                $(v).closest('.subject_image').removeClass('hide');
                
            })
        }
    });

    //Subject Save Operations

    $(document).on('click', '#subject_image_save', function() {
        
        if (!$("#subject_data_form").valid()) {
            return false;
        } else {
            $("#subject_data_form").submit();
        }
        
    });



    //Chapter
    // Add album chapter
    $(document).on('click', '.add_album_chp', function() {
        var id = $(this).attr('data-id');
        $.ajax({
            type : 'get',
            url : '/form/'+id,
            success : function(res) {
                
                $('body').html(res);
                $("#chapter_section").fadeIn("slow");

                $('.subject_collapse_box').addClass('collapsed-box');
                $('.common').removeClass('fa-minus');
                $('.common').addClass('fa-plus');
                $('.subject_box_body').css('display','none');

                getSubjectData(id, 'add_album_chapter');
                // $('#chapter_section').animateCss('bounce');
                getSubjectImage(id);   
                // notification(lang.chapter_operation_steps, 'growl', 'genie', 'notice', 15000);

            },
            complete: function() {

                chosenInit('chosen-select');
                $('#chapter_select').val("add_new");
                $('.chapter_content').focus();

                $('.add_chp_data').removeClass('hide');
                chosenUpdate();
                slimScrollInit('.height_adjust', '17.7em');
                initFormForChapter("post", "subject/"+id+"/chapter", "chapter_data_form");
                $('.dropdown_image_menu').smartmenus();
               

                
            }
        })
    });

    $(document).on('change', '#chapter_select', function() {
        
        if ($(this).val() == "add_new") {
           
            $('.chapter_content').val("");
            // 
            $('.page_box').remove();
            initFormForChapter("post", "subject/"+$('#subject_select').val()+"/chapter", "chapter_data_form");
            $('.dropdown_image_menu').smartmenus();
        } else {
            // var chapter_content =$('#chapter_select_chosen').find('span').html();
            // console.log(chapter_content);
           $('.chapter_content').focus();
            getChapterData($('#subject_select').val(),$(this).val());
            console.log("change chapter");
            // $('section.chapter_collapse').css("min-height","0em");
            initFormForChapter("put", "subject/"+$('#subject_select').val()+"/chapter/"+$(this).val(), "chapter_data_form");
        }

    });


    // $(document).on('click', '.save_album_chapter', function() {
    //     $('#page_content').summernote({
    //         focus: true,
    //     });
    // })
    $(document).on('click', '#chp_list_delete', function() {
        
        if ($('#chapter_select').val() == "add_new") {
            notification( "Invalid Selection", 'bar', 'slidetop', 'notice', timer);
        } else {
            bootbox.confirm(lang.are_you_sure_title, function(res) {
                if (res) {
                    deleteChapter($("#subject_select").val(), $("#chapter_select").val());
                    chosenUpdate();
                    $('.album_chapter_title').html('Memories Album Chapter');

                }
            });
        }
    })

    $(document).on('click', '.add_new_chapter', function() {
        $('.chapter_content').focus();
        notification(lang.chapter_operation_steps, 'growl', 'genie', 'notice', 15000);

    })



    function getChapterData(subject_id , chapter_id ) {
        
        $.ajax({
            'url' : '/subject/'+subject_id+'/chapter/'+chapter_id,
            type : 'get',
            success: function(res) {
                
                $('.chapter_content').val($('#chapter_select :selected').text())
                $('.page_box').remove();
                $('.chapter_box').after(res);
            },
            complete: function() {

                var chapter_title = $('.chapter_section').find('.chapter_content').val();
                // console.log(chapter_title);
                chosenUpdate();
                $('.album_chapter_title').html('Memories Album Chapter - &nbsp;'+chapter_title );
                $('.album_chapter_title').addClass('album_title');
                $('#chapter_content').focus();
                $('section.chapter_collapse').css("min-height","0em");
                getPageData($('#subject_select').val(), $('#chapter_select').val(), "add_new");
                getPageImage( "add_new");
                $('#page_select').val("add_new");

                // var subject_visi_value = $('.sub_visibility.active').attr('data-vis-value');
                // if ( subject_visi_value == 3) {
                    // 
                //     $('.chapter_section').addClass('box-private');
                // } else if ( subject_visi_value == 2 ) {
                    // 
                //     $('.album_form').addClass('box-shared');
                // } else {
                    // 
                //     $('.album_form').addClass('box-public');
                // }
            }

        })
    }

    function deleteChapter(subject_id, chapter_id) {
        $.ajax({
            url : 'subject/'+subject_id+'/chapter/'+chapter_id,
            type : 'delete',
            success : function(html) {
                $('#chapter_select option[value="'+chapter_id+'"]').remove();
                $('#chapter_select').val("add_new");
                chosenUpdate();
                $('.chapter_content').val("");
                $('.page_box').remove();
                initFormForChapter("post", "subject/"+$('#subject_select').val()+"/chapter", "chapter_data_form");
                $('.dropdown_image_menu').smartmenus();
                notification( lang.chapter_delete_success, 'bar', 'slidetop', 'notice', timer);

            },
            error : function(err) {
                
            }
        });
    }

    $(document).on('click', '.chp_visibility', function() {
        var vis_name = $(this).attr('data-vis-name');
        console.log(vis_name);
        $('.chp_visibility').removeClass('border_color_radius').removeClass('active');
        $(this).addClass('border_color_radius').addClass('active');
    });

    //Page

    $(document).on('change', '#page_select', function() {
        
        console.log($('#subject_select').val(), $('#chapter_select').val(), $(this).val());
        getPageData($('#subject_select').val(), $('#chapter_select').val(), $(this).val());
        getPageImage( $(this).val())
        console.log($(this).val());
        $('#page_content').summernote({
            focus: true
        });
       
    });

    $(document).on('click', '#page_list_delete', function() {
        if ($('#page_select').val() == "add_new") {
            notification( "Invalid Selection", 'bar', 'slidetop', 'notice', timer);
        } else {
            bootbox.confirm(lang.are_you_sure_title, function(res) {
                if (res) {
                    deletePage($("#subject_select").val(), $("#chapter_select").val(), $("#page_select").val());
                }
            });
        }
    })

    function getPageData(subject_id, chapter_id, page_id, operation_name = '') {
        startLoader('sk-circle','Loading...');
        $.ajax({
            'url' : '/subject/'+subject_id+'/chapter/'+chapter_id +'/page/'+page_id,
            type : 'get',
            success: function(res) {

                
                $('.page_content_section').html(res);
            },
            complete: function() {
                chosenInit('page_select');

                // var page_title = $('.chapter_section').find('.chapter_content').val();
                // 
                // $('.album_page_title').html('Album Page - &nbsp;'+page_title );

                $('section.chapter_collapse').css("min-height","0em");
                $('#page_select').val(page_id);
                chosenUpdate();
                $('.dropdown_image_menu').smartmenus();
                if (page_id == "add_new") {
                    console.log("if");
                    $('#page_content').summernote({
                        focus: true,
                        toolbar:[
                            ['style', ['bold', 'italic', 'underline', 'clear']],
                             ['color', ['color']]
                        ]
                    });
                    initFormForPage("post", 'subject/'+$('#subject_select').val()+'/chapter/'+$('#chapter_select').val()+'/page', 'page_data_form')

                } else {
                    console.log("else");
                    $('#page_content').summernote({
                        // focus: true,
                        toolbar:[
                            ['style', ['bold', 'italic', 'underline', 'clear']],
                             ['color', ['color']]
                        ]
                    });
                    initFormForPage("put", 'subject/'+$('#subject_select').val()+'/chapter/'+$('#chapter_select').val()+'/page/'+$('#page_select').val(), 'page_data_form')
                    
                }
                // $('.subject_box_body').css('display','none');
                

                var page_title = $( "#page_select option:selected" ).text();
                
                $('.album_page_title').html(page_title);
                $('.album_page_title').addClass('album_title');
                
               

                stopLoader();

            }

        })
    }

    function getPageImage(page_id) {
        startLoader('sk-circle','Loading...');
        $.ajax({
            'url' : 'page/'+page_id+'/images',
            type : 'get',
            success: function(res) {
                $('.page_image_section').html(res);
            },
            complete : function() {
                // dropdownInit('dropdown-toggle');
                pluploadPageInit()
                slimScrollInit('.height_adjust', '24.1em');
                $('.dropdown_image_menu').smartmenus();
                stopLoader();
                global_page_counter = $('.page_image_handler .uploaded:last').attr('data-sort-id');
                if (global_page_counter == undefined) {
                    global_page_counter = 0;
                }
                
            }

        })
    }

    function deletePage(subject_id, chapter_id, page_id) {
        $.ajax({
            url : 'subject/'+subject_id+'/chapter/'+chapter_id+'/page/'+page_id,
            type : 'delete',
            success : function(html) {
                getPageData($('#subject_select').val(), $('#chapter_select').val(), "add_new");
                getPageImage("add_new")
                notification( lang.page_delete_success, "bar", "slidetop", "notice", timer);

            },
            error : function(err) {
                
            }
        });
    }

    $(document).on('click', '.page_visibility', function() {
        var vis_name = $(this).attr('data-vis-name');
        $('.page_visibility').removeClass('border_color_radius').removeClass('active');
        $(this).addClass('border_color_radius').addClass('active');
    });

    $(document).on('click', '.page_btn_visibility', function() {
        var vis_name = $(this).attr('data-vis-name');
        // console.log(vis_name);
        $('.visibility').removeClass('border_color_radius');
        $(this).addClass('border_color_radius');
        $('.page_image_handler').find('.page_image').not('.'+vis_name).addClass('hide');
        $('.page_image_handler').find('.'+vis_name).removeClass('hide');
    })


    // $(document).on('click', '.sub_visibility', function() {
    //     var vis_name = $(this).attr('data-vis-name');
    //     $('.sub_visibility').removeClass('border_color_radius').removeClass('active');
    //     $(this).addClass('border_color_radius').addClass('active');
    // });

    $(document).on('click', '.visibility', function() {
        var vis_name = $(this).attr('data-vis-name');
        $('.visibility').removeClass('border_color_radius');
        $(this).addClass('border_color_radius');
        $('.subject_image_handler').find('.subject_image').not('.'+vis_name).addClass('hide');
        $('.subject_image_handler').find('.'+vis_name).removeClass('hide');
    });

    $(document).on('click', '#page_image_save', function() {
        
        if (!$("#page_data_form").valid()) {
            return false;
        } else {
            $("#page_data_form").submit();
        }
        
    });

    //Media

    $(document).on('click', '.edit_image',function(e) {
        myPixie = pixieInit();
        img = $(this).closest('ul').closest('ul').closest('div').siblings('img')
        curr_image = $(img).attr('src')
        level_val = $(img).attr('data-level_val')
        common_ref = $(this).attr('data-common_ref');
        var myPixie = Pixie.setOptions({
                onSave: function(data, img) {
                    
                    $.ajax({
                        type: 'POST',
                        url: '/save-image',
                        data: { 
                            imgData: data,
                            url : curr_image,
                        },
                    }).success(function(response) {
                        if (level_val == 1 || level_val == 2)
                            getSubjectImage(common_ref);
                        else if (level_val == 4) {
                            getPageImage(common_ref);
                        }
                    });
                },
                 onSaveButtonClick: function() {
                    myPixie.save('png', '10');
                }
            });
        myPixie.open({
            url: $(this).closest('ul').closest('ul').closest('div').siblings('img').attr('src'),
            image: $(this).closest('ul').closest('ul').closest('div').siblings('img')
        });

    });

    $(document).on('click', '.delete_image',function(e) {
        var self = $(this);
        bootbox.confirm(lang.are_you_sure_delete_page_title, function(result) {
            if (result) {
                var media_id = $(self).attr('data-id');
                var level_val = $(self).attr('data-level-val');
                
                var level_map = $('.level_group').find("[data-level-name='" + level_val + "']");
                var level_badge = $('.level_group').find("[data-level-name='" + level_val + "']").find('span');
                var level_count = $(level_map).attr('data-count');

                var temp_level_val = $(self).closest('ul').closest('ul').closest('div').closest('.thumbnail').find('img').attr('data-level-val')
                var temp_level_map = $('.level_group').find("[data-level-name='" + temp_level_val + "']");
                var temp_level_badge = $('.level_group').find("[data-level-name='" + temp_level_val + "']").find('span');
                var temp_level_count = $(temp_level_map).attr('data-count');
                
                var level_map_all = $('.level_group').find("[data-level-name='all']");
                var level_badge_all = $('.level_group').find("[data-level-name='all']").find('span');
                var level_count_all = $(level_map_all).attr('data-count');
                
                if (media_id == undefined) {
                    $(self).closest('ul').closest('ul').closest('div').closest('.thumbnail').parent().remove()
                    $(temp_level_map).attr('data-count', (temp_level_count - 1))
                    $(temp_level_badge).html((temp_level_count - 1))
                    
                    $(level_map_all).attr('data-count', (level_count_all - 1))
                    $(level_badge_all).html((level_count_all - 1))
                } else {
                    $.ajax({
                        url : "media/"+media_id,
                        type : 'delete',
                        success : function(html) {
                            $(self).closest('ul').closest('ul').closest('div').closest('.thumbnail').parent().remove();
                            notification( lang.image_delete_success, 'bar', 'slidetop', 'notice', timer);
                            $(level_map).attr('data-count', (level_count - 1))
                            $(level_badge).html((level_count - 1))
                            
                            $(level_map_all).attr('data-count', (level_count_all - 1))
                            $(level_badge_all).html((level_count_all - 1))

                            notification( lang.image_delete_success, 'bar', 'slidetop', 'notice', timer);
                        },
                        error : function(err) {
                            
                        }
                    });
                }
            }
        })
    })

    $(document).on('click', '.change_img_vis > li > a, .change_img_vis_temp > li > a',function(e) {
        var data_id = $(this).attr('data-id');
        var visible = $(this).attr('data-visible_id');
        var self = $(this)
        if (visible == 1) {
            var color = "public_icon_color";
            var title = "public"
            $(this).closest('ul').siblings('a').html('<i class="fa fa-eye"></i>Privacy (public)<span class="caret right-caret" style="color: #7A7A7A!important;"></span>');
            $(this).closest('ul').closest('ul').closest('div').closest('.thumbnail').find('img').attr('data-visibility-val', 1);
        } else if (visible == 2) {
            var color = "shared_icon_color";
            var title = "shared"
            $(this).closest('ul').siblings('a').html('<i class="fa fa-eye"></i>Privacy (shared)<span class="caret right-caret" style="color: #7A7A7A!important;"></span>');
            $(this).closest('ul').closest('ul').closest('div').closest('.thumbnail').find('img').attr('data-visibility-val', 2);
        } else {
            var color = "private_icon_color";
            var title = "private"
            $(this).closest('ul').siblings('a').html('<i class="fa fa-eye"></i>Privacy (private)<span class="caret right-caret" style="color: #7A7A7A!important;"></span>');
            $(this).closest('ul').closest('ul').closest('div').closest('.thumbnail').find('img').attr('data-visibility-val', 3);

        }
        if ($(this).closest('ul').hasClass('change_img_vis')) {
            $.ajax({
                url : 'media/hide/'+ data_id.trim(),
                type : 'put',
                data : {
                    visible_id : visible
                },
                success : function(html) {
                    $(self).closest('ul').closest('ul').closest('div').siblings('.book').find('i')
                    .removeClass('public_icon_color')
                    .removeClass('shared_icon_color')
                    .removeClass('private_icon_color')
                    .addClass(color)
                    .attr('data-original-title', title)
                    notification( lang.hide_success, 'bar', 'slidetop', 'notice', timer);
                    
                    $(self).closest('ul').closest('ul').closest('div').closest('.subject_image')
                    .removeClass('public')
                    .removeClass('shared')
                    .removeClass('private')
                    .addClass(title);
                },
                error : function(err, res, val) {
                }
            });
        } else {
            $(self).closest('ul').closest('ul').closest('div').siblings('.book').find('i')
            .removeClass('public_icon_color')
            .removeClass('shared_icon_color')
            .removeClass('private_icon_color')
            .addClass(color)
            .attr('data-original-title', title);

            $(self).closest('ul').closest('ul').closest('div').closest('.subject_image')
            .removeClass('public')
            .removeClass('shared')
            .removeClass('private')
            .addClass(title);
        }
    })

    $(document).on('click', '.level_image',function(e) {
        media_id = $(this).attr('data-id');
        subject_id = $(this).attr('subject_id');
        level_val = $(this).attr('data-level_val');
        og_level_val = $(this).attr('data-og-level-val');
        og_icon =  $(this).find('i').attr('data-og-level-icon');
        inv_icon =  $(this).find('i').attr('data-inv-level-icon');
        var self = $(this);
        $.ajax({
            url : "media/level/"+ media_id.trim(),
            type : 'put',
            data : {
                level_val : level_val
            },
            success : function(html) {
                getSubjectImage(subject_id)
                
                notification(lang.set_album_image_success, 'bar', 'slidetop', 'notice', timer);
            },
            error : function(err) {
                
            }
        });
    })

    $(document).on('click', '.date_image, .date_image_temp', function() {
        var self = $(this);
        media_id = $(self).attr('data-id')
        html =  "<div class='input-group date' id='datetimepicker1'>"
          html += '<div id="datepicker" class="margin_left_85" data-date=""> </div>'
        html += '</div>'


        bootbox.dialog({
            message: html,
            title: "Add date for your image",
            onEscape: function() {

            },
            show: true,
            backdrop: true,
            closeButton: true,
            animate: true,
            className: "image_datepicker"
        }).find("div.modal-dialog").addClass("confirmWidth_30");
        
        $('#datepicker').datepicker();
        $('#datepicker').on('changeDate', function(ev){
            var date = $('#datepicker').datepicker("getDate");
            date = moment(date).format('YYYY-MM-DD');
            $(self).html('<i class="fa fa-calendar"></i>&nbsp;Tag Date ('+date+')')
            $(self).attr("data-date", date);
            if ($(self).hasClass('date_image')) {
                $.ajax({
                    url : "media/date/"+ media_id.trim(),
                    type : 'put',
                    data : {
                        date : date
                    },
                    success : function(html) {
                        
                        notification(lang.image_tag_date_success, 'bar', 'slidetop', 'notice', timer);
                    },
                    error : function(err) {
                        
                    }
                });
            } else {
                $(self).closest('.thumbnail').find('img').attr('data-date_added', date)
            }
            $('.bootbox-close-button').trigger('click');
        });
    })

    var mouseX = "";
    var mouseY = "";
    $(document).on('click', '.thumbnail img', function(e) {
        var imgtag = $(this).parent(); // get the div to append the tagging list
        mouseX = ( e.pageX - $(imgtag).offset().left ) - 5; // x and y axis
        mouseY = ( e.pageY - $(imgtag).offset().top ) - 10;
       
        $( '#tagit' ).remove( ); // remove any tagit div first
        html = '<div id="tagit">'
            html += '<div class="tagit_box">'
            html += '</div>'
            html += '<div style="" class="name">'
                html += '<input type="text" name="txtname" id="tagname" placeholder = "Enter Tag Name"/>'
            html += '</div>'
        html += '</div>'
        
        $( imgtag ).append(html);
        $( '#tagit' ).css({ top : mouseY, left:mouseX });

        $('#tagname').focus();
    });

    $(document).on('keyup', '#tagname', function(e) {
        if (e.keyCode == 13) {
            
            var tag = $('#tagname').val();
            var tag_html = $('.tag_box_hidden').html();

            

            var self  = $(this);
            
            var curr_tag_box = $(this).closest('#tagit').closest('.thumbnail').find('.tag_box');
            var img = $(this).closest('#tagit').closest('.thumbnail').find('img');
            $(this).closest('#tagit').closest('.thumbnail').find('.tag_box').append(tag_html);
           
            $('#tagit').addClass("hide");
            $('#tagit').remove();
            $('#tagname').val("");
            
            $(curr_tag_box).find('.tag_box_div:last-child .tag_box_text').prepend(tag);

            

            $(curr_tag_box).find('.tag_box_div:last-child .tag_box_text').attr('data-val', tag);
            $(curr_tag_box).find('.tag_box_div:last-child .tag_box_text').attr('data-x-cordinate', mouseX);
            $(curr_tag_box).find('.tag_box_div:last-child .tag_box_text').attr('data-y-cordinate', mouseY);
            // slimScrollInit('.tag_box', '76px');
           
            $('.tag_box_div').not('.tag_box div:last-child').removeClass('hide').css('width', ((tag.length)+1) + 'em');
            $('.tag_box_text').attr('title',tag);
            
            
            if ($(img).hasClass('tag_dyn')) {
                $.ajax({
                    url : 'tags',
                    type : 'post',
                    data : {
                        level_val : $(img).attr('data-level_val'),
                        media_id : $(img).attr('data-media_id'),
                        subject_id : $('#subject_select').val(),
                        chapter_id : $('#chapter_select').val(),
                        page_id : $('#page_select').val(),
                        tag_name : tag,
                        x_cordinate : mouseX,
                        y_cordinate : mouseY

                    },
                    success: function(res) {
                        
                        
                        $(self).closest('.thumbnail').find('#tagit').remove();
                        $(curr_tag_box).find('.tag_box_div:last-child .tag_box_text span')
                        .removeClass('remove_tag_temp')
                        .addClass('remove_tag')
                        .attr('data-tag_id', res.tag_id);

                        notification( lang.tag_saved_success, 'bar', 'slidetop', 'notice', timer);
                    }

                }) 
            }
        }
    });

    $(document).on('keyup', '.page_caption', function(e) {
        if (e.keyCode == 13) {
          
           
            
            if ($(this).hasClass('cap_dyn')) {
                $.ajax({
                    url : 'media/caption/'+$(this).attr('data-media_id'),
                    type : 'put',
                    data : {
                        caption : $(this).val()

                    },
                    success: function(res) {
                        
                        notification( "caption saved succesfully", 'bar', 'slidetop', 'notice', timer);
                    }

                }) 
            }
        }
    });


    // mouseover the tagboxes that is already there but opacity is 0.
    $( document).on( 'mouseover', '.tag_box_text', function( ) {
        var left = $(this).attr('data-x-cordinate');
        var top = $(this).attr('data-y-cordinate');
        $(this)
        .closest('.tag_box')
        .closest('.thumbnail')
        .append('<div id="tagit" style="top: '+top+'px; left: '+left+'px;"><div class="tagit_box"></div></div>')
        
    }).on( 'mouseout', '.tag_box_text', function( ) {
       
       $(this).closest('.tag_box').closest('.thumbnail').find('#tagit').remove()
    });

    $(document).on('click', '.remove_tag_temp, .remove_tag', function() {
        if ($(this).hasClass('remove_tag')) {
            $.ajax({
                url : 'tags/'+$(this).attr('data-tag_id'),
                type : 'delete',
                success: function(res) {
                    $('#tagit').remove();
                    // notification( lang.tag_delete_success, 'bar', 'slidetop', 'notice', timer);
                }

            })
        }
        $(this).closest('.tag_box_div').remove();
                    notification( lang.tag_delete_success, 'bar', 'slidetop', 'notice', timer);
        
    })


    

    $(document).on('click', '.inner-close', function() {
        window.location.reload(false);
        startLoader('sk-circle', 'Loading...');
        stopLoader();

    });

    function initForm(type, url, form_id) {
        options.type = type;
        options.url = url;
        options.form = form_id;
        
        
        $('#'+form_id).ajaxForm(options);
    }

    function initFormForChapter(type, url, form_id) {
        option_chapter.type = type;
        option_chapter.url = url;
        option_chapter.form = form_id;
        
        
        $('#'+form_id).ajaxForm(option_chapter);
    }

    function initFormForPage(type, url, form_id) {
        option_page.type = type;
        option_page.url = url;
        option_page.form = form_id;
        
        
        // $('.page_content').focus();
        $('#'+form_id).ajaxForm(option_page);
    }


    function afterSubjectSave() {
        if (options.type == "post") {
            $('.subject_collapse_box').addClass('collapsed-box');
            $('.common').removeClass('fa-minus');
            $('.common').addClass('fa-plus');
            $('.subject_box_body').css('display','none');
        }
    }

    function afterChapterSave() {
        // code for chapter body and adding plus sign
        if (option_chapter.type == "post") {
            $('.chapter_collapse_box').addClass('collapsed-box');
            $('.common').removeClass('fa-minus');
            $('.common').addClass('fa-plus');
            $('.chapter_box_body').css('display','none');
        }
    }
    
    
    var options = {
        type: "",
        url: '',
        datatype: "json",

        beforeSerialize: function ($form, options) {
            // startLoader('sk-circle');
        },
        beforeSubmit: function (arr, $form, options) {
            
            var obj_front = {};
            var obj_back = {};

            if ($form[0].id == "subject_data_form") {
                if ($('#subject_select').val() != "add_new") {
                    options.type = "put";
                    options.url = "subject/"+$('#subject_select').val();

                }

                // $('.subject_image_handler').find('img[data-level_val=1]').each(function(e, i) { 
                //     obj_front[$(i).attr('data-media_id')] = (e+1)
                // });
                // arr.push({name : "front_sequence", value : JSON.stringify(obj_front)});

                // $('.subject_image_handler').find('img[data-level_val=2]').each(function(e, i) {
                //     obj_back[$(i).attr('data-media_id')] = (e+1)
                // });
                // arr.push({name : "back_sequence", value : JSON.stringify(obj_back)});

                $('.subject_image_handler').find('img').each(function(e, i) {
                    obj_back[$(i).attr('data-media_id')] = (e+1)
                });
                arr.push({name : "sequence_order", value : JSON.stringify(obj_back)});


                arr.push({name : "visible_id", value :  $('.sub_visibility.active') .attr('data-vis-value') });
                
            }
            
            startLoader('sk-circle');
            return $("#subject_data_form").valid();
        },
        success: function (response) {
           
            if (f_uploader.files.length > 0) {
                f_uploader.bind('BeforeUpload', function(up, file) {
                    
                    // $('#subject_image_handler div img').map(function(j,i) { 
                    //     if ($(i).attr('data-file_id') == file.id) {
                    //         $(document).find("[data-file_id='" + file.id + "']").attr('data-sort-id', (j + 1));
                    //         console.log("file ", file.id);
                    //         console.log("before ", $(document).find("[data-file_id='" + file.id + "']").attr('data-sort-id'));
                    //         console.log("image_name ", $(document).find("[data-file_id='" + file.id + "']").attr('data-image-name'));
                    //     } 
                    // }).get();
                    
                    var temp = [], obj = [];
                    // console.log("file ", file.id)
                    // console.log("after ", $(document).find("[data-file_id='" + file.id + "']").attr('data-sort-id'));
                    // console.log("image_name ", $(document).find("[data-file_id='" + file.id + "']").attr('data-image-name'));

                    f_uploader.settings.multipart_params["subject_id"] = response.subject_id;
                    f_uploader.settings.multipart_params["visibility"] =  $(document).find("[data-file_id='" + file.id + "']").attr('data-visibility-val');
                    f_uploader.settings.multipart_params["file_size"] =  $(document).find("[data-file_id='" + file.id + "']").attr('data-media-size');
                    f_uploader.settings.multipart_params["sequence_order"] =  $(document).find("[data-file_id='" + file.id + "']").attr('data-sort-id');
                    f_uploader.settings.multipart_params["date_added"] =  $(document).find("[data-file_id='" + file.id + "']").attr('data-date_added');
                    $(document).find("[data-file_id='" + file.id + "']")
                    .closest('.thumbnail')
                    .find('.tag_box')
                    .find('.tag_box_div')
                    .find('.tag_box_text')
                    .each(function(i, item) {
                        temp.push($(item).attr('data-val'));
                        temp.push($(item).attr('data-x-cordinate'));
                        temp.push($(item).attr('data-y-cordinate'));
                        
                    
                    });

                    f_uploader.settings.multipart_params["tags"] = JSON.stringify(temp);
                });
                f_uploader.start();
                
            }

            if (b_uploader.files.length > 0) {

                b_uploader.bind('BeforeUpload', function(up, file) {
                
                // $('#subject_image_handler div img').map(function(j,i) { 
                //     if ($(i).attr('data-file_id') == file.id) {
                //         $(document).find("[data-file_id='" + file.id + "']").attr('data-sort-id', (j + 1));
                //     } 
                // }).get();
            

                b_uploader.settings.multipart_params["subject_id"] = response.subject_id;
                b_uploader.settings.multipart_params["visibility"] =  $(document).find("[data-file_id='" + file.id + "']").attr('data-visibility-val');
                b_uploader.settings.multipart_params["file_size"] =  $(document).find("[data-file_id='" + file.id + "']").attr('data-media-size');
                b_uploader.settings.multipart_params["sequence_order"] =  $(document).find("[data-file_id='" + file.id + "']").attr('data-sort-id');
                b_uploader.settings.multipart_params["date_added"] =  $(document).find("[data-file_id='" + file.id + "']").attr('data-date_added');
                temp = [];
                $(document).find("[data-file_id='" + file.id + "']")
                .closest('.thumbnail')
                .find('.tag_box')
                .find('.tag_box_div')
                .find('.tag_box_text')
                .each(function(i, item) {
                    temp.push($(item).attr('data-val'));
                    temp.push($(item).attr('data-x-cordinate'));
                    temp.push($(item).attr('data-y-cordinate'));
                    
                });
                b_uploader.settings.multipart_params["tags"] = JSON.stringify(temp);
                
                });
                b_uploader.start();
            }

            var front_count = f_uploader.files.length;
            var back_count = b_uploader.files.length;
            var final_count = front_count + back_count;
            var total_uploaded_count = 0;

            b_uploader.bind('UploadComplete', function() {

                total_uploaded_count += back_count;

                if (total_uploaded_count == final_count) {
                    getSubjectData(response.subject_id);
                    getSubjectImage(response.subject_id); 
                    stopLoader();

                    afterSubjectSave();
                    console.log('back uploader');
                    notification( lang.subject_saved_success, 'bar', 'slidetop', 'notice', timer);
                   
                }
            });
                
            f_uploader.bind('UploadComplete', function() {
                total_uploaded_count += front_count;
                if (total_uploaded_count == final_count) {
                    getSubjectData(response.subject_id);
                    getSubjectImage(response.subject_id);
                    stopLoader();

                    afterSubjectSave();
                    console.log('front uploader');
                    notification( lang.subject_saved_success, 'bar', 'slidetop', 'notice', timer);
                    
                }

            });

            if (back_count == 0 && front_count == 0) {
                getSubjectData(response.subject_id);
                getSubjectImage(response.subject_id);          
                stopLoader();
                afterSubjectSave();

                console.log("back front 0");
                notification( lang.subject_saved_success, 'bar', 'slidetop', 'notice', 3000);
                // $('.chapter_content').focus();
                
            }
            // $('.chapter_content').focus();
            // notification(lang.chapter_operation_steps, 'growl', 'genie', 'notice', 15000);

        },
        statusCode: {
            400: function (xhr, ajaxOptions, thrownError) {
                // .showErrors(xhr.responseJSON.errors);
            },
            404: function (xhr, ajaxOptions, thrownError) {
                // 
            },
            500: function (xhr, ajaxOptions, thrownError) {
                // bootstrap_alert_danger(xhr.responseJSON.error);
            }
        },
        complete: function () {

        },
        error : function(e) {
            stopLoader();
        }

    };

    var option_chapter = {
        type: "",
        url: '',
        datatype: "json",

        beforeSerialize: function ($form, options) {
            
           
        },
        beforeSubmit: function (arr, $form, options) {
            // 
            // $('#subject_data_form').validator('validate')
            if ($form[0].id == "chapter_data_form") {
                arr.push({name : "visible_id", value :  $('.chp_visibility.active') .attr('data-vis-value') });
            }

            startLoader('sk-circle');
            return $('#chapter_data_form').valid();
        },
        success: function (response) {
            
            if (option_chapter.type == "put") {
                // getChapterData(response.subject_id, response.chapter_id)
                $("#chapter_select option[value='"+response.chapter_id+"']").remove();
                chosenUpdate();
                $('#chapter_select > optgroup:eq(1)').append('<option value="'+response.chapter_id+'">'+response.chapter_name+'</option>');
                chosenUpdate();
                $('#chapter_select').val(response.chapter_id);
                chosenUpdate();
                $('.chapter_content').val(response.chapter_name);
                var chapter_title = $('.chapter_section').find('.chapter_content').val();
                // console.log(chapter_title);
                chosenUpdate();
                $('.album_chapter_title').html('Memories Album Chapter - &nbsp;'+chapter_title );
                $('.album_chapter_title').addClass('album_title');
                 
                
            } else {
                
                
                getChapterData(response.subject_id, response.chapter_id)
                getPageData(response.subject_id, response.chapter_id, "add_new")
                $('#chapter_select > optgroup:eq(1)').append('<option value="'+response.chapter_id+'">'+response.chapter_name+'</option>');
                chosenUpdate();
                $('#chapter_select').val(response.chapter_id);
                chosenUpdate();
            }
            stopLoader();
            afterSubjectSave();
            afterChapterSave();

            $('.subject_collapse_box').addClass('collapsed-box');
            $('.common').removeClass('fa-minus');
            $('.common').addClass('fa-plus');
            $('.subject_box_body').css('display','none');

            $('#page_content').summernote({
                // focus: true,
            });

            
            

            
        },
        statusCode: {
            400: function (xhr, ajaxOptions, thrownError) {
                // .showErrors(xhr.responseJSON.errors);
            },
            404: function (xhr, ajaxOptions, thrownError) {
                // 
            },
            500: function (xhr, ajaxOptions, thrownError) {
                // bootstrap_alert_danger(xhr.responseJSON.error);
            }
        },
        complete: function () {
            // notification(lang.chapter_operation_steps, 'growl', 'genie', 'notice', 15000);
            
            notification( lang.chapter_save_success, 'bar', 'slidetop', 'notice', timer);

        },
        error : function(e) {
            stopLoader();
            notification( lang.page_operation_steps, 'growl', 'genie', 'notice', 10000);
            
        }

    };

    var option_page = {
        type: "",
        url: '',
        datatype: "json",

        beforeSerialize: function ($form, options) {
            
                       
        },
        beforeSubmit: function (arr, $form, options) {
            // 
            // $('#subject_data_form').validator('validate')
            if ($form[0].id == "page_data_form") {
                arr.push({name : "visible_id", value :  $('.page_visibility.active').attr('data-vis-value') });
                
            }
            var obj_page = {};
            var page_sequence = {};

            $('#page_image_handler .page_image').each(function(i, e) {
                obj_page[$(e).find('.caption textarea').attr('data-media_id')] = $(e).find('.caption textarea').val()
            })
            arr.push({name : "page_caption", value : JSON.stringify(obj_page)});

            $('.page_image_handler').find('img').each(function(e, i) { 
                page_sequence[$(i).attr('data-media_id')] = (e+1)
            });
            arr.push({name : "page_sequence", value : JSON.stringify(page_sequence)});

            startLoader('sk-circle');
            return $('#page_data_form').valid();
        },
        success: function (response) {

            if (p_uploader.files.length > 0) {
                p_uploader.bind('BeforeUpload', function(up, file) {
                    
                    // $('#page_image_handler div img').map(function(j,i) { 
                    //     if ($(i).attr('data-file_id') == file.id) {
                    //         $(document).find("[data-file_id='" + file.id + "']").attr('data-sort-id', (j + 1));
                    //         console.log("file ", file.id);
                    //         console.log("before ", $(document).find("[data-file_id='" + file.id + "']").attr('data-sort-id'));
                    //     } 
                    // }).get();
                    

                    p_uploader.settings.multipart_params["page_id"] = response.page_id;
                    p_uploader.settings.multipart_params["visibility"] =  $(document).find("[data-file_id='" + file.id + "']").attr('data-visibility-val');
                    p_uploader.settings.multipart_params["file_size"] =  $(document).find("[data-file_id='" + file.id + "']").attr('data-media-size');
                    p_uploader.settings.multipart_params["sequence_order"] =  $(document).find("[data-file_id='" + file.id + "']").attr('data-sort-id');
                    p_uploader.settings.multipart_params["caption"] =  $(document).find("[data-file_id='" + file.id + "']").siblings('.caption').find('textarea').val();
                    p_uploader.settings.multipart_params["date_added"] =  $(document).find("[data-file_id='" + file.id + "']").attr('data-date_added');
                    temp = [];

                    console.log("file ", file.id)

                    console.log("after ", $(document).find("[data-file_id='" + file.id + "']").attr('data-sort-id'));
                    
                    $(document).find("[data-file_id='" + file.id + "']")
                    .closest('.thumbnail')
                    .find('.tag_box')
                    .find('.tag_box_div')
                    .find('.tag_box_text')
                    .each(function(i, item) {
                        temp.push($(item).attr('data-val'));
                        temp.push($(item).attr('data-x-cordinate'));
                        temp.push($(item).attr('data-y-cordinate'));
                        
                    });
                    
                    


                    p_uploader.settings.multipart_params["tags"] = JSON.stringify(temp);
                    
                });
                p_uploader.start();
            }

            var page_count = p_uploader.files.length;

            p_uploader.bind('UploadComplete', function() {
                stopLoader();
                // notification( lang.page_save_success, 'bar', 'slidetop', 'notice', timer);  
                getPageData(response.subject_id, response.chapter_id, response.page_id)
                getPageImage(response.page_id);
            });

            if (page_count == 0) {
                
                getPageData(response.subject_id, response.chapter_id, response.page_id)
                getPageImage(response.page_id);
                stopLoader();
                notification( lang.page_save_success, 'bar', 'slidetop', 'notice', timer);
                
            }

        },
        statusCode: {
            400: function (xhr, ajaxOptions, thrownError) {
                // .showErrors(xhr.responseJSON.errors);
            },
            404: function (xhr, ajaxOptions, thrownError) {
                // 
            },
            500: function (xhr, ajaxOptions, thrownError) {
                // bootstrap_alert_danger(xhr.responseJSON.error);
            }
        },
        complete: function () {
           
        },
        error : function(e) {
            stopLoader();
        }

    };


    function centerModal() {
        $(this).css('display', 'block');
        var $dialog = $(this).find(".modal-dialog");
        var offset = ($(window).height() - $dialog.height()) / 2;
        // Center modal vertically in window
        $dialog.css("margin-top", offset);
    }

    $('.modal').on('show.bs.modal', centerModal);
    $(window).on("resize", function () {
        $('.modal:visible').each(centerModal);
    });


// })