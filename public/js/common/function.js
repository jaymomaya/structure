function htmlDecode(input){
  var e = document.createElement('div');
  e.innerHTML = input;
  return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function disableAll(seg ,route_name) {
    var op = route_name.replace(seg + '.', '');
    
    if (op == "show") {
        $('input').prop('disabled', true);
        $('select').prop('disabled', true);
        $('textarea').prop('disabled', true);
        $('select').trigger('chosen:updated');
    }
}

 function selectToggle() {
    $(document).on('change', '.select_all', function() {
        if ($('thead tr').find('input').is(':checked')) {
            $('tbody tr').find('input').prop('checked', true);
        } else {
            $('tbody tr').find('input').prop('checked', false);
        }
    })
}

function CallCkeditor(){
    var editor = CKEDITOR.replace( 'pm_feature' ); 
    editor.on("instanceReady", function(){
        this.document.on("keyup", keyup);
    });
    function keyup() {
     $('.cke_voice_label').html('');
     var iframe = '<html dir="ltr" lang="en">';
     iframe += $('.cke_wysiwyg_frame').contents().find("html").html();
     iframe += '</html>';
     $('#full_html').val(iframe);
    }
    // var ckview = document.getElementById("pm_feature");
    // CKEDITOR.replace(ckview,{
    //     language:'en-gb',
    //     footnotesPrefix: 'a',
    //     customConfig:  'custom_config.js'
    // });
}

function chosenInit(option = "Select a option", width = '100%', deselect = true, class_name = "chosen-select") {
    $('.'+class_name).chosen({
        allow_single_deselect: deselect,
        placeholder_text_single: option,
        no_results_text: "Oops, nothing found!",
        width: width
    });
}

function datatableInitWithButtons(obj) {
    var table = $('#'+obj.table_name).DataTable({
        order: [[ obj.order.column, obj.order.mode ]],
        columnDefs: [{ 
            orderable: false, width: '1em', targets: obj.sort_disabled_targets,
        }],
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        colReorder: true,
        // stateSave: true,
        dom: "<'row'<'col-sm-12 bottom-buffer'B>>"+
            "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row'<'col-sm-12 bottom-buffer'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        
        buttons: [
            'colvis',
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
        ]
    });

    $('.dataTable').wrap('<div class="dataTables_scroll" />');

    $('.dataTables_scroll').slimscroll({
        alwaysVisible: true,
        railVisible: true,
        axis: 'x',
    });

    $('#'+obj.table_name).find('tfoot th').each(function () {
        var title = $(this).text();
        $(this).not('.no-search').html('<input type="text" class = "form-control input-sm" placeholder="Search '+title+'" />' );
    });

    table.columns().every(function() {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that.search( this.value ).draw();
            }
        });
    });

    $('.dataTables_length select').addClass('chosen-select');
    chosenInit('', '5em');
    return table;
}


function datatableInitWithNesting(obj) {
    function fnFormatDetails(table_id, html) {
        var sOut = "<table id=\"table_" + table_id + "\">";
        sOut += html;
        sOut += "</table>";
        return sOut;
    }

    var iTableCounter = 1;
    var oTable;
    var oInnerTable;
    var TableHtml;

    // Run On HTML Build
    TableHtml = $("#"+ obj.inner_table).html();


    // Insert a 'details' column to the table
    var nCloneTh = document.createElement('th');
    var nCloneTd = document.createElement('td');
    nCloneTd.innerHTML = '<img src="/img/plus.png">';
    nCloneTd.className = "center";

    $('#'+obj.outer_table).find('thead tr').each(function () {
        this.insertBefore(nCloneTh, this.childNodes[0]);
    });

    $('#'+obj.outer_table).find('tbody tr').each(function () {
        this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
    });

    //Initialse DataTables, with no sorting on the 'details' column
    var oTable = $('#'+obj.outer_table).dataTable({
        order: [[ obj.order.column, obj.order.mode ]],
        columnDefs: [{ 
            orderable: false, width: '1em', targets: obj.sort_disabled_targets,
        }],
        dom: "<'row'<'col-sm-12 bottom-buffer'B>>"+
            "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row'<'col-sm-12 bottom-buffer'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",

        buttons: [
            'colvis',
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
        ],
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        colReorder: true,
        stateSave: true,
    });

    /* Add event listener for opening and closing details
    * Note that the indicator for showing which row is open is not controlled by DataTables,
    * rather it is done here
    */
    $(document).on('click', '#'+obj.outer_table+' tbody td img' ,function () {
        var nTr = $(this).parents('tr')[0];
        if (oTable.fnIsOpen(nTr)) {
             // This row is already open - close it 
            this.src = "/img/plus.png";
            oTable.fnClose(nTr);
        }
        else {
            /* Open this row */
            this.src = "/img/minus.png";
            oTable.fnOpen(nTr, fnFormatDetails(iTableCounter, TableHtml), 'details');
            $("#table_" + iTableCounter).addClass('table').addClass('table-striped').addClass('table-bordered').addClass('nowrap').removeClass('hide');
            oInnerTable = $("#table_" + iTableCounter).DataTable({
                order: [[ obj.inner_order.column, obj.order.mode ]],
                columnDefs: [{ 
                    orderable: false, width: '1em', targets: obj.inner_sort_disabled_targets,
                }],
                dom: 
                    "<'row'<'col-sm-6'l><'col-sm-5'f>>" +
                    "<'row'<'col-sm-12 bottom-buffer'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-6'p>>",

                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                colReorder: true,

                // stateSave: true,
            });

            initDatatable();

            $("#table_" + iTableCounter+"_wrapper").css("padding", "1em");
            iTableCounter = iTableCounter + 1;

        }
    });

    function initDatatable() {
        $('.dataTable').wrap('<div class="dataTables_scroll" />');

        $('.dataTables_scroll').slimscroll({
            alwaysVisible: true,
            railVisible: true,
            axis: 'x',
        });

        $('#'+obj.outer_table).find('tfoot th').each(function () {
            var title = $(this).text();
            $(this).not('.no-search').html('<input type="text" class = "form-control input-sm" placeholder="Search '+title+'" />' );
        });

        oTable.api().columns().every(function() {
            var that = this;
     
            $( 'input', this.footer() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that.search( this.value ).draw();
                }
            });
        });

        $('.dataTables_length select').addClass('chosen-select');
        chosenInit('', '5em');
        return oTable;
    }

    initDatatable(); 
}
function handleBackendUpload(e) {
    
    var fileExt = $('.sample_file').val().split('.').pop();
    var filename = $('.sample_file').val().match(/^.*?([^\\/.]*)[^\\/]*$/)[1];
    var token = $('[name="_token"]').val();
    
    var d = new Date();
    var str = window.location.href;
    var split = str.split('/');
    var url = str.substring(7,24);
    var stor_file_name = filename+d.getTime();
    storeUploadFiles(filename, token, stor_file_name);
    
    function storeUploadFiles(filename, token, stor_file_name)
    {
        var formData = new FormData();
        var file = document.getElementById("sample_file").files;
        for(i=0;i<file.length;i++)
        {
            formData.append("file["+i+"]",file[i]);
        }
        formData.append('stor_file_name',stor_file_name);
        
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'http://'+ url +'/store-file');
        xhr.setRequestHeader("Accept", "multipart/form-data");
        xhr.onload = function (e) {
            if (xhr.readyState === 4) {
                console.log(1);
                if (xhr.status === 200) {
                    console.log(xhr.responseText.trim());
                    if(xhr.responseText.trim() === 'success') {
                        console.log(3);
                        $.ajax({
                            url: window.location.href,
                            type: 'post',
                            data: {
                                'l_event_name' : $('.event').val(),
                                'jv_purpose' : $('.purpose').val(),
                                'filename' : filename,
                                'fileExt' : fileExt,
                                'stored_name' : stor_file_name,
                                'payout_type' : $('.payout_type option:selected').val(),
                                'payout_batch' : $('.month_year').val(),
                                'from_date' : $('.from_date').val(),
                                'to_date' : $('.to_date').val(),
                                'month' : $('.bh_month').val(),
                                'prefix' : lang.prefix                                              
                            },
                            success: function(data){
                                 alert('successfully submitted')
                            }
                        });
                    }          
                } else {
                  console.error(xhr.statusText);
                }
            }
        };
        xhr.send(formData);
    }

}
function datatableInitWithButtonsAndRowGrouping(obj) {
    var table = $('#'+obj.table_name).DataTable({
        "columnDefs": [
            { "visible": false, "targets": obj.column_visibility }
        ],
        "order": [[ obj.order.column, obj.order.order_by ]],
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "displayLength": -1,
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching" : false,
        dom: "<'row'<'col-sm-12 bottom-buffer'B>>"+
            "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row'<'col-sm-12 bottom-buffer'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",

        buttons: [
            'colvis',
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
        ],
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
            var points_total = 0;
            var points_possible = 0;
            var points_earned = 0;
            api.column(obj.column_visibility, {page:'current'} ).data().each( function ( group, i ) {
                
                if ( last !== group ) {
                    rows.data().each(function(val, i) {
                        points_total += parseInt(val[3]);  
                        points_possible += parseInt(val[5]);
                        points_earned += parseInt($(val[6]).val());
                    });
                    $(rows).eq( i ).before(
                        '<tr class="group quality_check_total">'+
                            '<td colspan="2" class = "bg-grey">'+group+'</td>'+
                            '<td colspan="1" class = "bg-grey points_total">'+points_total+'</td>'+
                            '<td colspan="1" class = "bg-grey"></td>'+
                            '<td colspan="1" class = "bg-grey points_possible">'+points_possible+'</td>'+
                            '<td colspan="1" class = "bg-grey points_earned">'+points_earned+'</td>'+
                        '</tr>'
                    );
 
                    last = group;
                }
            } );
        }
    });

    $('.dataTables_length select').addClass('chosen-select');
    chosenInit('', '5em', false);
    return table;
}

function datatableInitWithButtonsRecon(obj) {
    var table = $('#'+obj.table_name).DataTable({
        order: [[ obj.order.column, obj.order.mode ]],
        columnDefs: [{ 
            orderable: false, width: '1em', targets: obj.sort_disabled_targets,
        }],
        colReorder: true,
        stateSave: true,
        pageLength: 5,
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        dom: "<'row'<'col-sm-12 bottom-buffer'B>>"+
            "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row'<'col-sm-12 bottom-buffer'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        
        buttons: [
            'colvis',
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
        ]
    });

    $('.dataTable').wrap('<div class="dataTables_scroll" />');

    $('.dataTables_scroll').slimscroll({
        alwaysVisible: true,
        railVisible: true,
        axis: 'x',
    });

    $('#'+obj.table_name).find('tfoot th').each(function () {
        var title = $(this).text();
        $(this).not('.no-search').html('<input type="text" class = "form-control input-sm" placeholder="Search '+title+'" />' );
    });

    table.columns().every(function() {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that.search( this.value ).draw();
            }
        });
    });


    return table;
}

function datatableInitWithButtonsAndDynamic(obj) {
    var table = $('#'+obj.table_name).DataTable({
        order: [[ obj.order.column, obj.order.mode ]],
        columnDefs: [{ 
            orderable: false, width: '1em', targets: obj.sort_disabled_targets,
        }],
        colReorder: true,
        // stateSave: true,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        dom: "<'row'<'col-sm-12 bottom-buffer'B>>"+
            "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row'<'col-sm-12 bottom-buffer'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",

        buttons: [
            'colvis',
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
        ],
        processing: true,
        serverSide: true,
        ajax: {
            url : obj.ajax_url
        },
        "columns": obj.column_data
    });

    $('.dataTable').wrap('<div class="dataTables_scroll" />');

    $('.dataTables_scroll').slimscroll({
        alwaysVisible: true,
        railVisible: true,
        axis: 'x',
    });

    $('#'+obj.table_name).find('tfoot th').each(function () {
        var title = $(this).text();
        $(this).not('.no-search').html('<input type="text" class = "form-control input-sm" placeholder="Search '+title+'" />' );
    });

    var state = table.state.loaded();
    if (state) {
        table.columns().eq(0).each(function (colIdx) {
            var colSearch = state.columns[colIdx].search;

            if (colSearch.search) {
                $('input', table.column(colIdx).footer()).val(colSearch.search);
            }
        });

        table.draw();
    }

    table.columns().every(function() {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that.search( this.value ).draw();
            }
        });
    });

    $('.dataTables_length select').addClass('chosen-select');
    chosenInit('', '5em');

    return table;
}



function multiSelect(obj) {
    $(obj.elem_name).multiSelect({
        keepOrder: true,
        selectableHeader: "<input type='text' class='"+obj.selectableHeader.class+" form-control' autocomplete='off' placeholder='Search "+obj.selectableFooter.name+"'><br>",
        selectionHeader: "<input type='text' class='"+obj.selectionHeader.class+" form-control' autocomplete='off' placeholder='Search "+obj.selectionFooter.name+"'><br>",
        selectableFooter: "<div class='custom-footer "+obj.selectableFooter.class+"'>"+obj.selectableFooter.name+"</div>",
        selectionFooter: "<div class='custom-footer "+obj.selectionFooter.class+"'>"+obj.selectionFooter.name+"</div>",

        afterInit: function(ms) {
            
        },
        afterSelect: obj.afterSelect,
        afterDeselect: obj.afterDeselect
        
    });

    $(document).on('keyup', '.'+obj.selectableHeader.class, function(e) {
        var filter = jQuery(this).val();
        jQuery(".ms-selectable .ms-list li").not('.ms-selected').each(function () {
            if (jQuery(this).text().search(new RegExp(filter, "i")) < 0) {
                jQuery(this).hide();
            } else {
                jQuery(this).show()
            }
        });
    });

    $(document).on('keyup', '.'+obj.selectionHeader.class, function(e) {
        var filter = jQuery(this).val();
        jQuery(".ms-selection .ms-list .ms-selected").each(function () {
            if (jQuery(this).text().search(new RegExp(filter, "i")) < 0) {
                jQuery(this).hide();
            } else {
                jQuery(this).show()
            }
        });
    });
}

function initWizard() {
    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }

    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }
    
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });

    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
    /*ends*/
}

function dateInit(class_name) {
    $('.'+class_name).datepicker({
        todayHighlight : true,
        autoclose : true,
        format : 'yyyy-mm-dd'
    });
}

function dateRangePickerInit(class_name) {
    $('.'+class_name).daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
            format: 'YYYY-MM-DD h:mm A'
        }
    });
}


function uniqArray(a) {
    var seen = {};
    var out = [];
    var len = a.length;
    var j = 0;
    for(var i = 0; i < len; i++) {
         var item = a[i];
         if(seen[item] !== 1) {
               seen[item] = 1;
               out[j++] = item;
         }
    }
    return out;
}





function handleFile(e) {
    var table = this.table;
    function toJson(workbook) {
        var result = {};
        workbook.SheetNames.forEach(function(sheetName) {
            var roa = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
            if(roa.length > 0){
                result[sheetName] = roa;
            }
        });
        return result;
    }

    function objectRename(obj){
        Object.keys(obj).forEach(function (key) {
            var k = lang.prefix + key.toLowerCase().trim().replace(/ /g,"_");
            if (k !== key) {
                obj[k] = obj[key];
                delete obj[key];
            }
        });
        return (obj);
    }


    var files = e.target.files;
    var i,f;
   for (i = 0, f = files[i]; i != files.length; ++i) {
        var reader = new FileReader();
        var name = f.name;
        reader.onload = function(e) {
            var data = e.target.result;
            var workbook = XLSX.read(data, {type: 'binary'});
            
            var first_sheet_name = workbook.SheetNames[0];
            
            var sheet = workbook.Sheets[first_sheet_name];
            var data = XLSX.utils.sheet_to_json(sheet, {header:1});
            var excel_data = [];
            if (data && data[0]) {
                for (var i = 0, L = data[0].length ; i < L; i++) {
                    var val = data[0][i].toLowerCase().trim().replace(/ /g,"_");
                    if (val != "status") {
                        excel_data[i] = val;
                    }
                }
                
                var diff = $(excel_data).not(lang.columns).get();
                if(lang.columns) {
                    var index = lang.columns.indexOf('status');
                    if (index > 0)
                    lang.columns.splice(index, 1);
                }

                
                var extra = $(lang.columns).not(excel_data).get();
                if(lang.exclude_excel_column) {
                    diff = $(diff).not(lang.exclude_excel_column).get();
                    extra = $(extra).not(lang.exclude_excel_column).get();
                }
                
                if (diff && (extra) && diff.length == 0 && (extra.length == 0)) {
                    var res = toJson(workbook);
                    var sheet1 = res[first_sheet_name];
                    if (sheet1 && sheet1.length > 0) {
                        for (var i = 0, L = sheet1.length ; i < L; i++) {
                            objectRename(sheet1[i]);
                        }
                    }

                    $.ajax({
                        url: window.location.href,
                        type: 'post',
                        data : {
                            'entity' : sheet1
                        },
                        success: function() {
                            table.draw();
                        },
                        error: function(err) {
                            
                        },
                        statusCode: ajaxStatusCode
                    })
                } else {
                    var val = "";
                    bootbox.alert(
                        '<span class = "text-danger"><strong>Invalid Format!! </strong></span><br>'+
                        '<span class = "text-danger"><strong>Please remove the following fields:</span><br>'+ 
                        diff.join(',').replace(',', '<br>') + 
                        '</strong> <br> '+
                        '<span class = "text-danger"><strong>Please add the following fields:</span><br>'+ 
                        extra.join(',').replace(',', '<br>') + 
                        '</strong> <br> '
                    );
                }
            }
            $('.excel_upload').val(null);
            

        };
        reader.readAsBinaryString(f);
    }
}

function handleLeadUpload(e) {
    var table = this.table;
    var filename = $('.upload').val().match(/^.*?([^\\/.]*)[^\\/]*$/)[1];
    
    var filename_url = window.location.href.substr(window.location.href.lastIndexOf('/') + 1).replace('-','_');
    
    if(filename !== filename_url) {
        bootbox.alert({message:'<span style="color:red;">filename does not match. required file name should be <strong style="color:black;">'+filename_url+'.xlsx</strong ></span>',callback: function () {
                location.reload();
            }
        });
    }

    function toJson(workbook) {
        var result = {};
        workbook.SheetNames.forEach(function(sheetName) {
            var roa = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
            if(roa.length > 0){
                result[sheetName] = roa;
            }
        });
        return result;
    }

    function objectRename(obj){
        Object.keys(obj).forEach(function (key) {
            var k = lang.prefix + key.toLowerCase().trim().replace(/ /g,"_");
            if (k !== key) {
                obj[k] = obj[key];
                delete obj[key];
            }
        });
        return (obj);
    }


    var files = e.target.files;
    var i,f;
    for (i = 0, f = files[i]; i != files.length; ++i) {
        var reader = new FileReader();
        var name = f.name;
        reader.onload = function(e) {
            var data = e.target.result;
            var workbook = XLSX.read(data, {type: 'binary'});

            var first_sheet_name = workbook.SheetNames[0];

            var sheet = workbook.Sheets[first_sheet_name];
            var data = XLSX.utils.sheet_to_json(sheet, {header:1});
            var excel_data = [];
            if (data && data[0]) {
                
                for (var i = 0, L = data[0].length ; i < L; i++) {
                    var val = data[0][i].toLowerCase().trim().replace(/ /g,"_");
                    if (val != "status") {
                        excel_data[i] = val;
                    }
                }
                
                var diff = $(excel_data).not(lang.columns).get();
                if(lang.columns) {
                    var index = lang.columns.indexOf('status');
                    if (index > 0)
                        lang.columns.splice(index, 1);
                }

                var extra = $(lang.columns).not(excel_data).get();
                
                if(lang.exclude_excel_column) {
                    diff = $(diff).not(lang.exclude_excel_column).get();
                    extra = $(extra).not(lang.exclude_excel_column).get();
                }
                

                // if (diff && (extra) && diff.length == 0 && (extra.length == 0)) {
                if(true){
                    var res = toJson(workbook);
                    var sheet1 = res[first_sheet_name];
                    if (sheet1 && sheet1.length > 0) {
                        for (var i = 0, L = sheet1.length ; i < L; i++) {
                            objectRename(sheet1[i]);
                        }
                    }

                    $.ajax({
                        url: window.location.href,
                        type: 'post',
                        data : {
                            'entity' : sheet1,
                            'l_event_name' : $('.event').val(),
                            'filename' : filename,
                            'cre_id' : $('.cre_ids').val(),
                            'vendor' : $('.vendor option:selected').val(),
                            'product_group' : $('.product_group option:selected').val(),
                        },
                        success: function() {
                            if(typeof lead_modal != undefined){
                                $('#lead_allocation_modal').modal('hide');
                                $.notify("Excel Uploaded","info");
                                setTimeout(function(){ 
                                    // location.reload(); 
                                }, 1000);
                                
                            }else{
                                table.draw();
                            }
                        },
                        error: function(err) {
                            
                        },
                        statusCode: ajaxStatusCode
                    })
                } else {
                    var val = "";
                    bootbox.alert(
                        '<span class = "text-danger"><strong>Invalid Format!! </strong></span><br>'+
                        '<span class = "text-danger"><strong>Please remove the following fields:</span><br>'+
                        diff.join(',').replace(',', '<br>') +
                        '</strong> <br> '+
                        '<span class = "text-danger"><strong>Please add the following fields:</span><br>'+
                        extra.join(',').replace(',', '<br>') +
                        '</strong> <br> '
                    );
                }
            }
            $('.excel_upload').val(null);


        };
        reader.readAsBinaryString(f);
    }
}

var ajaxStatusCode = {
    422: function (response) {
        bootbox.alert('<span style="color:red;">Error!!<br>Please Correct the following error:<br>'+
            getObjectValues(response.responseJSON)+
            '</span>');
    },
    200: function(response) {
        bootbox.alert('<span style="color:green;">'+
            response.message+
            '</span>');
    }
};

function getObjectValues(obj) {
    var a = [];
    $.each(obj, function(k, v) {
        a.push(v[0]);
        a.push('<br>');
    });
    var b = a.join('');
    return b;
}

function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function getActionIconAcl(actionArr) {
    var actionResult = [];
    for (var i = 0; i <= actionArr.length - 1; i++) {
        var pathArray = window.location.pathname.split( '/' );
        var segment_1 = pathArray[1];
        var segment_2 = pathArray[2];
        if(lang.permissionList && lang.permissionList[segment_1] && lang.permissionList[segment_1][segment_2] && lang.permissionList[segment_1][segment_2][actionArr[i].display_name]) {
            actionResult.push(actionArr[i]);
        }
    }
    return actionResult;
}

function appendSelectData(select, data, selected = "") {
    var s = $(select);
    $('<option />', { value: "", text: "" }).appendTo(s);
    $.each(data, function(k,v) {
        if (selected != "" && k == selected)
        $('<option />', { value: k, text: v, selected: "selected"}).appendTo(s);
        else
        $('<option />', { value: k, text: v}).appendTo(s);
    });
    $(select).trigger("chosen:updated");
}

//gayatri:
function removeSpecialCharacters(ele){
    var yourInput = $(ele).val();
    re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
    var isSplChar = re.test(yourInput);
    if(isSplChar)
    {
        var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
        $(ele).val(no_spl_char);
    }
}

function errorDisplay() {
    if(lang.errors) {
        $.each(lang.errors, function(k, v) {
            //last index of k is picked for comment reject in the event details screen.
            
            k = k.substring(k.lastIndexOf(".") + 1, k.length);
            var input = "";
            var prefix = k.split("_")[0];
            $.each(v, function(k1, v1) {
                v[k1] = v1.replace(" "+prefix+" ", " ");
            });
            
            input = $("[name='"+k+"']");
            if ($(input).closest('div').find('p').length == 0) {
                $(input).closest('div').append("<p class = 'help-block error'></p>");
                $(input).siblings('p.error').append(v.join("<br>"));
            }
            $(input).closest('div').addClass('has-error');
           
            input = "";
        });
        $(document).on('keyup', 'input', function() {
            
            $(this).closest('div').removeClass('has-error');
            $(this).siblings('p').remove();
        })
        $(document).on('change', 'select', function() {
            
            $(this).closest('div').removeClass('has-error');
            $(this).siblings('p').remove();
        })
    }
}

function queryStringToJSON(queryString) {            
    var pairs = queryString.slice(1).split('&');
    
    var result = {};
    pairs.forEach(function(pair) {
        pair = pair.split('=');
        result[pair[0]] = decodeURIComponent(pair[1] || '');
    });

    return JSON.parse(JSON.stringify(result));
}

$(document).on('keydown', '.numeric_field', function(e){
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             return;
    }
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

function datatabletooltip(table_obj, table_id = "#table"){
    
}

function generateGroundTotal(table_obj, table_id = "#table"){
    // setTimeout(function(){
        table_id = $(table_id).parent().siblings('.dataTables_scrollFoot').find('table');
        var count = $(table_id).find('.no-search_foot th').length;
        for(var i = 1; i < count; i++)
        {
            if($(table_id).find('.no-search_foot th:eq('+i+')').hasClass('ground_total'))
            {
                var total = 0;
                var column_head = $(table_obj.column(i).header()).html();
                table_obj.column(i).data().each(function(k,v){
                    if(k == null || k == '')
                    {
                        k = 0;
                    }
                    if(/<*>/.test(k) == true)
                    {
                        total = total + parseFloat($(k).text().replace(/,/g, ''));
                    }
                    else
                    {
                        total = total + parseFloat(k);
                    }
                })
                // var total = table_obj.column(i).data().sum()
                //empty the footer area before reload
                $(table_id).find('.no-search_foot th:eq('+i+')').empty();
                if(column_head == 'Due Amount' || column_head == 'Total Adjustment' || column_head == 'Net Amount' || column_head == 'Total Amount' || column_head == 'Total Paid' || column_head == 'Net Total' || column_head == 'Inr Amount' || column_head == 'Amount Paid' || column_head == 'Pending Amount' || column_head == 'Received Amount' || column_head == 'Amount Received' || column_head == 'Amount Due' || column_head == 'IMPS' || column_head == 'Cash' || column_head == 'Cheque' || column_head == 'NEFT' || column_head == 'RTGS' || column_head == 'DD' || column_head == 'CC' || column_head == 'Debit Card' || column_head == 'Online' || column_head == 'CC Avenue' || column_head == 'Total Quantity' || column_head == 'Bill Amount' || column_head == 'Outstanding' || column_head == 'Total Received Amount') {
                    $(table_id).find('.no-search_foot th:eq('+i+')').append(total.toFixed(2));      
                } else {
                    $(table_id).find('.no-search_foot th:eq('+i+')').append('');      
                }
            }
        }
        $(table_id).find('.no-search_foot th:eq(0)').empty();
        $(table_id).find('.no-search_foot th:eq(0)').append('TOTAL');      
    // }, 500)
}

function showLoading(){
    $('#divLoading').removeClass('hide');
}

$(document).on('click', '[name="save"]', function(){
    showLoading();
    
    setTimeout(function(){
        if($('.form-validate').find('.has-error').length > 0){
            console.log(111);
            stopLoading();
        }
    }, 1000);
});

stopLoading();

function stopLoading(){
    $('#divLoading').addClass('hide');   
}

function showWarning(ele, msg="Something is wrong here"){
    var warning = document.createElement('p');
    warning.setAttribute('style', 'color:red');
    warning.setAttribute('class', 'shownWarning');
    warning.append(msg);
    $(ele).parent().append(warning);
    $('.btn-success').prop('disabled', true);
}

function hideWarning(ele){
    $(ele).parent().find('.shownWarning').remove();
    $('.btn-success').prop('disabled', false);
}

$(document).on('keydown', function(e){
    if(e.keyCode == 13){
        e.preventDefault();
    }
})
$(document).on('click', '[name="save"]', function(){
    showLoader();
    setTimeout(function() { hideLoader(); }, 4000);
})
$(document).on('click', '[name="update"]', function(){
    showLoader();
    setTimeout(function() { hideLoader(); }, 4000);
})

// $(document).on('keyup','.chosen-search',function(e){
//     $('.chosen-select').html('<option></option>');
//     var this_search = $(this);
//     var search_data = "";
//     search_data = search_data + $(this).children().val();
//     var search_url = window.location.href;
//     var size = $(this).next().find('.active-result').size();
//     $.ajax({
//         url: search_url,
//         type: 'get',
//         data : {
//             'search_text' : search_data,
//             'size' : size,
//             'entity' : 'search'
//         },
//         success: function(res) {
//             $.each(res.roles, function( key, value ) {
//                 size = size + 1;
//                 $(this_search).parent().parent().prev().append('<option value='+key+'>'+value+'</option>');
//             });
//             $(this_search).parent().parent().prev().trigger('chosen:updated');

//             $(this_search).children().val(search_data);
//             // $(this).children().chosen({search_contains: true});
//         },
//         error: function(err) {

//         }
//     })
// });

// $(document).on('click', '[name="save1"]', function(){
//     showLoader();
//     setTimeout(function() { hideLoader(); }, 4000);
// })
$('#overlay').hide();
function showLoader(){
    $('#overlay').show();
    // $('#overlay').removeClass('hide');
}
function hideLoader(){
    $('#overlay').hide();
    // $('#overlay').addClass('hide');
}

function appendSelectData(select, data, selected = "") {
    var s = $(select);
    $('<option />', { value: "", text: "" }).appendTo(s);
    $.each(data, function(k,v) {
        if (selected != "" && k == selected)
        $('<option />', { value: k, text: v, selected: "selected"}).appendTo(s);
        else
        $('<option />', { value: k, text: v}).appendTo(s);
    });
    $(select).trigger("chosen:updated");
}

var input = "";
$('input').keydown(function(e){
    // ctrl +shift + c
    input = $(this);
    if(e.ctrlKey && e.shiftKey && e.which === 67) {
        ele = $(this);
        $('#CalculatorModal').modal('show');
        $('#CalculatorModal').find('input').each(function(){
            console.log($(this));
            $(this).val($(this).data('value'));
        })
    }
});

$(document).on('click','.calcuate_done',function(){
    var answer = $('#display').val();
    console.log(input);
    $(input).val(answer);
    $('#CalculatorModal').modal('toggle');

})