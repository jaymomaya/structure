$(document).ready(function() {

    $('.venue_box').slimScroll({
        height: '12em'
    });

    $('.asset_box').slimScroll({
        height: '12em'
    });

    $('.event_box').slimScroll({
        height: '12em'
    });

    $('.presenter_box').slimScroll({
        height: '12em'
    });

    $('.owner_box').slimScroll({
        height: '12em'
    });


    $(document).on('keyup', '#venue_search', function(e) {
        var filter = jQuery(this).val();
        jQuery(".venue_box div").each(function () {
            if (jQuery(this).text().search(new RegExp(filter, "i")) < 0) {
                jQuery(this).hide();
            } else {
                jQuery(this).show()
            }
        });
    });

    $(document).on('keyup', '#asset_search', function(e) {
        var filter = jQuery(this).val();
        jQuery(".asset_box div").each(function () {
            if (jQuery(this).text().search(new RegExp(filter, "i")) < 0) {
                jQuery(this).hide();
            } else {
                jQuery(this).show()
            }
        });
    });

    $(document).on('keyup', '#presenter_search', function(e) {
        var filter = jQuery(this).val();
        jQuery(".presenter_box div").each(function () {
            if (jQuery(this).text().search(new RegExp(filter, "i")) < 0) {
                jQuery(this).hide();
            } else {
                jQuery(this).show()
            }
        });
    });

    $(document).on('keyup', '#event_search', function(e) {
        var filter = jQuery(this).val();
        jQuery(".event_box div").each(function () {
            if (jQuery(this).text().search(new RegExp(filter, "i")) < 0) {
                jQuery(this).hide();
            } else {
                jQuery(this).show()
            }
        });
    });


    function ini_events(ele) {
        ele.each(function () {

            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
              title: $.trim($(this).text()),
            };

            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);

            // make the event draggable using jQuery UI
            $(this).draggable({
              zIndex: 1070,
              revert: true, // will cause the event to go back to its
              revertDuration: 0  //  original position after the drag
            });

        });
    }

    ini_events($('.venue_box div.external-event'));
    ini_events($('.asset_box div.external-event'));
    ini_events($('.presenter_box div.external-event'));
    ini_events($('.event_box div.external-event'));
    ini_events($('.event_type'));

    var arrayOfEvents = [];

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },

        //Random default events
        // events: [

        // ],
        // events: saveEvent(),
        eventRender: function(event, element) {
            
            
            element.find('.fc-content').append('<input type = "hidden" value = "'+$(this).attr('data_id')+'" name = "event['+$(this).attr('start')+']['+$(this).attr('data_attr')+']" data_attr = "'+$(this).attr('data_attr')+'" class = "hidden_attr '+$(this).attr('data_attr')+'">');
            element.find('.fc-content').append("<span class='closeon pull-right'><i class='fa fa-times' aria-hidden='true'></i></span>" );
            element.find(".closeon").click(function() {
                $('#calendar').fullCalendar('removeEvents',event._id);
            });
            element.find('.fc-event-title').append("<br/>" + event.description);
            
        },
        
        editable: true,
        eventLimit: true,
        droppable: true, // this allows things to be dropped onto the calendar !!!
        drop: function (date, allDay) { // this function is called when something is dropped
            
            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');

            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);

            // assign it the date that was reported
            
            copiedEventObject.data_id = $(this).data('id');
            copiedEventObject.data_attr = $(this).data('attr');
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;
            copiedEventObject.backgroundColor = $(this).css("background-color");
            copiedEventObject.borderColor = $(this).css("border-color");

            console.log("dropped");
            console.log(date.format('YYYY-MM-DD HH:mm:ss'));
            console.log(this.id);
            var defaultDuration = moment.duration($('#calendar').fullCalendar('option', 'defaultTimedEventDuration'));
            var end = date.clone().add(defaultDuration); // on drop we only have date given to us
            console.log('end is ' + end.format('YYYY-MM-DD HH:mm:ss'));
            

            
            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

            // renumberBlocks();

            // document.getElementById("calendarform").appendChild(input);


            // is the "remove after drop" checkbox checked?
            if ($('.drop_remove_venue').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();

            }
            if ($('.drop_remove_asset').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();
            }

        }
    });


    // $("#add-new-event").click(function (e) {
    //     e.preventDefault();
       
    //     //Get value and make sure it is not null
    //     var val = $("#new-event").val();

    //     if (val.length == 0) {
    //         return;
    //     }

    //     //Create events
    //     var event = $("<div />");
    //     var event_span = $("<span />");
    //     var event_span_i = $("<i />");
        
    //     event_span.addClass('pull-right').addClass('event_delete');
    //     event_span_i.addClass('fa').addClass('fa-times');
    //     event.addClass('bg-sharekhan').addClass("external-event");

    //     event.html(val);
    //     event.append(event_span);
    //     event_span.append(event_span_i);
    //     $('.event_box').prepend(event);

    //     //Add draggable funtionality
    //     ini_events(event);

    //     //Remove event from text input
    //     $("#new-event").val("");
    // });

    $(document).on('click', '#add-new-event', function() {
        var val = 'event-list';
        $.ajax({
            url : window.location.href + '/' +val,
            type : 'post',
            data : $('.'+val).serialize(),
            success : function(res) {
                
            }
        });
    })

    $(document).on('click', '.event_delete', function() {
        $(this).closest('.external-event').remove();
    })
        
    $(document).on('click', '.panel-heading span.clickable', function(e){
        var $this = $(this);
        if(!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideUp();
            $this.addClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        } else {
            $this.parents('.panel').find('.panel-body').slideDown();
            $this.removeClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        }
    });

    
});
