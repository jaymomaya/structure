

<?php $__env->startSection('custom-styles'); ?>
    
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
    <link href="<?php echo e(asset('/css/datatables/buttons.bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />

<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo e(ucwords(str_replace('-', ' ', Request::segment(1)))); ?> | <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

    <div class="panel">
        <div class="panel-heading ">   
            <div class = "pull-right">
                <div class="btn-group">
                    <a href="/<?php echo e(Request::path()); ?>/create">
                        <label class="btn btn-sharekhan" data-toggle="tooltip" data-target="" title="Add <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>" data-original-title="Add <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>"><i class="fa fa-plus"></i> Add <?php echo e(ucwords(str_replace('-', ' ',Request::segment(2)))); ?></label>
                    </a>
                </div>
            </div>
        </div>
         
        <div class="panel-body padding_top_3">
            <table id="table" class="datatable table display compact table-striped table-bordered  hover nowrap" cellspacing="0" role="grid" width="100%">
                <thead>
                    <tr>
                        <?php $__currentLoopData = $data['columns']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $column): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <th class = "thead_<?php echo e($column); ?>"><?php echo e(ucwords(str_replace('_', ' ', $column))); ?></th>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <?php $__currentLoopData = $data['columns']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $column): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(in_array($key ,$data['disable_footer_search'])): ?>
                                <th class = "tfoot_<?php echo e($column); ?> no-search"> <?php echo e(ucwords(str_replace('_', ' ', $column))); ?> </th>
                            <?php else: ?>
                                <th class = "tfoot_<?php echo e($column); ?>"> <?php echo e(ucwords(str_replace('_', ' ', $column))); ?> </th>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tr>
                </tfoot>
            </table>
        </div>
        
    </div>


    <div class="modal fade modal-fullscreen force-fullscreen" id="variation_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog Modal_body">
            <div class="modal-content border_radius " >
                <div class="modal-header bg-sharekhan border_radius_top clearfix" style="z-index: 1">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Variations</h4>
                </div>
            
                <div class="modal-body ">
                    <div class="col-md-12" style="padding-top: 5%;">  
                        <table id="table_od">
                            <thead>
                                <tr>
                                    <th>Item Name</th>
                                </tr>
                            </thead>
                                    
                            <tbody class="variation_body">
                                
                            </tbody>
                        </table>
                    </div>
                    </div>
                <div class="modal-footer">
                    <button type="button" id="#close_pd" class="btn btn-sharekhan" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['columns'] = $data['columns'];
        $js_data['pk'] = $data['pk'];
        $js_data['prefix'] = $data['prefix'];
        $js_data['status'] = config('constants.STATUS');
        $js_data['color'] = config('constants.COLOR_GENERAL');

        $js_data['columns_item_variations'] = $data['columns_item_variations'];
        $js_data['pk_item_variations'] = $data['pk_item_variations'];
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>

    <?php echo $__env->make('layouts.script_loaders.datatable_global', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo e(asset('/js/notify.js')); ?>"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            
            // initialize datatables
            var view = {
                display_name : "view",
                class : "view",
                title : "View",
                url : "/",
                href : true,
                i : {
                    class : "fa fa-eye width_icon clr-sk"
                }
            };
            var edit = {
                display_name : "edit",
                class : "edit",
                title : "Edit",
                url : "/edit",
                href : true,
                i : {
                    class : "fa fa-pencil width_icon clr-sk"
                }
            };
            var remove = {
                display_name : "delete",
                class : "delete",
                title : "Delete",
                url : "/",
                href : false,
                i : {
                    class : "fa fa-trash width_icon clr-sk"
                }
            };

            var actionArr = [view, edit, remove];
            // var actionResult = getActionIconAcl(actionArr);
            var action_obj = actionArr;

            var datatable_object = {
                table_name : $('table').attr('id'),
                order : {
                    state: false,
                    column : 1,
                    mode : "desc"
                },
                buttons : {
                    state : true,
                    colvis : true,
                    excel : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    },
                    pdf : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    },
                    csv : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    },
                    copy : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    }
                },
                info : true,
                paging : true,
                searching : true,
                ordering : true,
                iDisplayLength: 10,
                sort_disabled_targets : [],
                ajax_url : window.location.href,
                render: $.fn.dataTable.render.text(),
                column_data : datatableColumn(lang.columns, action_obj, lang.pk),
            }

            table = datatableInitGeneral(datatable_object);            
           
            $(document).on('click', '.delete', function() {
                var curr_url = $(this).attr('data-url');
                var row = $(this);
                bootbox.confirm("Are you sure you want to close this Record ?", function(result) {
                if (result) {
                   $.ajax({
                        url: curr_url,
                        type: 'DELETE',
                        data: {
                            entity : result
                        },
                        success: function(result) {
                            table.draw();
                        },
                        statusCode: ajaxStatusCode
                       });
                    }
                }); 
            });



            $(document).on('click', '.variations', function() { 
                var group_id = $(this).attr('data-value');
                $.ajax({
                        url: '/masters/item-master/variation-details',
                        type: 'GET',
                        data: {
                            entity : 'variation_details',
                            group_id : group_id,
                        },
                        success: function(result) {
                            console.log(result);
                            $('.variation_body').html('');

                            $.each(result, function (index, value) {
                                $('.variation_body').append('<tr>');
                                $('.variation_body').append('<td>'+value.im_name+'</td');
                                $('.variation_body').append('</tr>');
                                
                            });

                        },
                    });
                        
                /*var group_id = $(this).attr('data-value');
                var action_obj = [];
                var datatable_object2 = {
                    table_name : 'table_od',
                    order : {
                        state: false,
                        column : 1,
                        mode : "asc"
                    },
                    buttons : {
                        state : true ,
                        colvis : true,
                        excel : {
                            state: true,
                            columns_pd: [$('thead th:not(".thead_action")')] 
                        },
                        pdf : {
                            state: true,
                            columns_pd: [$('thead th:not(".thead_action")')] 
                        },
                    },
                    info : true,
                    paging : true,
                    searching : true,
                    ordering : true,
                    iDisplayLength: 10,
                    sort_disabled_targets : [],
                    ajax_url : window.location.href,
                    ajax_data : {
                        'entity' : 'variation_details',
                        'group_id' : group_id,
                    },
                    column_data : datatableColumn(lang.columns_item_variations, action_obj, lang.pk_item_variations),   
                }
                    table_od = datatableInitGeneral(datatable_object2);*/
            });

          
            
        });
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>