

<?php $__env->startSection('custom-styles'); ?>
    
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
    <link href="<?php echo e(asset('/css/datatables/buttons.bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />

<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo e(ucwords(str_replace('-', ' ', Request::segment(1)))); ?> | <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

    <div class="panel">
        <div class="panel-heading ">   
            <ul class="nav nav-tabs">
                <li class = "im_tabs inventory_master"><a href="?type=inventory-master">Inventory Master</a></li>
                <li class = "im_tabs inventory_master_report"><a href="?type=inventory-master-report">Inventory Master Report</a></li>
            </ul>
            <div class = "pull-right">
                <div class = "btn-group">
                    <label class="btn btn-sharekhan btn-file all_only upload_backend"  data-toggle="tooltip" data-target="" title="Upload Lead" data-original-title="Add <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>">
                    <i class="fa fa-upload" aria-hidden="true"></i> Upload Excel
                       <!-- <button class="incremental_upload"></button> -->
                    </label>
                </div>
                <div class="btn-group">
                    <a href="/<?php echo e(Request::path()); ?>/create">
                        <label class="btn btn-sharekhan add_inventory" data-toggle="tooltip" data-target="" title="Add <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>" data-original-title="Add <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>"><i class="fa fa-plus"></i> Add <?php echo e(ucwords(str_replace('-', ' ',Request::segment(2)))); ?></label>
                    </a>
                </div>
            </div>
        </div>
        <div class="panel-body padding_top_3">
            <table id="role" class="datatable table display compact table-striped table-bordered  hover nowrap" cellspacing="0" role="grid" width="100%">
                <thead>
                    <tr>
                        <?php $__currentLoopData = $data['columns']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $column): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <th class = "thead_<?php echo e($column); ?>"><?php echo e(ucwords(str_replace('_', ' ', $column))); ?></th>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <?php $__currentLoopData = $data['columns']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $column): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(in_array($key ,$data['disable_footer_search'])): ?>
                                <th class = "tfoot_<?php echo e($column); ?> no-search"> <?php echo e(ucwords(str_replace('_', ' ', $column))); ?> </th>
                            <?php else: ?>
                                <th class = "tfoot_<?php echo e($column); ?>"> <?php echo e(ucwords(str_replace('_', ' ', $column))); ?> </th>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tr>
                </tfoot>
            </table>
        </div>
        
    </div>
<?php echo Form::open(array('files'=>'true','id' => 'master_upload')); ?>

    <div id = "upload_backend_modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content border_radius">
                <div class="modal-header bg-sharekhan border_radius_top">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Upload Excel</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="excel_id" id="excel_id">
                     <?php echo Form::file('sample_file', array('class' => 'sample_file form-control', 'id' => 'sample_file')); ?>

                </div>
                <div class="modal-footer">
                     <input name="upload-file" id="upload-form" type="button" value="SUBMIT" class="btn btn-sharekhan btn-file"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['columns'] = $data['columns'];
        $js_data['pk'] = $data['pk'];
        $js_data['prefix'] = $data['prefix'];
        $js_data['status'] = config('constants.STATUS');
        $js_data['color'] = config('constants.COLOR_GENERAL');
        $js_data['screen_name'] = $data['screen_name'];
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>

    <?php echo $__env->make('layouts.script_loaders.datatable_global', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo e(asset('/js/notify.js')); ?>"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            
            // initialize datatables
            var view = {
                display_name : "view",
                class : "view",
                title : "View",
                url : "/",
                href : true,
                i : {
                    class : "fa fa-eye width_icon clr-sk"
                }
            };
            var edit = {
                display_name : "edit",
                class : "edit",
                title : "Edit",
                url : "/edit",
                href : true,
                i : {
                    class : "fa fa-pencil width_icon clr-sk"
                }
            };
            var remove = {
                display_name : "delete",
                class : "delete",
                title : "Delete",
                url : "/",
                href : false,
                i : {
                    class : "fa fa-trash width_icon clr-sk"
                }
            };

            var actionArr = [view, edit, remove];
            // var actionResult = getActionIconAcl(actionArr);
            var action_obj = actionArr;

            var datatable_object = {
                table_name : $('table').attr('id'),
                order : {
                    state: false,
                    column : 1,
                    mode : "desc"
                },
                buttons : {
                    state : true,
                    colvis : true,
                    excel : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    },
                    pdf : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    },
                    csv : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    },
                    copy : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    }
                },
                info : true,
                paging : true,
                searching : true,
                ordering : true,
                iDisplayLength: 10,
                sort_disabled_targets : [],
                ajax_url : window.location.href,
                column_data : datatableColumn(lang.columns, action_obj, lang.pk),
            }

            table = datatableInitGeneral(datatable_object);            
            var param = getParameterByName('type');
            if(param == 'inventory-master-report')
            {
                $('.im_tabs').removeClass('active');
                $('.inventory_master_report').addClass('active');
                $('.add_inventory').addClass('hide');
            }
            else
            {
                $('.im_tabs').removeClass('active');
                $('.inventory_master').addClass('active');
                
            }

            $(document).on('click','.upload_backend',function(){
                $('#upload_backend_modal').modal('show');
            })

           $('#upload-form').on('click', function(e){
                e.preventDefault();
                handleBackendUpload(e);
            });
        });
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>