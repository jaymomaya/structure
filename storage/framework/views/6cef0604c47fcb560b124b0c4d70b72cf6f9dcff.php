<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<script src="<?php echo e(asset('/plugins/jQuery/jQuery-3.3.1.min.js')); ?>"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="<?php echo e(asset('/js/bootstrap.min.js')); ?>" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="<?php echo e(asset('/js/app.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('/js/demo.js')); ?>" type="text/javascript"></script>

<script src="<?php echo e(asset('/js/slimScroll/jquery.slimscroll.min.js')); ?>"></script>
<script src="<?php echo e(asset('/js/fastclick/fastclick.js')); ?>"></script>
<script src="<?php echo e(asset('/js/smoothscroll.js')); ?>"></script>


<script src="<?php echo e(asset('/js/initial_loading.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('/js/common/script.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('/js/common/function.js')); ?>" type="text/javascript"></script>
<!-- <script src="<?php echo e(asset('/js/common/datatable_function.js')); ?>" type="text/javascript"></script> -->
<script src="<?php echo e(asset('/js/common/datatable_global.js')); ?>" type="text/javascript"></script>

<!-- bootbox popup -->
<script src="<?php echo e(asset('/js/common/bootbox.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('/js/common/jquery.validate.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('/js/common/all_validation.js')); ?>" type="text/javascript"></script>
<!-- <script src="https://cdn.ckeditor.com/4.9.1/standard/ckeditor.js"></script> -->
<script src="<?php echo e(asset('/ckeditor/ckeditor.js')); ?>" type="text/javascript"></script>

<!-- <script type="text/javascript" src="<?php echo e(asset('js/materialize/materialize.min.js')); ?>"></script> -->
<!--Jashal:Adding Script For Dynamic Product Add And Company  -->


<?php
    $js_data['env'] = env('APP_ENV');
    $js_data['errors'] = (isset($errors) && count($errors) > 0) ? $errors->toArray() : [];
    $js_data['user_id'] = isset(Auth::user()->emp_id) ? Auth::user()->emp_id : '';
?>
<?php echo $__env->yieldContent('php-to-js'); ?>

<script type="text/javascript">
    var lang = lang || {};
    $.extend(true, lang, <?php echo json_encode($js_data); ?>);
    errorDisplay();
    
</script>




<!-- Optionally, you can add Slimscroll and FastClick plugins.
Both of these plugins are recommended to enhance the
user experience. Slimscroll is required when using the
fixed layout. -->

<?php echo $__env->yieldContent('custom-scripts' ); ?>