<?php $__env->startSection('custom-styles'); ?>
  
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link href="<?php echo e(asset('/css/common/bootstrap-switch.min.css')); ?>" media="screen" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/css/common/bootstrap-datepicker.css')); ?>" />
    <link href="<?php echo e(asset('/css/common/chosen.min.css')); ?>" rel="stylesheet" type="text/css" />

<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo $__env->make('layouts.custom_partials.htmlheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    
    <?php echo $__env->make('layouts.custom_partials.contentheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<?php if(Request::segment(4)): ?>
    <?php echo Form::model($data, ['class' => 'form-validate', 'url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey(), Request::segment(4)], 'method' => 'put','enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php else: ?>
    <?php echo Form::open(['class' => 'form-validate', 'url' => "/".Request::segment(1)."/".Request::segment(2),'enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php endif; ?>
<?php $prefix = config('constants.QualityCheck.prefix'); ?>
<?php $prefix_qcd = config('constants.QualityCheckDetails.prefix'); ?>
<?php $prefix_gr = config('constants.GoodsReceived.prefix'); ?>

<input type="hidden" name="screen_name" value = "<?php echo e((isset($data) && isset($data['screen_name'])) ? $data['screen_name'] : ''); ?>">
<section class="panel">
    <div class="panel-body">
        <h4> <i class="fa fa-book"></i> Goods Received Detail</h4>
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label>Goods Received Number</label>
                    <input type="text" class="form-control required" id="purchase_order_num" placeholder="Purchase Order Number" value="<?php echo e(htmlValue('po_purchase_order_number', $data['po_details'])); ?>" readonly="readonly">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Goods Received Number</label>
                    <input type="text" name="<?php echo e($prefix_gr); ?>goods_received_no"  class="form-control required" id="purchase_order_num" placeholder="Purchase Order Number" value="<?php echo e(htmlValue($prefix_gr.'goods_received_no', $data)); ?>" readonly="readonly">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Vendor</label>
                    <?php echo e(Form::select('vendor_id',
                        (isset($data) && isset($data['vendor_list'])) ? $data['vendor_list'] : [],
                        htmlSelect('po_vendor_id', $data['po_details']),
                        array('name'=>$prefix.'vendor_id', 'id' => 'vendor_id', 'class' => 'form-control chosen-select required', 'data-name' => $prefix.'type', 'placeholder' => '', 'data-placeholder' => 'Select Vendor', 'disabled' => 'disabled'))); ?>

                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Goods Received Date</label>
                    <input type="text" class="form-control required" value="<?php echo e(htmlValue(($prefix_gr.'date'), $data)); ?>" readonly="readonly">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Total Quantity</label>
                    <input type="text"  class="form-control" id="total_quantity" placeholder="Total Quantity" readonly="readonly" value="<?php echo e(round(htmlValue($prefix_gr.'total_quantity', $data),2)); ?>">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Goods Received Remarks</label>
                    <input type="text"  class="form-control" id="remarks" placeholder="Remarks" value="<?php echo e(htmlValue($prefix_gr.'remarks', $data)); ?>" readonly="readonly">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="panel">
    <div class="panel-body">
        <h4> <i class="fa fa-book"></i>Quality Check Detail</h4>
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label>Quality Check Number *</label>
                    <input type="text" name="qc_quality_check_number"  class="form-control required" id="qc_quality_check_number" placeholder="QC Number" value = "<?php echo e(htmlValue('qc_quality_check_number', $data)); ?>" <?php echo e(setReadonly('qc_quality_check_number' , $data['readonly'])); ?>>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Quality Check Date *</label>
                    <input type="date" name="qc_date"  class="form-control required" id="qc_date" placeholder="Purchase Order Number" value="<?php echo e(Date('Y-m-d')); ?>">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Total Quality Pass Quantity</label>
                    <input type="text" name="qc_total_pass"  class="form-control required" id="qc_total_pass" placeholder="Total Quality Pass Quantity" value="<?php echo e(htmlValue($prefix.'purchase_order_number', $data)); ?>" readonly="readonly">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Total Quality Fail Quantity</label>
                    <input type="text" name="qc_total_fail"  class="form-control required" id="qc_total_fail" placeholder="Total Quality Fail Quantity" value="<?php echo e(htmlValue($prefix.'purchase_order_number', $data)); ?>" readonly="readonly">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Quality Check Remarks</label>
                    <input type="text" name="qc_remarks" class="form-control" placeholder="Remarks">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="panel">
    <div class="panel-body item_parent_row">
        <div class="row">
            <div class="col-md-4">
                <h4> <i class="fa fa-cubes"></i> Item Details </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label>Item</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Quantity</label>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <label>Unit</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Total Quantity Pass *</label>
                </div>
            </div>
        </div>
        <?php if(isset($data) && isset($data['item_details']) && sizeof($data['item_details'])): ?>
            <?php $__currentLoopData = $data['item_details']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="item_row">
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <?php echo e(Form::select('raw_material',
                                (isset($data) && isset($data['raw_material_list'])) ? $data['raw_material_list'] : [],
                                htmlSelect('grd_item_id', $value),
                                array('class' => 'form-control required chosen-select', 'data-name' => 'pod_item_id', 'placeholder' => '', 'data-placeholder' => 'Select Raw Material', 'disabled' => 'disabled'))); ?>

                            <input type="hidden" name="item_details[<?php echo e($value['goods_received_details_id']); ?>][qcd_item_id]" value="<?php echo e(htmlSelect('grd_item_id', $value)); ?>">
                            <input type="hidden" name="item_details[<?php echo e($value['goods_received_details_id']); ?>][qcd_id]" value="<?php echo e(htmlSelect('quality_check_details_id', $value)); ?>">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <input type="text" placeholder="Quantity" class="form-control pod_quantity required" data-name ="pod_quantity" value="<?php echo e(round(htmlValue('grd_goods_received', $value), 2)); ?>" readonly="readonly">
                            <input type="hidden" name="item_details[<?php echo e($value['goods_received_details_id']); ?>][qcd_price]" value="<?php echo e(htmlValue('pod_price', $value)); ?>">
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <?php echo e(Form::select('unit',
                                (isset($data) && isset($data['unit_master'])) ? $data['unit_master'] : [],
                                htmlSelect('pod_unit', $value),
                                array('name'=>'item_details['.$value["purchase_order_details_id"].'][pod_unit]', 'data-name' => 'pod_unit', 'class' => 'form-control chosen-select', 'placeholder' => '', 'data-placeholder' => 'Select Unit', 'disabled' => 'disabled'))); ?>

                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group has-feedback">
                            <input type="text" name="item_details[<?php echo e($value['goods_received_details_id']); ?>][qcd_fail_quantity]" placeholder="Total Quatity Fail" class="form-control required fail_quantity" data-name ="pod_total_amount" max="<?php echo e(htmlValue('grd_goods_received', $value)); ?>" readonly="readonly">
                            <i class="fa fa-thumbs-down form-control-feedback"></i>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Godown *</label>
                            <?php echo e(Form::select('raw_material',
                                (isset($data) && isset($data['godown_list'])) ? $data['godown_list'] : [],
                                htmlSelect('grd_item_id', $value),
                                array('name' => 'item_details['.$value['goods_received_details_id'].'][godown][0][qcd_godown]','class' => 'form-control required chosen-select', 'data-name' => 'qcd_godown', 'data-key' => $value['goods_received_details_id'],'placeholder' => '', 'data-placeholder' => 'Select Raw Material'))); ?>

                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group has-feedback">
                            <label>Total Quantity Pass *</label>
                            <input type="text" name="item_details[<?php echo e($value['goods_received_details_id']); ?>][godown][0][qcd_pass_quantity]" placeholder="Total Quatity Pass" class="form-control required pass_quantity" data-name ="qcd_pass_quantity" data-key="<?php echo e($value['goods_received_details_id']); ?>" max="<?php echo e(htmlValue('grd_goods_received', $value)); ?>">
                            <i class="fa fa-thumbs-up form-control-feedback"></i>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="btn pull-right btn-default add_godown_row"> Add Row</label>
                            <i class="fa fa-trash fa-lg pull-right hide delete_godown_row"></i>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
    </div>
</section>

<section class="panel">
    <div class="panel-body">
        <div class="btn-group">
            <button class="btn btn-success" name="save" value="save" type="submit" id="save_continue">Save </button>
        </div>
        <div class="btn-group pull-right">
            <a href="/<?php echo e(Request::segment(1)); ?>/<?php echo e(Request::segment(2)); ?>/"><button class="btn btn-danger" name="cancel" value="cancel" type="button" id="cancel">Cancel</button></a>
        </div>
    </div>
</section>

<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['screen_name'] = $data['screen_name'];
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>
    <?php echo $__env->make('layouts.script_loaders.datatable_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        chosenInit();
        
        $(document).on('click', '.add_godown_row', function(){
            var time = new Date().getTime();
            var parent = $(this).parent().parent().parent().clone();
            $(parent).find('input').each(function(){
                var name = $(this).data('name');
                var key = $(this).data('key');
                $(this).attr('name', 'item_details['+key+'][godown]['+time+']['+name+']');
                $(this).val('');
            });

            $(parent).find('select').each(function(k, v) {
                var n = $(v).attr('data-name');
                var key = $(v).data('key');    
                $(v).attr('name', 'item_details['+key+'][godown]['+time+']['+n+']');
                $(v).siblings('div').remove();
            });
            $(parent).find('.add_godown_row').addClass('hide');
            $(parent).find('.delete_godown_row').removeClass('hide');
            $(this).parent().parent().parent().parent().append(parent);
            chosenInit();
        })

        $(document).on('click', '.delete_godown_row', function(){
            $(this).parent().parent().parent().remove();
            $('.pass_quantity').keyup();
        })

        $(document).on('keyup blur', '.pass_quantity', function() {
            var parent = $(this).parent().parent().parent().parent();
            var qty = $(parent).find('.pod_quantity').val();
            var total = 0;
            $(parent).find('.pass_quantity').each(function(){
                var curr = parseFloat($(this).val());
                curr = (isNaN(curr) ? 0 : parseFloat($(this).val()));
                total = total + curr;
                console.log(qty, total, curr)
            });
            fQty = qty - total;
            $(parent).find('.fail_quantity').val(fQty);
            if(total > qty){
                $(parent).addClass('row-error');
                $('.btn-success').prop('disabled', true);
            } else {
                $(parent).removeClass('row-error');
                $('.btn-success').prop('disabled', false);
            }
        });

        $(document).on('keyup blur', '.fail_quantity', function() {
            var parent = $(this).parent().parent().parent().parent();
            qty = $(parent).find('.pod_quantity').val();
            pQty = qty - $(this).val();
            $(parent).find('.pass_quantity').val(pQty);
        });

        $(document).on('blur keyup', '.pass_quantity, .fail_quantity', function(){
            calculateTotalQuantity();    
        });
        
        function calculateTotalQuantity(){
            total_pass = 0;
            $('.pass_quantity').each(function(){
                total_pass += isNaN(parseFloat($(this).val())) ? 0 : parseFloat($(this).val());
            })
            $('#qc_total_pass').val(total_pass) 

            total_fail = 0;
            $('.fail_quantity').each(function(){
                total_fail += isNaN(parseFloat($(this).val())) ? 0 : parseFloat($(this).val());
            })
            $('#qc_total_fail').val(total_fail) 
        }

        $(document).on('blur keyup','#qc_quality_check_number',function(){
            var order_no = $(this).val();
            $.ajax({
                type: 'get',
                url:'/purchase-module/quality-check/check-order-number',
                data : {
                    order_no : order_no
                },
                success: function(res){
                    console.log(res);
                    if(res > 0)
                    {
                        $('#qc_quality_check_number').parent().addClass('has-error');
                        var p = "<p class = 'help-block error'>Quality Check Number exists</p>";
                        $('#qc_quality_check_number').siblings('p').remove();
                        $('#qc_quality_check_number').parent().append(p);
                        $('#save_continue').prop('disabled', true);
                        $('#save_return').prop('disabled', true);
                    } else {
                        $('#qc_quality_check_number').parent().removeClass('has-error');
                        $('#qc_quality_check_number').siblings('p').remove();
                        $('#save_continue').prop('disabled', false);
                        $('#save_return').prop('disabled', false);
                    }
                }
            });
        })
    });

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>