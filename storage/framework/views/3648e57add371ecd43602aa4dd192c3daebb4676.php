<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style>
        /*.solid {border-style: groove;}*/
        /*.item, td, th {*/
            /**/
        /*}*/
        table, tr, td, th {
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            /*border-collapse: collapse;
            border: 1px solid #000;*/
        }

        /*table + table, table + table tr:first-child th, table + table tr:first-child td {*/
            /*border-top: 0;*/
            /*border-left: hidden;*/
        /*}*/

        .item {
            width: 100%;
            border-collapse: collapse;
            /*border-collapse: collapse;
            border: 1px solid #000;*/
        }

        .item td {
            border-collapse: collapse;
            border: 1px solid #000;
            height: 30px;
            vertical-align: bottom;
        }
        .item tr,.item th {
            border-collapse: collapse;
            border: 1px solid #000; 
        }
        tfoot {
          text-align: right;
        }
        tfoot tr:last-child {
          background: #f0f0f2;
        }

    </style>
</head>
<?php $first = 0; ?>
<?php $last = 2; ?>



<body onload="window.print();" style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;">
    <table style="width: 100%;border-collapse: collapse;border: 1px solid #000;">
        <tr style="border-collapse: collapse;border: 1px solid #000;">
            <td colspan="2" style="border-collapse: collapse;border: 1px solid #000;">
                <center>
                    <div style="text-transform: capitalize;">Purchase Order Details</div>
                </center>
            </td>
        </tr>
        <tr style="border-collapse: collapse;border: 1px solid #000;">
            <td style="width: 50%;padding: 1px;border-collapse: collapse;border: 1px solid #000;">
                <span style="text-transform: capitalize; font-size: 14px;">To:<br/><?php echo e($data[0]->vm_name); ?></span><br/><?php echo e($data[0]->vm_address_line1); ?> <?php echo e($data[0]->vm_address_line2); ?><br/><?php echo e($data[0]->vm_city); ?><br/><?php echo e($data[0]->vm_state); ?><br/><?php echo e($data[0]->vm_country); ?><br/><?php echo e($data[0]->vm_pincode); ?><br/>
            </td>
            <td style="width: 50%;padding: 1px;border-collapse: collapse;border: 1px solid #000;">
                <span style="text-transform: capitalize; font-size: 14px;padding: 15px;"><b>Furniture Kraft International Pvt Ltd</b><br/><span style="text-transform: capitalize; font-size: 12px;padding: 15px;">Global Complex L/1, 16-17-18-19<br/>L/1, 16A-17A-18A-19A, Dapoda Road<br/>Ovali Village, Near Hotel Ruchita<br/>Mankoli Naka,Anjur Phata<br/>Bhiwandi , Dist. Thane - 421302</span><br/>GSTIN/UIN : 27AABCF1765H1ZO<br/>Email: sales@furniturekraft.com</span>
                <br/>
            </td>
        </tr>
        <tr style="border-collapse: collapse;border: 1px solid #000;">
            <td style="width: 50%;padding: 1px;border-collapse: collapse;border: 1px solid #000;">
                <span style="text-transform: capitalize; font-size: 14px;padding: 2px;"><b>Furniture Kraft International Pvt Ltd</b><br/><span style="text-transform: capitalize; font-size: 12px;padding: 15px;">Global Complex L/1, 16-17-18-19<br/>L/1, 16A-17A-18A-19A, Dapoda Road<br/>Ovali Village, Near Hotel Ruchita<br/>Mankoli Naka,Anjur Phata<br/>Bhiwandi , Dist. Thane - 421302</span><br/>GSTIN/UIN : 27AABCF1765H1ZO<br/>Email: sales@furniturekraft.com</span>
                <br/>
            </td>
            <td style="width: 50%;padding: 1px;border-collapse: collapse;border: 1px solid #000;">
                <span style="text-transform: capitalize; font-size: 14px;">Purchase Order No :<br/><?php echo e($data[0]->po_purchase_order_number); ?></span><br/>Date:<?php echo e($data[0]->po_date); ?><br/>Expected Delivery Date:<?php echo e($data[0]->po_exp_delivery_date); ?><br/>
            </td>
        </tr>
        <?php $total = 0; ?>
        <tr style="border-collapse: collapse;border: 1px solid #000;">
            <td colspan="3" style="border-collapse: collapse;border: 1px solid #000;">
                <table class="item" style="table-layout:fixed">
                    <tr style="text-align: right;border-top:hidden;border-left: hidden;border-right: hidden;">
                        <th>S.No.</th>
                        <th>Material Description</th>
                        <th>Due On</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                        <th>Price</th>
                        <th>Amount</th>
                    </tr>
                    <?php $__currentLoopData = $data['po_details']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr style="text-align: right;border-top:hidden;border-left: hidden;border-right: hidden;">
                            <td style="border: 1px solid;"><?php echo e($key + 1); ?></td>
                            <td> <?php echo e($value->im_name); ?></td>
                            <td> <?php echo e($data[0]->po_date); ?></td>
                            <td><?php echo e($value->um_name); ?></td>
                            <td><?php echo e($value->pod_price); ?></td>
                            <td> <?php echo e($value->pod_quantity); ?></td>
                            <td><?php echo e($value->pod_total_amount); ?></td>
                            <?php $total = $total + $value->pod_total_amount; ?>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <tr style="text-align: right;border-top:hidden;border-left: hidden;border-right: hidden;">
                        <td></td>
                        <td>Product Amount</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?php echo e($total); ?></td>
                    </tr>
                    <tr style="text-align: right;border-top:hidden;border-left: hidden;border-right: hidden;">
                        <td></td>
                        <td>Total</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?php echo e($total); ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="border-collapse: collapse;border: 1px solid #000;">
            <td colspan="2" style="border-collapse: collapse;border: 1px solid #000;">
                <div style="text-transform: capitalize;text-align: right;">Amount : <?php echo e($data['amount_words']); ?> Rupees Only/-</div>
            </td>
        </tr>
        <!-- <tr style="border-collapse: collapse;border: 1px solid #000;">
            <td colspan="2" style="border-collapse: collapse;border: 1px solid #000;">
                <span style="text-transform: capitalize; font-size: 14px;padding: 2px;">Mode Of Transport:<br/>Transporter:<br/>Bankers:<br/>Company S.T. No. :<br/>Company C.S.T. No. :<br/>Company E.C.C. No. :<br/></span>
            </td>
        </tr> -->
        <tr style="border-collapse: collapse;border: 1px solid #000;">
            <td colspan="2" style="border-collapse: collapse;border: 1px solid #000;">
                <div style="text-transform: capitalize;text-align: right;">Note : <?php echo e($data[0]->po_remarks); ?></div>
            </td>
        </tr>
        <tr style="border-collapse: collapse;border: 1px solid #000;">
            <td style="width: 50%;padding: 1px;border-collapse: collapse;border: 1px solid #000;">
                <center>
                    <div style="text-transform: capitalize;"><b>IMPORTANT INSTRUCTIONS</b></div>
                </center>
            </td>
            <td style="width: 50%;padding: 1px;border-collapse: collapse;border: 1px solid #000;">
                <div style="text-transform: capitalize;">For Furniture Kraft International Pvt Ltd</div>
                <table style="width: 100%;border-collapse: collapse;border: 1px solid #000;">
                    <tr style="border-collapse: collapse;border: 1px solid #000;">
                        <td colspan="2" style="border-collapse: collapse;border: 1px solid #000;">
                            <center>
                                <div style="text-transform: capitalize;">Prepared By</div>
                            </center>
                        </td>
                        <td style="width: 50%;padding: 1px;border-collapse: collapse;border: 1px solid #000;">
                            <span style="text-transform: capitalize; font-size: 14px;padding: 15px;">Computer generated document hence signature is not required</span>
                            <br/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>