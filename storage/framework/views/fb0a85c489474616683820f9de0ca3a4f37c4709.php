<?php $__env->startSection('custom-styles'); ?>
  
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link href="<?php echo e(asset('/css/common/bootstrap-switch.min.css')); ?>" media="screen" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/css/common/bootstrap-datepicker.css')); ?>" />
    <link href="<?php echo e(asset('/css/common/chosen.min.css')); ?>" rel="stylesheet" type="text/css" />
    <style type="text/css">
        input[type="checkbox"][readonly] {
            pointer-events: none;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo $__env->make('layouts.custom_partials.htmlheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    
    <?php echo $__env->make('layouts.custom_partials.contentheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<?php if(Request::segment(4) && Request::segment(4) == 'edit'): ?>
    <?php echo Form::model($data, ['class' => 'form-validate', 'url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey()], 'method' => 'put','enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php elseif(Request::segment(4)): ?>
    <?php echo Form::model($data, ['class' => 'form-validate', 'url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey(),Request::segment(4)], 'method' => 'put','enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php else: ?>
    <?php echo Form::open(['class' => 'form-validate', 'url' => "/".Request::segment(1)."/".Request::segment(2),'enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php endif; ?>
<?php $prefix = config('constants.WorkOrder.prefix'); ?>
<?php $prefix_wd = config('constants.WorkOrderDetails.prefix'); ?>
<input type="hidden" name="screen_name" value = "<?php echo e((isset($data) && isset($data['screen_name'])) ? $data['screen_name'] : ''); ?>">
<section class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <h4><i class="fa fa-book"></i> Work Order Details</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label>Work Order Number *</label>
                    <input type="text" name="wo_order_no"  class="form-control required" id="wo_order_no" placeholder="Work Order Number" value="<?php echo e(htmlValue($prefix.'order_no', $data)); ?>" <?php echo e(setReadonly($prefix."order_no" , $data['readonly'])); ?>>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Work Order Date *</label>
                    <input type="date" name="wo_date"  class="form-control required" id="wo_date" placeholder="Work Order Number" value="<?php echo e(htmlValue($prefix.'date', $data)); ?>" <?php echo e(setReadonly($prefix."date" , $data['readonly'])); ?>>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Select Customer *</label>
                    <i class="fa fa-plus pull-right customer_add"></i>
                    <i class="fa fa-refresh pull-right customer_refresh"></i>
                    <?php echo e(Form::select('customer_id',
                        (isset($data) && isset($data['customer_list'])) ? $data['customer_list'] : [],
                        isset($data) && isset($data['wo_customer']) ? htmlSelect($prefix.'customer', $data) : htmlSelect('inv_customer', $data),
                        array('name'=>'wo_customer', 'id' => 'customer_id', 'class' => 'form-control chosen-select required customer_select', 'placeholder' => '', 'data-placeholder' => 'Select Customer', setDisable($prefix.'customer', $data['disabled'])))); ?>

                    <input type="hidden" name="inv_customer" value="<?php echo e(htmlValue($prefix.'customer', $data)); ?>">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Remarks</label>
                    <input type="text" name="wo_remarks" class="form-control" value="<?php echo e(htmlValue($prefix.'remarks', $data)); ?>" placeholder="Remarks" <?php echo e(setReadonly($prefix."remarks" , $data['readonly'])); ?>>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Additional Remarks</label>
                    <input type="text" name="wo_add_remarks" class="form-control" value="<?php echo e(htmlValue($prefix.'add_remarks', $data)); ?>" placeholder="Additional Remarks" <?php echo e(setReadonly($prefix."remarks" , $data['readonly'])); ?>>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <h4><i class="fa fa-book"></i> Invoice Details</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label>Invoice Number</label>
                    <input type="text" name="inv_order_no" class="form-control inv_order_no" readonly="readonly" value="<?php echo e(htmlValue('inv_order_no', $data)); ?>">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Invoice Date *</label>
                    <input type="date" name="inv_date" class="form-control inv_date" value="<?php echo e(htmlValue('inv_date', $data)); ?>">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Total Quantity</label>
                    <input type="text" id="wo_total_quantity" name="inv_total_quantity" class="form-control inv_total_quantity" readonly="readonly" value="<?php echo e(htmlValue('inv_total_quantity', $data)); ?>">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Total Amount</label>
                    <input type="text" name="inv_total_amount" id="wo_total_amount" class="form-control inv_total_amount" readonly="readonly" value="<?php echo e(htmlValue('inv_total_amount', $data)); ?>">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Net Amount</label>
                    <input type="text" name="inv_net_amount" class="form-control inv_net_amount" readonly="readonly" value="<?php echo e(htmlValue('inv_net_amount', $data)); ?>">
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <h4><i class="fa fa-map"></i> Address Details</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" style="border-right: 1px solid black">
                <div class="form-group">
                    <label><i class="fa fa-user"></i> Customer's Address</label>
                </div>
            </div>
            <div class="col-md-6" style="border-left: 1px solid black">
                <div class="form-group">
                    <label><i class="fa fa-industry"></i> Company's Address</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" style="border-right: 1px solid black">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Address Line 1</label>
                        <input type="text" name="inv_customer_address1" class="form-control inv_customer_address1" value="<?php echo e(isset($data) && isset($data['customer']) && isset($data['customer']['cm_s_address_line1']) ? $data['customer']['cm_s_address_line1'] : htmlValue('inv_company_address1', $data)); ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>City</label>
                        <input type="text" name="inv_customer_city" class="form-control inv_customer_city"  value="<?php echo e(isset($data) && isset($data['customer']) && isset($data['customer']['cm_s_city']) ? $data['customer']['cm_s_city'] : htmlValue('inv_customer_city', $data)); ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>State</label>
                        <?php echo e(Form::select('customer_state',
                            (isset($data) && isset($data['customer_state'])) ? $data['customer_state'] : [],
                            htmlSelect($prefix.'customer_state', $data),
                            array('name'=>'inv_customer_state', 'id' => 'customer_state', 'class' => 'form-control chosen-select required customer_state', 'placeholder' => '', 'data-placeholder' => 'Select Inhouse State', setDisable($prefix.'customer_state', $data['disabled'])))); ?>

                        <input type="hidden" name="inv_customer_state" class="wo_customer_state" value="<?php echo e(htmlValue('wo_customer_state', $data)); ?>">
                        <input type="hidden" name="customer_state_type" class="customer_state_type">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Address Line 2</label>
                        <input type="text" name="inv_customer_address2" class="form-control inv_customer_address2"  value="<?php echo e(isset($data) && isset($data['customer']) && isset($data['customer']['cm_s_address_line2']) ? $data['customer']['cm_s_address_line2'] : htmlValue('inv_customer_address2', $data)); ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Pincode</label>
                        <input type="text" name="inv_customer_pincode" class="form-control inv_customer_pincode"  value="<?php echo e(isset($data) && isset($data['customer']) && isset($data['customer']['cm_s_pincode']) ? $data['customer']['cm_s_pincode'] : htmlValue('inv_customer_pincode', $data)); ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>County</label>
                        <input type="text" name="inv_customer_country" class="form-control inv_customer_country"  value="<?php echo e(isset($data) && isset($data['customer']) && isset($data['customer']['cm_s_country']) ? $data['customer']['cm_s_country'] : htmlValue('inv_customer_country', $data)); ?>">
                    </div>
                </div>
            </div>
            <div class="col-md-6" style="border-left: 1px solid black">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Address Line 1</label>
                        <input type="text" name="inv_company_address1" class="form-control inv_company_address1"  value="<?php echo e(isset($data) && isset($data['state']) && isset($data['state'][0]) && isset($data['state'][0]['sm_address_1']) ? $data['state'][0]['sm_address_1'] : htmlValue('inv_company_address1', $data)); ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>City</label>
                        <input type="text" name="inv_company_city" class="form-control inv_company_city"  value="<?php echo e(isset($data) && isset($data['state']) && isset($data['state'][0]) && isset($data['state'][0]['sm_city']) ? $data['state'][0]['sm_city'] : htmlValue('inv_company_city', $data)); ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>State</label>
                        <?php echo e(Form::select('inhouse_state',
                            (isset($data) && isset($data['state_list'])) ? $data['state_list'] : [],
                            (isset($data['state']) && isset($data['state']) && isset($data['state'][0])) ? htmlSelect('sm_state', $data['state'][0]) : htmlSelect('wo_inhose_state', $data),
                            array('name'=>'inv_company_state', 'id' => 'inhouse_state', 'class' => 'form-control chosen-select required inhouse_state', 'placeholder' => '', 'data-placeholder' => 'Select Inhouse State', setDisable($prefix.'inhouse_state', $data['disabled'])))); ?>

                        <input type="hidden" name="inhouse_state_type" class="inhouse_state_type" value="<?php echo e(htmlValue('0', $data['state_type'])); ?>">
                        <input type="hidden" name="inv_company_state" class="wo_inhose_state" value="<?php echo e(htmlSelect('sm_state', $data['state'][0])); ?>">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Address Line 2</label>
                        <input type="text" name="inv_company_address2" class="form-control inv_company_address2" value="<?php echo e(isset($data) && isset($data['state']) && isset($data['state'][0]) && isset($data['state'][0]['sm_address_2']) ? $data['state'][0]['sm_address_2'] : htmlValue('inv_company_address2', $data)); ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Pincode</label>
                        <input type="text" name="inv_company_pincode" class="form-control inv_company_pincode"  value="<?php echo e(isset($data) && isset($data['state']) && isset($data['state'][0]) && isset($data['state'][0]['sm_pincode']) ? $data['state'][0]['sm_pincode'] : htmlValue('inv_company_pincode', $data)); ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>County</label>
                        <input type="text" name="inv_company_country" class="form-control inv_company_country" value="<?php echo e(isset($data) && isset($data['state']) && isset($data['state'][0]) && isset($data['state'][0]['sm_country']) ? $data['state'][0]['sm_country'] : htmlValue('inv_company_country', $data)); ?>">
                    </div>
                </div>
            </div>
        </div>
        <hr>
    </div>
</section>
<section class="panel">
    <div class="panel-body item_row_parent">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <h4><i class="fa fa-dropbox"></i> Item Details</h4>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">    
                    <label>Select Finish Goods</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">                
                    <label>Quantity</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">                
                    <label>Unit</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">                
                    <label>Price</label>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">                
                    <label>Tax</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">                
                    <label>Amount</label>
                </div>
            </div>
        </div>
        <?php if(isset($data) && isset($data['work_order_details']) && sizeof($data['work_order_details']) > 0): ?>
            <?php $__currentLoopData = $data['work_order_details']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="item_row">
                    <div class="row each_item_row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label><i class="fa fa-clipboard copy"></i></label>
                                <i class="fa fa-plus pull-right finish_goods_add"></i>
                                <i class="fa fa-refresh pull-right finish_goods_refresh"></i>
                                <?php echo e(Form::select('finish_goods_list',
                                    (isset($data) && isset($data['finish_goods_list'])) ? $data['finish_goods_list'] : [],
                                    $value['wod_item_id'],
                                    array('name'=>'item_details['.$value['work_order_details_id'].'][ind_item_id]', 'data-name' => 'ind_item_id','id' => 'finish_goods_list', 'class' => 'form-control chosen-select required finish_goods_list finish_goods_select', 'timestamp' => $value['work_order_details_id'],'placeholder' => '', 'data-placeholder' => 'Select Finish Goods',setDisable('item_details', $data['disabled'])))); ?>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" name="item_details[<?php echo e($value['work_order_details_id']); ?>][ind_quantity]" class="form-control quantity number" min="0" placeholder="Quantity" data-name = "wod_quantity" value="<?php echo e(round($value['wod_quantity'], 2)); ?>" <?php echo e(setReadonly('item_details' , $data['readonly'])); ?> max="<?php echo e(round($value['wod_quantity'], 2)); ?>">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <?php echo e(Form::select('unit',
                                    (isset($data) && isset($data['unit_master'])) ? $data['unit_master'] : [],
                                    $value['wod_unit'],
                                    array('name'=>'item_details['.$value['work_order_details_id'].'][ind_unit]', 'data-name' => 'wod_unit','id' => 'wod_unit', 'class' => 'form-control chosen-select wod_unit', 'placeholder' => '', 'data-placeholder' => 'Select Unit', 'data-name' => 'ind_unit', setDisable('item_details', $data['disabled'])))); ?>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" name="item_details[<?php echo e($value['work_order_details_id']); ?>][ind_rate]" class="form-control rate number" minimum="0" placeholder="Price" data-name = "wod_rate" value="<?php echo e(round($value['wod_rate'], 2)); ?>" <?php echo e(setReadonly('item_details' , $data['readonly'])); ?>> 
                            </div>
                        </div>
                        <div class = "col-md-1">
                            <div class="form-group">
                                <?php echo e(Form::select('tax_bracket',
                                    (isset($data) && isset($data['tax_bracket'])) ? $data['tax_bracket'] : [],
                                    $value['wod_tax'],
                                    array('name'=>'item_details['.$value['work_order_details_id'].'][ind_tax]' ,'data-name' => 'wod_tax', 'class' => 'form-control required chosen-select tax', 'placeholder' => '', 'data-placeholder' => 'Select Tax', setDisable('item_details', $data['disabled'])))); ?>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" name="item_details[<?php echo e($value['work_order_details_id']); ?>][ind_amount]" class="form-control amount number" placeholder="Amount" readonly="readonly" data-name = "ind_amount" value="<?php echo e(round($value['wod_amount'], 2)); ?>" <?php echo e(setReadonly('item_details' , $data['readonly'])); ?>>
                            </div>
                        </div>
                        <?php if(isset($data) && isset($data['screen_name']) && $data['screen_name'] != 'work-order-show'): ?>
                            <?php if($key == 0): ?> 
                                <div class="col-md-1">
                                    <div class="form-group pull-right">
                                        <i class="fa fa-trash fa-lg delete_item_row hide"></i>
                                    </div>
                                </div>
                            <?php else: ?> 
                                <div class="col-md-1">
                                    <div class="form-group pull-right">
                                        <i class="fa fa-trash fa-lg delete_item_row"></i>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php elseif(isset($data) && isset($data['invoice_details']) && sizeof($data['invoice_details']) > 0): ?>
            <?php $__currentLoopData = $data['invoice_details']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="item_row">
                    <div class="row each_item_row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label><i class="fa fa-clipboard copy"></i></label>
                                <i class="fa fa-plus pull-right finish_goods_add"></i>
                                <i class="fa fa-refresh pull-right finish_goods_refresh"></i>
                                <?php echo e(Form::select('finish_goods_list',
                                    (isset($data) && isset($data['finish_goods_list'])) ? $data['finish_goods_list'] : [],
                                    $value['ind_item_id'],
                                    array('name'=>'item_details['.$value['work_order_details_id'].'][ind_item_id]', 'data-name' => 'ind_item_id','id' => 'finish_goods_list', 'class' => 'form-control chosen-select required finish_goods_list finish_goods_select', 'timestamp' => $value['work_order_details_id'],'placeholder' => '', 'data-placeholder' => 'Select Finish Goods',setDisable('item_details', $data['disabled'])))); ?>

                                <input type="hidden" name="item_details['.$value['invoice_details_id'].'][ind_item_id]" value="<?php echo e(htmlValue('invoice_details_id', $value)); ?>">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" name="item_details[<?php echo e($value['invoice_details_id']); ?>][ind_quantity]" class="form-control quantity number" min="0" placeholder="Quantity" data-name = "ind_quantity" value="<?php echo e(round($value['ind_quantity'], 2)); ?>" <?php echo e(setReadonly('item_details' , $data['readonly'])); ?> max="<?php echo e(round($value['ind_quantity'], 2)); ?>">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <?php echo e(Form::select('unit',
                                    (isset($data) && isset($data['unit_master'])) ? $data['unit_master'] : [],
                                    $value['ind_unit'],
                                    array('name'=>'item_details['.$value['invoice_details_id'].'][ind_unit]', 'data-name' => 'ind_unit','id' => 'ind_unit', 'class' => 'form-control chosen-select ind_unit', 'placeholder' => '', 'data-placeholder' => 'Select Unit', 'data-name' => 'ind_unit', setDisable('item_details', $data['disabled'])))); ?>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" name="item_details[<?php echo e($value['invoice_details_id']); ?>][ind_rate]" class="form-control rate number" minimum="0" placeholder="Price" data-name = "ind_rate" value="<?php echo e(round($value['ind_rate'], 2)); ?>" <?php echo e(setReadonly('item_details' , $data['readonly'])); ?>> 
                            </div>
                        </div>
                        <div class = "col-md-1">
                            <div class="form-group">
                                <?php echo e(Form::select('tax_bracket',
                                    (isset($data) && isset($data['tax_bracket'])) ? $data['tax_bracket'] : [],
                                    $value['ind_tax'],
                                    array('name'=>'item_details['.$value['invoice_details_id'].'][ind_tax]' ,'data-name' => 'wod_tax', 'class' => 'form-control required chosen-select tax', 'placeholder' => '', 'data-placeholder' => 'Select Tax', setDisable('item_details', $data['disabled'])))); ?>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" name="item_details[<?php echo e($value['invoice_details_id']); ?>][ind_amount]" class="form-control amount number" placeholder="Amount" readonly="readonly" data-name = "ind_amount" value="<?php echo e(round($value['ind_amount'], 2)); ?>" <?php echo e(setReadonly('item_details' , $data['readonly'])); ?>>
                            </div>
                        </div>
                        <?php if(isset($data) && isset($data['screen_name']) && $data['screen_name'] != 'work-order-show'): ?>
                            <?php if($key == 0): ?> 
                                <div class="col-md-1">
                                    <div class="form-group pull-right">
                                        <i class="fa fa-trash fa-lg delete_item_row hide"></i>
                                    </div>
                                </div>
                            <?php else: ?> 
                                <div class="col-md-1">
                                    <div class="form-group pull-right">
                                        <i class="fa fa-trash fa-lg delete_item_row"></i>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            
        <?php endif; ?>
    </div>
</section>

<section class="panel">
    <div class="panel-body">
        <h4>Select Taxes</h4>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <div class="checkbox checkbox-sharekhan">
                            <input type="checkbox" name="taxes[]" readonly class="taxes styled select" value="1"
                            <?php if(isset($data) && isset($data['taxes']) && in_array('1', $data['taxes'])): ?>
                                checked="checked" 
                            <?php endif; ?>
                            >
                            <label><h5>NA</h5></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <div class="checkbox checkbox-sharekhan">
                            <input type="checkbox" name="taxes[]" readonly class="taxes styled select" value="CGST"
                            <?php if(isset($data) && isset($data['taxes']) && in_array('CGST', $data['taxes'])): ?>
                                checked="checked" 
                            <?php endif; ?>
                            >
                            <label><h5>CGST</h5></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <div class="checkbox checkbox-sharekhan">
                            <input type="checkbox" name="taxes[]" readonly class="taxes styled select" value="SGST"
                            <?php if(isset($data) && isset($data['taxes']) && in_array('SGST', $data['taxes'])): ?>
                                checked="checked" 
                            <?php endif; ?>
                            >
                            <label><h5>SGST</h5></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <div class="checkbox checkbox-sharekhan">
                            <input type="checkbox" name="taxes[]" readonly class="taxes styled select" value="UTGST"
                            <?php if(isset($data) && isset($data['taxes']) && in_array('UTGST', $data['taxes'])): ?>
                                checked="checked" 
                            <?php endif; ?>
                            >
                            <label><h5>UTGST</h5></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <div class="checkbox checkbox-sharekhan">
                            <input type="checkbox" name="taxes[]" readonly class="taxes styled select" value="IGST"
                            <?php if(isset($data) && isset($data['taxes']) && in_array('IGST', $data['taxes'])): ?>
                                checked="checked" 
                            <?php endif; ?>
                            >
                            <label><h5>IGST</h5></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-4">
                <label><h5>Total Amount</h5></label>
                    <div class="form-group">
                            <input type="text" name="totalamount"  class="form-control required" id="totalamount" placeholder="Total Amount" value="<?php echo e(round(htmlValue($prefix.'totalamount', $data), 2)); ?>" readonly="readonly">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                <label><h5>Tax Details</h5></label>
                    <div class="form-group tax_details" id= "tax_details">
                            
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                <label><h5>Net Amount</h5></label>
                    <div class="form-group">
                            <input type="text" name="wo_net_amount"  class="form-control required" id="wo_net_amount" placeholder="Total Amount" value="<?php echo e(round(htmlValue('wo_net_amount', $data), 2)); ?>" readonly="readonly">
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</section>
<div class="row sample_variation_row hide">
    <div class="col-md-3">
        <div class="form-group">
            <label>Variant Material</label>
            <?php echo e(Form::select('item_group_list',
                (isset($data) && isset($data['item_group_list'])) ? $data['item_group_list'] : [],
                null,
                array('data-name' => 'variant_group', 'class' => 'form-control chosen-select variant_group', 'placeholder' => '', 'data-placeholder' => 'Select Material'))); ?>

        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label>Select Variation *</label>
            <?php echo e(Form::select('item_master_list',
                (isset($data) && isset($data['item_master_list'])) ? $data['item_master_list'] : [],
                null,
                array('data-name' => 'variant_material', 'class' => 'form-control chosen-select variant_material', 'placeholder' => '', 'data-placeholder' => 'Select Variant'))); ?>

        </div>
    </div>
</div>

<section class="panel">
    <div class="panel-body">
        <div class="btn-group">
            <button class="btn btn-success" name="save" value="save" type="submit" id="save_continue">Save </button>
        </div>
        <div class="btn-group pull-right">
            <a href="/<?php echo e(Request::segment(1)); ?>/<?php echo e(Request::segment(2)); ?>/"><button class="btn btn-danger" name="cancel" value="cancel" type="button" id="cancel">Cancel</button></a>
        </div>
    </div>
</section>
<?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['screen_name'] = $data['screen_name'];
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>
    <?php echo $__env->make('layouts.script_loaders.datatable_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('/js/notify.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        chosenInit();
        calculateTotalQuantity();
        calculateTotalAmount();
        calculateTax();

        $(document).on('click','.copy',function(){
            var text = $(this).parent().parent().find('.finish_goods_list option:selected').text();
            var input = document.createElement('input');
            input.setAttribute('value', text);
            document.body.appendChild(input);
            input.select();
            var result = document.execCommand('copy');
            document.body.removeChild(input);
            $.notify('Item copied to clipboard', 'success');
        })
        function copy() {
                let text = $('this option:selected').text();
                var input = document.createElement('input');
                input.setAttribute('value', text);
                document.body.appendChild(input);
                input.select();
                var result = document.execCommand('copy');
                document.body.removeChild(input)
        }

        //change of item should fetch unit
        $(document).on('change', '.finish_goods_list', function() {
            let id = $(this).val();
            var that = $(this);
            let curr_url = '/purchase-module/work-order/get-unit'
            $.ajax({
                url: curr_url,
                type: 'get',
                data: {
                    entity : id
                },
                success: function(result) {
                    if(result) {
                        $(that).closest('.item_row').find('.wod_unit').val(result[0]).trigger("chosen:updated");
                    } else {
                        $(that).closest('.item_row').find('.wod_unit').val('').trigger("chosen:updated");
                    }
                }
            });

            $.ajax({
                url: '/api/get-item-tax/'+id,
                type: 'get',
                success: function(result) {
                    if(result) {
                        $(that).closest('.item_row').find('.tax').val(result[0]).trigger("chosen:updated");
                    } else {
                        $(that).closest('.item_row').find('.tax').val('').trigger("chosen:updated");
                    }
                }
            });
        });



        $(document).on('click', '#add_item_row', function(){
            var time = new Date().getTime();
            
            var row = $('.item_row:eq(0)').clone();
            $(row).find('.variation_row').remove();
            
            $(row).find('.each_item_row').find('input').each(function(){
                var name = $(this).data('name');
                $(this).attr('name', 'item_details['+time+']['+name+']');
                $(this).val('');
            });

            $(row).find('.each_item_row').find('select').each(function(k, v) {
                var n = $(v).attr('data-name');    
                $(v).attr('name', 'item_details['+time+']['+n+']');
                $(v).siblings('div').remove();
                $(v).val('').trigger('chosen:updated');
            });

            $(row).find('.later_item_row').find('select').each(function(k, v) {
                var n = $(v).attr('data-name');    
                var t = $(v).attr('data-key');
                if(n == 'wod_tax'){
                    $(v).val('').trigger('chosen:updated');
                }
                $(v).attr('name', 'item_details['+time+'][variant]['+t+']');
                $(v).siblings('div').remove();
            });

            $(row).find('.each_item_row').find('.finish_goods_list').attr('timestamp', time);
            $(row).find('.each_item_row').find('.delete_item_row').removeClass('hide');
            $('.item_row_parent').append(row);
            chosenInit();
        })

        $(document).on('click', '.delete_item_row', function(){
            $(this).parent().parent().parent().parent().remove();
            calculateTotalQuantity();
            calculateTotalAmount();
            calculateTax();
        })

        $(document).on('keyup blur change', '.quantity, .rate, .tax', function(){
            var row = $(this).parent().parent().parent();
            qty = parseFloat(row.find('.quantity').val());
            rate = parseFloat(row.find('.rate').val());
            isNaN(qty) ? qty = 0 : qty;
            isNaN(rate) ? rate = 0 : rate;
            row.find('.amount').val((rate * qty).toFixed(2));
            calculateTotalQuantity();
            calculateTotalAmount();
            calculateTax();

        })
        
        
        function calculateTax()
        {
            var fiveamount = 0;
            var twelveamount = 0;
            var eighteenamount = 0;
            var tweightamount = 0;
            var netamount = parseFloat($('#totalamount').val());
            
            $('.tax').each(function(){
                var count = 0;
                var row = $(this).parent().parent().parent();
                tax = row.find('.tax option:selected').text();
                tax = tax.slice(0,-1);
                qty = row.find('.quantity').val();
                rate = row.find('.rate').val();
                isNaN(qty) ? qty = 0 : qty;
                isNaN(rate) ? rate = 0 : rate;
                // var fivetaxtottal = 0;
                if(tax == 5)
                {
                    fiveamount = parseInt(fiveamount) + parseInt(rate * qty);
                    fivetaxtottal = parseFloat(fiveamount / 100)  * 5;
                    fivetaxtottal = fivetaxtottal.toFixed(2);
                    
                }
                if(tax == 12)
                {
                    twelveamount = parseInt(twelveamount) + parseInt(rate * qty);
                    twelvetaxtottal = parseFloat(twelveamount / 100)  * 12;
                    twelvetaxtottal = twelvetaxtottal.toFixed(2);
                    
                }
                if(tax == 18)
                {
                    eighteenamount = parseInt(eighteenamount) + parseInt(rate * qty);
                    eighteentaxtottal = parseFloat(eighteenamount / 100)  * 18;
                    eighteentaxtottal = eighteentaxtottal.toFixed(2);
                    
                }
                if(tax == 28)
                {
                    tweightamount = parseInt(tweightamount) + parseInt(rate * qty);
                    tweighttaxtottal = parseFloat(tweightamount / 100)  * 28;
                    tweighttaxtottal = tweighttaxtottal.toFixed(2);
                    
                }
                $('.tax_details').html('');
                
                if(fiveamount != 0)
                {
                    $('.taxes:checked').each(function(){
                        var length = $('.taxes:checked').length;
                        var tax = parseFloat(fivetaxtottal / length);
                        $('.tax_details').append($(this).val()+" "+parseFloat(5/length)+"% : "+tax);
                        count = count +1;
                        var input = document.createElement("input");
                        input.setAttribute("type", "hidden");
                        input.setAttribute("name", "tax["+count+"][itd_tax_name]");
                        input.setAttribute("value", $(this).val());
                        document.getElementById("tax_details").appendChild(input);
                        var input = document.createElement("input");
                        input.setAttribute("type", "hidden");
                        input.setAttribute("name", "tax["+count+"][itd_tax_percent]");
                        input.setAttribute("value", parseFloat(5/length));
                        document.getElementById("tax_details").appendChild(input);
                        var input = document.createElement("input");
                        input.setAttribute("type", "hidden");
                        input.setAttribute("name", "tax["+count+"][itd_tax_amount]");
                        input.setAttribute('class', 'count_tax');
                        input.setAttribute("value", parseFloat(tax));
                        document.getElementById("tax_details").appendChild(input);
                        $('.tax_details').append("<br>");
                    })
                }
                if(twelveamount != 0)
                {
                    $('.taxes:checked').each(function(){
                        var length = $('.taxes:checked').length;
                        var tax = parseFloat(twelvetaxtottal / length);
                        $('.tax_details').append($(this).val()+" "+parseFloat(12/length)+"% : "+tax);
                        $('.tax_details').append("<br>");
                        count = count +1;
                        var input = document.createElement("input");
                        input.setAttribute("type", "hidden");
                        input.setAttribute("name", "tax["+count+"][itd_tax_name]");
                        input.setAttribute("value", $(this).val());
                        document.getElementById("tax_details").appendChild(input);
                        var input = document.createElement("input");
                        input.setAttribute("type", "hidden");
                        input.setAttribute("name", "tax["+count+"][itd_tax_percent]");
                        input.setAttribute("value", parseFloat(12/length));
                        document.getElementById("tax_details").appendChild(input);
                        var input = document.createElement("input");
                        input.setAttribute("type", "hidden");
                        input.setAttribute("name", "tax["+count+"][itd_tax_amount]");
                        input.setAttribute('class', 'count_tax');
                        input.setAttribute("value", parseFloat(tax));
                        document.getElementById("tax_details").appendChild(input);
                    })
                }
                if(eighteenamount != 0)
                {
                    $('.taxes:checked').each(function(){
                        var length = $('.taxes:checked').length;
                        var tax = parseFloat(eighteentaxtottal / length);
                        $('.tax_details').append($(this).val()+" "+parseFloat(18/length)+"% : "+tax);
                        $('.tax_details').append("<br>");
                        count = count +1;
                        var input = document.createElement("input");
                        input.setAttribute("type", "hidden");
                        input.setAttribute("name", "tax["+count+"][itd_tax_name]");
                        input.setAttribute("value", $(this).val());
                        document.getElementById("tax_details").appendChild(input);
                        var input = document.createElement("input");
                        input.setAttribute("type", "hidden");
                        input.setAttribute("name", "tax["+count+"][itd_tax_percent]");
                        input.setAttribute("value", parseFloat(18/length));
                        document.getElementById("tax_details").appendChild(input);
                        var input = document.createElement("input");
                        input.setAttribute("type", "hidden");
                        input.setAttribute("name", "tax["+count+"][itd_tax_amount]");
                        input.setAttribute('class', 'count_tax');
                        input.setAttribute("value", parseFloat(tax));
                        document.getElementById("tax_details").appendChild(input);
                    })
                }
                if(tweightamount != 0)
                {
                    $('.taxes:checked').each(function(){
                        var length = $('.taxes:checked').length;
                        var tax = parseFloat(tweighttaxtottal / length);
                        $('.tax_details').append($(this).val()+" "+parseFloat(28/length)+"% : "+tax);
                        $('.tax_details').append("<br>");
                        count = count +1;
                        var input = document.createElement("input");
                        input.setAttribute("type", "hidden");
                        input.setAttribute("name", "tax["+count+"][itd_tax_name]");
                        input.setAttribute("value", $(this).val());
                        document.getElementById("tax_details").appendChild(input);
                        var input = document.createElement("input");
                        input.setAttribute("type", "hidden");
                        input.setAttribute("name", "tax["+count+"][itd_tax_percent]");
                        input.setAttribute("value", parseFloat(28/length));
                        document.getElementById("tax_details").appendChild(input);
                        var input = document.createElement("input");
                        input.setAttribute("type", "hidden");
                        input.setAttribute("name", "tax["+count+"][itd_tax_amount]");
                        input.setAttribute('class', 'count_tax');
                        input.setAttribute("value", parseFloat(tax));
                        document.getElementById("tax_details").appendChild(input);
                    })
                }
            });

            $('.count_tax').each(function(){
                var curr = parseFloat($(this).val());
                curr = isNaN(curr) ? 0 : curr;
                netamount = netamount + curr;
            });

            netamount = parseFloat(netamount);
            $('#wo_net_amount').val(netamount.toFixed(2)); 
            $('.inv_net_amount').val(netamount.toFixed(2));
        }

        function calculateTotalQuantity(){
            total_quantity = 0;
            $('.quantity').each(function(){
                qty = parseFloat($(this).val());
                isNaN(qty) ? qty = 0 : qty;
                total_quantity += qty;
            });
            $('#wo_total_quantity').val(total_quantity);

        }

        function calculateTotalAmount(){
            total_amount = 0;
            $('.amount').each(function(){
                amount = parseFloat($(this).val());
                isNaN(amount) ? amount = 0 : amount;
                total_amount += amount;
            })
            $('#wo_total_amount').val(total_amount);
            $('#totalamount').val(total_amount);
            
        }


        $(document).on('change', '.finish_goods_list', function(){
            var parent_row = $(this).parent().parent().parent().parent();
            var timestamp = $(this).attr('timestamp');
            var time = new Date().getTime();
            if($(this).val() != ''){
                // $.ajax({
                //     url: '/api/'+$(this).val()+'/check-for-variant/',
                //     type: 'get',
                //     success: function(res){
                //         $(parent_row).find('.variation_row').remove();
                //         if(res != 'fail'){
                //             if(res != undefined && res['item_list'] != undefined){
                //                 $.each(res['item_list'], function(k,v){
                //                     var row = $('.sample_variation_row').clone();
                //                     $(row).find('select').each(function(k1, v1){
                //                         var n = $(v1).attr('data-name');    
                //                         $(v1).attr('name', 'item_details['+timestamp+'][variant]['+k+']['+n+']');
                //                         $(v1).siblings('div').remove();          
                //                         $(v1).addClass('required');
                //                         if(n == 'variant_group'){
                //                             $(v1).val(k);
                //                             $(v1).find('option').not(':selected').remove();
                //                         } else {
                //                             appendSelectData(this, res['raw_material_list'][k]);
                //                         }
                //                     })
                //                     $(row).removeClass('hide');
                //                     $(row).removeClass('sample_variation_row').addClass('variation_row');
                //                     $(parent_row).append(row);
                //                     chosenInit();
                //                 })
                //             }
                //         } else {    
                //             $(parent_row).find('.variation_row').remove();
                //         }
                //     }
                // })
            }
        })

        $('.taxes').on('change', function() {
            if($('.taxes:checked').length > 2) {
                this.checked = false;
            }
            if($(this).val() == 1)
            {
                if($('input[type="checkbox"]').eq(0).is(':checked')){
                    $('input[type="checkbox"]').not($(this)).attr('disabled', true);
                    $('input[type="checkbox"]').not($(this)).prop("checked", false);
                    $('.tax_details').html('');
                }
                else{
                    $('input[type="checkbox"]').not($(this)).attr('disabled', false);
                    $('.tax_details').html('');
                }
            }
            else
            {
                calculateTax();
            }
        });

        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        $(".numeric").bind("keypress", function (e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
            $(".error").css("display", ret ? "none" : "inline");
            return ret;
        });
        $(".numeric").bind("paste", function (e) {
            var element = this;
            setTimeout(function () {
                var tip = $(element).val();
                    // REMOVE NON-NUMERIC CHARACTERS
                var cleanTip = tip.replace(/\D/g,'');
                $(element).val(cleanTip);
            }, 100);
        });

        $(document).on('click','.customer_add',function(){
            window.open("http://"+window.location.host+"/masters/customer-master/create", '_blank');
        });

        $(document).on('click','.customer_refresh',function(){
            $.ajax({
                type: 'get',
                url:'/api/get-customer',
                success: function(res){
                        $('.customer_select').empty();
                        appendSelectData('.customer_select',res['customer_list'],"");
                    }       
                });
        })

        $(document).on('click','.finish_goods_add',function(){
            window.open("http://"+window.location.host+"/masters/item-master/create", '_blank');
        })

        $(document).on('change', '.customer_select', function(){
            var cust_id = $(this).val();
            if(cust_id != ''){
                $.ajax({
                type: 'get',
                url:'/api/get-customer-state/'+cust_id,
                success: function(res){
                        $('.customer_state').empty();
                        appendSelectData('.customer_state',res['states']);
                        if(res['state_type'] != undefined && res['state_type'][0] != undefined){
                            $('.customer_state_type').val(res['state_type'][0]);
                        } else {
                            $('.customer_state_type').val('');
                        }
                        $('.customer_state').find('option:eq(1)').prop('selected', 'selected').trigger('chosen:updated');
                        $('.customer_state').prop('disabled', 'disabled').trigger('chosen:updated');
                        $('.wo_customer_state').val($('.customer_state').val());
                        checkStates();
                    }       
                });
            }
        })

        // $('.customer_select').change().trigger('chosen:updated');

        $(document).on('click','.finish_goods_refresh',function(){
            $.ajax({
                type: 'get',
                url:'/api/get-item',
                success: function(res){
                    $('.finish_goods_select').empty();
                    appendSelectData('.finish_goods_select',res['finish_goods_list'],"");
                }       
            });
        })
        
        var tax_condition = 0;
        // 1 = same state => CGST, SGST
        // 2 = same UT => CGST, UTGST
        // 3 = Diff States/UT => IGST
        // 0 => No
        // 
        checkStates();
        function checkStates(){
            var inhouse_state = $('.inhouse_state').val();
            var customer_state = $('.customer_state').val();
            var inhouse_state_type = $('.inhouse_state_type').val();
            var customer_state_type = $('.customer_state_type').val();
            
            if(inhouse_state == customer_state){
                if(inhouse_state_type == 'YES'  && customer_state_type == 'YES'){
                    tax_condition = 1;
                } else if(inhouse_state_type == 'NO' && customer_state_type == 'NO'){
                    tax_condition = 2;
                }
            } else if(inhouse_state_type != '' && customer_state_type != '' && customer_state_type != inhouse_state_type){
                tax_condition = 3
            } else {
                tax_condition = 0;
            }
            tickTaxes();
        }

        function tickTaxes(){
            $(".taxes").prop("checked", false);
            if(tax_condition == 0){
                $(".taxes[value=1]").prop("checked","true");
                calculateTax();
            } else if(tax_condition == 1){
                $(".taxes[value='CGST']").prop("checked","true");
                $(".taxes[value='SGST']").prop("checked","true");
                calculateTax();
            } else if(tax_condition == 2){
                $(".taxes[value='CGST']").prop("checked","true");
                $(".taxes[value='UTGST']").prop("checked","true");
                calculateTax();
            } else if(tax_condition == 3){
                $(".taxes[value='IGST']").prop("checked","true");
                calculateTax(); 
            }
        }

        $(document).find('.customer_select').change();

    });

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>