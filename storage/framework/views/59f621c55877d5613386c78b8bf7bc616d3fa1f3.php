<?php $__env->startSection('custom-styles'); ?>
  
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link href="<?php echo e(asset('/css/common/bootstrap-switch.min.css')); ?>" media="screen" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/css/common/bootstrap-datepicker.css')); ?>" />
    <link href="<?php echo e(asset('/css/common/chosen.min.css')); ?>" rel="stylesheet" type="text/css" />

<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo $__env->make('layouts.custom_partials.htmlheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    
    <?php echo $__env->make('layouts.custom_partials.contentheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<?php if(Request::segment(4)): ?>
    <?php echo Form::model($data, ['class' => 'form-validate', 'url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey()], 'method' => 'put','enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php else: ?>
    <?php echo Form::open(['class' => 'form-validate', 'url' => "/".Request::segment(1)."/".Request::segment(2),'enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php endif; ?>
<?php $prefix = config('constants.BranchMaster.prefix'); ?>
<input type="hidden" name="screen_name" value = "<?php echo e((isset($data) && isset($data['screen_name'])) ? $data['screen_name'] : ''); ?>">
<section class="panel">
    <div class="panel-body">
        <h4>Basic Detail</h4>
        <div class="row">
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Branch Name *</label>
                    <input type="text" class="form-control required" id="<?php echo e($prefix); ?>name" name = "<?php echo e($prefix); ?>name" placeholder="Branch Name" value = "<?php echo e(htmlValue($prefix.'name', $data)); ?>" <?php echo e(setDisable($prefix."name" , $data['disabled'])); ?>>
                </div>
            </div>
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Mobile No.</label>
                    <input type="text" class="form-control numeric" id="<?php echo e($prefix); ?>mobile" name = "<?php echo e($prefix); ?>mobile" placeholder="Mobile" value = "<?php echo e(htmlValue($prefix.'mobile', $data)); ?>" <?php echo e(setDisable($prefix."mobile" , $data['disabled'])); ?> maxlength="10">
                </div>
            </div>
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Alternate Mobile</label>
                    <input type="text" class="form-control numeric" id="<?php echo e($prefix); ?>alt_mobile" name = "<?php echo e($prefix); ?>alt_mobile" placeholder="Alternate Mobile" value = "<?php echo e(htmlValue($prefix.'alt_mobile', $data)); ?>" <?php echo e(setDisable($prefix."alt_mobile" , $data['disabled'])); ?> maxlength="10">
                </div>
            </div>
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Email</label>
                    <input type="text" class="form-control email" id="<?php echo e($prefix); ?>email" name = "<?php echo e($prefix); ?>email" placeholder="Email" value = "<?php echo e(htmlValue($prefix.'email', $data)); ?>" <?php echo e(setDisable($prefix."email" , $data['disabled'])); ?>>
                </div>
            </div> 
        </div>
        <div class="row">
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Alternate Email</label>
                    <input type="text" class="form-control" id="<?php echo e($prefix); ?>alt_email" name = "<?php echo e($prefix); ?>alt_email" placeholder="Alternate Email" value = "<?php echo e(htmlValue($prefix.'alt_email', $data)); ?>" <?php echo e(setDisable($prefix."alt_email" , $data['disabled'])); ?>>
                </div>
            </div>
            <div class = "col-md-6">
                <div class="form-group">
                    <label for="name">Address</label>
                    <input type="text" class="form-control" id="<?php echo e($prefix); ?>address" name = "<?php echo e($prefix); ?>address" placeholder="Address" value = "<?php echo e(htmlValue($prefix.'address', $data)); ?>" <?php echo e(setDisable($prefix."address" , $data['disabled'])); ?>>
                </div>
            </div>
            
        </div>
    </div>
</section>


<?php echo $__env->make('layouts.custom_partials.save_panel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['screen_name'] = $data['screen_name'];
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>
    <?php echo $__env->make('layouts.script_loaders.datatable_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        chosenInit();
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        $(".numeric").bind("keypress", function (e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
            $(".error").css("display", ret ? "none" : "inline");
            return ret;
        });
        $(".numeric").bind("paste", function (e) {
            var element = this;
            setTimeout(function () {
                var tip = $(element).val();
                    // REMOVE NON-NUMERIC CHARACTERS
                var cleanTip = tip.replace(/\D/g,'');
                $(element).val(cleanTip);
            }, 100);
        });
    });

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>