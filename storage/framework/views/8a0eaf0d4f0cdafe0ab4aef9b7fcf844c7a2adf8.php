<?php $__env->startSection('custom-styles'); ?>
  
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link href="<?php echo e(asset('/css/common/bootstrap-switch.min.css')); ?>" media="screen" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
    <link href="<?php echo e(asset('/css/common/chosen.min.css')); ?>" rel="stylesheet" type="text/css" />

<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo $__env->make('layouts.custom_partials.htmlheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    
    <?php echo $__env->make('layouts.custom_partials.contentheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<?php if(Request::segment(4)): ?>
    <?php echo Form::model($data, ['class' => 'form-validate', 'url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey(), Request::segment(4)], 'method' => 'put','enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php else: ?>
    <?php echo Form::open(['class' => 'form-validate', 'url' => "/".Request::segment(1)."/".Request::segment(2),'enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php endif; ?>
<?php $prefix = config('constants.PurchaseOrder.prefix'); ?>
<input type="hidden" name="screen_name" value = "<?php echo e((isset($data) && isset($data['screen_name'])) ? $data['screen_name'] : ''); ?>">
<?php if(isset($data) && isset($data['wo_details'])): ?>
    <section class="panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <h4><i class="fa fa-book"></i> Work Order Details</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Work Order Number</label>
                        <input type="text" name="wo_order_no"  class="form-control" id="wo_order_no" placeholder="Work Order Number" value="<?php echo e(htmlValue('wo_order_no', $data['wo_details'])); ?>" <?php echo e(setReadonly("wo_order_no" , $data['readonly'])); ?>>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Work Order Date</label>
                        <input type="date" name="wo_date"  class="form-control" id="wo_date" placeholder="Work Order Number" value="<?php echo e(htmlValue('wo_date', $data['wo_details'])); ?>" <?php echo e(setReadonly("wo_date" , $data['readonly'])); ?>>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Select Customer</label>
                        <?php echo e(Form::select('customer_id',
                            (isset($data) && isset($data['customer_list'])) ? $data['customer_list'] : [],
                            htmlSelect('wo_customer', $data['wo_details']),
                            array('name'=>'wo_customer', 'id' => 'customer_id', 'class' => 'form-control chosen-select', 'placeholder' => '', 'data-placeholder' => 'Select Customer',setDisable('wo_customer', $data['disabled'])))); ?>

                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Total Quantity</label>
                        <input type="text" name="wo_total_quantity"  class="form-control required" id="wo_total_quantity" placeholder="Total Quantity" value="<?php echo e(round(htmlValue('wo_total_quantity', $data['wo_details']), 2)); ?>" readonly="readonly">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Total Amount</label>
                        <input type="text" name="wo_total_amount"  class="form-control required" id="wo_total_amount" placeholder="Total Amount" value="<?php echo e(round(htmlValue('wo_total_amount', $data['wo_details']), 2)); ?>" readonly="readonly">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Work Order Remarks</label>
                        <input type="text" name="wo_remarks" class="form-control" value="<?php echo e(htmlValue('wo_remarks', $data['wo_details'])); ?>" placeholder="Remarks" <?php echo e(setReadonly("wo_remarks" , $data['readonly'])); ?>>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<section class="panel">
    <div class="panel-body">
        <h4> <i class="fa fa-book"></i> Order Basic Detail</h4>
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label>Select Vendor *</label>
                    <i class="fa fa-plus pull-right vendor_add"></i>
                    <i class="fa fa-refresh pull-right vendor_refresh"></i>
                    <?php echo e(Form::select('vendor_id',
                        (isset($data) && isset($data['vendor_list'])) ? $data['vendor_list'] : [],
                        htmlSelect($prefix.'vendor_id', $data),
                        array('name'=>$prefix.'vendor_id', 'id' => 'vendor_id', 'class' => 'form-control chosen-select required vendor_select', 'data-name' => $prefix.'type', 'placeholder' => '', 'data-placeholder' => 'Select Vendor'))); ?>

                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Purchase Order Number *</label>
                    <input type="text" name="<?php echo e($prefix); ?>purchase_order_number"  class="form-control required" id="purchase_order_num" placeholder="Purchase Order Number" value="<?php echo e(htmlValue($prefix.'purchase_order_number', $data)); ?>" <?php echo e(setReadonly("po_purchase_order_number" , $data['readonly'])); ?>>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Purchase Order Date *</label>
                    <input type="text" name="<?php echo e($prefix); ?>date" class="form-control required datepicker" value="<?php echo e(htmlValue(($prefix.'date'), $data)); ?>">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Total Quantity</label>
                    <input type="text" name="<?php echo e($prefix); ?>total_quantity"  class="form-control" id="total_quantity" placeholder="Total Quantity" readonly="readonly" 
                    <?php if(isset($data) && isset($data['po_total_quantity'])): ?>
                        value="<?php echo e(round(htmlValue($prefix.'total_quantity', $data), 2)); ?>"
                    <?php endif; ?>
                    >
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Total Amount</label>
                    <input type="text" name="<?php echo e($prefix); ?>total_amount"  class="form-control" id="total_amount" placeholder="Total Quantity" readonly="readonly" 
                    <?php if(isset($data) && isset($data['po_total_amount'])): ?>
                        value="<?php echo e(round(htmlValue($prefix.'total_amount', $data),2)); ?>"
                    <?php endif; ?>
                    >
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Expected Delivery Date *</label>
                    <input type="text" name="<?php echo e($prefix); ?>exp_delivery_date" class="form-control required datepicker" value="<?php echo e(htmlValue(($prefix.'exp_delivery_date'), $data)); ?>">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Purchase Order Remarks</label>
                    <input type="text" name="<?php echo e($prefix); ?>remarks"  class="form-control" id="remarks" placeholder="Remarks" value="<?php echo e(htmlValue($prefix.'remarks', $data)); ?>">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="panel">
    <div class="panel-body item_parent_row">
        <div class="row">
            <div class="col-md-4">
                <h4> <i class="fa fa-cubes"></i> Item Details </h4>
            </div>
            <div class="col-md-8">
                <i class="fa fa-plus-square fa-lg pull-right add-item-row" data-toggle="tooltip" title="Add Item Row"></i>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Item</label>
                    <i class="fa fa-plus pull-right item_add"></i>
                    <i class="fa fa-refresh pull-right item_refresh"></i>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Unit</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Quantity</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Price/Unit</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Total Amount</label>
                </div>
            </div>
        </div>
        <?php if(isset($data) && isset($data['item_details']) && sizeof($data['item_details'])): ?>
            <?php $__currentLoopData = $data['item_details']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="row item_row">
                <div class="col-md-3">
                    <div class="form-group">
                        <?php echo e(Form::select('raw_material',
                            (isset($data) && isset($data['raw_material_list'])) ? $data['raw_material_list'] : [],
                            htmlSelect('pod_item_id', $value),
                            array('name'=>'item_details[0][pod_item_id]', 'class' => 'form-control required chosen-select item_select', 'data-name' => 'pod_item_id', 'placeholder' => '', 'data-placeholder' => 'Select Raw Material'))); ?>

                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <?php echo e(Form::select('unit',
                            (isset($data) && isset($data['unit_master'])) ? $data['unit_master'] : [],
                            htmlSelect('pod_unit', $value),
                            array('name'=>'item_details[0][pod_unit]', 'data-name' => 'pod_unit', 'class' => 'form-control chosen-select', 'placeholder' => '', 'data-placeholder' => 'Select Unit'))); ?>

                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <input type="text" name='item_details[0][pod_quantity]' placeholder="Quantity" class="form-control pod_quantity required number" min="0" data-name ="pod_quantity" value="<?php echo e(round(htmlValue('pod_quantity', $value),2)); ?>">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <input type="text" name='item_details[0][pod_price]' placeholder="Price" class="form-control required pod_price number" min="0" data-name ="pod_price" value="<?php echo e(round(htmlValue('pod_price', $value),2)); ?>">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <input type="text" name='item_details[0][pod_total_amount]' placeholder="total_amount" class="form-control required total_amount" data-name ="pod_total_amount" readonly="readonly" value="<?php echo e(htmlValue('pod_total_amount', $value)); ?>">
                    </div>
                </div>
                <?php if($key == 0): ?>
                    <div class="col-md-1">
                        <i class="fa fa-trash hide fa-lg pull-right delete_item_row"></i>
                    </div>
                <?php else: ?>
                    <div class="col-md-1">
                        <i class="fa fa-trash fa-lg pull-right delete_item_row"></i>
                    </div>
                <?php endif; ?>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php else: ?>
            <div class="row item_row">
                <div class="col-md-3">
                    <div class="form-group">
                        <?php echo e(Form::select('raw_material',
                            (isset($data) && isset($data['raw_material_list'])) ? $data['raw_material_list'] : [],
                            htmlSelect('pod_item_id', $data),
                            array('name'=>'item_details[0][pod_item_id]', 'class' => 'form-control required chosen-select item_select', 'data-name' => 'pod_item_id', 'placeholder' => '', 'data-placeholder' => 'Select Raw Material'))); ?>

                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <?php echo e(Form::select('unit',
                            (isset($data) && isset($data['unit_master'])) ? $data['unit_master'] : [],
                            htmlSelect('pod_unit', $data),
                            array('name'=>'item_details[0][pod_unit]', 'data-name' => 'pod_unit', 'class' => 'form-control chosen-select', 'placeholder' => '', 'data-placeholder' => 'Select Unit'))); ?>

                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <input type="text" name='item_details[0][pod_quantity]' placeholder="Quantity" class="form-control pod_quantity required number" min="0" data-name ="pod_quantity">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <input type="text" name='item_details[0][pod_price]' placeholder="Price" class="form-control required pod_price number" min="0" data-name ="pod_price">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <input type="text" name='item_details[0][pod_total_amount]' placeholder="total_amount" class="form-control required total_amount" data-name ="pod_total_amount" readonly="readonly">
                    </div>
                </div>
                <div class="col-md-1">
                    <i class="fa fa-trash fa-lg pull-right delete_item_row"></i>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>

<?php if(isset($data) && isset($data['po_logs']) && sizeof($data['po_logs']) > 0): ?>
    <section class="panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <h4>Purchase Order Logs</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Sr. No.</th>
                            <th>Date</th>
                            <th>Action</th>
                            <th>Pending With</th>
                            <th>Created By</th>
                            <th>Comments / Remarks</th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $data['po_logs']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr> 
                                    <td><?php echo e($key + 1); ?></td>
                                    <td><?php echo e($value['pol_date']); ?></td>
                                    <td><?php echo e($value['action']); ?></td>
                                    <td><?php echo e($value['pending_with']); ?></td>
                                    <td><?php echo e($value['name']); ?></td>
                                    <td><?php echo e($value['pol_comments']); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php if(Request::segment(4) !== null && (Request::segment(4) == 'accounts-approve' || Request::segment(4) == 'admin-approve')): ?>
    <section class="panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="color-red">Approve / Reject Remarks</label>
                        <?php if(Request::segment(4) == 'admin-approve'): ?>
                        <input type="text" name="po_admin_remakrs" class="form-control" placeholder="Admin Remarks">
                        <?php elseif(Request::segment(4) == 'accounts-approve'): ?>
                        <input type="text" name="po_accoutns_remakrs" class="form-control" placeholder="Accounts Team Remarks">
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="btn-group">
                <button class="btn btn-success" name="save" value="3" type="submit" id="save_continue">Approve <i class="fa fa-thumbs-up"></i></button>
            </div>
            <div class="btn-group">
                <button class="btn btn-danger" name="save" value="4" type="submit" id="save_continue">Reject <i class="fa fa-thumbs-down"></i></button>
            </div>
            <div class="btn-group pull-right">
                <a href="/<?php echo e(Request::segment(1)); ?>/<?php echo e(Request::segment(2)); ?>/"><button class="btn btn-danger" name="cancel" value="cancel" type="button" id="cancel">Cancel</button></a>
            </div>
        </div>
    </section>
<?php else: ?>
    <?php echo $__env->make('layouts.custom_partials.save_panel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php endif; ?>

<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['screen_name'] = $data['screen_name'];
        $js_data['id'] = Request::segment(3);
        
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>
    <?php echo $__env->make('layouts.script_loaders.datatable_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        chosenInit();
        $(".datepicker").datepicker({
            startDate: new Date(),
            format: 'yyyy-mm-dd',
        });
        $(document).on('click', '.add-item-row', function(){
            var row = $('.item_row:eq(0)').clone();
            var time = new Date().getTime();
            $(row).find('input').each(function(){
                var name = $(this).data('name');
                $(this).attr('name', 'item_details['+time+']['+name+']');
                $(this).val('');
            });
            $(row).find('select').each(function(k, v) {
                var n = $(v).attr('data-name');    
                $(v).attr('name', 'item_details['+time+']['+n+']');
                $(v).siblings('div').remove();
            });
            $(row).find('.delete_item_row').removeClass('hide');
            $('.item_parent_row').append(row);
            chosenInit();
        });

        $(document).on('change', '#inventory_type', function(){
            inventory_type = $(this).val();
            $.ajax({
                url: window.location.href,
                type: 'get',
                data: {
                    inventory_type: inventory_type
                },
                success: function(res){
                    $('#source').empty();
                    appendSelectData('#source', res);
                }
            })
        })

        $(document).on('click', '.delete_item_row', function(){
            $(this).parent().parent().remove();
            calculateTotalAmount();
            calculateTotlaQuantity();
        })

        $(document).on('change', '#finish_goods', function(){
            var name = $('#finish_goods').find('option:selected').text();
            names = name.split(' ');
            var new_name = '';
            $(names).each(function(k,v){
                new_name += v[0];
            })
            new_name = new_name.toUpperCase();
            $('.bom_name').val(new_name);
        })

        $(document).on('keyup blur', '.pod_quantity, .pod_price', function(){
            var row = $(this).parent().parent().parent();
            calculateRowAmount(row);
        })

        function calculateRowAmount(ele){
            var price = $(ele).find('.pod_price').val();
            var qty = $(ele).find('.pod_quantity').val();
            if(isNaN(price)){
                price = 0;
            }
            if(isNaN(qty)){
                qty = 0;
            }
            var total = qty * price;
            $(ele).find('.total_amount').val(total);
            calculateTotalAmount();
            calculateTotlaQuantity();
        }

        function calculateTotalAmount(){
            total_amount = 0;
            $('.total_amount').each(function(){
                total_amount += parseFloat($(this).val());
            });
           $('#total_amount').val(total_amount) 
        }
        function calculateTotlaQuantity(){
            total_quantity = 0;
            $('.pod_quantity').each(function(){
                total_quantity += parseFloat($(this).val());
            })
            $('#total_quantity').val(total_quantity) 
        }
        /*$(document).on('blur','#purchase_order_num',function(){
            var order_no = $(this).val();
            $.ajax({
                type: 'get',
                url:'/purchase-module/purchase-order/'+lang.id+'/check-purchase-order-number',
                data : {
                    order_no : order_no
                },
                success: function(res){
                    console.log(res);
                    if(res > 0)
                    {
                        $('#purchase_order_num').parent().addClass('has-error');
                        var p = "<p class = 'help-block error'>Purchase Order Number exists</p>";
                        $('#purchase_order_num').siblings('p').remove();
                        $('#purchase_order_num').parent().append(p);
                        $('#save_continue').prop('disabled', true);
                        $('#save_return').prop('disabled', true);
                    } else {
                        $('#purchase_order_num').parent().removeClass('has-error');
                        $('#purchase_order_num').siblings('p').remove();
                        $('#save_continue').prop('disabled', false);
                        $('#save_return').prop('disabled', false);
                    }
                }
            });
        })*/


        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        $(".numeric").bind("keypress", function (e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
            $(".error").css("display", ret ? "none" : "inline");
            return ret;
        });
        $(".numeric").bind("paste", function (e) {
            var element = this;
            setTimeout(function () {
                var tip = $(element).val();
                    // REMOVE NON-NUMERIC CHARACTERS
                var cleanTip = tip.replace(/\D/g,'');
                $(element).val(cleanTip);
            }, 100);
        });


        $(document).on('click','.vendor_add',function(){
            window.open("/masters/vendor-master/create", '_blank');

        })
        $(document).on('click','.vendor_refresh',function(){
            $.ajax({
                type: 'get',
                url:'/api/get-vendor',
                success: function(res){
                        $('.vendor_select').empty();
                        appendSelectData('.vendor_select',res['vendor_list'],"");
                    }       
                });
        })

        $(document).on('click','.item_add',function(){
            window.open("/masters/item-master/create", '_blank');

        })
        $(document).on('click','.item_refresh',function(){
            $.ajax({
                type: 'get',
                url:'/api/get-item-raw',
                success: function(res){
                    $('.item_select').empty();
                    appendSelectData('.item_select',res['raw_material_list'],"");
                }       
            });
        });

        $(document).on('change', '.item_select', function(){
            var id = $(this).val();
            var ele = $(this);
            showLoading();
            $.ajax({
                type: 'get',
                url:'/api/get-cost-sheet',
                data: {
                    entity_id: id,
                },
                success: function(res){
                    if(res && res[0] != undefined && res[0]['igm_cost_sheet'] != undefined && res[0]['igm_cost_sheet'] != null && res[0]['igm_cost_sheet'] != 0){
                        $(ele).parent().parent().parent().find('.pod_price').attr('max', res[0]['igm_cost_sheet']);
                    } else {
                        $(ele).parent().parent().parent().find('.pod_price').removeAttr('max');
                    }
                    stopLoading();
                }       
            });
        })
    });

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>