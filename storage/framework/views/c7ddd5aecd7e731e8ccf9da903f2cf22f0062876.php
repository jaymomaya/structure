<?php $__env->startSection('custom-styles'); ?>
  
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link href="<?php echo e(asset('/css/common/bootstrap-switch.min.css')); ?>" media="screen" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/css/common/bootstrap-datepicker.css')); ?>" />
    <link href="<?php echo e(asset('/css/common/chosen.min.css')); ?>" rel="stylesheet" type="text/css" />

<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo $__env->make('layouts.custom_partials.htmlheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    
    <?php echo $__env->make('layouts.custom_partials.contentheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<?php if(Request::segment(4)): ?>
    <?php echo Form::model($data, ['class' => 'form-validate', 'url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey()], 'method' => 'put','enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php else: ?>
    <?php echo Form::open(['class' => 'form-validate', 'url' => "/".Request::segment(1)."/".Request::segment(2),'enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php endif; ?>
<?php $prefix = config('constants.ItemMaster.prefix'); ?>
<?php $prefix_igm = config('constants.ItemGroupMaster.prefix'); ?>
<input type="hidden" name="screen_name" value = "<?php echo e((isset($data) && isset($data['screen_name'])) ? $data['screen_name'] : ''); ?>">
<section class="panel">
    <div class="panel-body">
        <h4>Basic Detail</h4>
        <div class="row">
            <div class = "col-md-4">
                <div class="form-group">
                    <label for="name">Name *</label>
                    <input type="text" class="form-control unit_name required" id="<?php echo e($prefix); ?>name" name = "<?php echo e($prefix); ?>name" placeholder="Item Name" value = "<?php echo e(htmlValue($prefix_igm.'name', $data)); ?>" <?php echo e(setDisable($prefix."name" , $data['disabled'])); ?>>
                </div>
            </div>
            <!-- <div class = "col-md-2">
                <div class="form-group">
                    <label for="name">Material Type *</label>
                    <?php echo e(Form::select('material_type',
                        (isset($data) && isset($data['material_type'])) ? $data['material_type'] : [],
                        htmlSelect($prefix_igm.'material_type', $data),
                        array('name'=>$prefix.'material_type', 'class' => 'form-control chosen-select material_type required', 'placeholder' => '', 'data-placeholder' => 'Select Material Type', setDisable($prefix.'material_type', $data['disabled'])))); ?>

                </div>
            </div> -->
            <!-- <div class="col-md-2">
                <div class="form-group">
                    <label>Select Vendor</label>
                    <i class="fa fa-plus pull-right vendor_add"></i>
                    <i class="fa fa-refresh pull-right vendor_refresh"></i>
                    <?php echo e(Form::select('vendor_id',
                        (isset($data) && isset($data['vendor_list'])) ? $data['vendor_list'] : [],
                        htmlSelect($prefix_igm.'vendor_id', $data),
                        array('name'=>$prefix.'vendor_id[]', 'id' => 'vendor_id', 'class' => 'form-control chosen-select vendor_select', 'data-name' => $prefix.'type', 'data-placeholder' => 'Select Vendor' , 'multiple' => 'multiple'))); ?>

                </div>
            </div> -->
            <div class = "col-md-2">
                <div class="form-group">
                    <label for="name">Item Group *</label>
                    <i class="fa fa-plus pull-right item_group_add"></i>
                    <i class="fa fa-refresh pull-right item_group_refresh"></i>
                    <?php echo e(Form::select('group_name',
                        (isset($data) && isset($data['group_name'])) ? $data['group_name'] : [],
                        htmlSelect($prefix_igm.'group_id', $data),
                        array('name'=>$prefix.'group_id', 'class' => 'form-control chosen-select required item_group_select', 'placeholder' => '', 'data-placeholder' => 'Select Item Group', setDisable($prefix.'group_id', $data['disabled'])))); ?>

                </div>
            </div>
            <div class = "col-md-2">
                <div class="form-group">
                    <label for="name">Unit</label>
                    <i class="fa fa-plus pull-right unit_add"></i>
                    <i class="fa fa-refresh pull-right unit_refresh"></i>
                    <?php echo e(Form::select('unit_master',
                        (isset($data) && isset($data['unit_master'])) ? $data['unit_master'] : [],
                        htmlSelect($prefix_igm.'unit', $data),
                        array('name'=>$prefix.'unit', 'class' => 'form-control chosen-select unit_select', 'placeholder' => '', 'data-placeholder' => 'Select Unit', setDisable($prefix.'unit', $data['disabled'])))); ?>

                </div>
            </div>
            <div class = "col-md-2">
                <div class="form-group">
                    <label for="name">Tax *</label>
                    <?php echo e(Form::select('tax_bracket',
                        (isset($data) && isset($data['tax_bracket'])) ? $data['tax_bracket'] : [],
                        (isset($data) && isset($data['igm_tax'])) ? htmlSelect($prefix_igm.'tax', $data) : 1,
                        array('name'=>$prefix.'tax', 'class' => 'form-control required chosen-select', 'placeholder' => '', 'data-placeholder' => 'Select Tax', setDisable($prefix.'unit', $data['disabled'])))); ?>

                </div>
            </div>
            <div class = "col-md-2">
                <div class="form-group">
                    <label for="name">HSN Code</label>
                    <input type="text" class="form-control required hsn_code" id="<?php echo e($prefix); ?>hsn_code" name = "<?php echo e($prefix); ?>hsn_code" placeholder="HSN Code" value = "<?php echo e(htmlValue($prefix_igm.'hsn_code', $data)); ?>" <?php echo e(setDisable($prefix."hsn_code" , $data['disabled'])); ?>>
                </div>
            </div> 
        </div>
        <div class="row">
            <div class = "col-md-8">
                <div class="form-group">
                    <label for="name">Remarks</label>
                    <input type="text" class="form-control remarks" id="<?php echo e($prefix); ?>remarks" name = "<?php echo e($prefix); ?>remarks" placeholder="Remarks" value = "<?php echo e(htmlValue($prefix_igm.'remarks', $data)); ?>" <?php echo e(setDisable($prefix."remarks" , $data['disabled'])); ?>>
                </div>
            </div> 
            <div class = "col-md-2">
                <div class="form-group">
                    <label for="rol">ROL</label>
                    <input type="text" class="form-control required number" id="<?php echo e($prefix); ?>rol" name = "<?php echo e($prefix); ?>rol" placeholder="ROL" value = "<?php echo e(htmlValue($prefix_igm.'rol', $data)); ?>" <?php echo e(setDisable($prefix."rol" , $data['disabled'])); ?>>
                </div>
            </div>
            <div class = "col-md-2">
                <div class="form-group">
                    <label for="rol">Cost Sheet</label>
                    <input type="text" class="form-control required number" id="<?php echo e($prefix); ?>cost_sheet" name = "<?php echo e($prefix); ?>cost_sheet" placeholder="Cost Sheet Price" value = "<?php echo e(htmlValue($prefix_igm.'cost_sheet', $data)); ?>" <?php echo e(setDisable($prefix."cost_sheet" , $data['disabled'])); ?>>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- <section class="panel">
    <div class="panel-body">
        <h4>Product Variation </h4>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="checkbox checkbox-sharekhan">
                        <input type="checkbox" name="igm_has_variation" class="igm_has_variation styled select" value="1"
                        <?php if(isset($data) && isset($data['igm_has_variation']) && $data['igm_has_variation'] == 1): ?>
                        checked="checked"
                        <?php endif; ?> 
                        >
                        <label><h5>Does this item has Variation.?</h5></label>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <?php if(isset($data) && isset($data['item_master']) && sizeof($data['item_master']) > 1): ?>
            <div class="product_row_parent">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Product Name</label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Size</label>
                            <i class="fa fa-plus pull-right size_add"></i>
                            <i class="fa fa-refresh pull-right size_refresh"></i>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Color</label>
                            <i class="fa fa-plus pull-right color_add"></i>
                            <i class="fa fa-refresh pull-right color_refresh"></i>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Style</label>
                            <i class="fa fa-plus pull-right style_add"></i>
                            <i class="fa fa-refresh pull-right style_refresh"></i>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group pull-right">
                            <i class="fa fa-lg fa-plus add_product_row"></i>
                        </div>
                    </div>
                </div>
            <?php $__currentLoopData = $data['item_master']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="row product_row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="text" class="form-control variant_name" name="variant[<?php echo e($value['item_master_id']); ?>][name]" placeholder="Name" data-name = "name" readonly="readonly" value="<?php echo e($value['im_name']); ?>">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <?php echo e(Form::select('size',
                                (isset($data) && isset($data['size'])) ? $data['size'] : [],
                                htmlSelect($prefix.'size', $value),
                                array('name'=>'variant['.$value['item_master_id'].'][size]', 'data-name' => 'size', 'class' => 'form-control chosen-select variant_size size_select', 'placeholder' => '', 'data-placeholder' => 'Select Size', setDisable($prefix.'group_id', $data['disabled'])))); ?>

                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <?php echo e(Form::select('color',
                                (isset($data) && isset($data['color'])) ? $data['color'] : [],
                                htmlSelect($prefix.'color', $value),
                                array('name'=>'variant['.$value['item_master_id'].'][color]', 'data-name' => 'color', 'class' => 'form-control chosen-select variant_color color_select', 'placeholder' => '', 'data-placeholder' => 'Select Color', setDisable('color', $data['disabled'])))); ?>

                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <?php echo e(Form::select('style',
                                (isset($data) && isset($data['style'])) ? $data['style'] : [],
                                htmlSelect($prefix.'style', $value),
                                array('name'=>'variant['.$value['item_master_id'].'][style]', 'data-name' => 'style', 'class' => 'form-control chosen-select variant_style style_select', 'placeholder' => '', 'data-placeholder' => 'Select Style', setDisable('style', $data['disabled'])))); ?>

                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group pull-right">
                            <?php if($key == 0): ?>
                            <i class="fa fa-trash fa-lg hide delete_row"></i>
                            <?php else: ?>
                            <i class="fa fa-trash fa-lg delete_row"></i>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        <?php else: ?>
        <div class="product_row_parent hide">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Product Name</label>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Size</label>
                        <i class="fa fa-plus pull-right size_add"></i>
                        <i class="fa fa-refresh pull-right size_refresh"></i>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Color</label>
                        <i class="fa fa-plus pull-right color_add"></i>
                        <i class="fa fa-refresh pull-right color_refresh"></i>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Style</label>
                        <i class="fa fa-plus pull-right style_add"></i>
                        <i class="fa fa-refresh pull-right style_refresh"></i>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group pull-right">
                        <i class="fa fa-lg fa-plus add_product_row"></i>
                    </div>
                </div>
            </div>
            <div class="row product_row">
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" class="form-control variant_name" name="variant[0][name]" placeholder="Name" data-name = "name" readonly="readonly">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <?php echo e(Form::select('size',
                            (isset($data) && isset($data['size'])) ? $data['size'] : [],
                            htmlSelect($prefix.'group_id', $data),
                            array('name'=>'variant[0][size]', 'data-name' => 'size', 'class' => 'form-control chosen-select variant_size size_select', 'placeholder' => '', 'data-placeholder' => 'Select Size', setDisable($prefix.'group_id', $data['disabled'])))); ?>

                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <?php echo e(Form::select('color',
                            (isset($data) && isset($data['color'])) ? $data['color'] : [],
                            htmlSelect($prefix.'group_id', $data),
                            array('name'=>'variant[0][color]', 'data-name' => 'color', 'class' => 'form-control chosen-select variant_color color_select', 'placeholder' => '', 'data-placeholder' => 'Select Color', setDisable('color', $data['disabled'])))); ?>

                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <?php echo e(Form::select('style',
                            (isset($data) && isset($data['style'])) ? $data['style'] : [],
                            htmlSelect($prefix.'group_id', $data),
                            array('name'=>'variant[0][style]', 'data-name' => 'style', 'class' => 'form-control chosen-select variant_style style_select', 'placeholder' => '', 'data-placeholder' => 'Select Style', setDisable('style', $data['disabled'])))); ?>

                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group pull-right">
                        <i class="fa fa-trash fa-lg hide delete_row"></i>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
</section> -->
<section class="panel">
    <div class="panel-body">
        <h4> <i class="fa fa-file"></i> Item Images </h4>
        <?php if(isset($data) && isset($data['screen_name']) && $data['screen_name'] != 'item-master-show'): ?>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="btn btn-sharekhan"><i class="fa fa-upload"></i> Upload Item Images
                            <input type="file" name="file[]" class="file_uploads" multiple="multiple" style="display: none;">
                        </label>
                        <div class="documents_names"></div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if(isset($data) && isset($data['id_file']) && sizeof($data['id_file']) > 0): ?>
            <div class="row">
                <?php $__currentLoopData = $data['id_file']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-3">
                        <div class="form-group">
                            <a href="<?php echo e($value['id_path']); ?>" target="_blank"><label class="btn btn-sharekhan"><i class="fa fa-file"></i> <?php echo e($value['id_doc_original_name']); ?></label></a>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        <?php endif; ?>
    </div>
</section>


<?php echo $__env->make('layouts.custom_partials.save_panel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['screen_name'] = $data['screen_name'];
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>
    <?php echo $__env->make('layouts.script_loaders.datatable_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        chosenInit();
        // $('.btn-success').prop('disabled',true);
        $(document).on('change', '.file_uploads', function(e) {
           var files = e.target.files;
           $('.documents_names').empty();
           var names = [];
           for (var i = 0; i < $(this).get(0).files.length; ++i) {
               names.push($(this).get(0).files[i].name);
           }
           if(names.length > 0)
           {
               var file_list = document.createElement('UL');
               
               $(names).each(function(k,v){
                   var remove_label = document.createElement('LABEL');
                   remove_label.append("REMOVE");
                   remove_label.setAttribute('style', 'color:blue; margin-left:15px');
                   var file_name_label = document.createElement("LI");
                   file_name_label.setAttribute('class', 'file_name_label');
                   file_name_label.append(v);
                   file_list.append(file_name_label);
               });
               $('.documents_names').append(file_list);
           }
        });
        $(document).on('click', '.add_product_row', function(){
            var row = $('.product_row:eq(0)').clone();
            var time = new Date().getTime();
            $(row).find('input').each(function(){
                var name = $(this).data('name');
                $(this).attr('name', 'variant['+time+']['+name+']');
            });
            $(row).find('select').each(function(k, v) {
                var n = $(v).attr('data-name');    
                $(v).attr('name', 'variant['+time+']['+n+']');
                $(v).siblings('div').remove();
                $(v).attr("selected", false).trigger("chosen:updated");;

            });
            row.find('.delete_row').removeClass('hide');
            $('.product_row_parent').append(row);
            chosenInit();
        })

        $(document).on('click', '.delete_row', function(){
            var row = $(this).parent().parent().parent();
            row.remove();
        });

        $(document).on('keyup change', '#im_name, .variant_size, .variant_style, .variant_color', function(){
            generateName();
        });

        function generateName(){
            $('.product_row').each(function(){
                var product_row = $(this);
                var name = $('#im_name').val();
                var size = $(this).find('.variant_size').find('option:selected').text();
                var style = $(this).find('.variant_style').find('option:selected').text();
                var color = $(this).find('.variant_color').find('option:selected').text();
                $(this).find('.variant_name').val('');
                $(this).find('.variant_name').val(name + ' - ' + size + ' ' + color + ' ' + style);

                var item = name + ' - ' + size + ' ' + color + ' ' + style;    
                $.ajax({
                type: 'get',
                url:'/masters/item-master/check-name',
                data : {
                    entity : 'check-name',
                    item : item
                },
                success: function(res){
                    console.log(1);
                    if(res != 0) {
                        console.log($('.variant_name').parent());
                        $(product_row).find('.variant_name').parent().addClass('has-error');
                        var p = "<p class = 'help-block error'>Name already exists</p>";
                        $(product_row).find('.variant_name').siblings('p').remove();
                        $(product_row).find('.variant_name').parent().append(p);
                        $('#save_return').prop('disabled', true);
                        } else {
                            $(product_row).find('.variant_name').parent().removeClass('has-error');
                            $(product_row).find('.variant_name').siblings('p').remove();
                            $('#save_return').prop('disabled', false);
                        }

                    }       
                });
            })
        }

        $(document).on('change', '.igm_has_variation', function(){
            if($('.igm_has_variation').is(':checked')){
                $('.product_row_parent').removeClass('hide');
                $('.product_row').each(function(){
                    console.log($(this).index());
                    if($(this).index() == 1){
                        $(this).find('input').val('');
                        $(this).find('select').val('').trigger('chosen:updated');
                    } else {
                        $(this).remove();
                    }
                })
            } else {
                $('.product_row_parent').addClass('hide');
            }
        })

        $(document).on('keyup blur','.unit_name',function(){
            var item = $(this).val();    
            $.ajax({
            type: 'get',
            url:'/masters/item-master/check-name',
            data : {
                entity : 'check-name',
                item : item
            },
            success: function(res){
                console.log(1);
                if(res != 0) {
                    console.log($('.unit_name').parent());
                    $('.unit_name').parent().addClass('has-error');
                    var p = "<p class = 'help-block error'>Name already exists</p>";
                    $('.unit_name').siblings('p').remove();
                    $('.unit_name').parent().append(p);
                    $('#save_return').prop('disabled', true);
                    } else {
                        $('.unit_name').parent().removeClass('has-error');
                        $('.unit_name').siblings('p').remove();
                        $('#save_return').prop('disabled', false);
                    }

                }       
            });
        })

        $(document).on('change','.material_type',function(){
            var val = $(this).val();
            if(val == 2)
            {
                $('.igm_has_variation').prop('disabled', true);
            }
            else
            {
                $('.igm_has_variation').prop('disabled', false);
            }
            console.log(val);
        })

        $(document).on('click','.vendor_add',function(){
            window.open("/masters/vendor-master/create", '_blank');

        })
        $(document).on('click','.vendor_refresh',function(){
            $.ajax({
                type: 'get',
                url:'/api/get-vendor',
                success: function(res){
                        $('.vendor_select').empty();
                        appendSelectData('.vendor_select',res['vendor_list'],"");
                    }       
                });
        })

        $(document).on('click','.item_group_add',function(){
            window.open("/masters/item-group/create", '_blank');

        })
        $(document).on('click','.item_group_refresh',function(){
            $.ajax({
                type: 'get',
                url:'/api/get-item-group',
                success: function(res){
                        $('.item_group_select').empty();
                        appendSelectData('.item_group_select',res['group_name'],"");
                    }       
                });
        })

        $(document).on('click','.unit_add',function(){
            window.open("http://"+window.location.host+"/masters/unit-master/create", '_blank');

        })
        $(document).on('click','.unit_refresh',function(){
            $.ajax({
                type: 'get',
                url:'/api/get-unit',
                success: function(res){
                        $('.unit_select').empty();
                        appendSelectData('.unit_select',res['unit_master'],"");
                    }       
                });
        })

        $(document).on('click','.size_add , .color_add, .style_add',function(){
            window.open("http://"+window.location.host+"/settings/setting?type=item-variation", '_blank');

        })
        $(document).on('click','.size_refresh',function(){
            $.ajax({
                type: 'get',
                url:'/api/get-size',
                success: function(res){
                        $('.size_select').empty();
                        appendSelectData('.size_select',res['size'],"");
                    }       
                });
        })

        $(document).on('click','.color_refresh',function(){
            $.ajax({
                type: 'get',
                url:'/api/get-color',
                success: function(res){
                        $('.color_select').empty();
                        appendSelectData('.color_select',res['color'],"");
                    }       
                });
        })

        $(document).on('click','.style_refresh',function(){
            $.ajax({
                type: 'get',
                url:'/api/get-style',
                success: function(res){
                        $('.style_select').empty();
                        appendSelectData('.style_select',res['style'],"");
                    }       
                });
        })
    });

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>