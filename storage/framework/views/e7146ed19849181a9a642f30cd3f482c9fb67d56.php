<?php $__env->startSection('custom-styles'); ?>
  
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link href="<?php echo e(asset('/css/common/bootstrap-switch.min.css')); ?>" media="screen" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/css/common/bootstrap-datepicker.css')); ?>" />
    <link href="<?php echo e(asset('/css/common/chosen.min.css')); ?>" rel="stylesheet" type="text/css" />

<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo $__env->make('layouts.custom_partials.htmlheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    
    <?php echo $__env->make('layouts.custom_partials.contentheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<?php if(Request::segment(4)): ?>
    <?php echo Form::model($data, ['class' => 'form-validate', 'url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey(), Request::segment(4)], 'method' => 'put','enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php else: ?>
    <?php echo Form::open(['class' => 'form-validate', 'url' => "/".Request::segment(1)."/".Request::segment(2),'enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php endif; ?>
<?php $prefix = config('constants.BillOfMaterialMaster.prefix'); ?>
<?php $prefix_rb = config('constants.RawMaterialBom.prefix'); ?>
<input type="hidden" name="screen_name" value = "<?php echo e((isset($data) && isset($data['screen_name'])) ? $data['screen_name'] : ''); ?>">
<section class="panel">
    <div class="panel-body">
        <h4> <i class="fa fa-book"></i> Meterial Basic Detail</h4>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Finish Goods *</label>
                    <?php echo e(Form::select('raw_material',
                        (isset($data) && isset($data['finish_goods_list'])) ? $data['finish_goods_list'] : [],
                        htmlSelect($prefix.'finish_goods_id', $data),
                        array('name'=>$prefix.'finish_goods_id', 'id' => 'finish_goods', 'class' => 'form-control chosen-select required', 'data-name' => $prefix_rb.'raw_material_id', 'placeholder' => '', 'data-placeholder' => 'Select Finish Good' ,setDisable($prefix.'finish_goods_id', $data['disabled'])))); ?>

                </div>
            </div>
            <div class = "col-md-2">
                <div class="form-group">
                    <label for="name">Material Unit</label>
                    <?php echo e(Form::select('unit',
                        (isset($data) && isset($data['unit_master'])) ? $data['unit_master'] : [],
                        (Request::segment(4) != null && Request::segment(4) == 'duplicate' ? null : htmlSelect($prefix.'unit', $data)),
                        array('name'=>$prefix.'unit', 'class' => 'form-control chosen-select bom_unit', 'placeholder' => '', 'data-placeholder' => 'Select Unit',setDisable($prefix.'unit', $data['disabled'])))); ?>

                </div>
            </div>
            <div class = "col-md-2">
                <div class="form-group">
                    <label for="name">Labour Cost (Amt)</label>
                    <input type="text" class="form-control labour_cost" id="<?php echo e($prefix); ?>labour_cost" name = "<?php echo e($prefix); ?>labour_cost" placeholder="Labour Cost Amount" value = "<?php echo e(htmlValue($prefix.'labour_cost', $data)); ?>" <?php echo e(setReadonly($prefix."labour_cost" , $data['readonly'])); ?>>
                </div>
            </div>
            <div class = "col-md-2">
                <div class="form-group">
                    <label for="name">Labour Cost (%)</label>
                    <input type="text" class="form-control labour_cost_per required number" max="100" min="0" id="<?php echo e($prefix); ?>labour_cost_per" name = "<?php echo e($prefix); ?>labour_cost_per" placeholder="Labour Cost Percentage" value = "<?php echo e(htmlValue($prefix.'labour_cost_per', $data)); ?>" <?php echo e(setReadonly($prefix."labour_cost_per" , $data['readonly'])); ?>>
                </div>
            </div>
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">No. of Workers</label>
                    <input type="text" class="form-control no_workers" id="<?php echo e($prefix); ?>no_workers" name = "<?php echo e($prefix); ?>no_workers" placeholder="No Workers" value = "<?php echo e(htmlValue($prefix.'no_workers', $data)); ?>" <?php echo e(setReadonly($prefix."no_workers" , $data['readonly'])); ?>>
                </div>
            </div>
            <div class = "col-md-12">
                <div class="form-group">
                    <label for="name">Remarks</label>
                    <input type="text" class="form-control remarks" id="<?php echo e($prefix); ?>remarks" name = "<?php echo e($prefix); ?>remarks" placeholder="Remarks" value = "<?php echo e(htmlValue($prefix.'remarks', $data)); ?>" <?php echo e(setReadonly($prefix."remarks" , $data['readonly'])); ?>>
                </div>
            </div> 
        </div>
    </div>
</section>
<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <h4> <i class="fa fa-book"></i> Ask Later Material details </h4>
            </div>
        </div>
        <?php if(isset($data) && isset($data['groups']) && sizeof($data['groups']) > 0): ?>
            <?php $__currentLoopData = $data['groups']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="row later_item_row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Group</label>
                        <?php echo e(Form::select('groups',
                            (isset($data)) ? $data['groups'] : [],
                            $key,
                            array('name'=>'item_details[variant]['.$key.']' ,'data-name' => 'wod_tax', 'class' => 'form-control  chosen-select item_group', 'placeholder' => '', 'data-placeholder' => 'NA', setDisable('item_group', $data['disabled'])))); ?>

                        <input type="hidden" name="item_details[<?php echo e($key); ?>][blm_parent_group_id]" value="<?php echo e($key); ?>" data-key=<?php echo e($key); ?>>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Quantity</label>
                        <input type="text" name="item_details[<?php echo e($key); ?>][blm_quantity]" class="form-control" placeholder="Quantity"
                        <?php if(isset($data['later_material']) && isset($data['later_material'][$key])): ?>
                        value="<?php echo e($data['later_material'][$key]); ?>" 
                        <?php endif; ?>
                        <?php echo e(setReadonly("later_qty" , $data['readonly'])); ?>>
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
    </div>
    <hr>
    <div class="panel-body bank_details_parent">
        <div class="row">
            <div class="col-md-4">
                <h4> <i class="fa fa-book"></i> Raw Material Details </h4>
            </div>
            <?php if(isset($data) && isset($data['screen_name']) && $data['screen_name'] != 'bill-of-material-show'): ?>
                <div class="col-md-8">
                    <i class="fa fa-plus-square fa-lg pull-right add-product-row" data-toggle="tooltip" title="Add Product Row"></i>
                </div>
            <?php endif; ?>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Group</label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Raw Material</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Unit</label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Quantity</label>
                </div>
            </div>
        </div>
        <?php if(isset($data) && isset($data['raw_bom']) && sizeof($data['raw_bom']) > 0): ?>
            <?php $__currentLoopData = $data['raw_bom']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="row product_details_row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <?php echo e(Form::select('raw_material',
                                (isset($data) && isset($data['item_groups'])) ? $data['item_groups'] : [],
                                htmlSelect($prefix_rb.'item_group_id', $value),
                                array('name'=>'raw_bom['.$key.'][rmb_item_group_id]', 'class' => 'form-control required chosen-select item_group_id', 'id'=>'raw_bom[0][item_group_id]','data-name' => $prefix_rb.'item_group_id', 'placeholder' => '', 'data-placeholder' => 'Select Group', setDisable('raw_bom', $data['disabled'])))); ?>

                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <?php echo e(Form::select('raw_material',
                                (isset($data) && isset($data['raw_material_list'])) ? $data['raw_material_list'] : [],
                                htmlSelect('rmb_raw_material_id', $value),
                                array('name'=>'raw_bom['.$key.'][rmb_raw_material_id]', 'class' => 'form-control chosen-select rmb_raw_material_id', 'data-name' => $prefix_rb.'raw_material_id', 'placeholder' => '','id'=>'raw_bom['.$key.'][rmb_raw_material_id]' ,'data-placeholder' => 'NA',setDisable('raw_bom', $data['disabled'])))); ?>

                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <?php echo e(Form::select('unit',
                                (isset($data) && isset($data['unit_master'])) ? $data['unit_master'] : [],
                                htmlSelect('rmb_unit', $value),
                                array('name'=>'raw_bom['.$key.'][rmb_unit]', 'data-name' => $prefix_rb.'unit', 'class' => 'form-control chosen-select rmb_unit', 'placeholder' => '', 'data-placeholder' => 'Select Unit',setDisable('raw_bom', $data['disabled'])))); ?>

                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <input type="text" name="raw_bom[<?php echo e($key); ?>][rmb_quantity]" placeholder="Quantity" class="rmb_quantity form-control required number " value="<?php echo e(htmlSelect('rmb_quantity', $value)); ?>" data-name ="<?php echo e($prefix_rb); ?>quantity" <?php echo e(setDisable('raw_bom', $data['disabled'])); ?>>
                        </div>
                    </div>
                    <?php if(isset($data) && isset($data['screen_name']) && $data['screen_name'] != 'bill-of-material-show' && $key != 0): ?>
                        <div class="col-md-1">
                            <i class="fa fa-trash delete_row fa-lg pull-right"></i>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
        <?php if(isset($data) && isset($data['screen_name']) && $data['screen_name'] == 'bill-of-material-create'): ?>
            <div class="row product_details_row">
                <div class="col-md-3">
                    <div class="form-group">
                        <?php echo e(Form::select('raw_material',
                            (isset($data) && isset($data['item_groups'])) ? $data['item_groups'] : [],
                            htmlSelect($prefix_rb.'item_group_id', $data),
                            array('name'=>'raw_bom[0][rmb_item_group_id]', 'class' => 'form-control required chosen-select item_group_id', 'id'=>'raw_bom[0][item_group_id]','data-name' => $prefix_rb.'item_group_id', 'placeholder' => '', 'data-placeholder' => 'Select Group'))); ?>

                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?php echo e(Form::select('raw_material',
                            [],
                            htmlSelect($prefix_rb.'raw_material_id', $data),
                            array('name'=>'raw_bom[0][rmb_raw_material_id]', 'class' => 'form-control chosen-select rmb_raw_material_id', 'id'=>'raw_bom[0][rmb_raw_material_id]','data-name' => $prefix_rb.'raw_material_id', 'placeholder' => '', 'data-placeholder' => 'NA'))); ?>

                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <?php echo e(Form::select('unit',
                            (isset($data) && isset($data['unit_master'])) ? $data['unit_master'] : [],
                            htmlSelect('raw_bom[0]['.$prefix_rb.'unit]', $data),
                            array('name'=>'raw_bom[0]['.$prefix_rb.'unit]', 'data-name' => $prefix_rb.'unit', 'class' => 'form-control chosen-select rmb_unit', 'placeholder' => '', 'data-placeholder' => 'Select Unit'))); ?>

                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <input type="text" name='raw_bom[0][<?php echo e($prefix_rb); ?>quantity]' placeholder="Quantity" class="form-control required number rmb_quantity" data-name ="<?php echo e($prefix_rb); ?>quantity">
                    </div>
                </div>
                <div class="col-md-1">
                    <i class="fa fa-trash delete_row hide fa-lg pull-right"></i>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>


<?php echo $__env->make('layouts.custom_partials.save_panel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['screen_name'] = $data['screen_name'];
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>
    <?php echo $__env->make('layouts.script_loaders.datatable_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        chosenInit();
        $(document).on('click', '.add-product-row', function(){
            
            var row = $('.product_details_row:eq(0)').clone();
            var time = new Date().getTime();
            
            $(row).find('input').each(function(){
                var name = $(this).data('name');
                $(this).attr('name', 'raw_bom['+time+']['+name+']');
                $(this).val('');
            });
            $(row).find('select').each(function(k, v) {
                var n = $(v).attr('data-name');    
                $(v).siblings('div').remove();
                $(v).attr("selected", false).trigger("chosen:updated");
                if(n == 'rmb_item_group_id'){
                    $(v).val($('.item_group_id:last').val()).trigger("chosen:updated");
                    getRawMaterial($(v));
                }
                $(v).attr('name', 'raw_bom['+time+']['+n+']');
                $(v).attr('id', 'raw_bom['+time+']['+n+']');

            });
            row.find('.delete_row').removeClass('hide');
            $('.bank_details_parent').append(row);
            chosenInit();
            raw_material();
        });

        $(document).on('change', '#finish_goods', function(){
            var name = $('#finish_goods').find('option:selected').text();
            names = name.split(' ');
            var new_name = '';
            $(names).each(function(k,v){
                new_name += v[0];
            })
            new_name = new_name.toUpperCase();
            $('.bom_name').val(new_name + Math.floor(Math.random() * 6) + 1);
            let id = $(this).val();
            var that = $(this);
            let curr_url = '/purchase-module/work-order/get-unit'
            $.ajax({
                url: curr_url,
                type: 'get',
                data: {
                    entity : id
                },
                success: function(result) {
                    if(result) {
                        $(that).closest('.row').find('.bom_unit').val(result[0]).trigger("chosen:updated");
                        console.log($(that));
                    } else {
                        $(that).closest('.row').find('.bom_unit').val('').trigger("chosen:updated");
                    }
                }
            });

        })
        $(document).on('change', '.rmb_raw_material_id', function(){

            var element = ($(this).attr('id'));
            raw_material();
                
            // raw_material(element);
            var name = $('.rmb_raw_material_id').find('option:selected').text();
            names = name.split(' ');
            var new_name = '';
            $(names).each(function(k,v){
                new_name += v[0];
            });

            new_name = new_name.toUpperCase();
            $('.bom_name').val(new_name + Math.floor(Math.random() * 6) + 1);
            let id = $(this).val();
            var that = $(this);
            let curr_url = '/purchase-module/work-order/get-unit'
            if(id != ''){
                $.ajax({
                    url: curr_url,
                    type: 'get',
                    data: {
                        entity : id
                    },
                    success: function(result) {
                        if(result) {
                            $(that).closest('.product_details_row').find('.rmb_unit').val(result[0]).trigger("chosen:updated");
                        } else {
                            $(that).closest('.product_details_row').find('.rmb_unit').val('').trigger("chosen:updated");
                            $(that).parents('.product_details_row').find('.rmb_quantity').val('0');
                        }
                    }
                })
            } else {
                $(that).closest('.product_details_row').find('.rmb_unit').val('').trigger("chosen:updated");
                $(that).parents('.product_details_row').find('.rmb_quantity').val('0');
                raw_material();
            }
        });

        $(document).on('click', '.delete_row', function(){
            var row = $(this).parent().parent();
            row.remove();
            raw_material();
        })

        function raw_material()
        {   
            var obj = {};
            $('.rmb_raw_material_id').each(function(){
                obj[$(this).attr('name')] = $(this).val();
                $(this).find('option').attr('disabled', false);
                $(this).trigger('chosen:updated');
            });
            
            $.each(obj, function(k,v){
                $('.rmb_raw_material_id').each(function(){
                    if($(this).attr('name') != k){
                        $(this).find('option[value="'+v+'"]').attr('disabled', true);
                        $(this).trigger('chosen:updated');
                    }
                })
            })
        }
        
        $(document).on('change', '.item_group_id', function(){
            getRawMaterial($(this)); 
        });

        if(lang.screen_name == 'bill-of-material-duplicate'){
            $('.item_group_id').each(function(){
                raw_material($(this));
            })
        }

        function getRawMaterial(ele){
            var id = $(ele).val();
            var grp_id = $(ele).parent().parent().parent().find('.rmb_raw_material_id');
            if(id != ''){
                $.ajax({
                    url: window.location.href,
                    type: 'get',
                    data: {
                        entity: 'get-raw-materials',
                        entity_id: id,
                    },
                    success: function(res){
                        $(grp_id).empty().trigger('chosen:updated');
                        appendSelectData(grp_id, res);
                        raw_material();
                    }, 
                    error: function(res){
                        $(grp_id).empty().trigger('chosen:updated');
                    }
                })
            } else {
                $(grp_id).empty().trigger('chosen:updated');
            }
        }
    });

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>