<ol class="breadcrumb">
    <li><a href="/<?php echo e(Request::segment(1)); ?>/<?php echo e(Request::segment(2)); ?>/"><i class="fa fa-users"></i> <?php echo e(ucwords(str_replace('-', ' ', Request::segment(1)))); ?> </a></li>
    <li><a href="/<?php echo e(Request::segment(1)); ?>/<?php echo e(Request::segment(2)); ?>/"><i class="fa fa-user"></i> <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?> </a></li>
    <?php if(Request::segment(4)): ?>
        <li><a href="javascript:;"><i class="fa fa-pencil"></i> <?php echo e(ucwords(str_replace('-', ' ', Request::segment(4)))); ?> </a></li>
    <?php else: ?>
        <?php if(Request::segment(3) || Request::segment(3) == "create"): ?>
            <li><a href="javascript:;"><i class="fa fa-plus"></i> <?php echo e(ucwords(str_replace('-', ' ', Request::segment(3)))); ?> </a></li>
        <?php elseif(Route::currentRouteName() == Request::segment(2).".show"): ?>
            <li><a href="javascript:;"><i class="fa fa-eye"></i> <?php echo e(ucwords(str_replace('-', ' ','show'))); ?> </a></li>
        <?php endif; ?>
    <?php endif; ?>
</ol>