<?php $__env->startSection('custom-styles'); ?>
    
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
    <link href="<?php echo e(asset('/css/datatables/buttons.bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('/css/common/chosen.min.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo e(ucwords(str_replace('-', ' ', Request::segment(1)))); ?> | <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
    
    <div class="panel">
        <?php echo Form::open(['class' => 'form-validate', 'url' => "/".Request::segment(1)."/".Request::segment(2),'enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

        <div class="panel-heading ">   
            <div class = "row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Work Order No</label>
                        <input type="text" name="wo_order_no" class="form-control required" id="wo_order_no" value="<?php echo e(htmlValue('wo_order_no', $data)); ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Purchase Order No</label>
                        <input type="text" name="po_order_no" class="form-control" id="po_order_no" value="<?php echo e(htmlValue('po_order_no', $data)); ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Select Work Order</label>
                        <?php echo e(Form::select('group_name',
                            (isset($data) && isset($data['group_name'])) ? $data['group_name'] : [],
                            htmlSelect('wo_id', $data),
                            array('name'=>'wo_id', 'class' => 'form-control chosen-select item_group_select', 'placeholder' => '', 'data-placeholder' => 'Select Work Order'))); ?>

                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group pull-right" style="margin-top: 15px">
                        <button type="submit" class="btn btn-success">Search</button>
                    </div>
                </div>
            </div>
        </div>
        <?php echo Form::close(); ?>

    </div>

    <section class="panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-3">
                    <h3><i class="fa fa-calendar"></i> Timeline</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <ul class="timeline">
                        <li class="time-label">
                            <span class="bg-red">
                                Work Order No:
                                <?php if(isset($data) && isset($data['work_order']) && isset($data['work_order'][0]) && isset($data['work_order'][0]['order_no'])): ?>
                                    <?php echo e($data['work_order'][0]['order_no']); ?>

                                <?php endif; ?>
                            </span>
                        </li>
                        <li>
                            <i class="fa fa-lg fa-envelope bg-blue"></i>
                            <div class="timeline-item">
                                
                                <h3 class="timeline-header"><a href="#">Work Order Created By:</a> 
                                    <?php if(isset($data) && isset($data['work_order']) && isset($data['work_order'][0]) && isset($data['work_order'][0]['created_by'])): ?>
                                        <?php echo e($data['work_order'][0]['created_by']); ?>

                                    <?php endif; ?>
                                </h3>
                                <div class="timeline-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Date</th>
                                                    <th>Customer</th>
                                                    <th>Total Amount</th>
                                                    <th>Total Quantity</th>
                                                    <th>Remarks</th>
                                                    <th>Approval Status</th>
                                                </tr>
                                            </thead>
                                            <?php if(isset($data) && isset($data['work_order']) && isset($data['work_order'][0]) && isset($data['work_order'][0])): ?>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo e($data['work_order'][0]['order_no']); ?></td>
                                                    <td><?php echo e($data['work_order'][0]['wo_date']); ?></td>
                                                    <td><?php echo e($data['work_order'][0]['customer']); ?></td>
                                                    <td><?php echo e($data['work_order'][0]['total_amount']); ?></td>
                                                    <td><?php echo e($data['work_order'][0]['total_quantity']); ?></td>
                                                    <td><?php echo e($data['work_order'][0]['remarks']); ?></td>
                                                    <td><?php echo e($data['work_order'][0]['approval_status']); ?></td>
                                                </tr>
                                            </tbody>
                                            <?php endif; ?>
                                        </table>
                                    </div>  
                                </div>
                                <div class="timeline-footer">
                                    <a class="btn btn-primary btn-xs" data-toggle="collapse" data-target="#work_order_details" >Read more</a>
                                    <div class="row collapse" id="work_order_details"> 
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Item</th>
                                                        <th>Unit</th>
                                                        <th>Quantity</th>
                                                        <th>Tax</th>
                                                        <th>Rate</th>
                                                        <th>Total</th>
                                                    </tr>
                                                </thead>
                                                <?php if(isset($data) && isset($data['work_order_details']) && isset($data['work_order_details']) && sizeof($data['work_order_details']) > 0): ?>
                                                <tbody>
                                                    <?php $__currentLoopData = $data['work_order_details']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr>
                                                        <td><?php echo e($value['im_name']); ?></td>
                                                        <td><?php echo e($value['um_name']); ?></td>
                                                        <td><?php echo e($value['wod_quantity']); ?></td>
                                                        <td><?php echo e($value['tax']); ?></td>
                                                        <td><?php echo e($value['wod_rate']); ?></td>
                                                        <td><?php echo e($value['wod_amount']); ?></td>
                                                    </tr>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </tbody>
                                                <?php endif; ?>
                                            </table>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <i class="fa fa-user bg-aqua"></i>
                            <div class="timeline-item">
                                <h3 class="timeline-header no-border"><a href="#">Approved By</a> </h3>
                            </div>
                        </li>
                        <br>
                        <li class="time-label">
                            <span class="bg-yellow">
                                Purchase Order Details
                            </span>
                        </li>
                        <li>
                            <i class="fa fa-id-card bg-blue"></i>
                            <div class="timeline-item">
                                
                                <div class="timeline-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Purchase Order Number</th>
                                                    <th>Vendor</th>
                                                    <th>Total Amount</th>
                                                    <th>Total Quantity</th>
                                                    <th>Date</th>
                                                    <th>Exp Delivery Date</th>
                                                    <th>Accounts Approval</th>
                                                    <th>Admin Approval</th>
                                                    <th>GR Status</th>
                                                    <th>QC Status</th>
                                                    <th>Created By</th>
                                                </tr>
                                            </thead>
                                            <?php if(isset($data) && isset($data['purchase_order']) && sizeof($data['purchase_order']) > 0): ?>
                                            <tbody>
                                                <?php $__currentLoopData = $data['purchase_order']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo e($value['po_purchase_order_number']); ?></td>
                                                    <td><?php echo e($value['vm_name']); ?></td>
                                                    <td><?php echo e($value['po_total_amount']); ?></td>
                                                    <td><?php echo e($value['po_total_quantity']); ?></td>
                                                    <td><?php echo e($value['po_date']); ?></td>
                                                    <td><?php echo e($value['po_exp_delivery_date']); ?></td>
                                                    <td><?php echo e($value['accounte_approval']); ?></td>
                                                    <td><?php echo e($value['admin_approval']); ?></td>
                                                    <td><?php echo e($value['goods_received']); ?></td>
                                                    <td><?php echo e($value['qc_status']); ?></td>
                                                    <td><?php echo e($value['created_by']); ?></td>
                                                </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>
                                            <?php endif; ?>
                                        </table>
                                    </div>  
                                </div>
                            </div>
                        </li>
                        <li class="time-label">
                            <span class="bg-blue">
                                Goods Received Details
                            </span>
                        </li>
                        <li>
                            <i class="fa fa-file-text-o bg-green"></i>
                            <div class="timeline-item">
                                
                                <div class="timeline-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Purchase Order Number</th>
                                                    <th>Goods Received Number</th>
                                                    <th>Goods Received Date</th>
                                                    <th>Total Quantity</th>
                                                    <th>Remarks</th>
                                                    <th>Created By</th>
                                                </tr>
                                            </thead>
                                            <?php if(isset($data) && isset($data['goods_received']) && sizeof($data['goods_received']) > 0): ?>
                                            <tbody>
                                                <?php $__currentLoopData = $data['goods_received']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo e($value['po_purchase_order_number']); ?></td>
                                                    <td><?php echo e($value['gr_goods_received_no']); ?></td>
                                                    <td><?php echo e($value['gr_date']); ?></td>
                                                    <td><?php echo e($value['gr_total_quantity']); ?></td>
                                                    <td><?php echo e($value['gr_remarks']); ?></td>
                                                    <td><?php echo e($value['created_by']); ?></td>
                                                </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>
                                            <?php endif; ?>
                                        </table>
                                    </div>  
                                </div>
                            </div>
                        </li>
                        <li class="time-label">
                            <span class="bg-maroon">
                                Quality Check Details
                            </span>
                        </li>
                        <li>
                            <i class="fa fa-check bg-red"></i>
                            <div class="timeline-item">
                                
                                <div class="timeline-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Purchase Order Number</th>
                                                    <th>Quality Check Number</th>
                                                    <th>Quality Check Date</th>
                                                    <th>Total Quantity</th>
                                                    <th>Remarks</th>
                                                    <th>Created By</th>
                                                </tr>
                                            </thead>
                                            <?php if(isset($data) && isset($data['quality_check']) && sizeof($data['quality_check']) > 0): ?>
                                            <tbody>
                                                <?php $__currentLoopData = $data['quality_check']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo e($value['po_purchase_order_number']); ?></td>
                                                    <td><?php echo e($value['qc_quality_check_number']); ?></td>
                                                    <td><?php echo e($value['qc_date']); ?></td>
                                                    <td><?php echo e($value['qc_total_pass']); ?></td>
                                                    <td><?php echo e($value['qc_remarks']); ?></td>
                                                    <td><?php echo e($value['created_by']); ?></td>
                                                </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>
                                            <?php endif; ?>
                                        </table>
                                    </div>  
                                </div>
                            </div>
                        </li>
                        <li class="time-label">
                            <span class="bg-aqua">
                                Work Order Dates
                            </span>
                        </li>
                        <li>
                            <i class="fa fa-calendar bg-yellow"></i>
                            <div class="timeline-item">
                                
                                <div class="timeline-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Start Date</th>
                                                    <th>End Date</th>
                                                    <th>Item</th>
                                                    <th>Quantity</th>
                                                </tr>
                                            </thead>
                                            <?php if(isset($data) && isset($data['work_order_dates']) && sizeof($data['work_order_dates']) > 0): ?>
                                            <tbody>
                                                <?php $__currentLoopData = $data['work_order_dates']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo e($value['wds_start_date']); ?></td>
                                                    <td><?php echo e($value['wds_end_date']); ?></td>
                                                    <td><?php echo e($value['im_name']); ?></td>
                                                    <td><?php echo e($value['wds_quantity']); ?></td>
                                                </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>
                                            <?php endif; ?>
                                        </table>
                                    </div>  
                                </div>
                            </div>
                        </li>
                        <li class="time-label">
                            <span class="bg-red">
                                Request Raw Material
                            </span>
                        </li>
                        <li>
                            <i class="fa fa-cubes bg-blue"></i>
                            <div class="timeline-item">
                                
                                <div class="timeline-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Request Number</th>
                                                    <th>Date</th>
                                                    <th>Total Quantity</th>
                                                    <th>Supply Status</th>
                                                    <th>Created By</th>
                                                </tr>
                                            </thead>
                                            <?php if(isset($data) && isset($data['request_raw_material']) && sizeof($data['request_raw_material']) > 0): ?>
                                            <tbody>
                                                <?php $__currentLoopData = $data['request_raw_material']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo e($value['rrmm_request_no']); ?></td>
                                                    <td><?php echo e($value['rrmm_date']); ?></td>
                                                    <td><?php echo e($value['rrmm_total_quantity']); ?></td>
                                                    <td><?php echo e($value['code_desc']); ?></td>
                                                    <td><?php echo e($value['created_by']); ?></td>
                                                </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>
                                            <?php endif; ?>
                                        </table>
                                    </div>  
                                </div>
                            </div>
                        </li>
                        <li class="time-label">
                            <span class="bg-green">
                                Send For Packaging
                            </span>
                        </li>
                        <li>
                            <i class="fa fa-archive bg-orange"></i>
                            <div class="timeline-item">
                                
                                <div class="timeline-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Request Number</th>
                                                    <th>Date</th>
                                                    <th>Total Quantity</th>
                                                    <th>Supply Status</th>
                                                    <th>Created By</th>
                                                </tr>
                                            </thead>
                                            <?php if(isset($data) && isset($data['send_for_packaging']) && sizeof($data['send_for_packaging']) > 0): ?>
                                            <tbody>
                                                <?php $__currentLoopData = $data['send_for_packaging']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo e($value['sfpm_request_no']); ?></td>
                                                    <td><?php echo e($value['sfpm_date']); ?></td>
                                                    <td><?php echo e($value['sfpm_total_quantity']); ?></td>
                                                    <td><?php echo e($value['code_desc']); ?></td>
                                                    <td><?php echo e($value['created_by']); ?></td>
                                                </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>
                                            <?php endif; ?>
                                        </table>
                                    </div>  
                                </div>
                            </div>
                        </li>
                        <li class="time-label">
                            <span class="bg-maroon">
                                Send to Warehouse
                            </span>
                        </li>
                        <li>
                            <i class="fa fa-industry bg-aqua"></i>
                            <div class="timeline-item">
                                
                                <div class="timeline-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Request Number</th>
                                                    <th>Date</th>
                                                    <th>Total Quantity</th>
                                                    <th>Supply Status</th>
                                                    <th>Created By</th>
                                                </tr>
                                            </thead>
                                            <?php if(isset($data) && isset($data['send_to_warehouse']) && sizeof($data['send_to_warehouse']) > 0): ?>
                                            <tbody>
                                                <?php $__currentLoopData = $data['send_to_warehouse']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo e($value['stw_request_no']); ?></td>
                                                    <td><?php echo e($value['stw_date']); ?></td>
                                                    <td><?php echo e($value['stw_total_quantity']); ?></td>
                                                    <td><?php echo e($value['code_desc']); ?></td>
                                                    <td><?php echo e($value['created_by']); ?></td>
                                                </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>
                                            <?php endif; ?>
                                        </table>
                                    </div>  
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['permissionList'] = $data['permissionList'];
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>

    <?php echo $__env->make('layouts.script_loaders.datatable_global', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo e(asset('/js/notify.js')); ?>"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            chosenInit();
            $(document).on('keyup change click blur', '#wo_order_no', function(){
                if($(this).val() != ''){
                    $('#po_order_no').val('');
                    $('.item_group_select').removeClass('required');
                    $('#wo_order_no').addClass('required');
                } else {
                    $('.item_group_select').addClass('required');
                    $('#wo_order_no').removeClass('required');
                }
            })

            $(document).on('keyup change click blur', '#po_order_no', function(){
                if($(this).val() != ''){
                    $('#wo_order_no').val('');
                    $('.item_group_select').addClass('required');   
                    $('#wo_order_no').removeClass('required');
                } else {
                    $('#wo_order_no').addClass('required');
                    $('.item_group_select').removeClass('required');
                }
            });

            $(document).on('blur', '#po_order_no', function(){
                showLoading();
                if($(this).val() != '')
                {
                    $.ajax({
                        url: window.location.href,
                        type: 'get',
                        data : {
                            po_order_no: $('#po_order_no').val(),
                        },
                        success: function(res){
                            $('.item_group_select').empty().trigger('chosen:updated');
                            appendSelectData('.item_group_select', res);
                            stopLoading();
                        }
                    })
                } else {
                    $('.item_group_select').empty().trigger('chosen:updated');
                    stopLoading();
                }
            })
        });
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>