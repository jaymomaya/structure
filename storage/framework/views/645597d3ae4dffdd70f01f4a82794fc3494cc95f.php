

<?php $__env->startSection('custom-styles'); ?>
  
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link href="<?php echo e(asset('/css/common/bootstrap-switch.min.css')); ?>" media="screen" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
    <link href="<?php echo e(asset('/css/common/chosen.min.css')); ?>" rel="stylesheet" type="text/css" />
    <style type="text/css">
        table input{
            background-color: transparent!important;
        } 
        .input-warning{
            background-color: #e1a9a9!important;  
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo $__env->make('layouts.custom_partials.htmlheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    
    <?php echo $__env->make('layouts.custom_partials.contentheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<?php if(Request::segment(4) && Request::segment(4) == 'edit'): ?>
    <?php echo Form::model($data, ['class' => 'form-validate', 'url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey()], 'method' => 'put','enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php elseif(Request::segment(4)): ?>
    <?php echo Form::model($data, ['class' => 'form-validate', 'url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey(), Request::segment(4)], 'method' => 'put','enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php else: ?>
    <?php echo Form::open(['class' => 'form-validate', 'url' => "/".Request::segment(1)."/".Request::segment(2),'enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php endif; ?>
<input type="hidden" name="screen_name" value = "<?php echo e((isset($data) && isset($data['screen_name'])) ? $data['screen_name'] : ''); ?>">
<section class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Work Order No</label>
                    <input type="text" name="wo_id" class="form-control" data-name="rvd_quantity" placeholder="Work Order No" value="<?php echo e(htmlValue('0', $data['work_order_no'])); ?>" <?php echo e(setDisable('all', $data['disabled'])); ?>>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Date</label>
                    <input type="date" name="rv_date" class="form-control" data-name="rvd_quantity" placeholder="Date" value="<?php echo e(htmlValue('rv_date', $data)); ?>" <?php echo e(setDisable('date', $data['disabled'])); ?>>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Remarks</label>
                    <input type="text" name="rv_remarks" class="form-control" data-name="rvd_quantity" placeholder="Remarks" value="<?php echo e(htmlValue('rv_remarks', $data)); ?>" <?php echo e(setDisable('remarks', $data['disabled'])); ?>>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="panel">
    <div class="panel-body">
        <div class="item_parent_row">
            <?php if(Request::segment(4) != '' && Request::segment(4) == 'approve' || Request::segment(4) == 'edit'): ?>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Item</label>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Unit</label>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Quantity</label> 
                    </div>
                </div>
                <div class="col-md-3">
                    <?php if(Request::segment(4) != '' && Request::segment(4) != 'approve' || Request::segment(4) == 'edit'): ?>
                    <label><i class="fa fa-plus fa-lg add_item_row"></i></label>
                    <?php endif; ?>
                </div>
            </div>
            <?php else: ?>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Item</label>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Unit</label>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <label>Req. Qty</label> 
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Godown</label> 
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <label>Avl. Qty</label> 
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <label>Prev. Qty</label> 
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <label>Supply Qty</label> 
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <?php if(isset($data) && isset($data['item_details']) && sizeof($data['item_details']) > 0): ?>
                <?php if(Request::segment(4) != '' && Request::segment(4) == 'approve' || Request::segment(4) == 'edit' ): ?>
                    <?php $__currentLoopData = $data['item_details']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="row item_details_row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <?php echo e(Form::select('raise_item',
                                    (isset($data) && isset($data['item_list'])) ? $data['item_list'] : [],
                                    htmlSelect('rvd_item', $value),
                                    array('name' => 'item_details['.$key.'][rvd_item]','class' => 'form-control required item_list chosen-select', 'data-name' => 'rvd_item','placeholder' => '', 'data-placeholder' => 'Select Item', setDisable('item', $data['disabled'])))); ?>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <?php echo e(Form::select('raise_item',
                                    (isset($data) && isset($data['unit_list'])) ? $data['unit_list'] : [],
                                    htmlSelect('rvd_unit', $value),
                                    array('name' => 'item_details['.$key.'][rvd_unit]','class' => 'form-control required unit_list chosen-select', 'data-name' => 'rvd_unit','placeholder' => '', 'data-placeholder' => 'Select Unit', setDisable('unit', $data['disabled'])))); ?>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" name="item_details[<?php echo e($key); ?>][rvd_quantity]" class="form-control required quantity number" data-name="rvd_quantity" placeholder="Quantity" value="<?php echo e(htmlValue('rvd_quantity', $value)); ?>" <?php echo e(setDisable('qty', $data['disabled'])); ?>>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <?php if(Request::segment(4) != '' && Request::segment(4) == 'edit'): ?>
                                <?php if($key == 0): ?>
                                    <label><i class="fa fa-trash delete_item_row hide fa-lg pull-right"></i></label>
                                <?php else: ?> 
                                    <label><i class="fa fa-trash delete_item_row fa-lg pull-right"></i></label>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?> 
                     <?php $__currentLoopData = $data['item_details']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="row item_details_row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <?php echo e(Form::select('raise_item',
                                    (isset($data) && isset($data['item_list'])) ? $data['item_list'] : [],
                                    htmlSelect('rvd_item', $value),
                                    array('name' => 'item_details['.$value['raise_voucher_details_id'].'][rvd_item]','class' => 'form-control required item_list chosen-select', 'data-name' => 'rvd_item','placeholder' => '', 'data-placeholder' => 'Select Item', setDisable('all', $data['disabled'])))); ?>

                                <input type="hidden" name="item_details[<?php echo e($value['raise_voucher_details_id']); ?>][rvd_item]" value="<?php echo e(htmlSelect('rvd_item', $value)); ?>">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <?php echo e(Form::select('raise_item',
                                    (isset($data) && isset($data['unit_list'])) ? $data['unit_list'] : [],
                                    htmlSelect('rvd_unit', $value),
                                    array('name' => 'item_details['.$value['raise_voucher_details_id'].'][rvd_unit]','class' => 'form-control unit_list chosen-select', 'data-name' => 'rvd_unit','placeholder' => '', 'data-placeholder' => 'Select Unit', setDisable('all', $data['disabled'])))); ?>

                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <input type="text" name="item_details[<?php echo e($value['raise_voucher_details_id']); ?>][rvd_quantity]" class="form-control required quantity number" data-name="rvd_quantity" placeholder="Quantity" value="<?php echo e(htmlValue('rvd_quantity', $value)); ?>" <?php echo e(setDisable('all', $data['disabled'])); ?>>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <?php echo e(Form::select('raise_item',
                                    (isset($data) && isset($data['godown_list'])) ? $data['godown_list'] : [],
                                    [],
                                    array('name' => 'item_details['.$value['raise_voucher_details_id'].'][godown]', 'data-key' => htmlSelect('rvd_item', $value), 'class' => 'form-control required godown_list chosen-select', 'data-name' => 'godown','placeholder' => '', 'data-placeholder' => 'Select Godown'))); ?>

                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <input type="text" name="item_details[<?php echo e($value['raise_voucher_details_id']); ?>][rvd_quantity]" class="form-control required available number" data-name="rvd_quantity" placeholder="Quantity" <?php echo e(setDisable('all', $data['disabled'])); ?> data-need = "<?php echo e(htmlValue('rvd_quantity', $value)); ?>">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <input type="text" name="item_details[<?php echo e($value['raise_voucher_details_id']); ?>][prev_qty]" class="form-control required prev_qty number" data-name="prev_qty" placeholder="Prev Quantity" value="<?php echo e(htmlValue('prev_qty', $value)); ?>"  readonly="readonly">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <input type="text" name="item_details[<?php echo e($value['raise_voucher_details_id']); ?>][rvd_quantity]" class="form-control required supply_qty number" data-name="rvd_quantity" placeholder="Quantity" min="0" max="<?php echo e(htmlValue('rvd_quantity', $value)); ?>">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <label><i class="fa fa-trash delete_item_row hide fa-lg pull-right"></i></label>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <?php if(Request::segment(4) != '' && Request::segment(4) == 'supply'): ?>
        <div class="row">
            <div class="col-md-3 pull-right">
                <div class="form-group">
                    <label>Total Quantity Supplied</label>
                    <input type="input" name="total_qty" id="total_qty" class="form-control" readonly="readonly">
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
</section>
<section class="panel">
    <div class="panel-body">
        <?php if(Request::segment(4) != '' && Request::segment(4) == 'approve'): ?>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Remakrs</label>
                    <input type="text" name="rv_approve_remarks" class="form-control" placeholder="Remarks">
                </div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(Request::segment(4) != '' && Request::segment(4) != 'approve'): ?>
        <div class="btn-group">
            <button class="btn btn-success" name="save" value="save" type="submit" id="save_continue">Save </button>
        </div>
        <?php else: ?>
        <div class="btn-group">
            <button class="btn btn-success" name="rv_approve_status" value="3" type="submit" id="save_continue"><i class="fa fa-check fa-lg"></i> Approved </button>
        </div>
        <div class="btn-group">
            <button class="btn btn-danger" name="rv_approve_status" value="4" type="submit" id="save_continue"> <i class="fa fa-times fa-lg"></i> Reject </button>
        </div>
        <?php endif; ?>
        <div class="btn-group pull-right">
            <a href="/<?php echo e(Request::segment(1)); ?>/<?php echo e(Request::segment(2)); ?>/"><button class="btn btn-danger" name="cancel" value="cancel" type="button" id="cancel">Cancel</button></a>
        </div>
    </div>
</section>

<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['screen_name'] = $data['screen_name'];
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>
    <?php echo $__env->make('layouts.script_loaders.datatable_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        chosenInit();
        
        $(document).on('change', '.godown_list', function(){
            var id = $(this).data('key');
            var godown_id = $(this).val();
            var ele = $(this);
            var curr = $(ele).parent().parent().parent().find('.available');
            var supply_qty = $(ele).parent().parent().parent().find('.supply_qty');
            showLoading();
            $.ajax({
                url: '/api/get-item-stock',
                type: 'get',
                data:{
                    id: id,
                    godown_id: godown_id
                }, success : function(res){
                    if(res[0] != undefined && res[0]['available'] != undefined){
                        console.log($(curr).data('need'), res[0]['available']);
                        if(parseFloat($(curr).data('need')) > res[0]['available']){
                            $(supply_qty).attr('max', res[0]['available']);
                            $(supply_qty).val(res[0]['available']);
                        } else {
                            $(supply_qty).attr('max', $(curr).data('need'));
                            $(supply_qty).val($(curr).data('need'));
                        }
                        $(curr).val(res[0]['available']);
                    } 
                    else {
                        $(curr).val(0);  
                        $(supply_qty).val(0);
                    }
                    stopLoading();
                    calculateTotal();
                }
            })
        })

        $(document).on('keyup', '.supply_qty', function(){
            calculateTotal();
        })

        function calculateTotal(){
            var total = 0;
            $('.supply_qty').each(function(){
                var i = parseFloat($(this).val());
                total = total + i;
            })
            $('#total_qty').val(total);
        }

        $(document).on('click', '.delete_item_row', function(){
            $(this).parent().parent().parent().remove();
        });

        $(document).on('click', '.add_item_row', function(){
            var row = $('.item_details_row:eq(0)').clone();
            var time = new Date().getTime();
            row.find('input').each(function(){
                $(this).val('');
                var n = $(this).data('name');
                $(this).attr('name', 'item_details['+time+']['+n+']');
            });
            $(row).find('select').each(function(k, v) {
                var n = $(v).attr('data-name');    
                $(v).attr('name', 'item_details['+time+']['+n+']');
                $(v).siblings('div').remove();
                $(v).val('').trigger('chosen:updated');
            });
            $(row).find('.delete_item_row').removeClass('hide');
            $('.item_parent_row').append(row);
            chosenInit();
        });

        $(document).on('change', '.item_list', function() {
            var id = $(this).val();
            var that = $(this);
            $.ajax({
                url: window.location.href,
                type: 'get',
                data: {
                    entity : 'get-unit',
                    entity_id : id,
                },
                success: function(result) {
                    if(result) {
                        $(that).closest('.item_details_row').find('.unit_list').val(result['im_unit']).trigger("chosen:updated");
                    } else {
                        $(that).closest('.item_details_row').find('.unit_list').val('').trigger("chosen:updated");
                    }
                }
            });
        });
    });

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>