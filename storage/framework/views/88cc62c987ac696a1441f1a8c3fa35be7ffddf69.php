<script type="text/javascript" src="<?php echo e(asset('/js/datatablesglobal/datatables.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('/js/datatablesglobal/jquery.dataTables.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('/js/datatablesglobal/dataTables.buttons.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('/js/datatablesglobal/buttons.colVis.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('/js/datatablesglobal/jszip.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('/js/datatablesglobal/pdfmake.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('/js/datatablesglobal/vfs_fonts.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('/js/datatablesglobal/buttons.html5.min.js')); ?>"></script>