

<?php $__env->startSection('custom-styles'); ?>
    
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
    <link href="<?php echo e(asset('/css/common/chosen.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('/css/datatables/buttons.bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />

<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo e(ucwords(str_replace('-', ' ', Request::segment(1)))); ?> | <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('main-content'); ?>
<?php if(Request::segment(4)): ?>
    <?php echo Form::model($data, ['class' => 'form-validate', 'url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey()], 'method' => 'put','enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php else: ?>
    <?php echo Form::open(['class' => 'form-validate', 'url' => "/".Request::segment(1)."/".Request::segment(2),'enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php endif; ?>
    <input type="hidden" name="type" id="type">
    <section class="panel">
        <div class="panel-heading">
            <ul class="nav nav-tabs">
                <li class = "settings_tabs logo"><a href="?type=logo">Logo</a></li>
                <li class = "settings_tabs address"><a href="?type=address">Address</a></li>
                <li class = "settings_tabs item_variation"><a href="?type=item-variation">Item Variations</a></li>
            </ul>
        </div>
        <div class="panel-body hide setting_body logo_body">   
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label><h4>Company Logo</h4></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="btn btn-sharekhan"><i class="fa fa-upload"></i> Upload Logo
                            <input type="file" name="file" class="file_uploads" accept="image/*" id="imgInp" style="display: none;">
                        </label>
                        <div class="documents_names"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Preview</label>
                        <br>
                        <?php if(isset($data) && isset($data[0]) && isset($data[0]['sm_logo_src']) && $data[0]['sm_logo_src'] != ''): ?>
                            <img id="blah" src="/img/<?php echo e($data[0]['sm_logo_src']); ?>" alt="your image"  height="<?php echo e($data[0]['sm_logo_height']); ?>" width="<?php echo e($data[0]['sm_logo_width']); ?>">
                            <input type="hidden" name="sm_logo_height" id="sm_logo_height">
                            <input type="hidden" name="sm_logo_width" id="sm_logo_width">
                        <?php else: ?>
                            <img id="blah" src="/img/preview.png" alt="your image"  height="250px" width="250px">
                            <input type="hidden" name="sm_logo_height" id="sm_logo_height">
                            <input type="hidden" name="sm_logo_width" id="sm_logo_width">
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body hide setting_body address_body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Address Line 1</label>
                        <input type="text" name="address[sm_address_1]" placeholder="Address Line 1" class="form-control" value="<?php echo e((isset($data[0])) ? htmlValue('sm_address_1', $data[0]) : ''); ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Pincode</label>
                        <input type="text" name="address[sm_pincode]" placeholder="Pincode" class="form-control" value="<?php echo e((isset($data[0])) ? htmlValue('sm_pincode', $data[0]) : ''); ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>State</label>
                        <select class="chosen-select state_list" name="address[sm_state]">
                        <?php if(isset($data) && isset($data['state_list'])): ?>
                        <?php $__currentLoopData = $data['state_list']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option
                            <?php if(isset($data) && isset($data[0]) && isset($data[0]['sm_state']) && $data[0]['sm_state'] == $value['states_id']): ?>
                            selected = 'selected'
                            <?php endif; ?>
                            data-country="<?php echo e($value['s_country']); ?>" value="<?php echo e($value['states_id']); ?>"><?php echo e($value['s_name']); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Address Line 2</label>
                        <input type="text" name="address[sm_address_2]" placeholder="Address Line 2" class="form-control" value="<?php echo e((isset($data[0])) ? htmlValue('sm_address_2', $data[0]) : ''); ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>City</label>
                        <input type="text" name="address[sm_city]" placeholder="City" class="form-control" value="<?php echo e((isset($data[0])) ? htmlValue('sm_city', $data[0]) : ''); ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Country</label>
                        <input type="text" name="address[sm_country]" placeholder="Country" class="form-control" value="<?php echo e((isset($data[0])) ? htmlValue('sm_country', $data[0]) : ''); ?>">
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body hide setting_body variation_body">
            <div class="row">
                <div class="col-md-4 size_row_parent">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Size</label>
                                <i class="add_size_row fa fa-plus fa-lg" style="margin-left: 25px"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row sample_size_row hide">
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="text" class="form-control required" data-name="vs_size">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <i class="fa fa-trash delete_row hide fa-lg"></i>
                        </div>
                    </div>
                    <?php if(isset($data) && isset($data['size']) && sizeof($data['size']) > 0): ?>
                        <?php $__currentLoopData = $data['size']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="row size_row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="text" class="form-control required" name="size[<?php echo e($value['variation_size_id']); ?>][vs_size]" data-name="vs_size" value="<?php echo e($value['vs_size']); ?>">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <i class="fa fa-trash delete_row fa-lg" data-id = "<?php echo e($value['variation_size_id']); ?>" data-type = "size"></i>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                        <div class="row size_row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="text" class="form-control required" name="size[0][vs_size]" data-name="vs_size">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <i class="fa fa-trash delete_row hide fa-lg"></i>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="col-md-4 color_row_parent">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Color</label>
                                <i class="add_color_row fa fa-plus fa-lg" style="margin-left: 25px"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row sample_color_row hide">
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="text" class="form-control required" data-name="vc_color">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <i class="fa fa-trash delete_row hide fa-lg"></i>
                        </div>  
                    </div>
                    <?php if(isset($data) && isset($data['color']) && sizeof($data['color']) > 0): ?>
                        <?php $__currentLoopData = $data['color']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="row color_row">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input type="text" class="form-control required" name="color[<?php echo e($value['variation_color_id']); ?>][vc_color]" data-name="vc_color" value="<?php echo e($value['vc_color']); ?>">
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <i class="fa fa-trash delete_row fa-lg" data-id = "<?php echo e($value['variation_color_id']); ?>" data-type = "color"></i>
                                </div>  
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?> 
                        <div class="row color_row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="text" class="form-control required" name="color[0][vc_color]" data-name="vc_color">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <i class="fa fa-trash delete_row hide fa-lg"></i>
                            </div>  
                        </div>
                    <?php endif; ?>
                </div>
                <div class="col-md-4 style_row_parent">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Style</label>
                                <i class="add_style_row fa fa-plus fa-lg" style="margin-left: 25px"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row sample_style_row hide">
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="text" class="form-control required" data-name="vst_style">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <i class="fa fa-trash delete_row hide fa-lg"></i>
                        </div>
                    </div>
                    <?php if(isset($data) && isset($data['style']) && sizeof($data['style']) > 0): ?>
                        <?php $__currentLoopData = $data['style']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="row style_row">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input type="text" class="form-control required" name="style[<?php echo e($value['variation_style_id']); ?>][vst_style]" data-name="vst_style" value="<?php echo e($value['vst_style']); ?>">
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <i class="fa fa-trash delete_row fa-lg" data-id = "<?php echo e($value['variation_style_id']); ?>" data-type = "style"></i>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                        <div class="row style_row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="text" class="form-control required" name="style[0][vst_style]" data-name="vst_style">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <i class="fa fa-trash delete_row hide fa-lg"></i>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

<?php echo $__env->make('layouts.custom_partials.save_panel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['prefix'] = $data['prefix'];
        $js_data['status'] = config('constants.STATUS');
        $js_data['color'] = config('constants.COLOR_GENERAL');
        $js_data['screen_name'] = $data['screen_name'];
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>

    <?php echo $__env->make('layouts.script_loaders.datatable_global', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo e(asset('/js/notify.js')); ?>"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            var param = getParameterByName('type');
            chosenInit();
            if(param == 'item-variation')
            {
                $('.settings_tabs').removeClass('active');
                $('.item_variation').addClass('active');
                $('.setting_body').addClass('hide');
                $('.variation_body').removeClass('hide');
                $('#type').val('item-variation');
            } else if(param == 'address'){
                $('.settings_tabs').removeClass('active');
                $('.address').addClass('active');
                $('.setting_body').addClass('hide');
                $('.address_body').removeClass('hide');
                $('#type').val('address');
            }
            else
            {
                $('.settings_tabs').removeClass('active');
                $('.logo').addClass('active');
                $('.setting_body').addClass('hide');
                $('.logo_body').removeClass('hide');
                $('#type').val('logo');
            }

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        var image = new Image();
                        image.src = e.target.result;
                        $('#blah').attr('src', e.target.result);
                        image.onload = function () {
                            var height = this.height;
                            var width = this.width;
                            if(height >= width){
                                min_height = height / 450;
                                height = height / min_height;
                                width = width / min_height;
                            } else {
                                min_width = width / 450;
                                height = height / min_width;
                                width = width / min_width;
                            }
                            $('#blah').attr('height', height);
                            $('#blah').attr('width', width);
                            $('#sm_logo_height').val(height);
                            $('#sm_logo_width').val(width)
                        };
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#imgInp").change(function(e) {
                readURL(this);
            });

            $(document).on('click', '.add_size_row', function(){
                var row = $('.sample_size_row:eq(0)').clone();
                $(row).removeClass('sample_size_row').addClass('size_row');
                $(row).removeClass('hide');
                var time = new Date().getTime();
                row.find('input').each(function(){
                    var data_name = $(this).attr('data-name');
                    $(this).attr('name', 'size['+time+']['+data_name+']');
                    $(this).val('');
                });
                row.find('.delete_row').removeClass('hide');
                $('.size_row_parent').append(row);
            });

            $(document).on('click', '.add_color_row', function(){
                var row = $('.sample_color_row:eq(0)').clone();
                $(row).removeClass('sample_color_row').addClass('color_row');
                $(row).removeClass('hide');
                var time = new Date().getTime();
                row.find('input').each(function(){
                    var data_name = $(this).attr('data-name');
                    $(this).attr('name', 'color['+time+']['+data_name+']');
                    $(this).val('');
                });
                row.find('.delete_row').removeClass('hide');
                $('.color_row_parent').append(row);
            });

            $(document).on('click', '.add_style_row', function(){
                var row = $('.sample_style_row:eq(0)').clone();
                $(row).removeClass('sample_style_row').addClass('style_row');
                $(row).removeClass('hide');
                var time = new Date().getTime();
                row.find('input').each(function(){
                    var data_name = $(this).attr('data-name');
                    $(this).attr('name', 'style['+time+']['+data_name+']');
                    $(this).val('');
                });
                row.find('.delete_row').removeClass('hide');
                $('.style_row_parent').append(row);
            });

            $(document).on('click', '.delete_row', function(){
                var ele = $(this);
                var data_id = $(this).data('id');
                var type = $(this).data('type');
                console.log(data_id, type);
                if(data_id == '' || type == '' || data_id == undefined || type == undefined){
                    $(this).parent().parent().remove();
                } else {
                    $.ajax({
                        url: '/settings/setting/delete-variation',
                        type: 'put',
                        data: {
                            entity_type: type,
                            entity_id: data_id
                        }, success: function(res){
                            if(res == 'success'){
                                $.notify('Record Deleted', 'success');
                                $(ele).parent().parent().remove();
                            } else {
                                $.notify('Something Went Wrong', 'danger');
                            }
                        }

                    })
                }
            })
        });
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>