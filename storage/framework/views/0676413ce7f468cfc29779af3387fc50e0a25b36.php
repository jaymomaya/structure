

<?php $__env->startSection('custom-styles'); ?>
  
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link href="<?php echo e(asset('/css/common/bootstrap-switch.min.css')); ?>" media="screen" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
    <link href="<?php echo e(asset('/css/common/chosen.min.css')); ?>" rel="stylesheet" type="text/css" />
    <style type="text/css">
        table input{
            background-color: transparent!important;
        } 
        .input-warning{
            background-color: #e1a9a9!important;  
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo $__env->make('layouts.custom_partials.htmlheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    
    <?php echo $__env->make('layouts.custom_partials.contentheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<?php if(Request::segment(4)): ?>
    <?php echo Form::model($data, ['class' => 'form-validate', 'url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey(), Request::segment(4)], 'method' => 'put','enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php else: ?>
    <?php echo Form::open(['class' => 'form-validate', 'url' => "/".Request::segment(1)."/".Request::segment(2),'enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php endif; ?>
<input type="hidden" name="screen_name" value = "<?php echo e((isset($data) && isset($data['screen_name'])) ? $data['screen_name'] : ''); ?>">
<section class="panel">
    <div class="panel-body">
        <div class="row">
            <?php if(Request::segment(4) != 'send-for-packaging'): ?>
                <div class="col-md-7">
            <?php else: ?>
                <div class="col-md-12">
            <?php endif; ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><i class="fa fa-book"></i> Work Order Details</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead style="background-color:grey">
                              <tr style="color:white">
                                <?php if(Request::segment(4) != 'add-delivery-date' || Request::segment(4) != 'edit-delivery-date' || Request::segment(4) != 'approve-delivery-date'): ?>
                                    <th>Request No.</th>
                                <?php endif; ?>
                                <th>Work Order No.</th>
                                <th>Work Order Date</th>
                                <th>Customer</th>
                                <th>Total Quantity</th>
                                <th>Total Amount</th>
                              </tr>
                            </thead>
                            <tbody>
                                <?php if(isset($data)): ?>
                                    <tr>
                                        <?php if(Request::segment(4) != 'add-delivery-date' || Request::segment(4) != 'edit-delivery-date' || Request::segment(4) != 'approve-delivery-date'): ?>
                                            <td><?php echo e(htmlValue('request_no', $data)); ?></td>
                                        <?php endif; ?>
                                        <td><?php echo e(htmlValue('wo_order_no', $data)); ?></td>
                                        <td><?php echo e(htmlValue('wo_date', $data)); ?></td>
                                        <td><?php echo e(htmlValue('wo_customer', $data)); ?></td>
                                        <td><?php echo e(round(htmlValue('wo_total_quantity', $data), 2)); ?></td>
                                        <td><?php echo e(round(htmlValue('wo_total_amount', $data), 2)); ?></td>
                                    </tr>
                                <?php endif; ?>
                                <tr>
                                    <th>Remarks: </th>
                                    <td colspan="4"><?php echo e(htmlValue('wo_remarks', $data)); ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php if(Request::segment(4) != 'send-for-packaging'): ?>
            <div class="row col-md-5">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><i class="fa fa-cubes"></i> Finish Goods Details</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead style="background-color:grey">
                          <tr style="color:white">
                            <th>Sr. No.</th>
                            <th>Item</th>
                            <th>Quantity</th>
                            <th>Unit</th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php if(isset($data) && isset($data['finish_goods_details'])): ?>
                                <?php $__currentLoopData = $data['finish_goods_details']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($key+1); ?></td>
                                        <td><?php echo e($value['item']); ?></td>
                                        <td><?php echo e(round($value['quantity'], 2)); ?></td>
                                        <td><?php echo e($value['unit']); ?></td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php endif; ?>
        </div>
        <?php if(Request::segment(4) != '' && Request::segment(4) == 'raise-voucher'): ?>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Remarks</label>
                    <input type="text" name="rv_remarks" class="form-control" placeholder="Remarks">
                </div>
            </div>
        </div>
        <?php endif; ?>
        <hr>
        <?php if(Request::segment(4) != '' && (Request::segment(4) == 'add-delivery-date' || Request::segment(4) == 'edit-delivery-date' || Request::segment(4) == 'approve-delivery-date')): ?>
            <div class="row">
                <div class="col-md-4">
                    <label><i class="fa fa-cubes"></i> Required Raw Material Details</label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-11">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" border="1px" id="table">
                            <thead style="background-color:grey">
                                <tr style="color:white">
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <?php $__currentLoopData = $data['finish_goods_details']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <th>
                                            <?php echo e($value['item']); ?> <span class="pull-right"> <?php echo e(round($value['quantity'], 2)); ?> <?php echo e($value['unit']); ?></span>
                                            <input type="hidden" class="max" value="<?php echo e(round($value['quantity'], 2)); ?>">
                                        </th>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                                </tr>
                            </thead>
                            <tbody class="date_body">
                                <?php if(isset($data) && isset($data['dates']) && sizeof($data['dates']) > 0): ?>
                                    <?php $__currentLoopData = $data['dates']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k1 => $v1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><input type="date" name="date[<?php echo e($k1); ?>][start_date]" class="form-control required start_date" data-key="start_date" min="<?php echo e(date('Y-m-d')); ?>" value="<?php echo e(htmlValue('wds_start_date', $v1)); ?>" <?php echo e(setDisable('all', $data['disabled'])); ?>></td>
                                        <td><input type="date" name="date[<?php echo e($k1); ?>][end_date]" class="form-control required end_date" data-key="end_date" min="<?php echo e(date('Y-m-d')); ?>" value="<?php echo e(htmlValue('wds_end_date', $v1)); ?>" <?php echo e(setDisable('all', $data['disabled'])); ?>></td>
                                        <?php $__currentLoopData = $data['finish_goods_details']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <td>
                                                <input type="text" class="form-control number count_value" name="date[<?php echo e($k1); ?>][<?php echo e($value['work_order_details_id']); ?>]" data-key="<?php echo e($value['work_order_details_id']); ?>" value="<?php echo e(htmlValue($value['work_order_details_id'], $v1['aqty'])); ?>" <?php echo e(setDisable('all', $data['disabled'])); ?>>
                                                <?php if(($key + 1) == sizeof($data['finish_goods_details']) && $k1 != 0): ?>
                                                    <i class="fa fa-trash fa-lg pull-right hide delete_row" style="margin-top: 15px"></i>
                                                <?php endif; ?>
                                            </td>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?> 
                                <tr>
                                    <td><input type="text" name="date[0][start_date]" class="form-control required start_date datepicker" data-key="start_date" min="<?php echo e(date('Y-m-d')); ?>"></td>
                                    <td>
                                            <input type="text" name="date[0][end_date]" class="form-control required end_date datepicker" data-key="end_date" min="<?php echo e(date('Y-m-d')); ?>">
                                    </td>
                                    <?php $__currentLoopData = $data['finish_goods_details']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <td>
                                            <input type="text" class="form-control number count_value" name="date[0][<?php echo e($value['work_order_details_id']); ?>]" data-key="<?php echo e($value['work_order_details_id']); ?>">
                                            <?php if(($key + 1) == sizeof($data['finish_goods_details'])): ?>
                                                <i class="fa fa-trash fa-lg pull-right hide delete_row" style="margin-top: 15px"></i>
                                            <?php endif; ?>
                                        </td>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tr>
                                <?php endif; ?>
                            </tbody>
                            <tfoot style="background-color: #e1dbdb">
                                <tr>
                                    <th colspan="2">Total</th>
                                    <?php $__currentLoopData = $data['finish_goods_details']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <th>
                                            <input type="text" disabled="disabled" data-max="<?php echo e(round($value['quantity'], 2)); ?>" class="form-control total_foot number" value="0" data-key="<?php echo e($value['work_order_details_id']); ?>">
                                        </th>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                                <tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="col-md-1">
                    <?php if(Request::segment(4) != '' && Request::segment(4) != 'approve-delivery-date'): ?>
                    <div class="form-group">
                        <label class="btn btn-default add_date_row"> Add Date</label>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <br>
            <hr>
        <?php elseif(Request::segment(4) != '' && Request::segment(4) == 'request-raw-materials'): ?>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><i class="fa fa-cubes"></i> Finish Goods Details</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead style="background-color:grey">
                          <tr style="color:white">
                            <th>Sr. No.</th>
                            <th>Item</th>
                            <th>Unit</th>
                            <th>Issue Quantity</th>
                            <th>Prev Issue</th>
                            <th>Issue Quantity</th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php if(isset($data) && isset($data['finish_goods_details'])): ?>
                                <?php $__currentLoopData = $data['finish_goods_details']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($key+1); ?></td>
                                        <td><?php echo e($value['item']); ?></td>
                                        <td><?php echo e($value['unit']); ?></td>
                                        <td><?php echo e(round($value['quantity'], 2)); ?></td>
                                        <td><?php echo e($value['prev_req']); ?></td>
                                        <td>
                                            <input type="text" class="form-control number required quantity" name="fg_request[<?php echo e($value['work_order_details_id']); ?>][quantity]" value="" min="0" max="<?php echo e($value['quantity'] - $value['prev_req']); ?>">
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="5"><labeL class="pull-right">Total Requested Quantity</labeL></th>
                                <td><input type="text" name="rrmm_total_quantity" id="total_quantity" class="form-control" readonly="readonly"></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <?php elseif(Request::segment(4) != '' && Request::segment(4) == 'raise-voucher'): ?>
        <div class="item_parent_row">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Item</label>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Unit</label>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Quantity</label> 
                    </div>
                </div>
                <div class="col-md-3">
                    <label><i class="fa fa-plus fa-lg add_item_row"></i></label>
                </div>
            </div>
            <div class="row item_row">
                <div class="col-md-3">
                    <div class="form-group">
                        <?php echo e(Form::select('raise_item',
                            (isset($data) && isset($data['item_list'])) ? $data['item_list'] : [],
                            htmlSelect('grd_item_id', $data),
                            array('name' => 'item_details[0][rvd_item]','class' => 'form-control required item_list chosen-select', 'data-name' => 'rvd_item','placeholder' => '', 'data-placeholder' => 'Select Item'))); ?>

                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?php echo e(Form::select('raise_item',
                            (isset($data) && isset($data['unit_list'])) ? $data['unit_list'] : [],
                            htmlSelect('grd_item_id', $data),
                            array('name' => 'item_details[0][rvd_unit]','class' => 'form-control required unit_list chosen-select', 'data-name' => 'rvd_unit','placeholder' => '', 'data-placeholder' => 'Select Unit'))); ?>

                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" name="item_details[0][rvd_quantity]" class="form-control required quantity number" data-name="rvd_quantity" placeholder="Quantity">
                    </div>
                </div>
                <div class="col-md-3">
                    <label><i class="fa fa-trash delete_item_row hide fa-lg pull-right"></i></label>
                </div>
            </div>
        </div>
        <?php else: ?>
            <div class="row col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><i class="fa fa-cubes"></i> Finish Goods Details</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead style="background-color:grey">
                          <tr style="color:white">
                            <th>Sr. No.</th>
                            <th>Item</th>
                            <th>Quantity</th>
                            <th>Unit</th>
                            <th>Issue Quantity</th>
                            <th>Previously Send for Packaging</th>
                            <th>Send for Packaging</th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php if(isset($data) && isset($data['finish_goods_details'])): ?>
                                <?php $__currentLoopData = $data['finish_goods_details']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($key+1); ?></td>
                                        <td><?php echo e($value['item']); ?></td>
                                        <td><?php echo e(round($value['quantity'],2)); ?></td>
                                        <td><?php echo e($value['unit']); ?></td>
                                        <td><?php echo e($value['issue_qty']); ?></td>
                                        <td>
                                            <input type="text" class="form-control" readonly="readonly" value="<?php echo e(round($value['prev_send'], 2)); ?>">
                                        </td>
                                        <td>
                                            <input type="text" name="item_details[<?php echo e($value['work_order_details_id']); ?>][sfp_quantity]" class="form-control quantity number" placeholder="Max = <?php echo e(round($value['issue_qty'] - $value['prev_send'],2)); ?>" max="<?php echo e(round($value['issue_qty'] - $value['prev_send'],2)); ?>">
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </tbody>
                        <tfoot>
                            <td colspan="4">Total</td>
                            <td><input type="text" readonly="readonly" class="form-control" value="<?php echo e(round($data['sfpm_total_quantity'], 2)); ?>"></td>
                            <td><input type="text" name="sfpm_total_quantity" readonly="readonly" class="form-control total_quantity" id ="total_quantity"></td>
                        </tfoot>
                    </table>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Remakrs</label>
                        <input type="text" name="sfpm_remarks" class="form-control" placeholder="Remarks">
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>
<?php if(Request::segment(4) != '' && (Request::segment(4) == 'edit-delivery-date' || Request::segment(4) == 'approve-delivery-date')): ?>
<section class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Date Edit Reason *</label>
                    <input type="text" name="wo_date_edit_reason" class="form-control required" placeholder="Date Edit Reason" <?php echo e(setDisable('all', $data['disabled'])); ?> value="<?php echo e(htmlValue('wo_date_edit_reason', $data)); ?>">
                </div>  
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<section class="panel">
    <div class="panel-body">
        <?php if(Request::segment(4) != '' && Request::segment(4) != 'approve-delivery-date'): ?>
        <div class="btn-group">
            <button class="btn btn-success" name="save" value="save" type="submit" id="save_continue">Save </button>
        </div>
        <?php else: ?>
        <div class="btn-group">
            <button class="btn btn-success" name="wo_dates_approval" value="2" type="submit" id="save_continue"><i class="fa fa-check fa-lg"></i> Approved </button>
        </div>
        <div class="btn-group">
            <button class="btn btn-danger" name="wo_dates_approval" value="3" type="submit" id="save_continue"> <i class="fa fa-times fa-lg"></i> Reject </button>
        </div>
        <?php endif; ?>
        <div class="btn-group pull-right">
            <a href="/<?php echo e(Request::segment(1)); ?>/<?php echo e(Request::segment(2)); ?>/"><button class="btn btn-danger" name="cancel" value="cancel" type="button" id="cancel">Cancel</button></a>
        </div>
    </div>
</section>

<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['screen_name'] = $data['screen_name'];
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>
    <?php echo $__env->make('layouts.script_loaders.datatable_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        chosenInit();
        $(".datepicker").datepicker({
            startDate: new Date(),
            format: 'yyyy-mm-dd',
        });
        $(document).on('change, keyup', '.quantity', function(){
            var total = 0;
            $('.quantity').each(function(){
                var cur_val = parseFloat($(this).val());
                isNaN(cur_val) ? cur_val = 0 : '';
                total += cur_val;
            });
            $('#total_quantity').val(total);
        })
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        
        $(document).on('click', '.add_item_row', function(){
            var row = $('.item_row:eq(0)').clone();
            var time = new Date().getTime();
            row.find('input').each(function(){
                $(this).val('');
                var n = $(this).data('name');
                $(this).attr('name', 'item_details['+time+']['+n+']');
            });
            $(row).find('select').each(function(k, v) {
                var n = $(v).attr('data-name');    
                $(v).attr('name', 'item_details['+time+']['+n+']');
                $(v).siblings('div').remove();
                $(v).val('').trigger('chosen:updated');
            });
            $(row).find('.delete_item_row').removeClass('hide');
            $('.item_parent_row').append(row);
            chosenInit();
        });

        $(document).on('click', '.add_date_row', function(){
            var row = $('.date_body').find('tr:eq(0)').clone();
            var time = new Date().getTime();
            row.find('input').each(function(){
                $(this).val('');
                var n = $(this).data('key');
                $(this).attr('name', 'date['+time+']['+n+']');
            });
            $(row).find('.delete_row').removeClass('hide');
            $('.date_body').append(row);
            $(".datepicker").datepicker({
                startDate: new Date(),
                format: 'yyyy-mm-dd',
            });
        });

        $(document).on('click', '.delete_item_row', function(){
            $(this).parent().parent().parent().remove();
        });

        $(document).on('click', '.delete_row', function(){
            $(this).parent().parent().remove();
            calculateTotal();
            calculateGrandTotal();
        });

        $(document).on('keyup', '.count_value', function(){
            calculateTotal();
        });

        function calculateTotal(){
            $('.total_foot').each(function(){
                var key = $(this).data('key');
                var max = $(this).data('max');
                var total = 0;
                $('.date_body').find('input:text[data-key="'+key+'"]').each(function(){
                    var n = parseFloat($(this).val());
                    isNaN(n) ? n = 0 : '';
                    total += n;
                })
                $(this).val(total);
                if(total > max){
                    showWarning($(this));
                } else {
                    removeWarning($(this));
                }
            });
        }

        function showWarning(ele){
            var key = $(ele).data('key');
            $('.date_body').find('input:text[data-key="'+key+'"]').each(function(){
                $(this).addClass('input-warning');        
            })
            $(this).addClass('input-warning');

            calculateGrandTotal();
        }

        calculateGrandTotal();

        function removeWarning(ele){
            var key = $(ele).data('key');
            $('.date_body').find('input:text[data-key="'+key+'"]').each(function(){
                $(this).removeClass('input-warning');        
            })
            $(this).removeClass('input-warning');   
         
            calculateGrandTotal();
        }

        $(document).on('change', '.start_date', function(){
            var start = $(this).val();
            $(this).parent().parent().find('.end_date').attr('min', start);
        });

        calculateGrandTotal();
        calculateTotal();

        function calculateGrandTotal(){
            block = 0;
            $('.total_foot').each(function(){
                if($(this).val() != $(this).data('max')){
                    block = 1;
                }
            });

            console.log(block)
            if(block == 0){
                $('.btn-success').prop('disabled', false);
            } else {
                $('.btn-success').prop('disabled', true);
            }
        }

        $(document).on('change', '.item_list', function() {
            var id = $(this).val();
            var that = $(this);
            $.ajax({
                url: window.location.href,
                type: 'get',
                data: {
                    entity : 'get-unit',
                    entity_id : id,
                },
                success: function(result) {
                    if(result) {
                        $(that).closest('.item_row').find('.unit_list').val(result['im_unit']).trigger("chosen:updated");
                    } else {
                        $(that).closest('.item_row').find('.unit_list').val('').trigger("chosen:updated");
                    }
                }
            });
        });

    });

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>