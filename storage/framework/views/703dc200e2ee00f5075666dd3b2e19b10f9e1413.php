<?php $__env->startSection('custom-styles'); ?>
    
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
    <link href="<?php echo e(asset('/css/datatables/buttons.bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />

<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo e(ucwords(str_replace('-', ' ', Request::segment(1)))); ?> | <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

    <div class="panel">
        <div class="panel-heading ">   
            <ul class="nav nav-tabs">
                <li class = "packaging_tabs accept_package"><a href="?type=accept-package">Accept for packaging</a></li>
                <li class = "packaging_tabs send_to_warehouse"><a href="?type=send-to-warehouse">Send To WareHouse</a></li>
            </ul>    
        </div>
        <div class="panel-body padding_top_3">
            <table id="role" class="datatable table display compact table-striped table-bordered  hover nowrap" cellspacing="0" role="grid" width="100%">
                <thead>
                    <tr>
                        <?php $__currentLoopData = $data['columns']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $column): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <th class = "thead_<?php echo e($column); ?>"><?php echo e(ucwords(str_replace('_', ' ', $column))); ?></th>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <?php $__currentLoopData = $data['columns']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $column): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(in_array($key ,$data['disable_footer_search'])): ?>
                                <th class = "tfoot_<?php echo e($column); ?> no-search"> <?php echo e(ucwords(str_replace('_', ' ', $column))); ?> </th>
                            <?php else: ?>
                                <th class = "tfoot_<?php echo e($column); ?>"> <?php echo e(ucwords(str_replace('_', ' ', $column))); ?> </th>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div class="modal fade modal-fullscreen force-fullscreen" id="request_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog Modal_body">
            <div class="modal-content border_radius " >
                <div class="modal-header bg-sharekhan border_radius_top clearfix" style="z-index: 1">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Variations</h4>
                </div>
            
                <div class="modal-body ">
                    <div class="col-md-12" style="padding-top: 5%;">  
                        <div class="table-responsive">
                            <table id="table_od table table-bordered" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Item Name</th>
                                        <th>Quantity</th>
                                    </tr>
                                </thead>
                                <tbody class="variation_body">
                                </tbody>
                            </table>
                        </div>  
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="#close_pd" class="btn btn-sharekhan" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['columns'] = $data['columns'];
        $js_data['pk'] = $data['pk'];
        $js_data['prefix'] = $data['prefix'];
        $js_data['status'] = config('constants.STATUS');
        $js_data['color'] = config('constants.COLOR_GENERAL');
        $js_data['permissionList'] = $data['permissionList'];
        $js_data['screen_name'] = $data['screen_name'];
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>

    <?php echo $__env->make('layouts.script_loaders.datatable_global', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo e(asset('/js/notify.js')); ?>"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            var approve = {
                display_name : "packaging-accept",
                class : "accept_package",
                title : "Accept Package",
                url : "/packaging-accept",
                href : true,
                i : {
                    class : "fa fa-hand-lizard-o fa-lg width_icon clr-sk"
                }
            };

            var send_to_warehouse = {
                display_name : "packaging-send-to-warehouse",
                class : "send_to_warehouse",
                title : "Send To Warehouse",
                url : "/packaging-send-to-warehouse",
                href : true,
                i : {
                    class : "fa fa-industry fa-lg width_icon clr-sk"
                }
            };
            
            var param = getParameterByName('type');
            if(param == 'send-to-warehouse')
            {
                $('.packaging_tabs').removeClass('active');
                $('.send_to_warehouse').addClass('active');
                $('.filter').removeClass('hide');
                $('.dead_lead').removeClass('active');
                var actionArr = [send_to_warehouse];
            }
            else
            {
                $('.packaging_tabs').removeClass('active');
                $('.accept_package').addClass('active');
                $('.filter').addClass('hide');
                var actionArr = [approve];
            }
            // initialize datatables
            var actionResult = getActionIconAcl(actionArr);
            var action_obj = actionResult;
    
            var datatable_object = {
                table_name : $('table').attr('id'),
                order : {
                    state: false,
                    column : 1,
                    mode : "desc"
                },
                buttons : {
                    state : true,
                    colvis : true,
                    excel : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    },
                    pdf : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    },
                    csv : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    },
                    copy : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    }
                },
                info : true,
                paging : true,
                searching : true,
                ordering : true,
                iDisplayLength: 10,
                sort_disabled_targets : [],
                ajax_url : window.location.href,
                column_data : datatableColumn(lang.columns, action_obj, lang.pk),
            }

            table = datatableInitGeneral(datatable_object);            
           
            

            $(document).on('click', '.supply_to_warehouse', function() { 
                var id = $(this).attr('data-value');
                $.ajax({
                    url: window.location.href,
                    type: 'GET',
                    data: {
                        entity : 'warehouse_details',
                        id : id,
                    },
                    success: function(result) {
                        $('.variation_body').html('');
                        $.each(result, function (index, value) {
                            var tr = document.createElement('tr');
                            var td1 = document.createElement('td');
                            td1.append(value.date);
                            var td2 = document.createElement('td');
                            td2.append(value.im_name);
                            var td3 = document.createElement('td');
                            td3.append(value.qty);
                            tr.append(td1, td2, td3);
                            $('.variation_body').append(tr);
                        });
                    },
                });
            });


            $(document).on('click', '.request_raw_material', function() { 
                var id = $(this).attr('data-value');
                $.ajax({
                    url: window.location.href,
                    type: 'GET',
                    data: {
                        entity : 'supply_details',
                        id : id,
                    },
                    success: function(result) {
                        $('.variation_body').html('');
                        $.each(result, function (index, value) {
                            var tr = document.createElement('tr');
                            var td1 = document.createElement('td');
                            td1.append(value.date);
                            var td2 = document.createElement('td');
                            td2.append(value.im_name);
                            var td3 = document.createElement('td');
                            td3.append(value.qty);
                            tr.append(td1, td2, td3);
                            $('.variation_body').append(tr);
                        });
                    },
                });
            });

        });
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>