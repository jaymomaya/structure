<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style>
        /*.solid {border-style: groove;}*/
        /*.item, td, th {*/
            /**/
        /*}*/
        table, tr, td, th {
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            /*border-collapse: collapse;
            border: 1px solid #000;*/
        }

        /*table + table, table + table tr:first-child th, table + table tr:first-child td {*/
            /*border-top: 0;*/
            /*border-left: hidden;*/
        /*}*/

        .item {
            width: 100%;
            border-collapse: collapse;
            /*border-collapse: collapse;
            border: 1px solid #000;*/
        }

        .item td {
            border-collapse: collapse;
            border: 1px solid #000;
            height: 30px;
            vertical-align: bottom;
        }
        .item tr,.item th {
            border-collapse: collapse;
            border: 1px solid #000; 
        }
        tfoot {
          text-align: right;
        }
        tfoot tr:last-child {
          background: #f0f0f2;
        }

    </style>
</head>
<?php $first = 0; ?>
<?php $last = 2; ?>



<body onload="window.print();" style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;">
        <div class="invoice solid" style="page-break-after: always;">
            <table class="main"  style="width: 100%;border-collapse: collapse;border: 1px solid #000;">
                <tr style="border-collapse: collapse;border: 1px solid #000;">
                    <td colspan="3" style="border-collapse: collapse;border: 1px solid #000;">
                        <center>
                            <div style="text-transform: capitalize;">Bill of Material</div>
                                <tr style="border-collapse: collapse;border: 1px solid #000;">
                                    <td style="width: 50%;padding: 1px;border-collapse: collapse;border: 1px solid #000;">
                                    <span style="text-transform: capitalize; font-size: 14px;"><b>Finish Goods:</b><?php echo e($data[0]->igm_name); ?></span>
                                        <br/>
                                    </td>
                                    <td style="width: 50%;padding: 1px;border-collapse: collapse;border: 1px solid #000;">
                                    <span style="text-transform: capitalize; font-size: 14px;"><b>Unit:</b><?php echo e($data[0]->um_name); ?></span>
                                        <br/>
                                    </td>
                                </tr>
                        </center>
                    </td>
                </tr>
                <tr style="border-collapse: collapse;border: 1px solid #000;">
                    <td colspan="3" style="border-collapse: collapse;border: 1px solid #000;">
                        <table class="item" style="table-layout:fixed">
                            <tr style="text-align: right;border-top:hidden;border-left: hidden;border-right: hidden;">
                                <th>S.No.</th>
                                <th>Raw Material</th>
                                <th>Unit</th>
                                <th>Quantity</th>
                            </tr>
                            <?php $__currentLoopData = $data['raw_bom']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr style="text-align: right;border-top:hidden;border-left: hidden;border-right: hidden;">
                                    <td><?php echo e($key); ?></td>
                                    <td> <?php echo e($value->igm_name); ?></td>
                                    <td><?php echo e($value->um_name); ?></td>
                                    <td><?php echo e($value->rmb_quantity); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
</body>
</html>