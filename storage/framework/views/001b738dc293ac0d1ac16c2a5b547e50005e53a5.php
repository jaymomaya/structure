

<?php $__env->startSection('custom-styles'); ?>
  
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link href="<?php echo e(asset('/css/common/bootstrap-switch.min.css')); ?>" media="screen" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/css/common/bootstrap-datepicker.css')); ?>" />
    <link href="<?php echo e(asset('/css/common/chosen.min.css')); ?>" rel="stylesheet" type="text/css" />

<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo $__env->make('layouts.custom_partials.htmlheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    
    <?php echo $__env->make('layouts.custom_partials.contentheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<?php if(Request::segment(4)): ?>
    <?php echo Form::model($data, ['class' => 'form-validate', 'url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey(), Request::segment(4)], 'method' => 'put','enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php else: ?>
    <?php echo Form::open(['class' => 'form-validate', 'url' => "/".Request::segment(1)."/".Request::segment(2),'enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php endif; ?>
<input type="hidden" name="screen_name" value = "<?php echo e((isset($data) && isset($data['screen_name'])) ? $data['screen_name'] : ''); ?>">
<section class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><i class="fa fa-book"></i> Work Order Details</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead style="background-color:grey">
                              <tr style="color:white">
                                <th>Request No.</th>
                                <th>Work Order No.</th>
                                <th>Work Order Date</th>
                                <th>Customer</th>
                                <th>Total Quantity</th>
                                <th>Total Amount</th>
                              </tr>
                            </thead>
                            <tbody>
                                <?php if(isset($data)): ?>
                                    <tr>
                                        <td><?php echo e(htmlValue('rrmm_request_no', $data)); ?></td>
                                        <td><?php echo e(htmlValue('wo_order_no', $data['work_order_data'])); ?></td>
                                        <td><?php echo e(htmlValue('wo_date', $data['work_order_data'])); ?></td>
                                        <td><?php echo e(htmlValue('wo_customer', $data['work_order_data'])); ?></td>
                                        <td><?php echo e(round(htmlValue('wo_total_quantity', $data['work_order_data']), 2)); ?></td>
                                        <td><?php echo e(round(htmlValue('wo_total_amount', $data['work_order_data']), 2)); ?></td>
                                    </tr>
                                <?php endif; ?>
                                <tr>
                                    <th>Remarks: </th>
                                    <td colspan="5"><?php echo e(htmlValue('wo_remarks', $data)); ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><i class="fa fa-cubes"></i> Finish Goods Details</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead style="background-color:grey">
                          <tr style="color:white">
                            <th>Sr. No.</th>
                            <th>Item</th>
                            <th>Quantity</th>
                            <th>Unit</th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php if(isset($data) && isset($data['finish_goods_details'])): ?>
                                <?php $__currentLoopData = $data['finish_goods_details']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($key+1); ?></td>
                                        <td><?php echo e($value['item']); ?></td>
                                        <td><?php echo e(round($value['quantity'], 2)); ?></td>
                                        <td><?php echo e($value['unit']); ?></td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12 table-responsive">
                <table class="table table-bordered table-striped">
                    <thead style="background-color:grey">
                      <tr style="color:white">
                        <th>Sr. No.</th>
                        <th>Raw Material</th>
                        <th>Total Quantity (WIP + Available)</th>
                        <th>WIP</th>
                        <th>Available</th>
                        <th>Required</th>
                        <th>Shortfall</th>
                        <th>Po Quantity</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php if(isset($data) && isset($data['material_need'])): ?>
                            <?php $__currentLoopData = $data['material_need']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($key+1); ?></td>
                                    <td><?php echo e($value['name']); ?></td>
                                    <td><?php echo e($value['total']); ?></td>
                                    <td><?php echo e($value['wip']); ?></td>
                                    <td style="color:green; font-weight: bold"><?php echo e($value['available']); ?></td>
                                    <td style="color:orange; font-weight: bold"><?php echo e($value['quantity']); ?></td>
                                    <?php if($value['shortfall'] >= 0): ?>
                                        <td style="color:green; font-weight: bold"><?php echo e($value['shortfall']); ?></td>
                                    <?php else: ?>
                                        <td style="color:red; font-weight: bold"><?php echo e($value['shortfall']); ?></td>
                                    <?php endif; ?>
                                    <td style="color:black; font-weight: bold"><?php echo e($value['po_quantity']); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<section class="panel">
    <div class="panel-body"> 
        <div class="row">
            <div class="col-md-4">
                <label><i class="fa fa-cubes"></i> Required Raw Material Details</label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 table-responsive">
                <table class="table table-bordered table-striped">
                    <thead style="background-color:grey">
                        <tr style="color:white">
                            <th>Sr. No.</th>
                            <th>Raw Material</th>
                            <th>Request Quantity</th>
                            <th>Godown</th>
                            <th>Available in Godown</th>
                            <th>Previous Issue</th>
                            <th>Send Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(isset($data) && isset($data['raw_material_need'])): ?>
                            <?php $__currentLoopData = $data['raw_material_need']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($key+1); ?></td>
                                    <td><?php echo e($value['im_name']); ?></td>
                                    <td><?php echo e(round($value['qty'] , 2)); ?></td>
                                    <td>
                                        <div class="form-group">
                                            <?php echo e(Form::select('raw_material',
                                                (isset($data) && isset($data['godown_list'])) ? $data['godown_list'] : [],
                                                htmlSelect('grd_item_id', $value),
                                                array('name' => 'item_request['.$value['wob_raw_material_id'].'][godown]','class' => 'form-control godown_list chosen-select', 'data-name' => 'qcd_godown', 'data-key' => $value['wob_raw_material_id'],'placeholder' => '', 'data-placeholder' => 'Select Godown'))); ?>

                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" class="available form-control" readonly="readonly" data-need="<?php echo e(round($value['send_now'] , 2)); ?>">
                                    </td>
                                    <td><?php echo e($value['raised']); ?></td>
                                    <td>
                                        <input type="text" class="form-control quantity" name="item_request[<?php echo e($value['wob_raw_material_id']); ?>][quantity]" max="<?php echo e($value['send_now']); ?>" placeholder=" Max => <?php echo e(round($value['send_now'],2)); ?>" data-max = "<?php echo e($value['send_now']); ?>">
                                        
                                        <input type="hidden" class="form-control" name="item_request[<?php echo e($value['wob_raw_material_id']); ?>][item]" value="<?php echo e(round($value['wob_raw_material_id'], 2)); ?>">
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="6"><labeL class="pull-right">Total Requested Quantity</labeL></th>
                            <td><input type="text" name="rrmm_total_quantity" id="total_quantity" class="form-control" readonly="readonly" value=""></td>
                            <td><input type="hidden" name="rrmm_total_needed" value="<?php echo e($data['total_material_need']); ?>"></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
<section class="panel">
    <div class="panel-body">
        <div class="btn-group">
            <button class="btn btn-success" name="save" value="save" type="submit" id="save_continue">Save </button>
        </div>
        <div class="btn-group pull-right">
            <a href="/<?php echo e(Request::segment(1)); ?>/<?php echo e(Request::segment(2)); ?>/"><button class="btn btn-danger" name="cancel" value="cancel" type="button" id="cancel">Cancel</button></a>
        </div>
    </div>
</section>

<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['screen_name'] = $data['screen_name'];
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>
    <?php echo $__env->make('layouts.script_loaders.datatable_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        chosenInit();
            
        calculateTotal();

        function calculateTotal(){
            var total = 0;
            $('.quantity').each(function(){
                var cur_val = parseFloat($(this).val());
                isNaN(cur_val) ? cur_val = 0 : '';
                total += cur_val;
            });
            $('#total_quantity').val(total);
        }

        $(document).on('keyup', '.quantity', function(){
            calculateTotal();
        })

        $(document).on('change', '.godown_list', function(){
            var id = $(this).data('key');
            var godown_id = $(this).val();
            var ele = $(this);
            var curr = $(ele).parent().parent().parent().find('.available');
            var quantity = $(ele).parent().parent().parent().find('.quantity');
            showLoading();
            $.ajax({
                url: '/api/get-item-stock',
                type: 'get',
                data:{
                    id: id,
                    godown_id: godown_id
                }, success : function(res){
                    if(res[0] != undefined && res[0]['available'] != undefined){
                        if($(curr).data('need') > res[0]['available']){
                            $(quantity).attr('max', res[0]['available']);
                            $(quantity).val(res[0]['available']);
                        } else {
                            $(quantity).attr('max', $(quantity).data('max'));
                            $(quantity).val($(quantity).data('max'));
                        }
                        $(curr).val(res[0]['available']);
                    } 
                    else {
                        $(curr).val(0);  
                        $(quantity).val(0);
                    }
                    stopLoading();
                    calculateTotal();
                }
            })
        })
    });

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>