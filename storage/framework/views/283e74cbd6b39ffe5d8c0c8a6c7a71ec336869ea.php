

<?php $__env->startSection('custom-styles'); ?>
    
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
    <link href="<?php echo e(asset('/css/datatables/buttons.bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('/css/common/bootstrap-modal-carousel.min.css')); ?>" rel="stylesheet" type="text/css" />

<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo e(ucwords(str_replace('-', ' ', Request::segment(1)))); ?> | <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

    <div class="panel">
        <div class="panel-body padding_top_3">
            <table id="role" class="datatable table display compact table-striped table-bordered  hover nowrap" cellspacing="0" role="grid" width="100%">
                <thead>
                    <tr>
                        <?php $__currentLoopData = $data['columns']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $column): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <th class = "thead_<?php echo e($column); ?>"><?php echo e(ucwords(str_replace('_', ' ', $column))); ?></th>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <?php $__currentLoopData = $data['columns']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $column): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(in_array($key ,$data['disable_footer_search'])): ?>
                                <th class = "tfoot_<?php echo e($column); ?> no-search"> <?php echo e(ucwords(str_replace('_', ' ', $column))); ?> </th>
                            <?php else: ?>
                                <th class = "tfoot_<?php echo e($column); ?>"> <?php echo e(ucwords(str_replace('_', ' ', $column))); ?> </th>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tr>
                </tfoot>
            </table>
        </div>
        
    </div>

    <div class="modal fade modal-fullscreen force-fullscreen" id="work_order_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog Modal_body">
            <div class="modal-content border_radius " >
                <div class="modal-header bg-sharekhan border_radius_top clearfix" style="z-index: 1">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Work Order</h4>
                </div>
            
                <div class="modal-body ">
                    <div class="col-md-12" style="padding-top: 5%;">  
                        <table id="" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Material</th>
                                    <th>Unit</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                                    
                            <tbody class="work_order_body">
                                
                            </tbody>
                        </table>
                    </div>
                    </div>
                <div class="modal-footer">
                    <button type="button" id="#close_pd" class="btn btn-sharekhan" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

     <div class="modal fade modal-fullscreen force-fullscreen" id="days_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog Modal_body">
            <div class="modal-content border_radius " >
                <div class="modal-header bg-sharekhan border_radius_top clearfix" style="z-index: 1">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">All Days</h4>
                </div>
            
                <div class="modal-body ">
                    <div class="col-md-12" style="padding-top: 5%;">  
                        <table id="" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                </tr>
                            </thead>
                                    
                            <tbody class="days_body">
                                
                            </tbody>
                        </table>
                    </div>
                    </div>
                <div class="modal-footer">
                    <button type="button" id="#close_pd" class="btn btn-sharekhan" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['columns'] = $data['columns'];
        $js_data['pk'] = $data['pk'];
        $js_data['prefix'] = $data['prefix'];
        $js_data['status'] = config('constants.STATUS');
        $js_data['color'] = config('constants.COLOR_GENERAL');
        $js_data['permissionList'] = $data['permissionList'];
        $js_data['screen_name'] = $data['screen_name'];
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>

    <?php echo $__env->make('layouts.script_loaders.datatable_global', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo e(asset('/js/notify.js')); ?>"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            // initialize datatables
            
            var actionArr = [];
            var actionResult = getActionIconAcl(actionArr);
            var action_obj = actionResult;

            var datatable_object = {
                table_name : $('table').attr('id'),
                order : {
                    state: false,
                    column : 1,
                    mode : "desc"
                },
                buttons : {
                    state : true,
                    colvis : true,
                    excel : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    },
                    pdf : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    },
                    csv : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    },
                    copy : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    }
                },
                info : true,
                paging : true,
                searching : true,
                ordering : true,
                iDisplayLength: 10,
                sort_disabled_targets : [],
                ajax_url : window.location.href,
                render: {
                    "targets": '_all',
                    "render":$.fn.dataTable.render.text()
                },
                column_data : datatableColumn(lang.columns, action_obj, lang.pk),
            }
            
            table = datatableInitGeneral(datatable_object);     

            $(document).on('click', '.work_order', function() { 
                var work_order_id = $(this).attr('data-value');
                $.ajax({
                        url: '/report-management/planned-order/work-order-details',
                        type: 'GET',
                        data: {
                            entity : 'work_order_details',
                            work_order_id : work_order_id,
                        },
                        success: function(result) {
                            console.log(result);
                            $('.work_order_body').html('');

                            $.each(result, function (index, value) {
                                $('.work_order_body').append('<tr>');
                                $('.work_order_body').append('<td>'+value.im_name+'</td');
                                $('.work_order_body').append('<td>'+value.um_name+'</td');
                                $('.work_order_body').append('<td>'+value.wod_quantity+'</td');
                                $('.work_order_body').append('<td>'+value.wod_rate+'</td');
                                $('.work_order_body').append('<td>'+value.wod_amount+'</td');
                                $('.work_order_body').append('</tr>');
                                
                            });

                        },
                    });
               
            });
             $(document).on('click', '.days', function() { 
                var work_order_id = $(this).attr('data-value');
                $.ajax({
                    url: '/report-management/planned-order/days-details',
                    type: 'GET',
                    data: {
                        entity : 'days_details',
                        work_order_id : work_order_id,
                    },
                    success: function(result) {
                        console.log(result);
                        $('.days_body').html('');

                        $.each(result, function (index, value) {
                            $('.days_body').append('<tr>');
                            $('.days_body').append('<td>'+value.wds_start_date+'</td');
                            $('.days_body').append('<td>'+value.wds_end_date+'</td');
                            $('.days_body').append('</tr>');
                            
                        });

                    },
                });
            });

        });
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>