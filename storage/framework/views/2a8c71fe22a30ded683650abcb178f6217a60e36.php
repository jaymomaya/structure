

<?php $__env->startSection('custom-styles'); ?>
    
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
    <link href="<?php echo e(asset('/css/datatables/buttons.bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('/css/common/bootstrap-modal-carousel.min.css')); ?>" rel="stylesheet" type="text/css" />

<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo e(ucwords(str_replace('-', ' ', Request::segment(1)))); ?> | <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
<div id="printandpreview" role="dialog" tabindex="-1" class="fade in modal" style="padding-left: 17px;">
   <div class="modal-lg modal-dialog">
      <div class="modal-content" role="document">
         <div class="modal-body">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-md-5 margin-top-6"><span class="gf-image-preview font-40 text-blue-color"></span><label class="module-header text-blue-color modal-heading">&nbsp;&nbsp;Preview &amp; Print</label></div>
                  <div class="col-md-7 no-padding">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                    <button class="btn font-10 padding-20 btn-purple-border uppercase left-margin-20 pull-right margin-top-12" data-dismiss="modal">Close</button>
                    </div>
               </div>
               <div class="row">
                  <div class="underline col-md-12"></div>
                  <div class="col-md-10 col-md-offset-1">
                     <br>
                     <div class="row">
                        <div class="col-md-12">
                           <br>
                           <div>
                              <div></div>
                              <object width="100%" height="800px" style="border: 1px solid rgb(136, 136, 136);" type="application/pdf"></object>
                           </div>
                           <br><br>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="panel">
    <div class="panel-body padding_top_3">
        <table id="role" class="datatable table display compact table-striped table-bordered  hover nowrap" cellspacing="0" role="grid" width="100%">
            <thead>
                <tr>
                    <?php $__currentLoopData = $data['columns']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $column): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <th class = "thead_<?php echo e($column); ?>"><?php echo e(ucwords(str_replace('_', ' ', $column))); ?></th>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <?php $__currentLoopData = $data['columns']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $column): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if(in_array($key ,$data['disable_footer_search'])): ?>
                            <th class = "tfoot_<?php echo e($column); ?> no-search"> <?php echo e(ucwords(str_replace('_', ' ', $column))); ?> </th>
                        <?php else: ?>
                            <th class = "tfoot_<?php echo e($column); ?>"> <?php echo e(ucwords(str_replace('_', ' ', $column))); ?> </th>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['columns'] = $data['columns'];
        $js_data['pk'] = $data['pk'];
        $js_data['prefix'] = $data['prefix'];
        $js_data['status'] = config('constants.STATUS');
        $js_data['color'] = config('constants.COLOR_GENERAL');
        $js_data['permissionList'] = $data['permissionList'];
        $js_data['screen_name'] = $data['screen_name'];
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>

    <?php echo $__env->make('layouts.script_loaders.datatable_global', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo e(asset('/js/notify.js')); ?>"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            
            // initialize datatables
            var approve = {
                display_name : "product-voucher-approve",
                class : "product_voucher_approve",
                title : "Approve / Reject",
                url : "/approve",
                href : true,
                i : {
                    class : "fa fa-check fa-lg width_icon clr-sk"
                }
            };

            var supply = {
                display_name : "product-voucher-supply",
                class : "supply",
                title : "Supply",
                url : "/supply",
                href : true,
                i: {
                    class : "fa fa-shopping-cart width_icon clr-sk"
                }
            };

            var edit = {
                display_name : "product-voucher-edit",
                class : "edit",
                title : "Edit",
                url : "/edit",
                href : true,
                i: {
                    class : "fa fa-pencil width_icon clr-sk"
                }
            };

            var view = {
                display_name : "product-voucher-view",
                class : "view",
                title : "View",
                url : "/",
                href : true,
                i: {
                    class : "fa fa-eye width_icon clr-sk"
                }
            };

            var destroy = {
                display_name : "product-voucher-delete",
                class : "delete",
                title : "Delete",
                url : "/",
                href : false,
                i : {
                    class : "fa fa-trash width_icon clr-sk"
                }
            };

            var actionArr = [approve, supply, edit, destroy, view];
            var actionResult = getActionIconAcl(actionArr);
            var action_obj = actionResult;
    
            var datatable_object = {
                table_name : $('table').attr('id'),
                order : {
                    state: false,
                    column : 1,
                    mode : "desc"
                },
                buttons : {
                    state : true,
                    colvis : true,
                    excel : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    },
                    pdf : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    },
                    csv : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    },
                    copy : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    }
                },
                info : true,
                paging : true,
                searching : true,
                ordering : true,
                iDisplayLength: 10,
                sort_disabled_targets : [],
                ajax_url : window.location.href,
                column_data : datatableColumn(lang.columns, action_obj, lang.pk),
            }

            table = datatableInitGeneral(datatable_object);            
            

            $(document).on('click','.print',function(){
               var id = $(this).attr('data-id');
               $.ajax({
                    url : '/purchase-module/supply-raw-material/'+id+'/print-outside',
                    type : 'get',
                    success: function(res){
                        $('#printandpreview').find('object').attr('data', 'data:application/pdf;base64, '+res+'');
                        $('#printandpreview').modal('show');
                    }
               })
            })

            $(document).on('click','.mail',function(){
                var dataid = $(this).attr('data-id');
                $('#data_id').val(dataid);
                $('#mail_modal').modal('show'); 
            })
            $(document).on('click','.send_mail',function(){
                console.log(11);
                var mail_send = $('#mail_send_to').val();
                var mail_cc = $('#mail_cc').val();
                var mail_bcc = $('#mail_bcc').val();
                var mail_subject = $('#mail_subject').val();
                var mail_message = CKEDITOR.instances['mail_message'].getData();
                var id = $('#data_id').val();
                var url = window.location.href;
                url = url.split('/');
                console.log(url);
                url = url[0] + '/' + url[1] + '/' + url[2] + '/' + url[3] + '/' + url[4] + '/' + id;  
                console.log(url);
                if(mail_send == ""){
                  $('#mail_send_err').html("Email Id Is Required");
                }
                else if(mail_subject == ""){
                  $('#mail_subject_err').html("Mail Subject Is Required");
                }
                else{
                  showLoading();
                  $('#mail_send_err').html('');
                  $('#mail_subject_err').html('');
                  $('#mail_modal').modal('hide');
                  var data = ({
                        "mail_send" : mail_send,
                        "mail_bcc" : mail_bcc,
                        "mail_subject" : mail_subject,
                        "mail_message" : mail_message,
                        "mail_cc" : mail_cc, 
                  });
                  $.ajax({
                      url: url + '/generate-mail',
                      type: 'post',
                      data: {
                          entity_type : "Mail",
                          entity_data : data, 
                      },
                        success: function(res) {
                          stopLoading();
                          $.notify("Mail Sent Successfully","info");   
                        // alert('Mail successfully Send.');
                        },
                        error: function(res) {}
                    }) 
                }
            });

            $(document).on('click', '.request_raw_material', function() { 
                var id = $(this).attr('data-value');
                $.ajax({
                    url: window.location.href,
                    type: 'GET',
                    data: {
                        entity : 'supply_details',
                        id : id,
                    },
                    success: function(result) {
                        $('.variation_body').html('');
                        $.each(result, function (index, value) {
                            var tr = document.createElement('tr');
                            var td1 = document.createElement('td');
                            td1.append(value.date);
                            var td2 = document.createElement('td');
                            td2.append(value.rfg_request_no);
                            var td3 = document.createElement('td');
                            td3.append(value.im_name);
                            var td4 = document.createElement('td');
                            td4.append(value.qty);
                            tr.append(td1, td2, td3, td4);
                            $('.variation_body').append(tr);
                        });
                    },
                });
            });
        });
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>