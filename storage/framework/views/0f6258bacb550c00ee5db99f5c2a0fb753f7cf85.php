<?php $__env->startSection('custom-styles'); ?>
  
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link href="<?php echo e(asset('/css/common/bootstrap-switch.min.css')); ?>" media="screen" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/css/common/bootstrap-datepicker.css')); ?>" />
    <link href="<?php echo e(asset('/css/common/chosen.min.css')); ?>" rel="stylesheet" type="text/css" />

<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo $__env->make('layouts.custom_partials.htmlheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    
    <?php echo $__env->make('layouts.custom_partials.contentheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<?php if(Request::segment(4)): ?>
    <?php echo Form::model($data, ['class' => 'form-validate', 'url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey()], 'method' => 'put','enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php else: ?>
    <?php echo Form::open(['class' => 'form-validate', 'url' => "/".Request::segment(1)."/".Request::segment(2),'enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php endif; ?>
<?php $prefix = config('constants.VendorMaster.prefix'); ?>
<?php $prefix_d = config('constants.VendorDocuments.prefix'); ?>
<?php $prefix_bd = config('constants.VendorBankDetails.prefix'); ?>

<input type="hidden" name="screen_name" value = "<?php echo e((isset($data) && isset($data['screen_name'])) ? $data['screen_name'] : ''); ?>">
<section class="panel">
    <div class="panel-body">
        <div id ="document_modal" class="modal fade">
            <div class="modal-dialog" >
                <div class="modal-content border_radius">
                    <div class="modal-header bg-sharekhan border_radius_top">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Vendor Documents</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="gallerymodal">
                                <img class="hide" width="90%" height="300px" style="margin :5px;margin-top: 5%;margin-left: 5%">
                                <iframe class="hide" width="90%" height="300px" style="margin :5px;margin-top: 5%;margin-left: 5%"></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <?php if(isset($data) && isset($data['screen_name']) && $data['screen_name'] == 'vendor-master-edit'): ?>
                            <button type="button" class="btn btn-default delete_image">DELETE</button>
                        <?php endif; ?>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <h4> <i class="fa fa-user"></i> Basic Detail </h4>
        <div class="row">
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Vendor Name *</label>
                    <i class="fa fa-user pull-right"></i>
                    <input type="text" class="form-control unit_name required" id="<?php echo e($prefix); ?>name" name = "<?php echo e($prefix); ?>name" placeholder="Vendor Name" value = "<?php echo e(htmlValue($prefix.'name', $data)); ?>" <?php echo e(setReadonly($prefix."name" , $data['readonly'])); ?> maxlength="100">
                </div>
            </div>
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Contact Person</label>
                    <i class="fa fa-user pull-right"></i>
                    <input type="text" class="form-control unit_symbol" id="<?php echo e($prefix); ?>contact_person" name = "<?php echo e($prefix); ?>contact_person" placeholder="Contact Person" value = "<?php echo e(htmlValue($prefix.'contact_person', $data)); ?>" <?php echo e(setReadonly($prefix."contact_person" , $data['readonly'])); ?> maxlength="100">
                </div>
            </div>
            <div class = "col-md-2">
                <div class="form-group">
                    <label for="name">Phone no</label>
                    <i class="fa fa-phone pull-right"></i>
                    <input type="text" class="form-control phone_no numeric" id="<?php echo e($prefix); ?>phone_no" name = "<?php echo e($prefix); ?>phone_no" placeholder="Phone no" value = "<?php echo e(htmlValue($prefix.'phone_no', $data)); ?>" <?php echo e(setReadonly($prefix."phone_no" , $data['readonly'])); ?> maxlength="10">
                </div>
            </div> 
            <div class = "col-md-2">
                <div class="form-group">
                    <label for="name">Alt Phone no</label>
                    <i class="fa fa-phone pull-right"></i>
                    <input type="text" class="form-control alt_phone_no numeric" id="<?php echo e($prefix); ?>alt_phone_no" name = "<?php echo e($prefix); ?>alt_phone_no" placeholder="Alt Phone no" value = "<?php echo e(htmlValue($prefix.'alt_phone_no', $data)); ?>" <?php echo e(setReadonly($prefix."alt_phone_no" , $data['readonly'])); ?> maxlength="10">
                </div>
            </div> 
            <div class="col-md-2">
                <div class="form-group">
                    <label for="name">Mobile No</label>
                    <i class="fa fa-mobile pull-right"></i>
                    <input type="text" class="form-control mob_no numeric" id="<?php echo e($prefix); ?>mob_no" name = "<?php echo e($prefix); ?>mob_no" placeholder="Alt Phone no" value = "<?php echo e(htmlValue($prefix.'mob_no', $data)); ?>" <?php echo e(setReadonly($prefix."mob_no" , $data['readonly'])); ?> maxlength="10">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="name">Alt Mobile No</label>
                    <i class="fa fa-mobile pull-right"></i>
                    <input type="text" class="form-control alt_mob_no numeric" id="<?php echo e($prefix); ?>alt_mob_no" name = "<?php echo e($prefix); ?>alt_mob_no" placeholder="Alt Mob no" value = "<?php echo e(htmlValue($prefix.'alt_mob_no', $data)); ?>" <?php echo e(setReadonly($prefix."alt_mob_no" , $data['readonly'])); ?> maxlength="10">
                </div>
            </div>
            <div class = "col-md-4">
                <div class="form-group">
                    <label for="name">Email Id</label>
                    <i class="fa fa-envelope pull-right"></i>
                    <input type="text" class="form-control email" id="<?php echo e($prefix); ?>email" name = "<?php echo e($prefix); ?>email" placeholder="Email Id" value = "<?php echo e(htmlValue($prefix.'email', $data)); ?>" <?php echo e(setReadonly($prefix."email" , $data['readonly'])); ?> maxlength="100">
                </div>
            </div> 
            <div class = "col-md-4">
                <div class="form-group">
                    <label for="name">Alt Email Id</label>
                    <i class="fa fa-envelope pull-right"></i>
                    <input type="text" class="form-control alt_email" id="<?php echo e($prefix); ?>alt_email" name = "<?php echo e($prefix); ?>alt_email" placeholder="alt_Email Id" value = "<?php echo e(htmlValue($prefix.'alt_email', $data)); ?>" <?php echo e(setReadonly($prefix."alt_email" , $data['readonly'])); ?> maxlength="100">
                </div>
            </div> 
            <div class="col-md-2">
                <div class="form-group">
                    <label>Credit Period</label>
                    <i class="fa fa-calendar pull-right"></i>
                    <input type="text" class="form-control credit_period" id="<?php echo e($prefix); ?>credit_period" name = "<?php echo e($prefix); ?>credit_period" placeholder="Credit Period" value = "<?php echo e(htmlValue($prefix.'credit_period', $data)); ?>" <?php echo e(setReadonly($prefix."credit_period" , $data['readonly'])); ?> maxlength="100">
                </div>
            </div>
        </div>
        <hr>
        <h4>Address Details</h4>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Address Line 1</label>
                    <i class="fa fa-map-pin pull-right"></i>
                    <input type="text" name="<?php echo e($prefix); ?>address_line1" class="form-control" placeholder="Address Line 1" maxlength="100" value = "<?php echo e(htmlValue($prefix.'address_line1', $data)); ?>" <?php echo e(setReadonly($prefix."address_line1" , $data['readonly'])); ?>>
                </div>
                <div class="form-group">
                    <label>Address Line 2</label>
                    <i class="fa fa-map-pin pull-right"></i>
                    <input type="text" name="<?php echo e($prefix); ?>address_line2" class="form-control" placeholder="Address Line 2" maxlength="100" value = "<?php echo e(htmlValue($prefix.'address_line2', $data)); ?>" <?php echo e(setReadonly($prefix."address_line2" , $data['readonly'])); ?>>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Pincode</label>
                    <i class="fa fa-map pull-right"></i>
                    <input type="text" name="<?php echo e($prefix); ?>pincode" class="form-control numeric" placeholder="Pincode" maxlength="6" value = "<?php echo e(htmlValue($prefix.'pincode', $data)); ?>" <?php echo e(setReadonly($prefix."pincode" , $data['readonly'])); ?>>
                </div>
                <div class="form-group">
                    <label>City</label>
                    <i class="fa fa-map-o pull-right"></i>
                    <input type="text" name="<?php echo e($prefix); ?>city" class="form-control" placeholder="City" maxlength="100" value = "<?php echo e(htmlValue($prefix.'city', $data)); ?>" <?php echo e(setReadonly($prefix."city" , $data['readonly'])); ?>>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>State</label>
                    <i class="fa fa-map pull-right"></i>
                    <select class="chosen-select state_list" name="<?php echo e($prefix); ?>state">
                        <?php if(isset($data) && isset($data['state_list'])): ?>
                        <?php $__currentLoopData = $data['state_list']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option
                            <?php if(isset($data) && isset($data[$prefix.'state']) && $data[$prefix.'state'] == $value['states_id']): ?>
                            selected = 'selected'
                            <?php endif; ?>
                            data-country="<?php echo e($value['s_country']); ?>" value="<?php echo e($value['states_id']); ?>"><?php echo e($value['s_name']); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Country</label>
                    <i class="fa fa-map-o pull-right"></i>
                    <input type="text" name="<?php echo e($prefix); ?>country" class="form-control" placeholder="Country" maxlength="100" value = "<?php echo e(htmlValue($prefix.'country', $data)); ?>" <?php echo e(setReadonly($prefix."country" , $data['readonly'])); ?>>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Select Item Group</label>
                    <?php echo e(Form::select('item_group',
                        (isset($data) && isset($data['item_groups'])) ? $data['item_groups'] : [],
                        htmlSelect($prefix.'item_group', $data),
                        array('name'=>$prefix.'item_group[]', 'class' => 'form-control chosen-select item_group', 'multiple' => 'multiple','data-placeholder' => 'Select Item Groups', setDisable($prefix.'item_group', $data['disabled'])))); ?>

                </div>
            </div>
        </div>
    </div>
</section>

<div class="panel">
    <div class="panel-body doc_vendor">
        <h4> <i class="fa fa-file"></i> Vendor Documents </h4>
        <div class="row">
            <?php if(isset($data) && isset($data['screen_name']) && $data['screen_name'] != 'vendor-master-show'): ?>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="btn btn-sharekhan"><i class="fa fa-upload"></i> Upload Documents
                            <input type="file" class = "file_uploads" name="<?php echo e($prefix_d); ?>file[]" multiple="multiple" style="display: none;">
                        </label>
                        <div class="documents_names"></div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <?php if(isset($data) && isset($data['vd_file']) && sizeof($data['vd_file']) > 0): ?>
        <div class="row" id="documents">
            <?php $__currentLoopData = $data['vd_file']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-md-3">
                    <div class="form-group">
                        <?php $fileext = explode('.',$value['vd_path']); ?>
                        <?php if($fileext[1] == "jpg" || $fileext[1] == "jpeg" || $fileext[1] == "png" || $fileext[1] == "gif"): ?>
                            <label class="btn btn-sharekhan vendor_doc" file="<?php echo e($value['vd_path']); ?>" data-file ="image" target="_blank" vendor_documents_id = "<?php echo e($value['vendor_documents_id']); ?>"><i class="fa fa-file"></i> <?php echo e($value['vd_doc_original_name']); ?></label>
                        <?php else: ?>
                            <label class="btn btn-sharekhan vendor_doc" file="<?php echo e($value['vd_path']); ?>" data-file ="notimage" target="_blank" vendor_documents_id = "<?php echo e($value['vendor_documents_id']); ?>"><i class="fa fa-file"></i> <?php echo e($value['vd_doc_original_name']); ?></label>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <?php endif; ?>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <h4><i class="fa fa-user"></i>Vendor KYC details</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>GST No.</label>
                    <input type="text" name="<?php echo e($prefix); ?>gst_no" class="form-control" placeholder="GST NO" value="<?php echo e(htmlValue($prefix.'gst_no', $data)); ?>" <?php echo e(setReadonly($prefix."gst_no" , $data['readonly'])); ?> id="gst_no" maxlength="15" minlength="15">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>GST Type</label>
                    <?php echo e(Form::select('gst_type',
                        (isset($data) && isset($data['gst_type'])) ? $data['gst_type'] : [],
                        htmlSelect($prefix.'gst_type', $data),
                        array('name'=>$prefix.'gst_type', 'class' => 'form-control chosen-select gst_type required', 'placeholder' => '', 'data-placeholder' => 'Select GST Type', setDisable($prefix.'gst_type', $data['disabled'])))); ?>

                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>PAN No.</label>
                    <input type="text" name="<?php echo e($prefix); ?>pan_no" class="form-control" placeholder="GST NO" value="<?php echo e(htmlValue($prefix.'pan_no', $data)); ?>" <?php echo e(setReadonly($prefix."pan_no" , $data['readonly'])); ?> id="pan_no" maxlength="10" minlength="10">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body bank_details_parent">
        <div class="row">
            <div class="col-md-4">
                <h4> <i class="fa fa-bank"></i> Bank Details </h4>
            </div>
            <?php if(isset($data) && isset($data['screen_name']) && $data['screen_name'] != 'vendor-master-show'): ?>
                <div class="col-md-8">
                    <i class="fa fa-plus-square fa-lg pull-right add-bank-row" data-toggle="tooltip" title="Add Bank Detail Row"></i>
                </div>
            <?php endif; ?>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Bank Name</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Acc Holder Name</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Acc No</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Branch</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>IFSC Code</label>
                </div>
            </div>
        </div>
        <?php if(isset($data) && isset($data['vbd']) && sizeof($data['vbd']) > 0): ?>
            <?php $__currentLoopData = $data['vbd']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="row bank_details_row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="text" name="vbd[<?php echo e($key); ?>][<?php echo e($prefix_bd); ?>bank_name]" placeholder="Bank Name" class="form-control" data-name ="<?php echo e($prefix_bd); ?>bank_name" value = "<?php echo e(htmlValue($prefix_bd.'bank_name', $value)); ?>" <?php echo e(setReadonly($prefix."bank_name" , $data['readonly'])); ?>>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <input type="text" name="vbd[<?php echo e($key); ?>][<?php echo e($prefix_bd); ?>acc_name]" placeholder="Holder Name" class="form-control" data-name ="<?php echo e($prefix_bd); ?>acc_name" value = "<?php echo e(htmlValue($prefix_bd.'acc_name', $value)); ?>" <?php echo e(setReadonly($prefix."acc_name" , $data['readonly'])); ?>>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <input type="text" name="vbd[<?php echo e($key); ?>][<?php echo e($prefix_bd); ?>acc_no]" placeholder="Acc No" class="form-control" data-name ="<?php echo e($prefix_bd); ?>acc_no" value = "<?php echo e(htmlValue($prefix_bd.'acc_no', $value)); ?>" <?php echo e(setReadonly($prefix."acc_no" , $data['readonly'])); ?>>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <input type="text" name="vbd[<?php echo e($key); ?>][<?php echo e($prefix_bd); ?>branch]" placeholder="Branch" class="form-control" data-name ="<?php echo e($prefix_bd); ?>branch" value = "<?php echo e(htmlValue($prefix_bd.'branch', $value)); ?>" <?php echo e(setReadonly($prefix."branch" , $data['readonly'])); ?>>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <input type="text" name="vbd[<?php echo e($key); ?>][<?php echo e($prefix_bd); ?>ifsc_code]" placeholder="IFSC Code" class="form-control" data-name ="<?php echo e($prefix_bd); ?>ifsc_code" value = "<?php echo e(htmlValue($prefix_bd.'ifsc_code', $value)); ?>" <?php echo e(setReadonly($prefix."ifsc_code" , $data['readonly'])); ?>>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <?php if($key == 0): ?>
                            <i class="fa fa-trash fa-lg delete_row pull-right hide" style="padding-top: 15px"></i>
                        <?php else: ?>
                            <i class="fa fa-trash fa-lg delete_row pull-right" style="padding-top: 15px"></i>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
        <?php else: ?>
            <div class="row bank_details_row">
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" name="vbd[0][<?php echo e($prefix_bd); ?>bank_name]" placeholder="Bank Name" class="form-control" data-name ="<?php echo e($prefix_bd); ?>bank_name">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <input type="text" name="vbd[0][<?php echo e($prefix_bd); ?>acc_name]" placeholder="Holder Name" class="form-control" data-name ="<?php echo e($prefix_bd); ?>acc_name">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <input type="text" name="vbd[0][<?php echo e($prefix_bd); ?>acc_no]" placeholder="Acc No" class="form-control" data-name ="<?php echo e($prefix_bd); ?>acc_no">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <input type="text" name="vbd[0][<?php echo e($prefix_bd); ?>branch]" placeholder="Branch" class="form-control" data-name ="<?php echo e($prefix_bd); ?>branch">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <input type="text" name="vbd[0][<?php echo e($prefix_bd); ?>ifsc_code]" placeholder="IFSC Code" class="form-control" data-name ="<?php echo e($prefix_bd); ?>ifsc_code">
                    </div>
                </div>
                <div class="col-md-1">
                    <i class="fa fa-trash fa-lg hide delete_row pull-right" style="padding-top: 15px"></i>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>

<?php echo $__env->make('layouts.custom_partials.save_panel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['screen_name'] = $data['screen_name'];
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>
    <?php echo $__env->make('layouts.script_loaders.datatable_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('/js/notify.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        chosenInit();
        var curr_ele = {};
        $(document).on('click', '.add-bank-row', function(){
            var time = new Date().getTime();
            var row = $('.bank_details_row:eq(0)').clone();
            $(row).find('input').each(function(){
                var name = $(this).data('name');
                
                $(this).attr('name', 'vbd['+time+']['+name+']');
                $(this).val('');
            });
            row.find('.delete_row').removeClass('hide');
            $('.bank_details_parent').append(row);
        })

        $(document).on('click', '.delete_row', function(){
            var row = $(this).parent().parent();
            row.remove();
        })

        $(document).on('hover', '.delete_row', function(){
            var row = $(this).parent().parent();
        })

        $(document).on('keyup blur', '#pan_no, #gst_no', function(){
            $(this).val($(this).val().toUpperCase());
        })

        $(document).on('change', '.file_uploads', function(e) {
           var files = e.target.files;
           $('.documents_names').empty();
           var names = [];
           for (var i = 0; i < $(this).get(0).files.length; ++i) {
               names.push($(this).get(0).files[i].name);
           }
           if(names.length > 0)
           {
               var file_list = document.createElement('UL');
               
               $(names).each(function(k,v){
                   var remove_label = document.createElement('LABEL');
                   remove_label.append("REMOVE");
                   remove_label.setAttribute('style', 'color:blue; margin-left:15px');
                   var file_name_label = document.createElement("LI");
                   file_name_label.setAttribute('class', 'file_name_label');
                   file_name_label.append(v);
                   file_list.append(file_name_label);
               });
               $('.documents_names').append(file_list);
           }
        });


        $(document).on('click', '.vendor_doc', function(){
            curr_ele = $(this);
            var image = $(this).attr('file');
            var datafile = $(this).attr('data-file');
            var id = $(this).attr('vendor_documents_id');
            console.log("file imageview",image);
            if(datafile == "image")
            {
                $('.gallerymodal').find('img').removeClass('hide');
                $('.gallerymodal').find('img').addClass('show');
                $('.gallerymodal').find('img').attr('src',image);
                $('.delete_image').attr('document_id',id);
                $('.gallerymodal').find('iframe').removeClass('show');
                $('.gallerymodal').find('iframe').addClass('hide');
            }
            else
            {
                $('.gallerymodal').find('iframe').removeClass('hide');
                $('.gallerymodal').find('iframe').addClass('show');
                $('.gallerymodal').find('iframe').attr('src',image);
                $('.delete_image').attr('document_id',id);
                $('.gallerymodal').find('img').removeClass('show');
                $('.gallerymodal').find('img').addClass('hide');
            }
            
            $('#document_modal').modal('show');
        });

    $(document).on('click','.delete_image',function(){
        var id = $(this).attr('document_id');

        $.ajax({
            type: 'get',
            url:'/masters/vendor-master/delete_document',
            data : {
                entity : 'delete_document',
                id : id
            },
            success: function(res){
                $('#document_modal').modal('hide');
                    $.notify("Document Deleted", 'Success');
                    $(curr_ele).remove();    
                }
            });
    })

    var specialKeys = new Array();
    specialKeys.push(8); //Backspace
    $(".numeric").bind("keypress", function (e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
            $(".error").css("display", ret ? "none" : "inline");
            return ret;
        });
        $(".numeric").bind("paste", function (e) {
            var element = this;
            setTimeout(function () {
                var tip = $(element).val();
                    // REMOVE NON-NUMERIC CHARACTERS
                var cleanTip = tip.replace(/\D/g,'');
                $(element).val(cleanTip);
            }, 100);
        });
    });

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>