<!-- Main Header -->
<header class="main-header">

    <nav class="navbar navbar-static-top">
        <?php echo $__env->make('layouts.partials.session_flash', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <div class="container" style="margin-right: -14px; width :100%">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars fa-2x"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse" style="">
          <ul class="nav navbar-nav">
            <li class=""><a href="<?php echo e(url('/')); ?>">Dashboard <span class="sr-only"></span></a></li>
            
                <?php
                    $data = generateSidebar();
                ?>
            <!-- User Management -->
                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php
                        $tab_name = $key;
                        // var_dump($tab_name);
                        $tab_url = str_replace(' ', '-', strtolower($tab_name));  
                    ?>
                    <?php if(true): ?>
                    <li class="dropdown">
                        <a href="<?php echo e($val[0]['url']); ?>" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class='fa <?php echo e($val[0]["icon"]); ?>' aria-hidden="true"></i> <?php echo e($tab_name); ?></a>
                        <ul class="dropdown-menu" role="menu">
                            <?php $__currentLoopData = $val['inner_tab']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $inner_key => $inner_tab): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li ><a class = "<?php echo e($inner_tab['url']); ?>" href="<?php echo e(url($tab_url.'/'.$inner_tab['url'])); ?>"><i class='fa <?php echo e($inner_tab["icon"]); ?>' aria-hidden="true"></i><?php echo e($inner_key); ?></a></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </li>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Notifications Menu -->
            <!-- User Account Menu -->
            <?php if(Auth::guest()): ?>
                <li><a href="<?php echo e(url('/register')); ?>"><?php echo e(trans('adminlte_lang::message.register')); ?></a></li>
                <li><a href="<?php echo e(url('/login')); ?>"><?php echo e(trans('adminlte_lang::message.login')); ?></a></li>
            <?php else: ?>
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <img src="<?php echo e(asset('/img/user2-160x160.jpg')); ?>" class="user-image" alt="User Image"/>
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs"><?php echo e(Auth::user()->name); ?></span>
                    </a>
                    <ul class="dropdown-menu profile_ul_body" style="border: 2px solid #3c8dbc !important;border-radius: 3px !important;">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <img src="<?php echo e(asset('/img/user2-160x160.jpg')); ?>" class="img-circle" alt="User Image" />
                            <p>
                                <?php echo e(Auth::user()->name); ?>

                                <!-- <small><?php echo e(trans('adminlte_lang::message.login')); ?> Nov. 2012</small> -->
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body">
                            <div class="col-xs-4 text-center">
                                <!-- <a href="#"><?php echo e(trans('adminlte_lang::message.followers')); ?></a> -->
                            </div>
                            <div class="col-xs-4 text-center">
                                <!-- <a href="#"><?php echo e(trans('adminlte_lang::message.sales')); ?></a> -->
                            </div>
                            <div class="col-xs-4 text-center">
                                <!-- <a href="#"><?php echo e(trans('adminlte_lang::message.friends')); ?></a> -->
                            </div>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer" style="margin-top: -25px;">
                            <div class="pull-left">

                                <a href="/change_password">
                                        <button type="button" class="btn btn-sharekhan" data-original-title="Add Users" data-original-title="Add Users" data-toggle="tooltip" data-target="" title="Add <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>" data-original-title="Add <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>">
                                         Change Password</button>
                                    </a>
                                <!-- <a href="#" class="btn btn-default btn-flat"><?php echo e(trans('adminlte_lang::message.profile')); ?></a> -->
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo e(url('/logout')); ?>" class="btn btn-default btn-flat"><?php echo e(trans('adminlte_lang::message.signout')); ?></a>
                            </div>
                        </li>
                    </ul>
                </li>
            <?php endif; ?>
          </ul>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
<?php echo $__env->make('layouts.custom_partials.calculator', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>