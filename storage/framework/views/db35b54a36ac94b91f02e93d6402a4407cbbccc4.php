<?php $__env->startSection('custom-styles'); ?>
  
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link href="<?php echo e(asset('/css/common/bootstrap-switch.min.css')); ?>" media="screen" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
    <link href="<?php echo e(asset('/css/common/chosen.min.css')); ?>" rel="stylesheet" type="text/css" />

<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo $__env->make('layouts.custom_partials.htmlheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    
    <?php echo $__env->make('layouts.custom_partials.contentheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<?php if(Request::segment(4)): ?>
    <?php echo Form::model($data, ['class' => 'form-validate', 'url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey(), Request::segment(4)], 'method' => 'put','enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php else: ?>
    <?php echo Form::open(['class' => 'form-validate', 'url' => "/".Request::segment(1)."/".Request::segment(2),'enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php endif; ?>
<?php $prefix = config('constants.PurchaseOrder.prefix'); ?>
<?php $prefix_gr = config('constants.GoodsReceived.prefix'); ?>
<input type="hidden" name="screen_name" value = "<?php echo e((isset($data) && isset($data['screen_name'])) ? $data['screen_name'] : ''); ?>">
<section class="panel">
    <div class="panel-body">
        <h4> <i class="fa fa-book"></i> Order Basic Detail</h4>
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label>Goods Received No. *</label>
                    <input type="text" name="gr_goods_received_no"  class="form-control required" id="goods_received_no" placeholder="Goods Received Number" value = "<?php echo e(htmlValue('gr_goods_received_no', $data)); ?>" <?php echo e(setReadonly('gr_goods_received_no' , $data['readonly'])); ?>>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Goods Received Date *</label>
                    <input type="text" name="<?php echo e($prefix); ?>date" class="form-control required datepicker" value="<?php echo e(date('Y-m-d')); ?>" readonly="readonly">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Select Vendor *</label>
                    <?php echo e(Form::select('vendor_id',
                        (isset($data) && isset($data['vendor_list'])) ? $data['vendor_list'] : [],
                        htmlSelect($prefix.'vendor_id', $data),
                        array('name'=>$prefix.'vendor_id', 'id' => 'vendor_id', 'class' => 'form-control chosen-select required', 'data-name' => $prefix.'type', 'placeholder' => '', 'data-placeholder' => 'Select Vendor', 'disabled' => 'disabled'))); ?>

                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Purchase Order Number *</label>
                    <input type="text" name="<?php echo e($prefix); ?>purchase_order_number"  class="form-control required" id="purchase_order_num" placeholder="Purchase Order Number" value="<?php echo e(htmlValue($prefix.'purchase_order_number', $data)); ?>" <?php echo e(setDisable('purchase_order_number', $data['disabled'])); ?>>
                </div>
            </div>
            <div class="col-md-4">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Date *</label>
                        <input type="text" name="<?php echo e($prefix); ?>wo_date" class="form-control required datepicker" value="<?php echo e(htmlValue(($prefix.'date'), $data)); ?>" <?php echo e(setDisable('date', $data['disabled'])); ?>>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Total Quantity</label>
                        <input type="text" name="<?php echo e($prefix); ?>total_quantity"  class="form-control" id="total_quantity" placeholder="Total Quantity" readonly="readonly" value="<?php echo e(round(htmlValue($prefix.'total_quantity', $data), 2)); ?>" <?php echo e(setReadonly('total_quantity', $data['disabled'])); ?>>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Total Amount</label>
                        <input type="text" name="<?php echo e($prefix); ?>total_amount"  class="form-control" id="total_amount" placeholder="Total Quantity" readonly="readonly" value="<?php echo e(round(htmlValue($prefix.'total_amount', $data),2)); ?>" <?php echo e(setDisable('total_amount', $data['disabled'])); ?>>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Purchase Order Remarks</label>
                    <input type="text" name="<?php echo e($prefix); ?>remarks"  class="form-control" id="remarks" placeholder="Remarks" value="<?php echo e(htmlValue($prefix.'remarks', $data)); ?>" <?php echo e(setDisable('remarks', $data['disabled'])); ?>>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Goods Received Remakrs</label>
                    <input type="text" name="gr_remarks"  class="form-control" id="remarks" placeholder="Remarks">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="panel">
    <div class="panel-body item_parent_row">
        <div class="row">
            <div class="col-md-4">
                <h4> <i class="fa fa-cubes"></i> Item Details </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label>Item</label>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <label>Unit</label>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <label>Quantity</label>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <label>Prev Quantity Received</label>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <label>Price</label>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <label>Total Amount</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Goods Received Qty</label>
                </div>
            </div>
        </div>
        <?php if(isset($data) && isset($data['item_details']) && sizeof($data['item_details'])): ?>
            <?php $__currentLoopData = $data['item_details']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="row item_row">
                <div class="col-md-2">
                    <div class="form-group">
                        <?php echo e(Form::select('raw_material',
                            (isset($data) && isset($data['raw_material_list'])) ? $data['raw_material_list'] : [],
                            htmlSelect('pod_item_id', $value),
                            array('name'=>'item_details['.$value["purchase_order_details_id"].'][pod_item_id]', 'class' => 'form-control required chosen-select', 'data-name' => 'pod_item_id', 'placeholder' => '', 'data-placeholder' => 'Select Raw Material', 'disabled' => 'disabled'))); ?>

                        <input type="hidden" name="item_details[<?php echo e($value['purchase_order_details_id']); ?>][grd_item_id]" value="<?php echo e(htmlSelect('pod_item_id', $value)); ?>">
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <?php echo e(Form::select('unit',
                            (isset($data) && isset($data['unit_master'])) ? $data['unit_master'] : [],
                            htmlSelect('pod_unit', $value),
                            array('name'=>'item_details['.$value["purchase_order_details_id"].'][pod_unit]', 'data-name' => 'pod_unit', 'class' => 'form-control chosen-select', 'placeholder' => '', 'data-placeholder' => 'Select Unit', 'disabled' => 'disabled'))); ?>

                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <input type="text" name='item_details[<?php echo e($value["purchase_order_details_id"]); ?>][pod_quantity]' placeholder="Quantity" class="form-control pod_quantity required" data-name ="pod_quantity" value="<?php echo e(round(htmlValue('pod_quantity', $value),2)); ?>" <?php echo e(setDisable('pod_quantity', $data['disabled'])); ?>>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <input type="text" class="form-control" disabled="disabled" value="<?php echo e(round(htmlValue('prev_gr', $value),2)); ?>">
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <input type="text" name='item_details[<?php echo e($value["purchase_order_details_id"]); ?>][pod_price]' placeholder="Price" class="form-control required pod_price" data-name ="pod_price" value="<?php echo e(round(htmlValue('pod_price', $value),2)); ?>" <?php echo e(setDisable('pod_price', $data['disabled'])); ?>>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <input type="text" name='item_details[<?php echo e($value["purchase_order_details_id"]); ?>][pod_total_amount]' placeholder="Total Amount" class="form-control required total_amount" data-name ="pod_total_amount" <?php echo e(setDisable('pod_total_amount', $data['disabled'])); ?> value="<?php echo e(round(htmlValue('pod_total_amount', $value),2)); ?>">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <input type="text" name='item_details[<?php echo e($value["purchase_order_details_id"]); ?>][grd_goods_received]' placeholder="Goods Received" class="form-control required total_quantity number" data-name ="grd_goods_received" max = "<?php echo e(htmlValue('pod_quantity', $value) - round(htmlValue('prev_gr', $value),2)); ?>" min=0
                        <?php if(htmlValue('pod_quantity', $value) == htmlValue('prev_gr', $value)): ?>{
                        readonly="readonly"   value="0"
                        <?php endif; ?>

                    }
                        >
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <div class="row">
                <div class="col-md-7">
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Total Quantity Received</label>
                        <input type="text" class="form-control" readonly="readonly" name="gr_total_quantity" placeholder="Total Quantity Received" id="gr_total_quantity">
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>
<?php if(Request::segment(4) !== null && (Request::segment(4) == 'accounts-approve' || Request::segment(4) == 'admin-approve')): ?>
    <section class="panel">
        <div class="panel-body">
            <div class="btn-group">
                <button class="btn btn-success" name="save" value="approve" type="submit" id="save_continue">Approve <i class="fa fa-thumbs-up"></i></button>
            </div>
            <div class="btn-group">
                <button class="btn btn-danger" name="save" value="reject" type="submit" id="save_continue">Reject <i class="fa fa-thumbs-down"></i></button>
            </div>
            <div class="btn-group pull-right">
                <a href="/<?php echo e(Request::segment(1)); ?>/<?php echo e(Request::segment(2)); ?>/"><button class="btn btn-danger" name="cancel" value="cancel" type="button" id="cancel">Cancel</button></a>
            </div>
        </div>
    </section>
<?php else: ?>
    <?php echo $__env->make('layouts.custom_partials.save_panel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php endif; ?>

<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['screen_name'] = $data['screen_name'];
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>
    <?php echo $__env->make('layouts.script_loaders.datatable_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        chosenInit();

        $(".datepicker").datepicker({
            startDate: new Date(),
            format: 'yyyy-mm-dd',
        });
        $(document).on('keyup blur', '.total_quantity', function(){
            var row = $(this).parent().parent().parent();
            calculateTotlaQuantity();
        })

        function calculateTotlaQuantity(){
            total_quantity = 0;
            $('.total_quantity').each(function(){
                qty = parseFloat($(this).val())
                isNaN(qty) ? qty = 0 : '';
                total_quantity += qty;
            })
            $('#gr_total_quantity').val(total_quantity) 
        }

        /*$(document).on('blur keyup','#goods_received_no',function(){
            var order_no = $(this).val();
            $.ajax({
                type: 'get',
                url:'/purchase-module/goods-received/check-order-number',
                data : {
                    order_no : order_no
                },
                success: function(res){
                    console.log(res);
                    if(res > 0)
                    {
                        $('#goods_received_no').parent().addClass('has-error');
                        var p = "<p class = 'help-block error'>Goods Received Number exists</p>";
                        $('#goods_received_no').siblings('p').remove();
                        $('#goods_received_no').parent().append(p);
                        $('#save_continue').prop('disabled', true);
                        $('#save_return').prop('disabled', true);
                    } else {
                        $('#goods_received_no').parent().removeClass('has-error');
                        $('#goods_received_no').siblings('p').remove();
                        $('#save_continue').prop('disabled', false);
                        $('#save_return').prop('disabled', false);
                    }
                }
            });
        })*/

        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        $(".numeric").bind("keypress", function (e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
            $(".error").css("display", ret ? "none" : "inline");
            return ret;
        });
        $(".numeric").bind("paste", function (e) {
            var element = this;
            setTimeout(function () {
                var tip = $(element).val();
                    // REMOVE NON-NUMERIC CHARACTERS
                var cleanTip = tip.replace(/\D/g,'');
                $(element).val(cleanTip);
            }, 100);
        });
    });

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>