<?php $__env->startSection('custom-styles'); ?>
  
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link href="<?php echo e(asset('/css/common/bootstrap-switch.min.css')); ?>" media="screen" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/css/common/bootstrap-datepicker.css')); ?>" />
    <link href="<?php echo e(asset('/css/common/chosen.min.css')); ?>" rel="stylesheet" type="text/css" />

<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo $__env->make('layouts.custom_partials.htmlheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    
    <?php echo $__env->make('layouts.custom_partials.contentheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<?php if(Request::segment(4)): ?>
    <?php echo Form::model($data, ['class' => 'form-validate', 'url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey()], 'method' => 'put','enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php else: ?>
    <?php echo Form::open(['class' => 'form-validate', 'url' => "/".Request::segment(1)."/".Request::segment(2),'enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php endif; ?>
<?php $prefix = config('constants.CustomerMaster.prefix'); ?>
<?php $prefix_d = config('constants.CustomerDocuments.prefix'); ?>
<?php $prefix_bd = config('constants.CustomerBankDetails.prefix'); ?>

<input type="hidden" name="screen_name" value = "<?php echo e((isset($data) && isset($data['screen_name'])) ? $data['screen_name'] : ''); ?>">
<section class="panel">
    <div class="panel-body">
        <div id ="document_modal" class="modal fade">
            <div class="modal-dialog" >
                <div class="modal-content border_radius">
                    <div class="modal-header bg-sharekhan border_radius_top">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Customer Documents</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="gallerymodal">
                                <img class="hide" width="90%" height="300px" style="margin :5px;margin-top: 5%;margin-left: 5%">
                                <iframe class="hide" width="90%" height="300px" style="margin :5px;margin-top: 5%;margin-left: 5%"></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <?php if(isset($data) && isset($data['screen_name']) && $data['screen_name'] == 'customer-master-edit'): ?>
                            <button type="button" class="btn btn-default delete_image">DELETE</button>
                        <?php endif; ?>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <h4> <i class="fa fa-user"></i> Basic Detail </h4>
        <div class="row">
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Customer Name *</label>
                    <i class="fa fa-user pull-right"></i>
                    <input type="text" class="form-control unit_name required" id="<?php echo e($prefix); ?>name" name = "<?php echo e($prefix); ?>name" placeholder="Customer Name" value = "<?php echo e(htmlValue($prefix.'name', $data)); ?>" <?php echo e(setReadonly($prefix."name" , $data['readonly'])); ?> maxlength="100">
                </div>
            </div>
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Contact Person</label>
                    <i class="fa fa-user pull-right"></i>
                    <input type="text" class="form-control unit_symbol" id="<?php echo e($prefix); ?>contact_person" name = "<?php echo e($prefix); ?>contact_person" placeholder="Contact Person" value = "<?php echo e(htmlValue($prefix.'name', $data)); ?>" <?php echo e(setReadonly($prefix."contact_person" , $data['readonly'])); ?> maxlength="100">
                </div>
            </div>
            <div class = "col-md-2">
                <div class="form-group">
                    <label for="name">Phone no</label>
                    <i class="fa fa-phone pull-right"></i>
                    <input type="text" class="form-control phone_no numeric"  id="<?php echo e($prefix); ?>phone_no" name = "<?php echo e($prefix); ?>phone_no" placeholder="Phone no" value = "<?php echo e(htmlValue($prefix.'phone_no', $data)); ?>" <?php echo e(setReadonly($prefix."phone_no" , $data['readonly'])); ?> maxlength="10">
                </div>
            </div> 
            <div class = "col-md-4">
                <div class="form-group">
                    <label for="name">Email Id</label>
                    <i class="fa fa-envelope pull-right"></i>
                    <input type="text" class="form-control email" id="<?php echo e($prefix); ?>email" name = "<?php echo e($prefix); ?>email" placeholder="Email Id" value = "<?php echo e(htmlValue($prefix.'email', $data)); ?>" <?php echo e(setReadonly($prefix."email" , $data['readonly'])); ?> maxlength="100">
                </div>
            </div> 
        </div>
        <div class="row">
            <div class = "col-md-2">
                <div class="form-group">
                    <label for="name">Alt Phone no</label>
                    <i class="fa fa-phone pull-right"></i>
                    <input type="text" class="form-control alt_phone_no numeric" id="<?php echo e($prefix); ?>alt_phone_no" name = "<?php echo e($prefix); ?>alt_phone_no" placeholder="Phone no" value = "<?php echo e(htmlValue($prefix.'alt_phone_no', $data)); ?>" <?php echo e(setReadonly($prefix."alt_phone_no" , $data['readonly'])); ?> maxlength="10">
                </div>
            </div> 
            <div class = "col-md-4">
                <div class="form-group">
                    <label for="name">Alt Email Id</label>
                    <i class="fa fa-envelope pull-right"></i>
                    <input type="text" class="form-control alt_email" id="<?php echo e($prefix); ?>alt_email" name = "<?php echo e($prefix); ?>alt_email" placeholder="Alt Email Id" value = "<?php echo e(htmlValue($prefix.'alt_email', $data)); ?>" <?php echo e(setReadonly($prefix."alt_email" , $data['readonly'])); ?> maxlength="100">
                </div>
            </div> 
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Remarks</label>
                    <i class="fa fa-pencil pull-right"></i>
                    <input type="text" class="form-control remarks" id="<?php echo e($prefix); ?>remarks" name = "<?php echo e($prefix); ?>remarks" placeholder="Remarks" value = "<?php echo e(htmlValue($prefix.'remarks', $data)); ?>" <?php echo e(setReadonly($prefix."remarks" , $data['readonly'])); ?> maxlength="100">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="panel">
    <div class="panel-body">
        <h4>Address Details</h4>
        <h5>Registered Address</h5>
        <div class="row shipping_address">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Address Line 1</label>
                    <i class="fa fa-map-pin pull-right"></i>
                    <input type="text" name="<?php echo e($prefix); ?>s_address_line1" class="form-control address_line1" placeholder="Address Line 1" maxlength="100" value="<?php echo e(htmlValue($prefix.'s_address_line1', $data)); ?>" <?php echo e(setReadonly($prefix."s_address_line1" , $data['readonly'])); ?>>
                </div>
                <div class="form-group">
                    <label>Address Line 2</label>
                    <i class="fa fa-map-pin pull-right"></i>
                    <input type="text" name="<?php echo e($prefix); ?>s_address_line2" class="form-control address_line2" placeholder="Address Line 2" value="<?php echo e(htmlValue($prefix.'s_address_line2', $data)); ?>" maxlength="100" <?php echo e(setReadonly($prefix."s_address_line2" , $data['readonly'])); ?>>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Pincode</label>
                    <i class="fa fa-map pull-right"></i>
                    <input type="text" name="<?php echo e($prefix); ?>s_pincode" class="form-control pincode numeric" placeholder="Pincode" maxlength="6" value="<?php echo e(htmlValue($prefix.'s_pincode', $data)); ?>" <?php echo e(setReadonly($prefix."s_pincode" , $data['readonly'])); ?>>
                </div>
                <div class="form-group">
                    <label>City</label>
                    <i class="fa fa-map-o pull-right"></i>
                    <input type="text" name="<?php echo e($prefix); ?>s_city" class="form-control city" placeholder="City" maxlength="100" value="<?php echo e(htmlValue($prefix.'s_city', $data)); ?>" <?php echo e(setReadonly($prefix."s_city" , $data['readonly'])); ?>>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>State</label>
                    <i class="fa fa-map pull-right"></i>
                    <select class="chosen-select state_list required" name="<?php echo e($prefix); ?>s_state">
                        <?php if(isset($data) && isset($data['state_list'])): ?>
                        <?php $__currentLoopData = $data['state_list']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option
                            <?php if(isset($data) && isset($data[$prefix.'s_state']) && $data[$prefix.'s_state'] == $value['states_id']): ?>
                            selected = 'selected'
                            <?php endif; ?>
                            data-country="<?php echo e($value['s_country']); ?>" value="<?php echo e($value['states_id']); ?>"><?php echo e($value['s_name']); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Country</label>
                    <i class="fa fa-map-o pull-right"></i>
                    <input type="text" name="<?php echo e($prefix); ?>s_country" class="form-control country" placeholder="Country" maxlength="100" value="<?php echo e(htmlValue($prefix.'s_country', $data)); ?>" <?php echo e(setReadonly($prefix."s_country" , $data['readonly'])); ?>>
                </div>
            </div>
        </div>
        <hr>
        <h5>Delivery Address</h5>
        <input type="checkbox" name="<?php echo e($prefix); ?>same_address" class="same_address"><label>Same as Registered</label>
        <div class="row delivery_address" <?php echo e(setReadonly($prefix."s_country" , $data['readonly'])); ?>>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Address Line 1</label>
                    <i class="fa fa-map-pin pull-right"></i>
                    <input type="text" name="<?php echo e($prefix); ?>b_address_line1" class="form-control address_line1" placeholder="Address Line 1" value="<?php echo e(htmlValue($prefix.'b_address_line1', $data)); ?>" maxlength="100" <?php echo e(setReadonly($prefix."b_address_line1" , $data['readonly'])); ?>>
                </div>
                <div class="form-group">
                    <label>Address Line 2</label>
                    <i class="fa fa-map-pin pull-right"></i>
                    <input type="text" name="<?php echo e($prefix); ?>b_address_line2" class="form-control address_line2" placeholder="Address Line 2" maxlength="100" value="<?php echo e(htmlValue($prefix.'b_address_line2', $data)); ?>" <?php echo e(setReadonly($prefix."b_address_line2" , $data['readonly'])); ?>>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Pincode</label>
                    <i class="fa fa-map pull-right"></i>
                    <input type="text" name="<?php echo e($prefix); ?>b_pincode" class="form-control pincode numeric" placeholder="Pincode" maxlength="6" value="<?php echo e(htmlValue($prefix.'b_pincode', $data)); ?>" <?php echo e(setReadonly($prefix."b_pincode" , $data['readonly'])); ?>>
                </div>
                <div class="form-group">
                    <label>City</label>
                    <i class="fa fa-map-o pull-right"></i>
                    <input type="text" name="<?php echo e($prefix); ?>b_city" class="form-control city" placeholder="City" maxlength="100" value="<?php echo e(htmlValue($prefix.'b_city', $data)); ?>" <?php echo e(setReadonly($prefix."b_city" , $data['readonly'])); ?>>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>State</label>
                    <i class="fa fa-map pull-right"></i>
                    <select class="chosen-select state_list required" name="<?php echo e($prefix); ?>b_state">
                        <?php if(isset($data) && isset($data['state_list'])): ?>
                        <?php $__currentLoopData = $data['state_list']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option
                            <?php if(isset($data) && isset($data[$prefix.'b_state']) && $data[$prefix.'b_state'] == $value['states_id']): ?>
                            selected = 'selected'
                            <?php endif; ?>
                            data-country="<?php echo e($value['s_country']); ?>" value="<?php echo e($value['states_id']); ?>"><?php echo e($value['s_name']); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Country</label>
                    <i class="fa fa-map-o pull-right"></i>
                    <input type="text" name="<?php echo e($prefix); ?>b_country" class="form-control country" placeholder="Country" maxlength="100" value="<?php echo e(htmlValue($prefix.'b_country', $data)); ?>" <?php echo e(setReadonly($prefix."b_country" , $data['readonly'])); ?>>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="panel">
    <div class="panel-body doc_customer">
        <h4> <i class="fa fa-file"></i> Customer Documents </h4>
        <?php if(isset($data) && isset($data['screen_name']) && $data['screen_name'] != 'customer-master-show'): ?>
            <div class="row">
                <div class="col-md-3">
                    <label class="btn btn-sharekhan"><i class="fa fa-upload"></i> Upload Documents
                        <input type="file" class="file_uploads" name="<?php echo e($prefix_d); ?>file[]" multiple="multiple" style="display: none;">
                    </label>
                    <div class="documents_names"></div>
                </div>
            </div>
        <?php endif; ?>
        <br>
        <?php if(isset($data) && isset($data['cd_file']) && sizeof($data['cd_file']) > 0): ?>
        <div class="row" id="documents">
            <?php $__currentLoopData = $data['cd_file']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-md-3">
                    <div class="form-group">
                        
                        <?php $fileext = explode('.',$value['cd_path']); ?>
                        <?php if($fileext[1] == "jpg" || $fileext[1] == "jpeg" || $fileext[1] == "png" || $fileext[1] == "gif"): ?>
                            <label class="btn btn-sharekhan customer_doc" file="<?php echo e($value['cd_path']); ?>" data-file ="image" target="_blank" customer_documents_id = "<?php echo e($value['customer_documents_id']); ?>"><i class="fa fa-file"></i> <?php echo e($value['cd_doc_original_name']); ?></label>
                        <?php else: ?>
                            <label class="btn btn-sharekhan customer_doc" file="<?php echo e($value['cd_path']); ?>" data-file ="notimage" target="_blank" customer_documents_id = "<?php echo e($value['customer_documents_id']); ?>"><i class="fa fa-file"></i> <?php echo e($value['cd_doc_original_name']); ?></label>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <?php endif; ?>
        <br>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <h4><i class="fa fa-user"></i>Customer KYC details</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>GST No.</label>
                    <input type="text" name="<?php echo e($prefix); ?>gst_no" class="form-control required" placeholder="GST NO" value="<?php echo e(htmlValue($prefix.'gst_no', $data)); ?>" <?php echo e(setReadonly($prefix."gst_no" , $data['readonly'])); ?> id="gst_no" maxlength="15" minlength="15">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>GST Type</label>
                    <?php echo e(Form::select('gst_type',
                        (isset($data) && isset($data['gst_type'])) ? $data['gst_type'] : [],
                        htmlSelect($prefix.'gst_type', $data),
                        array('name'=>$prefix.'gst_type', 'class' => 'form-control chosen-select gst_type required', 'placeholder' => '', 'data-placeholder' => 'Select GST Type', setDisable($prefix.'gst_type', $data['disabled'])))); ?>

                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>PAN No.</label>
                    <input type="text" name="<?php echo e($prefix); ?>pan_no" class="form-control required" placeholder="GST NO" value="<?php echo e(htmlValue($prefix.'pan_no', $data)); ?>" <?php echo e(setReadonly($prefix."pan_no" , $data['readonly'])); ?> id="pan_no" maxlength="10" minlength="10">
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $__env->make('layouts.custom_partials.save_panel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['screen_name'] = $data['screen_name'];
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>
    <?php echo $__env->make('layouts.script_loaders.datatable_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('/js/notify.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        chosenInit();
        $(document).on('change', '.same_address', function(){
            if($('.same_address').is(':checked'))
            {
                $('.delivery_address').find('.pincode').val($('.shipping_address').find('.pincode').val()).prop('readonly', 'readonly');
                $('.delivery_address').find('.state_list').val($('.shipping_address').find('.state_list').val()).trigger('chosen:updated');
                $('.shipping_address').find('.form-control').each(function(){
                    $('.delivery_address').find('.'+($(this).attr('class')).replace('form-control ', '')).val($(this).val())
                    $('.delivery_address').find('.'+($(this).attr('class')).replace('form-control ', '')).prop('readonly', 'readonly');
                })
            } else {
                $('.delivery_address').find('.form-control').removeAttr('readonly');
            }
        })

        $(document).on('keyup blur', '#pan_no, #gst_no', function(){
            $(this).val($(this).val().toUpperCase());
        })

        $(document).on('change', '.file_uploads', function(e) {
           var files = e.target.files;
           $('.documents_names').empty();
           var names = [];
           for (var i = 0; i < $(this).get(0).files.length; ++i) {
               names.push($(this).get(0).files[i].name);
           }
           if(names.length > 0)
           {
               var file_list = document.createElement('UL');
               
               $(names).each(function(k,v){
                   var remove_label = document.createElement('LABEL');
                   remove_label.append("REMOVE");
                   remove_label.setAttribute('style', 'color:blue; margin-left:15px');
                   var file_name_label = document.createElement("LI");
                   file_name_label.setAttribute('class', 'file_name_label');
                   file_name_label.append(v);
                   file_list.append(file_name_label);
               });
               $('.documents_names').append(file_list);
           }
        });

        var curr_ele = {};

        $(document).on('click', '.customer_doc', function(){
            curr_ele = $(this);
            var image = $(this).attr('file');
            var datafile = $(this).attr('data-file');
            var id = $(this).attr('customer_documents_id');
            
            if(datafile == "image")
            {
                $('.gallerymodal').find('img').removeClass('hide');
                $('.gallerymodal').find('img').addClass('show');
                $('.gallerymodal').find('img').attr('src',image);
                $('.delete_image').attr('document_id',id);
                $('.gallerymodal').find('iframe').removeClass('show');
                $('.gallerymodal').find('iframe').addClass('hide');
            }
            else
            {
                $('.gallerymodal').find('iframe').removeClass('hide');
                $('.gallerymodal').find('iframe').addClass('show');
                $('.gallerymodal').find('iframe').attr('src',image);
                $('.delete_image').attr('document_id',id);
                $('.gallerymodal').find('img').removeClass('show');
                $('.gallerymodal').find('img').addClass('hide');
            }
            
            $('#document_modal').modal('show');
    });

    $(document).on('click','.delete_image',function(){
        var id = $(this).attr('document_id');
        $.ajax({
            type: 'get',
            url:'/masters/customer-master/delete_document',
            data : {
                entity : 'delete_document',
                id : id
            },
            success: function(res){
                $('#document_modal').modal('hide');
                $.notify("Document Deleted", 'Success');
                $(curr_ele).remove();    
            }
        });
    })

    var specialKeys = new Array();
    specialKeys.push(8); //Backspace
    $(".numeric").bind("keypress", function (e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
            $(".error").css("display", ret ? "none" : "inline");
            return ret;
        });
        $(".numeric").bind("paste", function (e) {
            var element = this;
            setTimeout(function () {
                var tip = $(element).val();
                    // REMOVE NON-NUMERIC CHARACTERS
                var cleanTip = tip.replace(/\D/g,'');
                $(element).val(cleanTip);
            }, 100);
        });
        
    });

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>