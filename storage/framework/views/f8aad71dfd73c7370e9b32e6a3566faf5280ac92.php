<style type="text/css">
        .cal_container {
            display: flex!important;
            align-items: center!important;
            justify-content: center!important;
            /*height*/: 100vh!important;
            /*width: 100vw!important;*/
        }

        #cal_container {
            padding: 8px 8px 20px 8px!important;
            margin: 20px auto!important;
            background-color: #ABABAB!important;
            border-radius: 4px!important;
            border-top: 2px solid #FFF!important;
            border-right: 2px solid #FFF!important;
            border-bottom: 2px solid #C1C1C1!important;
            border-left: 2px solid #C1C1C1!important;
            box-shadow: -3px 3px 7px rgba(0, 0, 0, .6), inset -100px 0px 100px rgba(255, 255, 255, .5)!important;
        }

        #display {
            display: block!important;
            margin: 15px auto!important;
            height: 42px!important;
            width: 174px!important;
            padding: 0 10px!important;
            border-radius: 4px!important;
            border-top: 2px solid #C1C1C1!important;
            border-right: 2px solid #C1C1C1!important;
            border-bottom: 2px solid #FFF!important;
            border-left: 2px solid #FFF!important;
            background-color: #FFF!important;
            box-shadow: inset 0px 0px 10px #030303, inset 0px -20px 1px rgba(150, 150, 150, .2)!important;
            font-size: 28px!important;
            color: #666!important;
            text-align: right!important;
            font-weight: 400!important;
        }

        .button {
            display: inline-block!important;
            margin: 2px!important;
            width: 42px!important;
            height: 42px!important;
            font-size: 16px!important;
            font-weight: bold!important;
            border-radius: 4px!important;
        }

        .mathButtons {
            margin: 2px 2px 6px 2px!important;
            color: #FFF!important;
            text-shadow: -1px -1px 0px #44006F!important;
            background-color: #434343!important;
            border-top: 2px solid #C1C1C1!important;
            border-right: 2px solid #C1C1C1!important;
            border-bottom: 2px solid #181818!important;
            border-left: 2px solid #181818!important;
            box-shadow: 0px 0px 2px #030303, inset 0px -20px 1px #2E2E2E!important;
        }

        .digits {
            color: #181818!important;
            text-shadow: 1px 1px 0px #FFF!important;
            background-color: #EBEBEB!important;
            border-top: 2px solid #FFF!important;
            border-right: 2px solid #FFF!important;
            border-bottom: 2px solid #C1C1C1!important;
            border-left: 2px solid #C1C1C1!important;
            border-radius: 4px!important;
            box-shadow: 0px 0px 2px #030303, inset 0px -20px 1px #DCDCDC!important;
        }

        .digits:hover,
        .mathButtons:hover,
        #clearButton:hover {
            background-color: #FFBA75!important;
            box-shadow: 0px 0px 2px #FFBA75, inset 0px -20px 1px #FF8000!important;
            border-top: 2px solid #FFF!important;
            border-right: 2px solid #FFF!important;
            border-bottom: 2px solid #AE5700!important;
            border-left: 2px solid #AE5700!important;
        }

        #clearButton {
            color: #FFF!important;
            text-shadow: -1px -1px 0px #44006F!important;
            background-color: #D20000!important;
            border-top: 2px solid #FF8484!important;
            border-right: 2px solid #FF8484!important;
            border-bottom: 2px solid #800000!important;
            border-left: 2px solid #800000!important;
            box-shadow: 0px 0px 2px #030303, inset 0px -20px 1px #B00000!important;
        }
    </style>

<div id="CalculatorModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Calculator</h4>
                </div>
                <div class="modal-body">
                    <div class="cal_container">
                        <fieldset id="cal_container">
                            <form name="calculator">
                                <input id="display" type="text" name="display" readonly>
                                <input class="button digits" type="button" data-value="7" onclick="calculator.display.value += '7'">
                                <input class="button digits" type="button" data-value="8" onclick="calculator.display.value += '8'">
                                <input class="button digits" type="button" data-value="9" onclick="calculator.display.value += '9'">
                                <input class="button mathButtons" type="button" data-value="+" onclick="calculator.display.value += ' + '">
                                <br>
                                <input class="button digits" type="button" data-value="4" onclick="calculator.display.value += '4'">
                                <input class="button digits" type="button" data-value="5" onclick="calculator.display.value += '5'">
                                <input class="button digits" type="button" data-value="6" onclick="calculator.display.value += '6'">
                                <input class="button mathButtons" type="button" data-value="-" onclick="calculator.display.value += ' - '">
                                <br>
                                <input class="button digits" type="button" data-value="1" onclick="calculator.display.value += '1'">
                                <input class="button digits" type="button" data-value="2" onclick="calculator.display.value += '2'">
                                <input class="button digits" type="button" data-value="3" onclick="calculator.display.value += '3'">
                                <input class="button mathButtons" type="button" data-value="x" onclick="calculator.display.value += ' * '">
                                <br>
                                <input id="clearButton" class="button" type="button" data-value="C" onclick="calculator.display.value = ''">
                                <input class="button digits" type="button" data-value="0" onclick="calculator.display.value += '0'">
                                <input class="button mathButtons equal" type="button" data-value="=" onclick="calculator.display.value = eval(calculator.display.value) , $('.calcuate_done').removeClass('hide')" >
                                <input class="button mathButtons" type="button" data-value="/" onclick="calculator.display.value += ' / '">
                            </form>
                        </fieldset>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                    Close</button>
                    <button type="button" class="btn btn-default calcuate_done hide">
                    Done</button>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
    
</script>