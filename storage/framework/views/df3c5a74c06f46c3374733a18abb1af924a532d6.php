<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style>
        /*.solid {border-style: groove;}*/
        /*.item, td, th {*/
            /**/
        /*}*/
        table, tr, td, th {
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            /*border-collapse: collapse;
            border: 1px solid #000;*/
        }

        /*table + table, table + table tr:first-child th, table + table tr:first-child td {*/
            /*border-top: 0;*/
            /*border-left: hidden;*/
        /*}*/

        .item {
            width: 100%;
            border-collapse: collapse;
            /*border-collapse: collapse;
            border: 1px solid #000;*/
        }

        .item td {
            border-collapse: collapse;
            border: 1px solid #000;
            height: 30px;
            vertical-align: bottom;
        }
        .item tr,.item th {
            border-collapse: collapse;
            border: 1px solid #000; 
        }
        tfoot {
          text-align: right;
        }
        tfoot tr:last-child {
          background: #f0f0f2;
        }

    </style>
</head>
<?php $first = 0; ?>
<?php $last = 2; ?>



<body onload="window.print();" style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;">
    <table style="width: 100%;border-collapse: collapse;border: 1px solid #000;">
        <tr style="border-collapse: collapse;border: 1px solid #000;">
            <td colspan="3" style="border-collapse: collapse;border: 1px solid #000;">
                <center>
                    <div style="text-transform: capitalize;">Supply Raw Material Details</div>
                </center>
            </td>
        </tr>
        <tr style="border-collapse: collapse;border: 1px solid #000;">
            <td style="width: 100%;padding: 1px;border-collapse: collapse;border: 1px solid #000;">
                <b>Request No.:</b><?php echo e($data['rrmm_request_no']); ?><br/>
                <b>Work Order No.:</b><?php echo e($data['work_order_data']->wo_order_no); ?><br/>       
            </td>
        </tr>
        <tr style="border-collapse: collapse;border: 1px solid #000;">
            <td colspan="3" style="border-collapse: collapse;border: 1px solid #000;">
                <center>
                    <div style="text-transform: capitalize;">Finish Goods Details Details</div>
                </center>
            </td>
        </tr>
        <tr style="border-collapse: collapse;border: 1px solid #000;">
            <td colspan="3" style="border-collapse: collapse;border: 1px solid #000;">
                <table class="item" style="table-layout:fixed">
                    <tr style="text-align: right;border-top:hidden;border-left: hidden;border-right: hidden;">
                        <th>S.No.</th>
                        <th>Item</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                    </tr>
                    <?php $__currentLoopData = $data['finish_goods_details']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr style="text-align: right;border-top:hidden;border-left: hidden;border-right: hidden;">
                            <td><?php echo e($key + 1); ?></td>
                            <td><?php echo e($value['item']); ?></td>
                            <td><?php echo e($value['quantity']); ?></td>
                            <td><?php echo e($value['unit']); ?></td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </table>
            </td>
        </tr>
        <tr style="border-collapse: collapse;border: 1px solid #000;">
            <td colspan="3" style="border-collapse: collapse;border: 1px solid #000;">
                <center>
                    <div style="text-transform: capitalize;"> Required Raw Material Details</div>
                </center>
            </td>
        </tr>
        <tr style="border-collapse: collapse;border: 1px solid #000;">
            <td colspan="3" style="border-collapse: collapse;border: 1px solid #000;">
                <table class="item" style="table-layout:fixed">
                    <tr style="text-align: right;border-top:hidden;border-left: hidden;border-right: hidden;">
                        <th>S.No.</th>
                        <th>Raw Material</th>
                        <th>Request Quantity</th>
                        <th>Send Quantity</th>
                    </tr>
                    <?php $__currentLoopData = $data['raw_material_need']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr style="text-align: right;border-top:hidden;border-left: hidden;border-right: hidden;">
                            <td><?php echo e($key + 1); ?></td>
                            <td><?php echo e($value['im_name']); ?></td>
                            <td><?php echo e($value['qty']); ?></td>
                            <td><?php echo e($value['qty']); ?></td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>