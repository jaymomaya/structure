

<?php $__env->startSection('custom-styles'); ?>
    
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
    <link href="<?php echo e(asset('/css/datatables/buttons.bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('/css/common/bootstrap-modal-carousel.min.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo e(ucwords(str_replace('-', ' ', Request::segment(1)))); ?> | <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
    <div class="panel">
        <div class="panel-heading ">   
            <div class = "pull-right">
                <div class="btn-group">
                    <a id="bulk_url" href="/<?php echo e(Request::path()); ?>/create">
                        <label class="btn btn-sharekhan raise_oreders" data-toggle="tooltip" data-target="" title="Raise <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>" data-original-title="Raise <?php echo e(ucwords(str_replace('-', ' ', Request::segment(2)))); ?>" disabled="disabled"><i class="fa fa-cubes"></i> Raise Bulk <?php echo e(ucwords(str_replace('-', ' ',Request::segment(2)))); ?></label>
                    </a>
                </div>
            </div>
        </div>
        <div class="panel-body padding_top_3">
            <table id="role" class="datatable table display compact table-striped table-bordered  hover nowrap" cellspacing="0" role="grid" width="100%">
                <thead>
                    <tr>
                        <?php $__currentLoopData = $data['columns']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $column): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <th class = "thead_<?php echo e($column); ?>"><?php echo e(ucwords(str_replace('_', ' ', $column))); ?></th>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <?php $__currentLoopData = $data['columns']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $column): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(in_array($key ,$data['disable_footer_search'])): ?>
                                <th class = "tfoot_<?php echo e($column); ?> no-search"> <?php echo e(ucwords(str_replace('_', ' ', $column))); ?> </th>
                            <?php else: ?>
                                <th class = "tfoot_<?php echo e($column); ?>"> <?php echo e(ucwords(str_replace('_', ' ', $column))); ?> </th>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['columns'] = $data['columns'];
        $js_data['pk'] = $data['pk'];
        $js_data['prefix'] = $data['prefix'];
        $js_data['status'] = config('constants.STATUS');
        $js_data['color'] = config('constants.COLOR_GENERAL');
        $js_data['screen_name'] = $data['screen_name'];
        $js_data['permissionList'] = $data['permissionList'];
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>

    <?php echo $__env->make('layouts.script_loaders.datatable_global', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo e(asset('/js/notify.js')); ?>"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            
            var action_obj = [];

            var datatable_object = {
                table_name : $('table').attr('id'),
                order : {
                    state: false,
                    column : 1,
                    mode : "desc"
                },
                buttons : {
                    state : true,
                    colvis : true,
                    excel : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    },
                    pdf : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    },
                    csv : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    },
                    copy : {
                        state: true,
                        columns: [$('thead th:not(".thead_action")')] 
                    }
                },
                info : true,
                paging : true,
                searching : true,
                ordering : true,
                iDisplayLength: 10,
                sort_disabled_targets : [],
                ajax_url : window.location.href,
                column_data : datatableColumn(lang.columns, action_obj, lang.pk),
            }

            table = datatableInitGeneral(datatable_object);            
            
            changeRowColorGeneral('thead_po_status', {'Partial' : 'orange', 'Completed': 'green'}, table);

            var ids = [];
            var url = $('#bulk_url').attr('href');


            $(document).on('change', '.work_order_id_chk', function(){
                checkWoIds();
            });


            function checkWoIds(){
                ids = [];
                $('.work_order_id_chk:checked').each(function(){
                    ids.push($(this).attr('data-id'));
                });
                if(ids.length == 0){
                    $('.raise_oreders').attr('disabled', true);
                } else {
                    $('.raise_oreders').removeAttr('disabled');
                }

                $('#bulk_url').attr('href', url+'?ids='+ids.toString());
            }
        });
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>