<?php $__env->startSection('custom-styles'); ?>
  
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link href="<?php echo e(asset('/css/common/bootstrap-switch.min.css')); ?>" media="screen" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/css/common/bootstrap-datepicker.css')); ?>" />
    <link href="<?php echo e(asset('/css/common/chosen.min.css')); ?>" rel="stylesheet" type="text/css" />

<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo $__env->make('layouts.custom_partials.htmlheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    
    <?php echo $__env->make('layouts.custom_partials.contentheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<?php if(Request::segment(4)): ?>
    <?php echo Form::model($data, ['class' => 'form-validate', 'url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey(), Request::segment(4)], 'method' => 'put','enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php else: ?>
    <?php echo Form::open(['class' => 'form-validate', 'url' => "/".Request::segment(1)."/".Request::segment(2),'enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php endif; ?>
<input type="hidden" name="screen_name" value = "<?php echo e((isset($data) && isset($data['screen_name'])) ? $data['screen_name'] : ''); ?>">
<section class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><i class="fa fa-book"></i> Work Order Details</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead style="background-color:grey">
                              <tr style="color:white">
                                <th>Work Order No.</th>
                                <th>Work Order Date</th>
                                <th>Customer</th>
                                <th>Total Quantity</th>
                                <th>Total Amount</th>
                              </tr>
                            </thead>
                            <tbody>
                                <?php if(isset($data)): ?>
                                    <tr>
                                        <td><?php echo e(htmlValue('wo_order_no', $data['work_order'])); ?></td>
                                        <td><?php echo e(htmlValue('wo_date', $data['work_order'])); ?></td>
                                        <td><?php echo e(htmlValue('wo_customer', $data['work_order'])); ?></td>
                                        <td><?php echo e(round(htmlValue('wo_total_quantity', $data['work_order']), 2)); ?></td>
                                        <td><?php echo e(round(htmlValue('wo_total_amount', $data['work_order']), 2)); ?></td>
                                    </tr>
                                <?php endif; ?>
                                <tr>
                                    <th>Remarks: </th>
                                    <td colspan="5"><?php echo e(htmlValue('wo_remarks', $data)); ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><i class="fa fa-cubes"></i> Production to Packacking Details</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead style="background-color:grey">
                          <tr style="color:white">
                            <th>Request No</th>
                            <th>Date</th>
                            <th>Total Quantity</th>
                            <th>Remarks</th>
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php if(isset($data) && isset($data)): ?>
                                <tr>
                                    <td><?php echo e(htmlValue('request_no', $data)); ?></td>
                                    <td><?php echo e($data['sfpm_date']); ?></td>
                                    <td><?php echo e(round($data['sfpm_total_quantity'], 2)); ?></td>
                                    <td><?php echo e($data['sfpm_acceptance_remark']); ?></td>
                                    <td><?php echo e($data['sfpm_accept_status']); ?></td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <hr>
        
            <div class="row col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><i class="fa fa-cubes"></i> Finish Goods Details</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead style="background-color:grey">
                          <tr style="color:white">
                            <th>Sr. No.</th>
                            <th>Item</th>
                            <th>Unit</th>
                            <th>Quantity</th>
                            <th>Prev Send</th>
                            <th>Send to Warehouse</th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php if(isset($data) && isset($data['send_for_packaging_details'])): ?>
                                <?php $__currentLoopData = $data['send_for_packaging_details']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($key+1); ?></td>
                                        <td><?php echo e($value->igm_name); ?></td>
                                        <td><?php echo e($value->um_name); ?></td>
                                        <td><?php echo e(round($value->sfp_quantity, 2)); ?></td>
                                        <td><?php echo e(round($value->prev_send, 2)); ?></td>
                                        <td>
                                            <input type="text" name="item_details[<?php echo e($value->send_for_packaging_details_id); ?>][stwd_quantity]" placeholder="Quantity" class="form-control quantity required" max="<?php echo e(round($value->sfp_quantity, 2) - round($value->prev_send, 2)); ?>" <?php echo e((round($value->sfp_quantity, 2) - round($value->prev_send, 2)) == 0 ? "readonly value=0" : ''); ?>> 
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5"><label>Total</label></td>
                                <td>
                                    <input type="text" name="stw_total_quantity" readonly="readonly" class="form-control total_quantity" placeholder="Total">
                                </td>
                            </tr>
                        </tfoot>                                
                    </table>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Remarks</label>
                        <input type="text" name="stw_remarks" class="form-control" placeholder="Remarks" value = "">
                    </div>
                </div>
            </div>
    </div>
</section>
<section class="panel">
    <div class="panel-body">
        <div class="btn-group">
            <button class="btn btn-success" name="save" value="save" type="submit" id="save_continue">Save</button>
        </div>
        <div class="btn-group pull-right">
            <a href="/<?php echo e(Request::segment(1)); ?>/<?php echo e(Request::segment(2)); ?>/"><button class="btn btn-danger" name="cancel" value="cancel" type="button" id="cancel">Cancel</button></a>
        </div>
    </div>
</section>

<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['screen_name'] = $data['screen_name'];
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>
    <?php echo $__env->make('layouts.script_loaders.datatable_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        chosenInit();
        $(document).on('change, keyup', '.quantity', function(){
            var total = 0;
            $('.quantity').each(function(){
                var cur_val = parseFloat($(this).val());
                isNaN(cur_val) ? cur_val = 0 : '';
                total += cur_val;
            });
            console.log(total,"total");
            $('.total_quantity').val(total);
        })


        /*Duplicate check     
        Add its web 
        add controller function from purchase order*/
        $(document).on('blur','#purchase_order_num',function(){
            var order_no = $(this).val();
            $.ajax({
                type: 'get',
                url:'/purchase-module/purchase-order/'+lang.id+'/check-purchase-order-number',
                data : {
                    order_no : order_no
                },
                success: function(res){
                    console.log(res);
                    if(res > 0)
                    {
                        $('#purchase_order_num').parent().addClass('has-error');
                        var p = "<p class = 'help-block error'>Purchase Order Number exists</p>";
                        $('#purchase_order_num').siblings('p').remove();
                        $('#purchase_order_num').parent().append(p);
                        $('#save_continue').prop('disabled', true);
                        $('#save_return').prop('disabled', true);
                    } else {
                        $('#purchase_order_num').parent().removeClass('has-error');
                        $('#purchase_order_num').siblings('p').remove();
                        $('#save_continue').prop('disabled', false);
                        $('#save_return').prop('disabled', false);
                    }
                }
            });
        })
    });

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>