<?php if(Session::has('message')): ?>
    <div style = "background-color: #43d376; width:100%;position:fixed;z-index: 1001;" class="alert alert-dismissible <?php echo e(Session::get('class')); ?>"><h4><?php echo e(Session::get('message')); ?></h4><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
    <?php echo e(Session::forget('message')); ?>

    <script type="text/javascript">
        setTimeout(function() {
            $(".alert").fadeTo(3000, 500).slideUp(500);
        }, 2500)
    </script>
<?php endif; ?>