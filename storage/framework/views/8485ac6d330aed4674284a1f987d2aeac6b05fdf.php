<?php $__env->startSection('custom-styles'); ?>
  
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link href="<?php echo e(asset('/css/common/bootstrap-switch.min.css')); ?>" media="screen" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/css/common/bootstrap-datepicker.css')); ?>" />
    <link href="<?php echo e(asset('/css/common/chosen.min.css')); ?>" rel="stylesheet" type="text/css" />

<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo $__env->make('layouts.custom_partials.htmlheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    
    <?php echo $__env->make('layouts.custom_partials.contentheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<?php if(Request::segment(4)): ?>
    <?php echo Form::model($data, ['class' => 'form-validate', 'url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey()], 'method' => 'put','enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php else: ?>
    <?php echo Form::open(['class' => 'form-validate', 'url' => "/".Request::segment(1)."/".Request::segment(2),'enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php endif; ?>
<?php $prefix = config('constants.ItemGroup.prefix'); ?>
<input type="hidden" name="screen_name" value = "<?php echo e((isset($data) && isset($data['screen_name'])) ? $data['screen_name'] : ''); ?>">
<section class="panel">
    <div class="panel-body">
        <h4>Basic Detail</h4>
        <div class="row">
            <div class = "col-md-3">
                <div class="form-group">
                    <label for="name">Item Group Name *</label>
                    <input type="text" class="form-control unit_name required" id="<?php echo e($prefix); ?>name" name = "<?php echo e($prefix); ?>group_name" placeholder="Group Name" value = "<?php echo e(htmlValue($prefix.'group_name', $data)); ?>" <?php echo e(setDisable($prefix."group_name" , $data['disabled'])); ?>>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Parent Group</label>
                    <?php echo e(Form::select('parent',
                        (isset($data) && isset($data['parent_list'])) ? $data['parent_list'] : [],
                        htmlSelect($prefix.'parent_id', $data),
                        array('name'=>$prefix.'parent_id', 'id' => 'parent', 'class' => 'form-control chosen-select group_select', 'data-name' => $prefix.'parent_id', 'data-placeholder' => 'Select Parent Group', 'placeholder' => ''))); ?>

                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Group Type*</label>
                    <?php echo e(Form::select('group_type',
                        (isset($data) && isset($data['material_type'])) ? $data['material_type'] : [],
                        htmlSelect($prefix.'group_type', $data),
                        array('name'=>$prefix.'group_type', 'id' => 'group_type', 'class' => 'form-control required chosen-select group_type', 'data-name' => $prefix.'group_type', 'data-placeholder' => 'Select Group Type', 'placeholder' => ''))); ?>

                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Ask Later in Work Order</label>
                    <div class="checkbox checkbox-sharekhan">
                        <input type="checkbox" name="ig_ask_later" class="ig_ask_later styled select" value="1"
                        <?php if(isset($data) && isset($data['ig_ask_later']) && $data['ig_ask_later'] == 1): ?>
                        checked="checked"
                        <?php endif; ?> 
                        >
                        <label></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class = "col-md-12">
                <div class="form-group">
                    <label for="name">Remarks</label>
                    <input type="text" class="form-control remarks" id="<?php echo e($prefix); ?>remarks" name = "<?php echo e($prefix); ?>remarks" placeholder="Remarks" value = "<?php echo e(htmlValue($prefix.'remarks', $data)); ?>" <?php echo e(setDisable($prefix."remarks" , $data['disabled'])); ?>>
                </div>
            </div> 
        </div>
    </div>
</section>


<?php echo $__env->make('layouts.custom_partials.save_panel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['screen_name'] = $data['screen_name'];
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>
    <?php echo $__env->make('layouts.script_loaders.datatable_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        chosenInit();
        $(document).on('change', '.group_select', function(){
            var id = $(this).val();
            if(id != ''){
                $.ajax({
                    url: '/api/get-group-type',
                    type: 'get',
                    data: {
                        id : id
                    },
                    success(res){
                        if(res != undefined && res['material_type'] == undefined){
                            if(res['item_group'] != undefined && res['item_group'][0]){
                                $('.group_type').val(res['item_group'][0]).attr('disabled', true).trigger('chosen:updated');
                            } else {
                                $('.group_type').attr('disabled', false).trigger('chosen:updated');
                            }

                            if(res['ask_later'] != undefined && res['ask_later'][0] && res['ask_later'][0] == 1){
                                $('.ig_ask_later').prop('checked', true);
                            } else {
                                $('.ig_ask_later').prop('checked', false);
                            }
                        } else {
                            $('.group_type').attr('disabled', false).trigger('chosen:updated');
                        }
                    }
                })
            } else {
                $('.group_type').attr('disabled', false).trigger('chosen:updated');
            }
        })
    });

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>