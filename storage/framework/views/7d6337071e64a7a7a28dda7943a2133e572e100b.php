<head>
    <meta charset="UTF-8">
    <title> Project Title - <?php echo $__env->yieldContent('htmlheader_title', 'Your title here'); ?> </title>

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo e(asset('/favicons/apple-icon-57x57.png')); ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo e(asset('/favicons/apple-icon-60x60.png')); ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo e(asset('/favicons/apple-icon-72x72.png')); ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo e(asset('/favicons/apple-icon-76x76.png')); ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo e(asset('/favicons/apple-icon-114x114.png')); ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo e(asset('/favicons/apple-icon-120x120.png')); ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo e(asset('/favicons/apple-icon-144x144.png')); ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo e(asset('/favicons/apple-icon-152x152.png')); ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo e(asset('/favicons/apple-icon-180x180.png')); ?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo e(asset('/favicons/android-icon-192x192.png')); ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo e(asset('/favicons/favicon-32x32.png')); ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo e(asset('/favicons/favicon-96x96.png')); ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo e(asset('/favicons/favicon-16x16.png')); ?>">
    <link rel="manifest" href="<?php echo e(asset('/favicons/manifest.json')); ?>">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff ">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no' name='viewport'>

    
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.7 -->
    <link href="<?php echo e(asset('/css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
    <!-- <link href="<?php echo e(asset('/css/bootstrap-theme.min.css')); ?>" rel="stylesheet" type="text/css" /> -->
    <!-- Font Awesome Icons -->
    <link href="<?php echo e(asset('/css/common/font-awesome.min.css')); ?>" rel="stylesheet" type="text/css" />

    <link href="<?php echo e(asset('/css/common/ionicons.min.css')); ?>" rel="stylesheet" type="text/css" />
    
    <!-- Theme style -->
    <link href="<?php echo e(asset('/css/AdminLTE.css')); ?>" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="<?php echo e(asset('/css/skins/skin-blue.css')); ?>" rel="stylesheet" type="text/css" />

    <link href="<?php echo e(asset('/css/common/bootstrap_checkbox.css')); ?>" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <!-- <link href="<?php echo e(asset('/plugins/iCheck/square/blue.css')); ?>" rel="stylesheet" type="text/css" /> -->

    <link href="<?php echo e(asset('/css/style.css')); ?>" rel="stylesheet" type="text/css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Materialize Yash -->
    <!-- <link type="text/css" rel="stylesheet" href="<?php echo e(asset('css/materialize/materialize.min.css')); ?>"  media="screen,projection"/> -->
    
    <?php echo $__env->yieldContent('custom-styles'); ?>
</head>
