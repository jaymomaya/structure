<?php $__env->startSection('custom-styles'); ?>
  
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link href="<?php echo e(asset('/css/common/bootstrap-switch.min.css')); ?>" media="screen" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
    <link href="<?php echo e(asset('/css/common/chosen.min.css')); ?>" rel="stylesheet" type="text/css" />

<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo $__env->make('layouts.custom_partials.htmlheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    
    <?php echo $__env->make('layouts.custom_partials.contentheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<?php if(Request::segment(4)): ?>
    <?php echo Form::model($data, ['class' => 'form-validate', 'url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey()], 'method' => 'put','enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php else: ?>
    <?php echo Form::open(['class' => 'form-validate', 'url' => "/".Request::segment(1)."/".Request::segment(2),'enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php endif; ?>
<?php $prefix = config('constants.InventoryMaster.prefix'); ?>
<input type="hidden" name="screen_name" value = "<?php echo e((isset($data) && isset($data['screen_name'])) ? $data['screen_name'] : ''); ?>">
<section class="panel">
    <div class="panel-body">
        <h4> <i class="fa fa-book"></i> Inventory Basic Detail</h4>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Inventory Type *</label>
                    <?php echo e(Form::select('inventory_type',
                        (isset($data) && isset($data['inventory_type'])) ? $data['inventory_type'] : [],
                        htmlSelect($prefix.'type', $data),
                        array('name'=>$prefix.'type', 'id' => 'inventory_type', 'class' => 'form-control chosen-select required', 'data-name' => $prefix.'type', 'placeholder' => '', 'data-placeholder' => 'Select Inventory Type',setDisable($prefix.'type', $data['disabled'])))); ?>

                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Reason / Source *</label>
                    <?php echo e(Form::select('source',
                        (isset($data) && isset($data['source'])) ? $data['source'] : [],
                        htmlSelect($prefix.'source', $data),
                        array('name'=>$prefix.'source', 'id' => 'source', 'class' => 'form-control chosen-select required', 'data-name' => $prefix.'source', 'placeholder' => '', 'data-placeholder' => 'Select Inventory Good',setDisable($prefix.'source', $data['disabled'])))); ?>

                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Date *</label>
                    <input type="text" name="<?php echo e($prefix); ?>date" class="form-control datepicker" <?php echo e(setReadonly($prefix."date" , $data['readonly'])); ?> value="<?php echo e(htmlValue($prefix.'date', $data)); ?>">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Total Quantity</label>
                    <input type="text" name="<?php echo e($prefix); ?>total_quantity"  class="form-control" id="total_quantity" placeholder="Total Quantity" readonly="readonly" id = "total_quantity" value="<?php echo e(htmlValue($prefix.'total_quantity', $data)); ?>">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Total Amount</label>
                    <input type="text" name="<?php echo e($prefix); ?>total_amount"  class="form-control" id="total_amount" placeholder="Total Quantity" readonly="readonly" id = "total_amount" value="<?php echo e(htmlValue($prefix.'total_amount', $data)); ?>">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Remakrs</label>
                    <input type="text" name="<?php echo e($prefix); ?>remarks"  class="form-control" id="remarks" placeholder="Remarks" <?php echo e(setReadonly($prefix."remarks" , $data['readonly'])); ?> value="<?php echo e(htmlValue($prefix.'remarks', $data)); ?>">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="panel">
    <div class="panel-body item_parent_row">
        <div class="row">
            <div class="col-md-4">
                <h4> <i class="fa fa-cubes"></i> Item Details </h4>
            </div>
            <div class="col-md-8">
                <i class="fa fa-plus-square fa-lg pull-right add-item-row" data-toggle="tooltip" title="Add Item Row"></i>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label>Item</label>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <label>Unit</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Godown</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Quantity</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Price</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Total Amount</label>
                </div>
            </div>
        </div>
        <?php if(isset($data) && isset($data['inventory_inward']) && sizeof($data['inventory_inward']) > 0): ?>
            <?php $__currentLoopData = $data['inventory_inward']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="row item_row">
            <div class="col-md-2">
                <div class="form-group">
                    <?php echo e(Form::select('raw_material',
                        (isset($data) && isset($data['raw_material_list'])) ? $data['raw_material_list'] : [],
                        htmlSelect('ini_item_id', $value),
                        array('name'=>'item_details[0][item_id]', 'class' => 'form-control required chosen-select', 'data-name' => 'item_id', 'placeholder' => '', 'data-placeholder' => 'Select Raw Material'))); ?>

                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <?php echo e(Form::select('unit',
                        (isset($data) && isset($data['unit_master'])) ? $data['unit_master'] : [],
                        htmlSelect($value['ini_item_id'], $data['item_unit']),
                        array('name'=>'item_details[0][unit]', 'data-name' => 'unit', 'class' => 'form-control chosen-select', 'placeholder' => '', 'data-placeholder' => 'Unit'))); ?>

                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <?php echo e(Form::select('unit',
                        (isset($data) && isset($data['godown_list'])) ? $data['godown_list'] : [],
                        htmlSelect('ini_godown_id', $value),
                        array('name'=>'item_details[0][godown]', 'data-name' => 'godown', 'class' => 'form-control required chosen-select', 'placeholder' => '', 'data-placeholder' => 'Godown'))); ?>

                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <input type="text" name='item_details[0][quantity]' placeholder="Quantity" class="form-control pod_quantity required numeric" data-name ="quantity" value="<?php echo e(htmlValue('ini_quantity', $value)); ?>">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <input type="text" name='item_details[0][price]' placeholder="Price" class="form-control required pod_price numeric" data-name ="price" value="<?php echo e(htmlValue('ini_price', $value)); ?>">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <input type="text" name='item_details[0][amount]' placeholder="Amount" class="form-control total_amount required" data-name ="amount" readonly="readonly" value="<?php echo e(htmlValue('ini_amount', $value)); ?>">
                </div>
            </div>
            <div class="col-md-1">
                <i class="fa fa-trash fa-lg pull-right hide delete_item_row"></i>
            </div>
        </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php else: ?>
            <div class="row item_row">
            <div class="col-md-2">
                <div class="form-group">
                    <?php echo e(Form::select('raw_material',
                        (isset($data) && isset($data['raw_material_list'])) ? $data['raw_material_list'] : [],
                        htmlSelect('item_id', $data),
                        array('name'=>'item_details[0][item_id]', 'class' => 'form-control required chosen-select', 'data-name' => 'item_id', 'placeholder' => '', 'data-placeholder' => 'Select Raw Material'))); ?>

                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <?php echo e(Form::select('unit',
                        (isset($data) && isset($data['unit_master'])) ? $data['unit_master'] : [],
                        htmlSelect('item_details[0][unit]', $data),
                        array('name'=>'item_details[0][unit]', 'data-name' => 'unit', 'class' => 'form-control chosen-select', 'placeholder' => '', 'data-placeholder' => 'Unit'))); ?>

                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <?php echo e(Form::select('unit',
                        (isset($data) && isset($data['godown_list'])) ? $data['godown_list'] : [],
                        htmlSelect('item_details[0][godown]', $data),
                        array('name'=>'item_details[0][godown]', 'data-name' => 'godown', 'class' => 'form-control required chosen-select', 'placeholder' => '', 'data-placeholder' => 'Godown'))); ?>

                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <input type="text" name='item_details[0][quantity]' placeholder="Quantity" class="form-control pod_quantity required numeric" data-name ="quantity">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <input type="text" name='item_details[0][price]' placeholder="Price" class="form-control required pod_price numeric" data-name ="price">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <input type="text" name='item_details[0][amount]' placeholder="Amount" class="form-control total_amount required" data-name ="amount" readonly="readonly">
                </div>
            </div>
            <div class="col-md-1">
                <i class="fa fa-trash fa-lg pull-right hide delete_item_row"></i>
            </div>
        </div>
        <?php endif; ?>

    </div>
</section>

<?php echo $__env->make('layouts.custom_partials.save_panel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['screen_name'] = $data['screen_name'];
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>
    <?php echo $__env->make('layouts.script_loaders.datatable_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        chosenInit();
        $(".datepicker").datepicker({
            startDate: new Date(),
            format: 'yyyy-mm-dd',
        });
        $(document).on('click', '.add-item-row', function(){
            var row = $('.item_row:eq(0)').clone();
            var time = new Date().getTime();
            $(row).find('input').each(function(){
                var name = $(this).data('name');
                $(this).attr('name', 'item_details['+time+']['+name+']');
                $(this).val('');
            });
            $(row).find('select').each(function(k, v) {
                var n = $(v).attr('data-name');    
                $(v).attr('name', 'item_details['+time+']['+n+']');
                $(v).siblings('div').remove();
            });
            row.find('.delete_item_row').removeClass('hide');
            $('.item_parent_row').append(row);
            chosenInit();
        });

        $(document).on('change', '#inventory_type', function(){
            inventory_type = $(this).val();
            $.ajax({
                url: window.location.href,
                type: 'get',
                data: {
                    inventory_type: inventory_type
                },
                success: function(res){
                    $('#source').empty();
                    appendSelectData('#source', res);
                }
            })
        })

        $(document).on('click', '.delete_item_row', function(){
            $(this).parent().parent().remove();
            calculateTotalAmount();
            calculateTotlaQuantity();
        })

        $(document).on('change', '#finish_goods', function(){
            var name = $('#finish_goods').find('option:selected').text();
            names = name.split(' ');
            var new_name = '';
            $(names).each(function(k,v){
                new_name += v[0];
            })
            new_name = new_name.toUpperCase();
            $('.bom_name').val(new_name);
        })

        $(document).on('keyup blur', '.pod_quantity, .pod_price', function(){
            var row = $(this).parent().parent().parent();
            calculateRowAmount(row);
        })

        function calculateRowAmount(ele){
            var price = $(ele).find('.pod_price').val();
            var qty = $(ele).find('.pod_quantity').val();
            if(isNaN(price)){
                price = 0;
            }
            if(isNaN(qty)){
                qty = 0;
            }
            var total = qty * price;
            $(ele).find('.total_amount').val(total);
            calculateTotalAmount();
            calculateTotlaQuantity();
        }

        function calculateTotalAmount(){
            total_amount = 0;
            $('.total_amount').each(function(){
                total_amount += parseFloat($(this).val());
            });
            isNaN(total_amount) ? total_amount = 0 : '';
           $('#total_amount').val(total_amount) 
        }
        function calculateTotlaQuantity(){
            total_quantity = 0;
            $('.pod_quantity').each(function(){
                total_quantity += parseFloat($(this).val());
            });
            isNaN(total_quantity) ? total_quantity = 0 : '';
            $('#total_quantity').val(total_quantity) 
        }

        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        $(".numeric").bind("keypress", function (e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
            $(".error").css("display", ret ? "none" : "inline");
            return ret;
        });
        $(".numeric").bind("paste", function (e) {
            var element = this;
            setTimeout(function () {
                var tip = $(element).val();
                    // REMOVE NON-NUMERIC CHARACTERS
                var cleanTip = tip.replace(/\D/g,'');
                $(element).val(cleanTip);
            }, 100);
        });
    });

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>