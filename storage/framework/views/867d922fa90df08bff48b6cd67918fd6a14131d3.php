

<?php $__env->startSection('custom-styles'); ?>
  
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link href="<?php echo e(asset('/css/common/bootstrap-switch.min.css')); ?>" media="screen" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
    <link href="<?php echo e(asset('/css/common/chosen.min.css')); ?>" rel="stylesheet" type="text/css" />

<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo $__env->make('layouts.custom_partials.htmlheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    
    <?php echo $__env->make('layouts.custom_partials.contentheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<?php if(Request::segment(4)): ?>
    <?php echo Form::model($data, ['class' => 'form-validate', 'url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey(), Request::segment(4)], 'method' => 'put','enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php else: ?>
    <?php echo Form::open(['class' => 'form-validate', 'url' => "/".Request::segment(1)."/".Request::segment(2),'enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php endif; ?>
<input type="hidden" name="screen_name" value = "<?php echo e((isset($data) && isset($data['screen_name'])) ? $data['screen_name'] : ''); ?>">
<section class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><i class="fa fa-book"></i> Work Order Details</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead style="background-color:grey">
                              <tr style="color:white">
                                <th>Work Order No.</th>
                                <th>Work Order Date</th>
                                <th>Customer</th>
                                <th>No. of Items</th>
                                <th>Total Amount</th>
                              </tr>
                            </thead>
                            <tbody>
                                <?php if(isset($data)): ?>
                                    <tr>
                                        <td><?php echo e(htmlValue('wo_order_no', $data)); ?></td>
                                        <td><?php echo e(htmlValue('wo_date', $data)); ?></td>
                                        <td><?php echo e(htmlValue('cm_name', $data)); ?></td>
                                        <td><?php echo e(round(htmlValue('wo_total_quantity', $data),2)); ?></td>
                                        <td><?php echo e(round(htmlValue('wo_total_amount', $data),2)); ?></td>
                                    </tr>
                                <?php endif; ?>
                                <tr>
                                    <th>Remarks: </th>
                                    <td colspan="4"><?php echo e(htmlValue('wo_remarks', $data)); ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><i class="fa fa-cubes"></i> Finish Goods Details</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead style="background-color:grey">
                          <tr style="color:white">
                            <th>Sr. No.</th>
                            <th>Item</th>
                            <th>Quantity</th>
                            <th>Unit</th>
                            <th>Rate</th>
                            <th>Amount</th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php if(isset($data) && isset($data['finish_goods_details'])): ?>
                                <?php $__currentLoopData = $data['finish_goods_details']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($key+1); ?></td>
                                        <td><?php echo e($value['item']); ?></td>
                                        <td><?php echo e($value['quantity']); ?></td>
                                        <td><?php echo e($value['unit']); ?></td>
                                        <td><?php echo e($value['rate']); ?></td>
                                        <td><?php echo e($value['amount']); ?></td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label><i class="fa fa-cubes"></i> Required Raw Material Details</label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 table-responsive">
                <table class="table table-bordered table-striped">
                    <thead style="background-color:grey">
                      <tr style="color:white">
                        <th>Sr. No.</th>
                        <th>Raw Material</th>
                        <th>Total Quantity (WIP + Available)</th>
                        <th>WIP</th>
                        <th>Available</th>
                        <th>Required (BOM)</th>
                        <th>Shortfall</th>
                        <th>Po Quantity</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php if(isset($data) && isset($data['raw_material_need'])): ?>
                            <?php $__currentLoopData = $data['raw_material_need']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($key+1); ?></td>
                                    <td><?php echo e($value['name']); ?></td>
                                    <td><?php echo e($value['total']); ?></td>
                                    <td><?php echo e($value['wip']); ?></td>
                                    <td style="color:green; font-weight: bold"><?php echo e($value['available']); ?></td>
                                    <td style="color:orange; font-weight: bold"><?php echo e($value['quantity']); ?></td>
                                    <?php if($value['shortfall'] >= 0): ?>
                                        <td style="color:green; font-weight: bold"><?php echo e($value['shortfall']); ?></td>
                                    <?php else: ?>
                                        <td style="color:red; font-weight: bold"><?php echo e($value['shortfall']); ?></td>
                                    <?php endif; ?>
                                    <td style="color:black; font-weight: bold"><?php echo e($value['po_quantity']); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<section class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <i class="fa fa-money fa-lg"></i> <label>Purchase Order</label>
            </div>
            <div class="col-md-4 pull-right">
                <a href="#" data-toggle="modal" data-target="#companyadd"><label class="btn btn-sharekhan pull-right"><i class="fa fa-plus"></i> Raise PO</label></a>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12 table-responsive">
                <table class="table table-bordered table-striped">
                    <thead style="background-color:grey">
                        <tr style="color:white">
                            <th>Purchase Order No</th>
                            <th>Date</th>
                            <th>Vendor</th>
                            <th>Total Quantity</th>
                            <th>Total Amount</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if(isset($data) && isset($data['po_details'])): ?>
                        <?php $__currentLoopData = $data['po_details']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($value['po_purchase_order_number']); ?></td>
                            <td><?php echo e($value['po_date']); ?></td>
                            <td><?php echo e($value['po_vendor_id']); ?></td>
                            <td><?php echo e(round($value['po_total_quantity'], 2)); ?></td>
                            <td><?php echo e(round($value['po_total_amount'], 2)); ?></td>
                            <td><?php echo e($value['po_remarks']); ?></td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="6"><center><label>NO Purchase Order Raised for this order <i class="fa fa-thumbs-down"></i></label></center></td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
            </table>
        </div>
    </div>
</section>

<section class="panel">
    <div class="panel-body">
        <div class="btn-group pull-right">
            <a href="/<?php echo e(Request::segment(1)); ?>/<?php echo e(Request::segment(2)); ?>/"><button class="btn btn-success" name="cancel" value="cancel" type="button" id="cancel">Save</button></a>
        </div>
    </div>
</section>

<?php echo Form::close(); ?>

<?php echo $__env->make('layouts.custom_partials.raise_po', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['screen_name'] = $data['screen_name'];
        $js_data['po_no'] = $data['po_purchase_order_number'];
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>
    <?php echo $__env->make('layouts.script_loaders.datatable_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('/js/userjs/dynamic_raise_po.js')); ?>" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        chosenInit();

        $(".datepicker").datepicker({
            startDate: new Date(),
            format: 'yyyy-mm-dd',
        });
        $(document).on('click','.vendor_add',function(){
            window.open("/masters/vendor-master/create", '_blank');

        })
        $(document).on('click','.vendor_refresh',function(){
            $.ajax({
                type: 'get',
                url:'/api/get-vendor',
                success: function(res){
                        $('.vendor_select').empty();
                        appendSelectData('.vendor_select',res['vendor_list'],"");
                    }       
                });
        })

        $(document).on('click','.item_add',function(){
            window.open("/masters/item-master/create", '_blank');

        })
        $(document).on('click','.item_refresh',function(){
            $.ajax({
                type: 'get',
                url:'/api/get-item-raw',
                success: function(res){
                        $('.item_select').empty();
                        appendSelectData('.item_select',res['raw_material_list'],"");
                    }       
                });
        })
    });

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>