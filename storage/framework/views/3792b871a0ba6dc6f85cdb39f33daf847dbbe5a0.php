<?php $__env->startSection('custom-styles'); ?>
  
    <?php echo $__env->make('layouts.style_loaders.token_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link href="<?php echo e(asset('/css/common/bootstrap-switch.min.css')); ?>" media="screen" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/css/common/bootstrap-datepicker.css')); ?>" />
    <link href="<?php echo e(asset('/css/common/chosen.min.css')); ?>" rel="stylesheet" type="text/css" />

<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
    <?php echo $__env->make('layouts.custom_partials.htmlheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('contentheader_title'); ?>
    
    <?php echo $__env->make('layouts.custom_partials.contentheader_title', ['title' => (isset($data) && isset($data['name']) ? $data['name'] : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-breadcrumb'); ?>
    <?php echo $__env->make('layouts.custom_partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<?php if(Request::segment(4)): ?>
    <?php echo Form::model($data, ['class' => 'form-validate', 'url' => ["/".Request::segment(1)."/".Request::segment(2), $data->getRouteKey(), Request::segment(4)], 'method' => 'put','enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php else: ?>
    <?php echo Form::open(['class' => 'form-validate', 'url' => "/".Request::segment(1)."/".Request::segment(2),'enctype' => 'multipart/form-data','autocomplete' => 'off']); ?>

<?php endif; ?>
<input type="hidden" name="screen_name" value = "<?php echo e((isset($data) && isset($data['screen_name'])) ? $data['screen_name'] : ''); ?>">
<section class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><i class="fa fa-book"></i> Work Order Details</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead style="background-color:grey">
                              <tr style="color:white">
                                <th>Work Order No.</th>
                                <th>Work Order Date</th>
                                <th>Customer</th>
                                <th>Total Quantity</th>
                                <th>Total Amount</th>
                              </tr>
                            </thead>
                            <tbody>
                                <?php if(isset($data)): ?>
                                    <tr>
                                        <td><?php echo e(htmlValue('wo_order_no', $data['work_order'])); ?></td>
                                        <td><?php echo e(htmlValue('wo_date', $data['work_order'])); ?></td>
                                        <td><?php echo e(htmlValue('wo_customer', $data['work_order'])); ?></td>
                                        <td><?php echo e(htmlValue('wo_total_quantity', $data['work_order'])); ?></td>
                                        <td><?php echo e(htmlValue('wo_total_amount', $data['work_order'])); ?></td>
                                    </tr>
                                <?php endif; ?>
                                <tr>
                                    <th>Remarks: </th>
                                    <td colspan="4"><?php echo e(htmlValue('wo_remarks', $data)); ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><i class="fa fa-cubes"></i> Production to Packacking Details</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead style="background-color:grey">
                          <tr style="color:white">
                            <th>Date</th>
                            <th>Total Quantity</th>
                            <th>Remarks</th>
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php if(isset($data)): ?>
                                <tr>
                                    <td><?php echo e($data['stw_date']); ?></td>
                                    <td><?php echo e($data['stw_total_quantity']); ?></td>
                                    <td><?php echo e($data['stw_acceptance_remark']); ?></td>
                                    <td><?php echo e($data['stw_accept_status']); ?></td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <hr>

        <div class="row col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label><i class="fa fa-cubes"></i> Finish Goods Details</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <label>Product</label>
                </div>
                <div class="col-md-2">
                    <label>Unit</label>
                </div>
                <div class="col-md-3">
                    <label>Quantity</label>
                </div>
            </div>
            <?php $__currentLoopData = $data['send_for_packaging_details']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="parent_row">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control" value="<?php echo e($value->igm_name); ?>" readonly="readonly">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" class="form-control" value="<?php echo e($value->um_name); ?>" readonly="readonly">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control total_quantity" value="<?php echo e($value->stwd_quantity); ?>" readonly="readonly">
                            </div>
                        </div>
                    </div>
                    <div class="item_row row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Godown *</label>
                                <?php echo e(Form::select('raw_material',
                                    (isset($data) && isset($data['godown_list'])) ? $data['godown_list'] : [],
                                    [],
                                    array('name' => 'items['.$value->stwd_wod_id.'][0][godown]','class' => 'form-control required chosen-select', 'data-name' => 'qcd_godown', 'data-key' => $value->stwd_wod_id,'placeholder' => '', 'data-placeholder' => 'Select Raw Material'))); ?>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Quantity</label>
                                <input type="text" name="items[<?php echo e($value->stwd_wod_id); ?>][0][quantity]}}" data-key="<?php echo e($value->stwd_wod_id); ?>" class="form-control quantity" required="required">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="btn add-row btn-default pull-right"> Add Row</label>
                            <label class="btn btn-default delete_row pull-right hide"><i class="fa fa-trash fa-lg"></i></label>
                        </div>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <br>
        <div class="row col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label><i class="fa fa-cubes"></i> Finish Goods Details</label>
                    </div>
                </div>
            </div>
            <div class="col-md-12 table-responsive">
                <table class="table table-bordered table-striped">
                    <thead style="background-color:grey">
                      <tr style="color:white">
                        <th>Sr. No.</th>
                        <th>Item</th>
                        <th>Unit</th>
                        <th>Quantity</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php if(isset($data) && isset($data['send_for_packaging_details'])): ?>
                            <?php $__currentLoopData = $data['send_for_packaging_details']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($key+1); ?></td>
                                    <td><?php echo e($value->igm_name); ?></td>
                                    <td><?php echo e($value->um_name); ?></td>
                                    <td><?php echo e($value->stwd_quantity); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                    </tbody>
                    
                </table>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Remarks</label>
                    <input type="text" name="stw_acceptance_remark" class="form-control" placeholder="Remarks" value = "<?php echo e($data['send_for_packaging']['sfpm_acceptance_remark']); ?>">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="panel">
    <div class="panel-body">
        <div class="btn-group">
            <button class="btn btn-success" name="save" value="save" type="submit" id="save_continue">Accept</button>
        </div>
        <div class="btn-group pull-right">
            <a href="/<?php echo e(Request::segment(1)); ?>/<?php echo e(Request::segment(2)); ?>/"><button class="btn btn-danger" name="cancel" value="cancel" type="button" id="cancel">Cancel</button></a>
        </div>
    </div>
</section>

<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('php-to-js'); ?>
    <?php
        $js_data = array();
        $js_data['env'] = env('APP_ENV');
        $js_data['screen_name'] = $data['screen_name'];
    ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>
    <?php echo $__env->make('layouts.script_loaders.datatable_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.script_loaders.excel_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset('/js/common/chosen.jquery.min.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        chosenInit();
        $(document).on('change, keyup', '.quantity', function(){
            chosenInit();
            var total = 0;
            $('.quantity').each(function(){
                var cur_val = parseFloat($(this).val());
                isNaN(cur_val) ? cur_val = 0 : '';
                total += cur_val;
            });
            $('#total_quantity').val(total);
        })

        $(document).on('click', '.add-row', function(){
            var row = $(this).parent().parent().clone();
            var parent = $(this).parent().parent().parent();
            var time = new Date().getTime();
            $(row).find('select').each(function(k, v) {
                var key = $(v).data('key');    
                $(v).attr('name', 'items['+key+']['+time+'][godown]');
                $(v).siblings('div').remove();
            });

            $(row).find('input').each(function(){
                var key = $(this).data('key');
                $(this).attr('name', 'items['+key+']['+time+'][quantity]');
                $(this).val('');
            });
            
            $(parent).append(row);
            chosenInit();
            $(row).find('.add-row').addClass('hide');
            $(row).find('.delete_row').removeClass('hide');
        })

        $(document).on('keyup', '.quantity', function(){
            var parent = $(this).parent().parent().parent().parent();
            var total = 0;
            var req_total = $(parent).find('.total_quantity').val();
            $(parent).find('.quantity').each(function(k, v) {
                var curr = parseFloat($(this).val());
                curr = isNaN(curr) ? 0 : curr;
                total += curr;
            });
            if(total > req_total){
                $(parent).css('border', '1px solid red');
                $(parent).css('padding', '5px');
                $('.btn-success').attr('disabled', true);
            } else {
                $(parent).css('border', '');
                $(parent).css('padding', '');
                $('.btn-success').attr('disabled', false);
            }
        })

        $(document).on('click', '.delete_row', function(){
            $(this).parent().parent().remove();
        });

    });

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>