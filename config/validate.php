<?php

return [
	'EmployeeMasterRequest' => [
		
		'em_phone_number' => [
			'required'
		],
		'em_email' => [
			'required'
		],
		'em_firstname' => [
			'required'
		],
	],
	
];