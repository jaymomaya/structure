<?php

return [
    
    'APP_URL_ENV' => '127.0.0.1:8000',
    // 'APP_URL_ENV' => 'localhost:8000',

    'EmployeeMaster' => [
        'table' =>'employee_master',
        'prefix' =>'em_',
    ],
    'Tabs' => [
        'table' => 'tabs'
    ],
    
    'States' => [
        'table' => 'states',
        'prefix' => 's_'    
    ],
	'timestamp' => ['created_at', 'updated_at'],
	'exclude_cd' => ['status_cd', 'created_by', 'updated_by', 'domain_id'],

    'ROLE' => [
        'super_admin' => 1
    ],
    'PERMISSION'=> [
        'super_admin' => 1,
    ],

    'CODE_ID_PARENT_TAB' => 100001,
    'CODE_ID_INNER_TAB' => 100002,
    'CODE_ID_PERMISSION_TAB' => 100003,

    'CODE_ID_STATUS' => 100004,
    'CODE_ID_GENDER' => 100007,
    'CODE_ID_SCHEME' => 100011,
    'CODE_ID_LIFETIME_FLAG' => 100012,
    'CODE_ID_COURSE_RETAKE' => 100013,
    'CODE_ID_FAMILY_SCHEME' => 100014,
    'CODE_ID_STUDENT_SCHEME' => 100015,
    'CODE_ID_AVAILABILITY' => 100016,
    'CODE_ID_COURSE_TYPE' => 100017,
    'CODE_ID_DAYSOFWEEK' => 100018,
    'CODE_ID_INCOMECONDITION' => 100019,
    'CODE_ID_COURSE_APPLICABILITY' => 100020,
    'STATUS' => [
        'open'  => [
            'VALUE' => 'OPEN',
            'COLOR' => 'orange',
            'OPTION' => [],
            'SHOW' => 0,
            'DB_VALUE' => 1
        ],
        'active'  => [
            'VALUE' => 'ACTIVE',
            'COLOR' => 'green',
            'OPTION' => ['INACTIVE' => 'INACTIVE', 'CLOSED' => 'CLOSED'],
            'SHOW' => 1,
            'DB_VALUE' => 2
        ],
        'inactive'  => [
            'VALUE' => 'INACTIVE',
            'COLOR' => 'red',
            'OPTION' => ['ACTIVE' => 'ACTIVE','CLOSED' => 'CLOSED'],
            'SHOW' => 1,
            'DB_VALUE' => 3
        ],
        'closed'  => [
            'VALUE' => 'CLOSED',
            'COLOR' => 'grey',
            'OPTION' => ['INACTIVE' => 'INACTIVE', 'ACTIVE' => 'ACTIVE','CLOSED' => 'CLOSED'],
            'SHOW' => 0,
            'DB_VALUE' => 4
        ],
        'Deleted' => [
            'VALUE' => 'Deleted',
            'COLOR' => 'red',
            'OPTION' => [],
            'SHOW' => 0,
            'DB_VALUE' => 4
        ],
        'system_defined' => [
            'VALUE' => 'SYSTEM_DEFINED',
            'COLOR' => 'ORANGE',
            'OPTION' => [],
            'SHOW' => 0,
            'DB_VALUE' => 5
        ],
    ],
    'COLOR_GENERAL' => [
        'OPEN' => 'yellow',
        'ACTIVE' => 'green',
        'INACTIVE' => 'red',
        'CLOSED' => 'grey',
        'system_defined'=> 'orange',
        'Deleted' => 'red',
        'ORDERRAISED' => 'green',
        'ORDERCANCELLED' => 'red',
        'PAYMENTCOMPLETED' =>'blue'
    ],
    'ITEM_ACCEPT_STATUS' => [
        'Send'  => [
            'VALUE' => 'Send',
            'DB_VALUE' => 1
        ],
        'accepted'  => [
            'VALUE' => 'Accepted',
            'DB_VALUE' => 2
        ],
        'rejected'  => [
            'VALUE' => 'Rejected',
            'DB_VALUE' => 3
        ],
        'sent_back'  => [
            'VALUE' => 'Sent Back',
            'DB_VALUE' => 4
        ],
    ],

];