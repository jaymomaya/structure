<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\City;
use App\Models\BranchMaster;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Datatables;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        
    }
    public function index(Request $request)
    {
        if($request->ajax()) {
            $data =  execSelect("
                SELECT 
                    u.id,
                    u.name as name,
                    u.email as email,
                    GROUP_CONCAT(DISTINCT bm.bm_name ORDER BY bm.branch_master_id) AS branch_list,
                    ANY_VALUE(r.name) as role
                FROM
                    roles as r,
                    model_has_roles mhr,
                    users AS u
                    LEFT JOIN branch_master AS bm
                        ON find_in_set(bm.branch_master_id, u.branch_master_id) > 0
                    where u.id = mhr.model_id AND r.id = mhr.role_id
                    AND u.branch_master_id = ?
                    group by u.id;", [\Auth::User()->branch_master_id]);
            $data = collect($data);
            return Datatables::of($data)->make(true);
        }   

        $data['columns'] = [];
        $data['columns'] = array_merge(['action','name','email','branch_list','role'], $data['columns'], []);
        $data['pk'] = 'id';
        $data['disable_footer_column'] = ['action'];
        $data['disable_footer_search'] = [];
        $data['disable_footer_search'] = getIndex($data['disable_footer_column'], $data['columns']);
        return view('users.user', ['data' => $data]);
        /*$data = User::orderBy('id','DESC')->paginate(5);
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);*/
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        /*if($request->all() != null)
        {
            $get_dropdown_data = $request->all();
            if(isset($get_dropdown_data['entity']) && $get_dropdown_data['entity'] == "search")
            {
                $data['roles'] = City::where('city_name','like',$get_dropdown_data['search_text'].'%')->take(5)->pluck('city_name','city_id');    
            }
            else
            {
                $minsize =  $get_dropdown_data['size'];
                $maxsize = $get_dropdown_data['size'] + 10;
                $data['roles'] = City::skip($maxsize)->take(10)->orderBy('city_name', 'ASC')->pluck('city_name','city_id');
                // return $data;
                if(sizeof($data['roles']) == 0)
                {
                    // return $data;
                    $data['roles'] = City::pluck('city_name','city_id')->take(20);
                    $data['start'] = 0;
                }
            }
            return $data;
        }
        else
        {
            $data['roles'] = City::take(20)->orderBy('city_name', 'ASC')->pluck('city_name','city_id');
            return view('users.create',compact('data'));
        }*/
        $data['branch_list'] = BranchMaster::pluck('bm_name',BranchMaster::getKeyField());
        $data['roles'] = Role::pluck('name','id');
        return view('users.create',compact('data'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);


        $input = $request->all();
        // $input['branch_master_id'] = implode(',',$input['branch_master_id'] );
        $input['password'] = Hash::make($input['password']);
        // return $input;


        $user = User::create($input);
        $user->assignRole($request->input('roles'));

        // return $input;
        return redirect("/".$request->segment(1)."/".$request->segment(2));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $user = User::find($id);
        $roles = Role::pluck('name','name')->all();
        $userRole = $user->roles->pluck('name','name')->all();
        $branch_list = BranchMaster::pluck('bm_name',BranchMaster::getKeyField());
        $branch_master_id = $user['branch_master_id'];
        // return $user;
        return view('users.show',compact('user','roles','userRole','branch_list','branch_master_id'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::pluck('name','name')->all();
        $userRole = $user->roles->pluck('name','name')->all();
        $branch_list = BranchMaster::pluck('bm_name',BranchMaster::getKeyField());
        $branch_master_id = $user['branch_master_id'];
        // return $user;
        return view('users.edit',compact('user','roles','userRole','branch_list','branch_master_id'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);


        $input = $request->all();
        // $input['branch_master_id'] = implode(',',$input['branch_master_id']);
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = array_except($input,array('password'));    
        }


        $user = User::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id',$id)->delete();


        $user->assignRole($request->input('roles'));


        return redirect()->route('users.index')
                        ->with('success','User updated successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        User::find($id)->delete();
        return responseJson(200, "Record Updated Successfully");
    }
}