<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('adminlte::auth.login');
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    protected function authenticated()
    {
        $user = \Auth::User();
        Session::put('user_id', $user->id);

        $files = array_diff(scandir(storage_path('framework/sessions')), array('.', '..', '.gitignore'));

        foreach ($files as $file) {
            $filepath = storage_path('framework/sessions/' . $file);
            $session = unserialize(file_get_contents($filepath));

            if ($session !== NULL && isset($session['user_id']) && $session['user_id'] === $user->id && $session['_token'] !== Session::get('_token')) {
                unlink($filepath);
            }
        }

        return redirect()->intended($this->redirectPath());
    }
}
