<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Datatables;
use DB;


class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct(Products $products)
    {
        $this->products = $products;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data =  execSelect("SELECT products_id,
                            p_name as name,
                            p_slace as slace,
                            p_animal as animal,
                            p_thing as thing,
                            p_cost as cost,
                            p_quantity as quantity,
                            p_desp as desp
                        FROM products r ORDER BY r.products_id DESC;", []);

            $data = collect($data);
            // dd($data);
            return Datatables::of($data)->make(true);
        }   

        $data['columns'] = excludeColumn(getColumnList($this->products), []); // Array to be excluded.
        $data['columns'] = array_merge(['action'], $data['columns'], []);
        
        $data['pk'] = 'products_id';
        $data['prefix'] = config('constants.Products.prefix');
        $data['disable_footer_column'] = ['action'];
        $data['disable_footer_search'] = [];
        // $data['user_type'] = \Auth::User()->user_type;        
        $data['disable_footer_search'] = getIndex($data['disable_footer_column'], $data['columns']);
        // dd($data);
        // return view('data.index',compact('data'))
            // ->with('i', ($request->input('page', 1) - 1) * 5);

        return view('data.index', ['data' => $data]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = Permission::get();
        return view('role.create',compact('permission'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'permission' => 'required',
        ]);


        $role = Role::create(['name' => $request->input('name')]);
        $role->syncPermissions($request->input('permission'));


        return redirect()->route('role.index')
                        ->with('success','Role created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
            ->where("role_has_permissions.role_id",$id)
            ->get();
        // dd($role);

        return view('role.show',compact('role','rolePermissions'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        // dd(11);
        $permission = Permission::get();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();
        return view('role.edit',compact('role','permission','rolePermissions'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'permission' => 'required',
        ]);


        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->save();


        $role->syncPermissions($request->input('permission'));


        return redirect()->route('role.index')
                        ->with('success','Role updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table("roles")->where('id',$id)->delete();
        return redirect()->route('role.index')
                        ->with('success','Role deleted successfully');
    }

    public function roleCreate()
    {
        $role = Role::create(['name' => 'writer']);
        $permission = Permission::create(['name' => 'edit articles']);
        
        $role->givePermissionTo($permission);
        $permission->assignRole($role);
    }
}