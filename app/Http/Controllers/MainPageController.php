<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\AccessControl\ChangePasswordRequest;
use App\Models\User;
use Flash;
/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class MainPageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        return view('main');
    }
    public function ChangePassword(){
        $data = [];
        return view('change_password', ['data' => $data]);
    }
    public function CheckPassword(Request $request){
        $data = $request->all();
        $user = \Auth::user();
        // dd($user);
        $curPassword = $data['current_password'];
        $newPassword = bcrypt($data['new_password']);
        $confirmPassword = bcrypt($data['password_confirmation']);
        if($data['new_password'] == $data['password_confirmation']){
            if (\Hash::check($curPassword, $user->password)) {

            $user->forceFill([
                'password' => bcrypt($data['new_password']),
                'remember_token' => Str::random(60),
            ])->save();

            flashMessage("Password changed Successfully.", 'alert-success');
            // return $request->session()->flash('success', 'Password changed');
            return redirect('/');
            }
            else
            {
                
                flashErrorMessage("Current Password Is Invalid.", 'alert-error');
                return redirect('/change_password');
            }
        }else{
            flashMessage("New Password And Confirm Password Not Matched.", 'alert-error');
            return redirect('/change_password');
        }

        
    }
}