<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use Datatables;


class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct(Role $roles)
    {
        $this->roles = $roles;        
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            // dd(12);
            $data =  Role::get();
            $data = collect($data);
            return Datatables::of($data)->make(true);
        }   

        // $data['columns'] = excludeColumn(getColumnList($this->roles), []);
       // dd(1); 
        $data['columns'] = [];
        $data['columns'] = array_merge(['action','name'], $data['columns'], []);
        // return $data['columns']; // Array to be excluded.
        $data['pk'] = 'id';
        // $data['prefix'] = config('constants.CreditorTypeMaster.prefix');
        $data['disable_footer_column'] = ['action'];
        $data['disable_footer_search'] = [];
        // $data['user_type'] = \Auth::User()->user_type;        
        $data['disable_footer_search'] = getIndex($data['disable_footer_column'], $data['columns']);
        // return $data;
        // return view('data.index',compact('data'))
            // ->with('i', ($request->input('page', 1) - 1) * 5);

        return view('role.role', ['data' => $data]);
        /*$roles = Role::orderBy('id','DESC')->paginate(5);
        // dd(11);
        return view('role.index',compact('roles'))
            ->with('i', ($request->input('page', 1) - 1) * 5);*/
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data['tab'] = sidebarData();

        $data['tab'] = objectToArray($data['tab']);
        $data['screen_name'] = 'role_edit';
        $data['selected_status'] = config('constants.STATUS.active.DB_VALUE');
        $data['readonly'] = ['status'];
        $data['disabled'] = ['status'];
        // return $data;
        return view('role.role_operation',compact('data'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'permission' => 'required',
        ]);

        // dd($request->input('permission'));
        $role = Role::create(['name' => $request->input('name')]);
        $role->syncPermissions($request->input('permission'));


        return redirect()->route('role.index')
                        ->with('success','Role created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Role::find($id);
        if(!isset($data))
        {
            abort(404);
        }

        $rolePermissions = permissionListViaRoles($id);
        
        $data['permission'] = $rolePermissions;
        // dd($data['permission']);
        return $this->createShow($data);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         return $this->show($id);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->save();
        $role->syncPermissions($request->input('permission'));

        return redirect()->route('role.index')
                        ->with('success','Role updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table("roles")->where('id',$id)->delete();
        return redirect()->route('role.index')
                        ->with('success','Role deleted successfully');
    }

    public function roleCreate()
    {
        $role = Role::create(['name' => 'writer']);
        $permission = Permission::create(['name' => 'edit articles']);
        
        $role->givePermissionTo($permission);
        $permission->assignRole($role);
    }

     public function createShow($data = "") {

        $data['tab'] = sidebarData();
        $data['tab'] = objectToArray($data['tab']);
        $data['screen_name'] = 'role_edit';
        $data['selected_status'] = config('constants.STATUS.active.DB_VALUE');
        $data['readonly'] = ['status'];
        $data['disabled'] = ['status'];

        // return $data['permission'];
        return view('role.role_operation', ['data' => $data]);
    }
}