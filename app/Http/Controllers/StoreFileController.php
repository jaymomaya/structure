<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Datatables;
use App\Models\User;
use Carbon\Carbon;

class StoreFileController extends BaseController
{
    
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store_file(Request $request)
    {
        $data = $request->all();
        $file = $request->file('file');
        $name = $file[0]->getClientOriginalName();
        $new_name = explode('.', $name);
        $extension = $file[0]->getClientOriginalExtension();
        $files1 = scandir(public_path().'/uploads/excel-upload/');
        $filesName = array();
        $count = 0;
        $dt = time();
        $image_name = $data['stor_file_name'].'.'.$extension;
        foreach ($file as $key => $value) {
            if(file_exists(public_path().'/uploads/excel-upload/'.$image_name))
            {
                unlink(public_path().'/uploads/excel-upload/'.$image_name);
                $value->move(public_path().'/uploads/excel-upload/',$image_name);
            }
            else
            {
                $value->move(public_path().'/uploads/excel-upload/',$image_name);    
            }
            $count++;
        }

        return "success";
    }
    
    public function index(Request $request)
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
     
    }   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
