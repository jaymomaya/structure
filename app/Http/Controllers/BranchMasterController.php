<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Datatables;
use App\Models\AccessControl\Role;
use App\Models\BranchMaster;
use App\Models\AccessControl\Permission;
use App\Models\SystemCodes;
use App\Models\Users;
use Flash;

class BranchMasterController extends BaseController
{
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(BranchMaster $branch_master)
    {
        $this->branch_master = $branch_master;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $data = execSelect("
                SELECT 
                    branch_master_id,
                    bm_name AS name,
                    bm_mobile AS mobile,
                    bm_alt_mobile as alt_mobile,
                    bm_email AS email,
                    bm_alt_email as alt_email,
                    bm_address as address
                from branch_master
                WHERE status = ?;
                ", [config('constants.STATUS.active.DB_VALUE')]);
            $data = collect($data);
            return Datatables::of($data)->make(true);
        }

        $type = $request->query('type');
        $data['columns'] = excludeColumn(getColumnList($this->branch_master), ['status']);
        $data['columns'] = array_merge(['action'], $data['columns']);
        $data['pk'] = BranchMaster::getKeyField();
        $data['disable_footer_search'] = ['action'];
        $data['prefix'] = [];
        $data['status'] = [];
        $data['update'] = [];
        $data['user_type'] = [];
        $data['permissionList'] = permissionList();
        // dd(permissionList());
        return view('branch_master', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        $data['readonly'] = [];
        $data['disabled'] = [];
        $data['screen_name'] = 'branch-master-create';
        return view('branch_master_operation', ['data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $save_cond = $request->save;
        
        beginTransaction();
            create($this->branch_master, $data);
        commit();
        
        if ($save_cond == "save_continue") {
            return redirect('/'.$request->path().'/create');
        } else if ($save_cond == "save_return") {
            return redirect('/'.$request->path());
        } else if($save_cond == "save_modal"){
            return $data['company_list'];
        } else {
            return excelResponseJson();
        }

        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = BranchMaster::find($id);
        $data['name'] = 'Show'; 
        $data['disabled'] = ['bm_name', 'bm_mobile', 'bm_alt_mobile','bm_email','bm_alt_email','bm_address'];
        $data['screen_name'] = 'branch-master-create';
        return view('branch_master_operation', ['data' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = BranchMaster::find($id);
        $data['disabled'] = [];
        $data['screen_name'] = 'branch-master-edit';
        return view('branch_master_operation', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        beginTransaction();
        fillUpdate($this->branch_master, $data, $id, BranchMaster::getKeyField());
        commit();
        flashMessage("Branch Master Updated Successfully.", 'alert-success');
        return redirect("/".$request->segment(1)."/".$request->segment(2));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $data['status'] = config('constants.STATUS.closed.DB_VALUE');
        beginTransaction();
        fillUpdate($this->branch_master, $data, $id, BranchMaster::getKeyField());
        commit();
        return responseJson(200, "Record Updated Successfully");
    }
}
