<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Datatables;
use App\Models\AccessControl\Role;
use App\Models\Masters\ItemMaster;
use App\Models\Masters\ItemGroupMaster;
use App\Models\Masters\RawMaterialBom;
use App\Models\AccessControl\Permission;
use App\Models\SystemCodes;
use App\Models\Users;
use App\Models\Masters\States;
use App\Models\Masters\VendorMaster;
use App\Models\Masters\ItemGroup;
use App\Models\Masters\UnitMaster;
use App\Models\Masters\CustomerMaster;
use App\Models\Masters\BillOfMaterialMaster;
use App\Models\Settings\VariationStyle;
use App\Models\Settings\VariationSize;
use App\Models\Settings\VariationColor;

class ApiController extends BaseController
{
    public function __construct()
    {
        
    }
    
    public function checkHasVariation(Request $request, $product_id){
        $data['item_list'] = RawMaterialBom::where('rmb_finish_goods_id', $product_id)
                            ->where('igm.igm_has_variation', '=', 1)
                            ->leftJoin('item_group_master as igm', 'igm.item_group_master_id', '=', 'rmb_raw_material_id')
                            ->pluck('rmb_raw_material_id')->toArray();
        
        if(isset($data) && isset($data['item_list']) && sizeof($data['item_list']) > 0){
            foreach ($data['item_list'] as $key => $value) {
                $data['raw_material_list'][$value] = ItemMaster::where('status', 2)->where('im_item_group_id', $value)->pluck('im_name','item_master_id');
            }
            $data['item_list'] = ItemGroupMaster::whereIn(ItemGroupMaster::getKeyField(), $data['item_list'])->pluck('igm_name', ItemGroupMaster::getKeyField());
        } else {
            return 'fail';
        }

        return $data;
    }

    public function getVendor()
    {
        $data['vendor_list'] = VendorMaster::where('status', config('constants.STATUS.active.DB_VALUE'))->where('domain_id',\Auth::User()->branch_master_id)->plucK('vm_name', VendorMaster::getKeyField());
        return $data;
    }

    public function getItemGroup()
    {
        $data['group_name'] = ItemGroup::where('status', config('constants.STATUS.active.DB_VALUE'))->where('domain_id',\Auth::User()->branch_master_id)->pluck('ig_group_name', ItemGroup::getKeyField());
        return $data;
    }

    public function getItemTax($id){
        return ItemMaster::where(ItemMaster::getKeyField(), $id)->pluck('im_tax');
    }

    public function getUnit()
    {
        $data['unit_master'] = UnitMaster::where('status', config('constants.STATUS.active.DB_VALUE'))->where('domain_id',\Auth::User()->branch_master_id)->pluck('um_name', UnitMaster::getKeyField());
        return $data;
    }

    public function getCustomer()
    {
        $data['customer_list'] = CustomerMaster::where('status', config('constants.STATUS.active.DB_VALUE'))->where('domain_id',\Auth::User()->branch_master_id)->pluck('cm_name', CustomerMaster::getKeyField());
        return $data;
    }

    public function getItem()
    {
        $data['bom'] = BillOfMaterialMaster::where('status',2)->where('domain_id',\Auth::User()->branch_master_id)->pluck('bom_finish_goods_id');
        $data['finish_goods_list'] = ItemGroupMaster::where('igm_material_type', 2)->where('status', config('constants.STATUS.active.DB_VALUE'))->whereIn(ItemGroupMaster::getKeyField(), $data['bom'])->where('domain_id',\Auth::User()->branch_master_id)->pluck('igm_name', ItemGroupMaster::getKeyField());
        return $data;
    }

    public function getItemRaw()
    {
         $data['raw_material_list'] = ItemMaster::where('im_material_type', 1)->where('status', config('constants.STATUS.active.DB_VALUE'))->where('domain_id',\Auth::User()->branch_master_id)->pluck('im_name', ItemMaster::getKeyField());

        return $data;
    }

    public function getSize()
    {
        $data['size'] = VariationSize::where('status', 2)->pluck('vs_size',VariationSize::getKeyField());
        return $data;
    }

    public function getColor()
    {
        $data['color'] = VariationColor::where('status', 2)->pluck('vc_color',VariationColor::getKeyField());
        return $data;
    }

    public function getStyle()
    {
        $data['style'] = VariationStyle::where('status', 2)->pluck('vst_style',VariationStyle::getKeyField());
        return $data;
    }    
        
    public function getGroupType(Request $request){
        $data = $request->all();
        if(isset($data['id']) && $data['id'] != ''){
            $data['item_group'] = ItemGroup::where(ItemGroup::getKeyField(), $data['id'])->pluck('ig_group_type');
            $data['ask_later'] = ItemGroup::where(ItemGroup::getKeyField(), $data['id'])->pluck('ig_ask_later');
            return $data;
        } else {
            $data['material_type'] = SystemCodes::where('code_id', 100005)->pluck('code_desc', 'code_val');
            return $data;
        }
    }        

    public function getCustomerState($id){
        $state = CustomerMaster::find($id);
        $data['states'] = States::where('states_id', $state['cm_s_state'])->pluck('s_name', 'states_id');
        $data['state_type'] = States::where('states_id', $state['cm_s_state'])->pluck('s_is_state');
        return $data;
    }

    public function getProductStock(Request $request){
        $data = $request->all();
        // return $data;
        $id = $data['id'];
        $godown_id = $data['godown_id'];

        $data = execSelect("
            SELECT 
                im.im_name,
                CAST((COALESCE(INV_IN.QTY, 0) - (COALESCE(INV_WIP.QTY, 0) - COALESCE(INV_OUT.QTY, 0)) - COALESCE(INV_OUT.QTY, 0))
                    AS DECIMAL (15 , 2 )) AS available,
                CAST((COALESCE(INV_WIP.QTY, 0) - COALESCE(INV_OUT.QTY, 0))
                    AS DECIMAL (15 , 2 )) AS wip,
                CAST(((COALESCE(INV_IN.QTY, 0) - (COALESCE(INV_WIP.QTY, 0) - COALESCE(INV_OUT.QTY, 0)) - COALESCE(INV_OUT.QTY, 0)) + (COALESCE(INV_WIP.QTY, 0) - COALESCE(INV_OUT.QTY, 0)))
                    AS DECIMAL (15 , 2 )) AS total
            FROM
                item_master AS im
                    LEFT JOIN
                (SELECT 
                    ini_godown_id,
                        ini_item_id,
                        SUM(ini_quantity) AS QTY
                FROM
                    inventory_inward
                WHERE ini_godown_id = ?
                GROUP BY ini_godown_id , ini_item_id) AS INV_IN ON (INV_IN.ini_item_id = im.item_master_id)
                    LEFT JOIN
                (SELECT 
                    iod_item_id, SUM(iod_quantity) AS QTY, iod_godown_id
                FROM
                    inventory_outward_details
                WHERE iod_godown_id = ?
                GROUP BY iod_item_id , iod_godown_id) AS INV_WIP ON (INV_WIP.iod_item_id = im.item_master_id)
                    LEFT JOIN
                (SELECT 
                    uii_raw_item_id, SUM(uii_qty) AS QTY, uii_godown_id
                FROM
                    user_item_inventory
                WHERE uii_godown_id = ?
                GROUP BY uii_raw_item_id , uii_godown_id) AS INV_OUT ON (INV_OUT.uii_raw_item_id = im.item_master_id)
                WHERE item_master_id = ?
            ", [$godown_id, $godown_id, $godown_id, $id]);

        $data = json_decode(json_encode($data), true);        
        return $data;
    }

    public function getVendorsProduct(Request $request){
        $data = $request->all();
        $id = $data['wo_ids'];
        $vendor_id = $data['id'];

        $groups = VendorMaster::find($vendor_id);

        if(isset($groups) && isset($groups['vm_item_group'])){
            $groups = $groups['vm_item_group'];
        } else{
            return ;
        }

        $data['raw_material_need'] = execSelect("
            SELECT
                TAB.name,
                TAB.im_group_id,
                TAB.unit,
                TAB.wob_raw_material_id,
                CAST(COALESCE(TAB.quantity, 0) AS DECIMAL (15 , 2 )) AS quantity,
                CAST((COALESCE(ITEM_IN.in_qty, 0) - (COALESCE(ITEM_WIP.wip_qty, 0) - COALESCE(inv_out.qty, 0)) - COALESCE(inv_out.qty, 0))
                    AS DECIMAL (15 , 2 )) AS available,
                CAST((COALESCE(ITEM_IN.in_qty, 0) - COALESCE(inv_out.qty, 0))
                    AS DECIMAL (15 , 2 )) AS total,
                CAST((COALESCE(ITEM_WIP.wip_qty, 0) - COALESCE(inv_out.qty, 0))
                    AS DECIMAL (15 , 2 )) AS wip,
                IF(((COALESCE(ITEM_IN.in_qty, 0) - (COALESCE(ITEM_WIP.wip_qty, 0) - COALESCE(inv_out.qty, 0)) - COALESCE(inv_out.qty, 0)) - COALESCE(TAB.quantity, 0)) >= 0,
                0,
                ((COALESCE(ITEM_IN.in_qty, 0) - (COALESCE(ITEM_WIP.wip_qty, 0) - COALESCE(inv_out.qty, 0)) - COALESCE(inv_out.qty, 0)) - COALESCE(TAB.quantity, 0))) AS shortfall,
                IF(((COALESCE(ITEM_IN.in_qty, 0) - (COALESCE(ITEM_WIP.wip_qty, 0) - COALESCE(inv_out.qty, 0)) - COALESCE(inv_out.qty, 0)) - COALESCE(TAB.quantity, 0)) >= 0,
                0,
                ABS((COALESCE(ITEM_IN.in_qty, 0) - (COALESCE(ITEM_WIP.wip_qty, 0) - COALESCE(inv_out.qty, 0)) - COALESCE(inv_out.qty, 0)) - COALESCE(TAB.quantity, 0))) AS abs_shortfall,
                CAST(COALESCE(ITEM_PO.po_qty, 0) AS DECIMAL (15 , 2 )) AS po_quantity
            FROM
                (SELECT 
                    IM.im_name AS name,
                    IM.im_unit AS unit,
                    IM.im_group_id,
                        wob_raw_material_id,
                        COALESCE(SUM(wob_quantity), 0) AS quantity
                FROM
                    work_order_bom
                LEFT JOIN item_master AS IM ON (IM.item_master_id = wob_raw_material_id)
                WHERE
                    wob_wo_id IN ({$id})
                GROUP BY wob_raw_material_id) AS TAB
                    LEFT JOIN
                (SELECT 
                    iod_item_id, SUM(iod_quantity) AS wip_qty
                FROM
                    inventory_outward_details
                GROUP BY iod_item_id) AS ITEM_WIP ON (ITEM_WIP.iod_item_id = TAB.wob_raw_material_id)
                    LEFT JOIN
                (SELECT 
                    SUM(ini_quantity) AS in_qty, ini_item_id
                FROM
                    inventory_inward
                GROUP BY ini_item_id) AS ITEM_IN ON (ITEM_IN.ini_item_id = TAB.wob_raw_material_id)
                    LEFT JOIN
                (SELECT 
                    pod_item_id AS item_id, SUM(pod_quantity) AS po_qty
                FROM
                    purchase_order_details
                LEFT JOIN (SELECT 
                    purchase_order_id, po_qc_status
                FROM
                    purchase_order) AS PO ON (PO.purchase_order_id = pod_po_id)
                WHERE
                    PO.po_qc_status = 1
                GROUP BY pod_item_id) AS ITEM_PO ON (ITEM_PO.item_id = TAB.wob_raw_material_id)
                    LEFT JOIN
                (SELECT 
                    uii_raw_item_id, SUM(uii_qty) AS qty
                FROM
                    user_item_inventory
                GROUP BY uii_raw_item_id) AS inv_out ON (inv_out.uii_raw_item_id = TAB.wob_raw_material_id)
            WHERE TAB.im_group_id IN ({$groups})
            HAVING shortfall < 0
            ORDER BY TAB.wob_raw_material_id DESC;", []);

        $data['raw_material_need'] = json_decode(json_encode($data['raw_material_need']), true);        
        return $data;
    }

    public function getCostSheet(Request $request){
        $data = $request->all();
        if(isset($data) && isset($data['entity_id']) && $data['entity_id'] != ''){
            return ItemGroupMaster::where(ItemGroupMaster::getKeyField(), $data['entity_id'])->select('igm_cost_sheet')->get();
        } else {
            return 0;
        }
    }
}