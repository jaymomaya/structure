<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Datatables;
use App\Models\Test\EmployeeMaster;
use App\Models\User;
use App\Http\Requests\Test\EmployeeMasterRequest;

class EmployeeMasterController extends BaseController
{
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(EmployeeMaster $employee_master)
    {
        $this->employee_master = $employee_master;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // dd(config('constants'));
        if ($request->ajax()) {

            $data =  execSelect("
                    SELECT 
                            *
                    From employee_master;", [\Auth::User()->domain_id]);

            $data = collect($data);
            // return $data;
            return Datatables::of($data)->make(true);
        }
        
        $data['columns'] = excludeColumn(getColumnList($this->employee_master), []); // Array to be excluded.
        $data['columns'] = array_merge(['action'],$data['columns'],[]);
        
        $data['pk'] = EmployeeMaster::getKeyField();
        $data['prefix'] = config('constants.EmployeeMaster.prefix');

        $data['update'] = canEdit();

        $data['disable_footer_column'] = ['action'];
        $data['disable_footer_search'] = ['action'];
        $data['user_type'] = \Auth::User()->user_type;
        $data['disable_footer_search'] = getIndex($data['disable_footer_column'], $data['columns']);
        $data['screen_name'] = "employee";
        $data['designation'] = EmployeeMaster::distinct()->pluck('em_designation', 'em_designation');
        // $data['permissionList'] = permissionList();
        // dd($data['permissionList']);
        return view('test.employee_master', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        //check domain of the employee
        $domain = \Auth::User()->domain_id;
        // tm_process_status 2 for active and 5 for system defined and domain ID 0 for system defined AG
        /*$data['employee_type'] = TypeMaster::distinct()
                                                    ->whereIn('domain_id', [$domain, 0])
                                                    ->whereIn('tm_process_status', [2,5])
                                                    ->pluck('tm_name', TypeMaster::getKeyField());

        $data['role'] = Role::getActive()
                        ->orderBy(Role::getKeyField(), 'desc')
                        ->where('status',config('constants.STATUS.active.DB_VALUE'))
                        ->where('domain_id', $domain)
                        ->orWhere('domain_id', 0)
                        ->pluck('name', Role::getKeyField());

        */// domain id will be checked and active employees with status 2 AG
        $data['employee_master'] = EmployeeMaster::distinct()
                                                    ->where('domain_id', $domain)
                                                    ->where('status',2)
                                                    ->pluck('em_name',EmployeeMaster::getKeyField());
        $data['screen_name'] = "create";
        $data['readonly'] = [];
        $data['disabled'] = [];
        return view('test.employee_master_add', ['data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        // return $data;
        $save_cond =  $request->save;
        $prefix = config('constants.EmployeeMaster.prefix');
        if(isset($data) && isset($data['filename']))
        {   $data = getSingleExcelDataAjax($request);
            return $data;
            foreach ($data['entity'] as $key => $value) {
                // return $key;
                $value['name'] = $value['em_email'];
                // return $value;
                $value['password'] = '123456';
                beginTransaction();
                $value['em_name'] = $value['em_firstname'].' '.$value['em_middlename'].' '.$value['em_lastname'];
                $res = create($this->employee_master, $value);        
                commit();
                # code...
            }
            
        }
        else
        {
            $data['name'] = $data['em_email'];
            $data['password'] = '123456';
            beginTransaction();
                $data['em_name'] = $data['em_firstname'].' '.$data['em_middlename'].' '.$data['em_lastname'];
                $res = create($this->employee_master, $data);
            flashMessage("Employee Master Added Successfully.", 'alert-success');
            commit();
        }
        

        if ($save_cond == "save_continue") {
            return redirect('/'.$request->path().'/create');
        } else if ($save_cond == "save_return") {
            return redirect('/'.$request->path());
        } else {
            return excelResponseJson();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function show(Request $request,$id)
    {
        $domain = \Auth::User()->domain_id;
        $data = EmployeeMaster::where('domain_id', $domain)->find($id);
        if(!isset($data)) {
            abort(404);
        }
        if($data['em_firstname'] == ''){
            $name = explode(" ", $data['em_name']);
            if(isset($name[0]))
            $data['em_firstname'] = $name[0];
            if(isset($name[1]))
            $data['em_middlename'] = $name[1];
            if(isset($name[2]))
            $data['em_lastname'] = $name[2];
        }

        $data['employee_master'] = EmployeeMaster::distinct()->pluck('em_name',EmployeeMaster::getKeyField());
        $data['screen_name'] = "view";
        $data['name'] = "show";
        $data['readonly'] = ['em_name','em_designation','em_phone_number','em_email','em_firstname','em_middlename','em_lastname','em_reportingto','em_groupcode','em_designationcode','em_department','em_aadhar_no','em_pan_no','em_bank_acc_num','em_ifsc_code','em_branch_name','em_bank_name','em_acc_holder_name','e_inc[0][from]','e_inc[0][to]','e_inc[0][amount]'];
        $data['disabled'] = ['em_name','em_designation','em_phone_number','em_email','em_firstname','em_middlename','em_lastname','em_reportingto','em_groupcode','em_designationcode','em_department','em_aadhar_no','em_pan_no','em_type_id', 'role'];
        return view('test.employee_master_add', ['data' => $data]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $domain = \Auth::User()->domain_id;
        $data = EmployeeMaster::where('domain_id', $domain)->find($id);
        if(!isset($data)) {
            abort(404);
        }
        if($data['em_firstname'] == ''){
            $name = explode(" ", $data['em_name']);
            if(isset($name[0]))
            $data['em_firstname'] = $name[0];
            if(isset($name[1]))
            $data['em_middlename'] = $name[1];
            if(isset($name[2]))
            $data['em_lastname'] = $name[2];
        }

       

       
        $data['employee_master'] = EmployeeMaster::distinct()
                                                    ->where('domain_id', $domain)
                                                    ->where('status',2)
                                                    ->pluck('em_name',EmployeeMaster::getKeyField());
        
       
        $data['screen_name'] = "edit";
        $data['name'] = "edit";
        $data['readonly'] = ['em_phone_number','em_email','em_type_id'];
        $data['disabled'] = ['em_phone_number','em_email','em_type_id'];
        return view('test.employee_master_add', ['data' => $data]);
    }


    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type = $request->type;
        $save_cond =  $request->save;
        $data = $request->all();
        $prefix = config('constants.EmployeeMaster.prefix');
        
            $data['status'] = config('constants.STATUS.'.strtolower($request->status).'.DB_VALUE');
            fillUpdate($this->employee_master, $data, $id, EmployeeMaster::getKeyField());
             
            commit();
            
            if ($save_cond == "save_continue") {
                return redirect("/".$request->segment(1)."/".$request->segment(2)."/".$request->segment(2));
            } else if ($save_cond == "save_return") {
                return redirect("/".$request->segment(1)."/".$request->segment(2));
            } else {
                return excelResponseJson();
            }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $data = $request->all();
        beginTransaction();
            $data['em_remark'] = $data['entity'];
            $data['status'] = config('constants.STATUS.closed.DB_VALUE');
            
            fillUpdate($this->employee_master, $data, $id, EmployeeMaster::getKeyField());
        commit();

        return responseJson(200, "Record Updated Successfully");
    }

    public function deleteDocument(Request $request)
    {
        $data = $request->all();
        $domain = \Auth::User()->domain_id;
        if(isset($data['entity_type']) && isset($data['entity_id']) && $data['entity_type'] == 'delete_document')
        {
            $file_name = EmployeeDocuments::where(EmployeeDocuments::getKeyField(), $data['entity_id'])->pluck('empd_file');
            if(isset($file_name[0]) && file_exists(public_path().'/documents/'.$file_name[0]))
            {
                unlink(public_path().'/documents/'.$file_name[0]);
            }
            $res = EmployeeDocuments::where('domain_id', $domain)->where(EmployeeDocuments::getKeyField(), $data['entity_id'])->delete();
            return $res;
        }
    }

}
