<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AclMiddleware
{
    public function __construct() {
        $this->perm = permissionList();
        // dd($this->perm);
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $method = $request->method();
        $sender = \Auth::User()->name;

        if ($method == "GET") {
            return $this->getCheck($next, $request); 
        } else if ($method == "POST") {
            return $this->postCheck($next, $request);
        } else if ($method == "PUT") {
            return $this->putCheck($next, $request);
        } else {
            return $next($request);
        }
    }

    public function getCheck($next, $request) {
        
        // will be used for custom calls
        $create_check = $request->segment(3);
        $create_check_i = $request->segment(4);
        $get_url = $request->segment(2);
        $api_url = $request->segment(1);

        $req = $request->path();
        
        if(isset($req) && $req == '/') {
            return $next($request);
        } else if($api_url == 'api' || $api_url == 'change_password'){
            return $next($request);
        } else {
            return $this->requestExec($next, $request, $get_url.'-view');
        }
    }

    public function postCheck($next, $request) {

        // will be used for custom calls of post
        $post_check = $request->segment(1);
        $base_url = $request->segment(2);
        $post_url = $request->segment(4);
        
        if($post_url == 'raise-po'){
            return $this->requestExec($next, $request, $base_url.'-raise-po');   
        } else if($post_url == 'store'){
            return $this->requestExec($next, $request, $base_url.'-create');
        } else if($base_url == 'order-timeline') {
            return $this->requestExec($next, $request, $base_url.'-view');
        } else if($post_url == 'generate-mail'){
            return $this->requestExec($next, $request, $base_url.'-mail');
        }
        return $this->requestExec($next, $request, $base_url.'-create');
    }

    public function putCheck($next, $request) {

        // will be used for custom calls of put
        $put_check_i = $request->segment(4);
        $put_check = $request->segment(5);
        $put_check_screen_i = $request->segment(3);
        $get_url = $request->segment(2);
        if($put_check_i == 'store'){
            return $this->requestExec($next, $request, $get_url.'-create');    
        } else if($put_check_i == 'approve'){
            return $this->requestExec($next, $request, $get_url.'-approve');
        } else if($put_check_i == 'repair'){
            return $this->requestExec($next, $request, $get_url.'-repair');
        } else if($put_check_i == 'accept'){
            return $this->requestExec($next, $request, $get_url.'-accept');
        }
        return $this->requestExec($next, $request, $get_url.'-edit');
    }

    public function requestExec($next, $request, $type) {
        
        $tab = $request->segment(1);
        $subtab = $request->segment(2);
        // dd($this->perm[$tab][$subtab]);
        if (isset($this->perm) &&
            isset($this->perm[$tab]) &&
            isset($this->perm[$tab][$subtab])
        ) {
            if (isset($this->perm[$tab][$subtab][$type])) {
                return $next($request);
            } else {
                abort(404);
            }
        } else {
            abort(404);
        }
    }
}
