<?php

namespace App\Http\Middleware;

use Closure;

class inputcheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if ($request->root() != env('APP_URL') || $_SERVER['HTTP_HOST'] != config('constants.APP_URL_ENV')) {
            abort(404);
        }
        
        $input = $request->all();
        if(isset($input) && !empty($input)) {
            array_walk_recursive($input, function(&$in) {
                $in = e(trim($in));
            });
            $request->merge($input);
            return $next($request);
        } else {
            return $next($request);
        }
    }
}
