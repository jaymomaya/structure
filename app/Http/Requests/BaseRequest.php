<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BaseRequest extends Request
{

    public function getRules($class_name, $request, $type = "", $filename = "") { 

        $rules = [];
        if ($request) {
            if ($filename != "") {
                $path = config('validate.'.$class_name)[$filename];
            } else {
                $path = config('validate.'.$class_name);
            }
           foreach ($path as $db_key => $db_value) {
                
                foreach ($path[$db_key] as $condition_key => $condition_value) {
                    foreach($request as $entity_key => $entity_val) {
                    
                        if ($type == "excel") {
                            $rules['entity.'.$entity_key.'.'.$db_key] = $condition_value;
                            
                        } else if ($type == "entry") {
                            $rules[$db_key] = $condition_value;
                        }
                    }
                }
            }
        }
        
        return $rules;
        
    }

    public function getMessages($class_name, $request, $type = "", $filename = "") {
        $messages = [];
        if ($request) {

            if ($filename != "") {
                $path = config('validate.'.$class_name)[$filename];
            } else {
                $path = config('validate.'.$class_name);
            }
            foreach ($path as $db_key => $db_value) {
                foreach ($path[$db_key] as $condition_key => $condition_value) {
                    foreach($request as $entity_key => $entity_val) {
                        if ($filename != "" && $filename != "manual") {
                            $entity = $entity_key + 1;
                            $messages['entity.'.$entity_key.'.'.$db_key.'.'.$condition_value] = 'Column : '.ucwords(str_replace('_', ' ', substr($db_key, strpos($db_key, "_") + 1 ))).', Row : '.($entity_key + 2).' ,value is '.$condition_value.'.';
                        } else {
                            $messages[$db_key.'.'.$condition_value] = ucwords(str_replace('_', ' ', substr($db_key, strpos($db_key, "_") + 1 ))) . ' is ' . $condition_value;
                        }
                    }
                }
            }
            
        }

        return $messages;
    }

    public function setRules($that) {
        $rules = [];
        $class_name = (new \ReflectionClass($that))->getShortName();
        $entity = $that->request->get('entity');
        $type = "excel";
        
        if (!isset($entity)){
            $entity = $that->request;
            $type = "entry";
        } elseif ($class_name == 'SolutionsRequest') {
            $entity = $that->request;
            $type = "entry";
        }

        $filename = $that->request->get('filename');

        $rules = $that->getRules($class_name, $entity, $type, $filename);
       
        return $rules;
    }

    public function setMessages($that) {
        $class_name = (new \ReflectionClass($that))->getShortName();
        $entity = $that->request->get('entity');
        $type = "excel";
        if (!isset($entity)){
            $entity = $that->request;
            $type = "entry";
        }

        $filename = $that->request->get('filename');
        
        $messages = $that->getMessages($class_name, $entity, $type, $filename);
        
        return $messages;
    }
}
