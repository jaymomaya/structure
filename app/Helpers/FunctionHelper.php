<?php

function generateRandomString($length = 10) {
    $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function objectToArray($d) {
    if (is_object($d)) {
        
        $d = get_object_vars($d);
    }

    if (is_array($d)) {
       
        return array_map(__FUNCTION__, $d);
    } else {
        
        return $d;
    }
}

function responseJson(
        $code = 503, 
        $message = 'Something went wrong, please try again later.', 
        $data = "",
        $type = "error"
    ) {
    
    $response = array(
        'code'      => $code,
        'message'   => $message,
        'type' => $type
    );

    if (isset($data) && is_array($data)) {
        $response[$data[0]] = $data[1];
    }


    return  response()->json($response, $code);
}

function excelResponseJson(
        $code = 200, 
        $message = 'Excel uploaded successfully.', 
        $type = "success"
    ) {
    
    $response = array(
        'code'      => $code,
        'message'   => $message,
        'type' => $type
    );

    return  response()->json($response, $code);
}

function successResponseJson(
        $code = 200, 
        $message = 'Record saved successfully.', 
        $type = "success"
    ) {
    
    $response = array(
        'code'      => $code,
        'message'   => $message,
        'type' => $type
    );

    return  response()->json($response, $code);
}

function flashMessage($message = 'Record added successfully.', $class = 'alert-success') {
    session()->flash('message', $message);
    session()->flash('class', $class);
}

function flashErrorMessage($message = 'Something went wrong, please try again later!', $class = 'alert-error') {
    session()->flash('message', $message);
    session()->flash('class', $class);
}

function excludeColumn($array, $list) {
    return array_values(array_diff( $array, $list ));
}

function getIndex($arr, $check) {
    $res = [];
    foreach ($arr as $key => $value) {
        if (in_array($value, $check)) {
            array_push($res, $key);
        }
    }
    return $res;
}


function removePrefix(array $input, $prefix) {
    $return = array();
    foreach ($input as $str) {
   
        $return[] = str_replace($prefix, '', $str);
        
    }
    return $return;
}

function generateSidebar() {
    // return 1;  
    $data = execSelect("
        SELECT 
            t1.tab_name AS inner_tab, t.tab_name, 
            t1.url AS inner_url, t.url as parent_url, 
            t1.tabs_id AS inner_tab_id, t.tabs_id AS parent_tab_id,
            t1.icon as inner_icon, t.icon as parent_icon,
            t.sequence_order as inner_order, t1.sequence_order as parent_order
        FROM
            tabs t
                RIGHT JOIN
            (SELECT 
                *
            FROM
                tabs t) AS t1 ON t.tabs_id = t1.parent_id
        WHERE
            t.tab_name IS NOT NULL and t1.is_visible = ? order by t.sequence_order, t1.sequence_order;
    ",[1]);

    // dd($data); 
    $data = objectToArray($data);
    $val = [];
    $inner_tab = [];
    

    foreach ($data as $key => $value) {
        
       
            $val[$value['tab_name']]['inner_tab'][$value['inner_tab']] = [
                'id' => $value['inner_tab_id'],
                'sequence_order' => $value['inner_order'],
                'icon' => $value['inner_icon'],
                'url' => $value['inner_url'],
            ];
            $tab_val = [
                'id' => $value['parent_tab_id'],
                'sequence_order' => $value['parent_order'],
                'url' => $value['parent_url'],
                'icon' => $value['parent_icon']
            ];
            
            array_push($val[$value['tab_name']], $tab_val);
           
    }


    return $val;
}

function generateSidebarY() {
    $data = execSelect("
        SELECT 
            t1.tab_name AS inner_tab, t.tab_name, 
            t1.url AS inner_url, t.url as parent_url, 
            t1.tabs_id AS inner_tab_id, t.tabs_id AS parent_tab_id,
            t1.icon as inner_icon, t.icon as parent_icon,
            t.sequence_order as inner_order, t1.sequence_order as parent_order
        FROM
            tabs t, tabs t1  
        WHERE
             t.tabs_id = t1.parent_id and t.tab_name IS NOT NULL and t1.is_visible = ? order by t.sequence_order, t1.sequence_order;
    ",[1]);

    $data = objectToArray($data);
    $val = [];
    $inner_tab = [];
    $perm = permissionList();

    foreach ($data as $key => $value) {
        // $tab_name = $value['parent_url'];
        // $inner_tab_name = $value['inner_url']; 

        $tab_name = str_replace(' ', '-',strtolower($value['tab_name']));
        $inner_tab_name = str_replace(' ', '-',strtolower($value['inner_tab']));

        if (isset($perm[$tab_name]) && isset($perm[$tab_name][$inner_tab_name]) && isset($perm[$tab_name][$inner_tab_name]['view']) && $perm[$tab_name][$inner_tab_name]['view']) {
            $val[$value['tab_name']]['inner_tab'][$value['inner_tab']] = [
                'id' => $value['inner_tab_id'],
                'sequence_order' => $value['inner_order'],
                'icon' => $value['inner_icon'],
                'url' => $value['inner_url'],
            ];
            $tab_val = [
                'id' => $value['parent_tab_id'],
                'sequence_order' => $value['parent_order'],
                'url' => $value['parent_url'],
                'icon' => $value['parent_icon']
            ];
            
            array_push($val[$value['tab_name']], $tab_val);
        }        
    }
    
    return $val;
}

function sidebarData() {
    $data = execSelect("
       SELECT 
        t1.tab_name AS inner_tab,
        t.tab_name,
        t1.tabs_id AS inner_tab_id,
        t.tabs_id AS parent_tab_id,
        t1.icon AS inner_icon,
        t.icon AS parent_icon,
        t.sequence_order AS inner_order,
        t1.sequence_order AS parent_order,
        p.name as permission_name,
        p.id as permission_id,
        t.tabs_id as dependent_id
    FROM
        tabs t,
        tabs t1,
        permissions p,
        tabs_permissions as tp
    WHERE
        t1.tabs_id = tp.tabs_id AND 
        p.id = tp.permission_id AND
        t.tabs_id = t1.parent_id
            AND t.tab_name IS NOT NULL
            AND t.is_visible = ?
    ORDER BY t.sequence_order , t1.sequence_order",[1]);


    $data = objectToArray($data);
    
    $val = [];
    $op = [];
    foreach ($data as $key => $value) {
        
        $val[$value['tab_name']]
        ['inner_tab']
        [$value['inner_tab']]
        ['permission_name']
        [$value['permission_name']] = 
        [
            'id' => $value['permission_id'],
            'dependent_id' => $value['dependent_id']
        ];

        $val[$value['tab_name']]['id'] = $value['parent_tab_id'];
        $val[$value['tab_name']]['icon'] = $value['parent_icon'];

        $val[$value['tab_name']]['inner_tab'][$value['inner_tab']]['id'] = $value['inner_tab_id'];
        $val[$value['tab_name']]['inner_tab'][$value['inner_tab']]['icon'] = $value['inner_icon'];
    }
    // dd($val);

    return $val;
}

function rolesList() {
    // dd(\Auth::user());
    $user = App\Models\User::where('id', \Auth::user()->id)->first();
    $data = $user->getRoles();
    $val = implode(",", $data);
    $val = str_replace(",", "|", $val);
    return $val;
}

function permissionList() {
    $user = App\User::where('id', \Auth::user()->id)->first();
    $data = $user->getAllPermissions();

    $res = [];
    $collection = collect($data);
    $arrays = $collection->values()->toArray();

    foreach ($arrays as $key => $value) {

       // identify the tab id from permission from tabs permission table
        if(isset($value) && isset($value['id']))
        $tab_id = App\Models\TabsPermissions::where('permission_id', $value['id'])->pluck('tabs_id');

        // identify the tab name and parent tab name from tabs id from tabs table
        if(isset($tab_id) && isset($tab_id[0])) {
            $tab_name_code = App\Models\Tabs::where('tabs_id',$tab_id[0])->select('url', 'parent_id')->get()->toArray();
            if(isset($tab_name_code) && isset($tab_name_code[0]) && isset($tab_name_code[0]['parent_id']) && isset($tab_name_code[0]['url'])) {
                $parent_tab_name_code = App\Models\Tabs::where('tabs_id', $tab_name_code[0]['parent_id'])->pluck('tab_name')->toArray();
                $tab_name = $tab_name_code[0]['url'];
                if(isset($parent_tab_name_code) && isset($parent_tab_name_code[0]))
                    $parent_tab_name = str_replace(' ', '-', strtolower($parent_tab_name_code[0]));
            }
        }

        if(isset($parent_tab_name) && isset($tab_name) && isset($value) && isset($value['name']))
        $res[$parent_tab_name][$tab_name][$value['name']] = true;

    }

    return $res;
}

function sideBarView() {
    dd(1);

}

function canEdit() {
    // $admin = \App\Models\AccessControl\Role::first();
    // dd($admin);
    if (isset($admin)) {
        $update = $admin->can('update');
        return $update;   
    }
}

function htmlValue($name, $data) {
    return \Input::old($name) ? \Input::old($name) : ((isset($data) && isset($data[$name])) ? $data[$name] : '');
}

function htmlSelect($name, $data) {
    return \Input::old($name) ? \Input::old($name) : ((isset($data) && isset($data[$name])) ? $data[$name] : null);
}

function setDisable($name, $disabled) {
    if (isset($disabled) && is_array($disabled)) {
        if(in_array($name, $disabled)) {
            return 'disabled';
        }
    }
}

function setReadOnly($name, $readonly) {
    if (isset($readonly) && is_array($readonly)) {
        if(in_array($name, $readonly)) {
            return 'readonly';
        }
    }
}

function notificationHelper($data = []) {
   
    $user_ids = $data[0]['notify_to'];
    $user = App\Models\User::get()->whereIn('emp_id', $user_ids);
    // $user = App\Models\User::find(38);
    // to send mailer notification
    foreach ($user as $key => $value) {
        $user[$key]->notify(new App\Notifications\SharekhanNotification($data));
    }
    
    event(new App\Events\BroadcastEvent($data));
}

function uniquesCommaSeparated($notify_data) {
    return explode(',',implode(',', array_keys(array_flip(explode(',', $notify_data)))));
}

function notifyDataCreateEvent($notify_to, $msg_string, $sender, $event_id, $url) {

    $notify_data = $notify_to[0]->notify;
    $notify_list = uniquesCommaSeparated($notify_data);
    $notify_list = array_diff($notify_list, array(\Auth::User()->emp_id));

    $notify_data = [
        'sender' => $sender,
        'notify_msg' => $msg_string,
        'notify_to' => $notify_list,
        'url' => $url,
        'entity_id' => $event_id
    ];

    notificationHelper([$notify_data]);
}
/*Jashal Code: To Give Error If Data Of uploaded file already exist */
function excelDataExistResponseJson(
        $code = 200, 
        $message = 'Upload Different File, Data Already Exist.', 
        $type = "error"
    ) {
    
    $response = array(
        'code'      => $code,
        'message'   => $message,
        'type' => $type
    );

    return  response()->json($response, $code);
}

//jay 27032018 to show tooltip
function showTooltip($msg = ''){
    echo '<i data-toggle="tooltip" title = "'.$msg.'" class="fa fa-question-circle pull-right" style="margin-bottom: 5px!important"></i>';
}

function getSingleExcelDataAjax($request) {
    $info = $request->all();
    // generate timestamp
    $date = new DateTime();

    $arr = $info;
    // dd($arr['file']);

    $arr['filename'] = $info['filename'];
    $arr['stored_name'] = $info['stored_name'];


    $path = public_path().'/uploads/excel-upload/'.$arr['stored_name'].'.'.$arr['fileExt'];
    
    $data = \Maatwebsite\ExcelLight\Reader::load($path);
    dd($data);
    // dd($data);

    $arr['sheet_name'] = $data->getTitle();
      
    if($data->count()) {
        foreach ($data as $key => $value) {

            foreach ($value as $k => $v) {
                $arrays[$arr['prefix'].$k] = $v;
            }
            
            $arr['entity'][] = $arrays;
        }    
    }
    return $arr;
}
function permissionListViaRoles($id) {

    $res = [];
    $rolePermissions = Spatie\Permission\Models\Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
            ->where("role_has_permissions.role_id",$id)
            ->select('id', 'name')
            ->get();

    $collection = collect($rolePermissions);
    $arrays = $collection->values()->toArray();

    foreach ($arrays as $key => $value) {
        // identify the tab id from permission from tabs permission table
        if(isset($value['id']))
        $tab_id = App\Models\TabsPermissions::where('permission_id', $value['id'])->pluck('tabs_id');

        // identify the tab name and parent tab name from tabs id from tabs table
        if(isset($tab_id) && isset($tab_id[0])) {
            $tab_name_code = App\Models\Tabs::where('tabs_id',$tab_id[0])->select('url', 'parent_id')->get()->toArray();
            if(isset($tab_name_code) && isset($tab_name_code[0]) && isset($tab_name_code[0]['parent_id']) && isset($tab_name_code[0]['url'])) {
                $parent_tab_name_code = App\Models\Tabs::where('tabs_id', $tab_name_code[0]['parent_id'])->pluck('tab_name')->toArray();
                $tab_name = $tab_name_code[0]['url'];
                if(isset($parent_tab_name_code) && isset($parent_tab_name_code[0]))
                    $parent_tab_name = str_replace(' ', '-', strtolower($parent_tab_name_code[0]));
            }
        }

        if(isset($parent_tab_name) && isset($tab_name) && isset($value) && isset($value['name']))
        $res[$parent_tab_name][$tab_name][$value['name']] = true;

    }
    return $res;
}