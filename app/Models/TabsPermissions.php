<?php

namespace App\Models;

use App\Models\BaseModel;
use Spatie\Permission\Traits\HasRoles;

class TabsPermissions extends BaseModel
{
    
    use HasRoles;

    public function __construct($attributes = array()) {

        $this->setTable(config('constants.'.((new \ReflectionClass($this))->getShortName()).'.table')); // table name
        
        $this->setKeyName(config('constants.'.((new \ReflectionClass($this))->getShortName()).'.table') .'_'. $this->getKeyName()); // primary key name
       
        $this->guard([$this->getKeyName()]); // Add more field to guard
        // dd(config('constants.timestamp'));
        $nonFillable = array_merge(config('constants.timestamp'), $this->getGuarded()); 

        // Fillables;
        $this->fillable(
            excludeColumn(
                array_merge(
                    \Schema::getColumnListing($this->getTable()),
                    $nonFillable
                ),
                $nonFillable
            )
        );

        parent::__construct($attributes);

    }
}
