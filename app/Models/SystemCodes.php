<?php

namespace App\Models;

use App\Models\BaseModel;
use Kodeine\Acl\Traits\HasRole;


class SystemCodes extends BaseModel
{

   public function __construct($attributes = array()) {

        $this->setTable(config('constants.'.((new \ReflectionClass($this))->getShortName()).'.table')); // table name
        
        $this->setKeyName('serial_no'); // primary key name
       
        $this->guard([$this->getKeyName()]); // Add more field to guard
 
        $nonFillable = array_merge(config('constants.timestamp'), $this->getGuarded()); 

        // Fillables;
        $this->fillable(
            excludeColumn(
                array_merge(
                    \Schema::getColumnListing($this->getTable()),
                    $nonFillable
                ),
                $nonFillable
            )
        );

        parent::__construct($attributes);
    }
}
