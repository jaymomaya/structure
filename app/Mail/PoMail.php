<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class PoMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content)
    {
        $this->content = $content;
    }

    /**
     * Build the message.
     
     *
     * @return $this
     */
    public function build()
    {
        if(isset($this->content) && isset($this->content['cc']) && isset($this->content['cc'][0]) && $this->content['cc'][0] !='' && isset($this->content['bcc']) && isset($this->content['bcc'][0]) &&  $this->content['bcc'][0] !=''){
            return $this
                ->from('anmol.gupta@solutionplanets.com', 'Accounts Team - FK')
                ->subject($this->content['mt_subject'])
                ->view('emails.test.po', ['data' => $this->content])
                ->cc($this->content['cc'])
                ->bcc($this->content['cc'])
                ->attach($this->content['filename'], [
                        'mime' => 'application/pdf',
                    ]);
        }
        elseif(isset($this->content) &&isset($this->content['cc']) && isset($this->content['cc'][0]) && $this->content['cc'][0] !=''){
            return $this
                ->from('anmol.gupta@solutionplanets.com', 'Accounts Team - FK')
                ->subject($this->content['mt_subject'])
                ->replyTo('hr@leadcrmindia.com', 'Accounts Team - FK')
                ->view('emails.test.po', ['data' => $this->content])
                ->cc($this->content['cc'])
                ->attach($this->content['filename'], [
                        'mime' => 'application/pdf',
                    ]);
        }
        elseif(isset($this->content) &&isset($this->content['bcc']) && isset($this->content['bcc'][0]) && $this->content['bcc'][0] !=''){
            return $this
                ->from('anmol.gupta@solutionplanets.com', 'Accounts Team - FK')
                ->subject($this->content['mt_subject'])
                ->replyTo('hr@leadcrmindia.com', 'Accounts Team - FK')
                ->view('emails.test.po', ['data' => $this->content])
                ->bcc($this->content['bcc'])
                ->attach($this->content['filename'], [
                        'mime' => 'application/pdf',
                    ]);
        }
        else{
        return $this
            ->from('anmol.gupta@solutionplanets.com', 'Accounts Team - FK')
            ->subject($this->content['mt_subject'])
            ->replyTo('hr@leadcrmindia.com', 'Accounts Team - FK')
            ->view('emails.test.po', ['data' => $this->content])
            ->attach($this->content['filename'], [
                    'mime' => 'application/pdf',
                ]);
        }
    }
}
